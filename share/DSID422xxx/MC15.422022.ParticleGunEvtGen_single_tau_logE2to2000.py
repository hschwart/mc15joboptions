evgenConfig.description = "Single taus with flat eta-phi and log E in [2, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "tau"]
	
include("MC15JobOptions/ParticleGun_Common.py")
	
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-15, 15)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(2000., 2000000.), eta=[-5.5, 5.5])

# Use EvtGen to decay the tau (3-prong)
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt', 'tau3.dec' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.blackList=[]
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"
genSeq.EvtInclusiveDecay.userDecayFile   = "tau3.dec"

print "CHECK ON TAUS BLACKLISTED", genSeq.EvtInclusiveDecay.blackList
