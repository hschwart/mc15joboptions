#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980646E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03989928E+01   # W+
        25     1.25327570E+02   # h
        35     3.00008930E+03   # H
        36     2.99999992E+03   # A
        37     3.00109383E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03522063E+03   # ~d_L
   2000001     3.02998298E+03   # ~d_R
   1000002     3.03430901E+03   # ~u_L
   2000002     3.03192554E+03   # ~u_R
   1000003     3.03522063E+03   # ~s_L
   2000003     3.02998298E+03   # ~s_R
   1000004     3.03430901E+03   # ~c_L
   2000004     3.03192554E+03   # ~c_R
   1000005     8.44266347E+02   # ~b_1
   2000005     3.02869453E+03   # ~b_2
   1000006     8.41809520E+02   # ~t_1
   2000006     3.02286241E+03   # ~t_2
   1000011     3.00670239E+03   # ~e_L
   2000011     3.00119983E+03   # ~e_R
   1000012     3.00531146E+03   # ~nu_eL
   1000013     3.00670239E+03   # ~mu_L
   2000013     3.00119983E+03   # ~mu_R
   1000014     3.00531146E+03   # ~nu_muL
   1000015     2.98613346E+03   # ~tau_1
   2000015     3.02188945E+03   # ~tau_2
   1000016     3.00537534E+03   # ~nu_tauL
   1000021     2.34353291E+03   # ~g
   1000022     2.01678373E+02   # ~chi_10
   1000023     4.26745043E+02   # ~chi_20
   1000025    -2.99920690E+03   # ~chi_30
   1000035     2.99929593E+03   # ~chi_40
   1000024     4.26907785E+02   # ~chi_1+
   1000037     3.00018721E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891565E-01   # N_11
  1  2     1.90704320E-04   # N_12
  1  3    -1.47228132E-02   # N_13
  1  4    -2.45814745E-04   # N_14
  2  1     1.95764995E-04   # N_21
  2  2     9.99654102E-01   # N_22
  2  3     2.62070869E-02   # N_23
  2  4     2.19708871E-03   # N_24
  3  1    -1.02370196E-02   # N_31
  3  2     1.69781875E-02   # N_32
  3  3    -7.06827504E-01   # N_33
  3  4     7.07108071E-01   # N_34
  4  1     1.05840979E-02   # N_41
  4  2    -2.00843648E-02   # N_42
  4  3     7.06746989E-01   # N_43
  4  4     7.07102036E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99313275E-01   # U_11
  1  2     3.70537301E-02   # U_12
  2  1    -3.70537301E-02   # U_21
  2  2     9.99313275E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995179E-01   # V_11
  1  2    -3.10516689E-03   # V_12
  2  1    -3.10516689E-03   # V_21
  2  2    -9.99995179E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98559021E-01   # cos(theta_t)
  1  2    -5.36645281E-02   # sin(theta_t)
  2  1     5.36645281E-02   # -sin(theta_t)
  2  2     9.98559021E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99717445E-01   # cos(theta_b)
  1  2    -2.37703631E-02   # sin(theta_b)
  2  1     2.37703631E-02   # -sin(theta_b)
  2  2     9.99717445E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06934311E-01   # cos(theta_tau)
  1  2     7.07279209E-01   # sin(theta_tau)
  2  1    -7.07279209E-01   # -sin(theta_tau)
  2  2    -7.06934311E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00140462E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49806462E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44066612E+02   # higgs               
         4     1.06156750E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49806462E+03  # The gauge couplings
     1     3.62147478E-01   # gprime(Q) DRbar
     2     6.38149581E-01   # g(Q) DRbar
     3     1.02637114E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49806462E+03  # The trilinear couplings
  1  1     2.13441980E-06   # A_u(Q) DRbar
  2  2     2.13445267E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49806462E+03  # The trilinear couplings
  1  1     7.51582459E-07   # A_d(Q) DRbar
  2  2     7.51672632E-07   # A_s(Q) DRbar
  3  3     1.55189943E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49806462E+03  # The trilinear couplings
  1  1     3.38960235E-07   # A_e(Q) DRbar
  2  2     3.38977052E-07   # A_mu(Q) DRbar
  3  3     3.43734771E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49806462E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53058144E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49806462E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.88073906E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49806462E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02430156E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49806462E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.22812955E+04   # M^2_Hd              
        22    -9.06865506E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41363467E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.18947333E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47991023E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47991023E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52008977E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52008977E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.65779181E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.81419063E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.95399887E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.86458207E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16492751E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.63938392E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.90320651E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.83895002E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.00789301E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.94079652E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69385196E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61710157E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.17466860E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.45376232E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.08674172E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.59677868E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.19454714E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.46364509E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.02818895E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.68683531E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.34339121E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.26583232E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.87509152E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.39257453E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.34576252E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.04644829E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.52291976E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.95612267E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97925486E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61854431E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.33467674E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.35732497E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.23896189E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.88333456E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.08270066E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17999461E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58754693E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.90854311E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.99388807E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.56329850E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41245283E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96119954E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.95271464E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61766168E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.87637155E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.26009013E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.23326129E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.81989508E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.08954615E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66656828E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52159469E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.68278427E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.27712292E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.42047447E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54784047E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.95612267E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97925486E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61854431E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.33467674E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.35732497E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.23896189E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.88333456E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.08270066E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17999461E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58754693E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.90854311E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.99388807E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.56329850E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41245283E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96119954E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.95271464E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61766168E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.87637155E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.26009013E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.23326129E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.81989508E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.08954615E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66656828E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52159469E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.68278427E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.27712292E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.42047447E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54784047E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89324288E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.98920453E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00223156E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.67668288E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.50117606E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99884780E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.60993485E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55167444E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999962E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.71324835E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86439643E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.81892988E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89324288E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.98920453E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00223156E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.67668288E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.50117606E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99884780E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.60993485E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55167444E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999962E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.71324835E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86439643E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.81892988E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.74930650E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52718680E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.15913549E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31367771E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69360258E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60861439E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13189960E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.17876208E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.00973231E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25915052E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.16637886E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89345053E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97055467E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99923486E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.67427741E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.40434711E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00370961E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.00004226E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89345053E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97055467E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99923486E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.67427741E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.40434711E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00370961E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.00004226E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89387296E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.96968875E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99897847E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.73012695E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.47650883E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00404561E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.97778313E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.94077377E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.18454249E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.38041305E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.56948555E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.53234633E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.14687654E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.97057189E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.98663725E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.39688298E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.04745253E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99929941E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.00593523E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.20917296E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04141507E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.57362030E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.96966795E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.96966795E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.00701705E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.60126239E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.31280136E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31280136E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.79060851E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.79060851E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.25093213E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.25093213E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.10994152E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.85316037E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.58476698E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.03590820E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.03590820E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09145653E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.82089025E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.28580920E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.28580920E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.86474438E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.86474438E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.05019880E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.05019880E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.66212698E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58437183E-01    2           5        -5   # BR(h -> b       bb     )
     7.09905131E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51306934E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.33130299E-04    2           3        -3   # BR(h -> s       sb     )
     2.31661866E-02    2           4        -4   # BR(h -> c       cb     )
     7.56035979E-02    2          21        21   # BR(h -> g       g      )
     2.59763163E-03    2          22        22   # BR(h -> gam     gam    )
     1.75677625E-03    2          22        23   # BR(h -> Z       gam    )
     2.36891952E-01    2          24       -24   # BR(h -> W+      W-     )
     2.97717218E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.71783820E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00573054E-01    2           5        -5   # BR(H -> b       bb     )
     6.68875450E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.36498375E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.90420666E-04    2           3        -3   # BR(H -> s       sb     )
     8.14022287E-08    2           4        -4   # BR(H -> c       cb     )
     8.12039403E-03    2           6        -6   # BR(H -> t       tb     )
     8.56153815E-06    2          21        21   # BR(H -> g       g      )
     5.10515094E-08    2          22        22   # BR(H -> gam     gam    )
     3.32553086E-09    2          23        22   # BR(H -> Z       gam    )
     7.34966229E-07    2          24       -24   # BR(H -> W+      W-     )
     3.67029932E-07    2          23        23   # BR(H -> Z       Z      )
     5.46889743E-06    2          25        25   # BR(H -> h       h      )
    -4.50793079E-25    2          36        36   # BR(H -> A       A      )
     1.05282071E-19    2          23        36   # BR(H -> Z       A      )
     8.33060895E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.12808727E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.16400026E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.64155078E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23166013E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.32172742E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.63549916E+01   # A decays
#          BR         NDA      ID1       ID2
     9.20989426E-01    2           5        -5   # BR(A -> b       bb     )
     6.84008945E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.41848879E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.97040644E-04    2           3        -3   # BR(A -> s       sb     )
     8.38206595E-08    2           4        -4   # BR(A -> c       cb     )
     8.35704676E-03    2           6        -6   # BR(A -> t       tb     )
     2.46107032E-05    2          21        21   # BR(A -> g       g      )
     6.47884668E-08    2          22        22   # BR(A -> gam     gam    )
     2.42349020E-08    2          23        22   # BR(A -> Z       gam    )
     7.48755430E-07    2          23        25   # BR(A -> Z       h      )
     9.11582001E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.28485172E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.55609374E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.78170836E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.97645492E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46881510E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.25587551E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21192499E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.40040506E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.29989160E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67429503E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24173907E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.85732785E-07    2          24        25   # BR(H+ -> W+      h      )
     5.18298911E-14    2          24        36   # BR(H+ -> W+      A      )
     5.01584655E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.80402768E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07852310E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
