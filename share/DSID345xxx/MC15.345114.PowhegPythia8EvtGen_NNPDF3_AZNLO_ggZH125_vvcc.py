#--------------------------------------------------------------
# Same as POWHEG+MiNLO+Pythia8 gg->Z+H+jet->llbbbar, but H->ccbar      
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# Higgs->ccbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 4 4' ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->vvccbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'adrian.buzatu@cern.ch', 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
#evgenConfig.minevents   = 5000
evgenConfig.process     = "gg->ZH, H->cc, Z->vv"
evgenConfig.inputfilecheck = "TXT"
