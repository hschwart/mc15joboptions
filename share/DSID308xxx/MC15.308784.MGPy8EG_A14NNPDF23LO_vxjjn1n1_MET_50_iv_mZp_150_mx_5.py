model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 180.
mZp = 150.
mHD = 125.
widthZp = 5.968303e-01
widthN2 = 3.147076e-03
filteff = 4.854369e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
