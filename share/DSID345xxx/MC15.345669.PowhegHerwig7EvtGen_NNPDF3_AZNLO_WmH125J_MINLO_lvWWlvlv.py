#--------------------------------------------------------------
# POWHEG+MINLO+Herwig7 W- +H+jet->lvWW production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")


from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

## only consider H->WW->lvlv decays
genSeq.Herwig7.Commands += [
  '## force H->WW decays',
  'do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;',
  'do /Herwig/Particles/h0:PrintDecayModes', # print out decays modes and branching ratios to the terminal/log.generate
  'do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;',
  'do /Herwig/Particles/W+:PrintDecayModes', 
  'do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;',
  'do /Herwig/Particles/W-:PrintDecayModes' 
]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Herwig7 H+W- +  jet->lvWW->lvlvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'ada.farilla@roma3.infn.it' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents   = 50
evgenConfig.process = "WmH, W->lv, H->WW->lvlv"
