from MadGraphControl.MadGraphUtils import *

######
## number of events to generate + safety margin
nevents=1.1*runArgs.maxEvents
gridpack_dir=None
gridpack_mode=False
mode=0
njobs=1
runName='run_01'



######
## map DSID to process settings
# select diagram removal scheme: DR1 or DR2?
tWH_DR1 = []
tWH_DR2 = [ 346486, 346511 ]
# select any BSM top Yukawa couplings (default: SM):
tWH_yt_minus1 = []
tWH_yt_plus2 = []
tWH_BSM = tWH_yt_minus1+tWH_yt_plus2 

DR_mode=''
if runArgs.runNumber in tWH_DR1:
    DR_mode='DR1'
elif runArgs.runNumber in tWH_DR2:
    DR_mode='DR2'
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



######
mgproc="generate p p > t w- h [QCD]"
mgprocadd="add process p p > t~ w+ h [QCD]"
name='tWH'
process="pp>tWH"
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''
gridpack_mode=False
gridpack_dir='madevent/'
    
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define q = u c d s b t
define q~ = u~ c~ d~ s~ b~ t~
"""+mgproc+"""
"""+mgprocadd+"""
output -f
""")
fcard.close()
    
extras = {'pdlabel'        : "'lhapdf'",
          'lhaid'          : 260000,
          'parton_shower'  :'PYTHIA8',
          'reweight_scale' : 'True',
          'reweight_PDF'   : 'True',
          'PDF_set_min'    : 260001,
          'PDF_set_max'    : 260100,
          'bwcutoff'       : 50.,
          'fixed_ren_scale' : "False",
	  'fixed_fac_scale' : "False",
          'dynamical_scale_choice' : 3 }

runName = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)


# fetch helpers required by do_+DR_mode+.py
DR_helper = 'DR_functions.py'
subprocess.Popen(['get_files','-data',DR_helper]).communicate()
if not os.access(DR_helper, os.R_OK):
    raise RuntimeError("Could not get_file required for automated DR removal: '%s"%DR_helper)



if DR_mode!='':
    include('MC15JobOptions/do_'+DR_mode+'.py')
else:
    raise RuntimeError("No DR mode found, this calculation will not converge!")

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)


######
## write madspin card
madspin_dir = 'my_madspin'
madspin_card_loc='madspin_card.dat'
if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('#set use_old_dir True\n')
else:
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
# for these numbers I get negligible(<1) amount of events above weights / 10k decayed events
fMadSpinCard.write('''set Nevents_for_max_weigth 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()
    


######
## select param card; default/None = SM
param_card_loc=None
if runArgs.runNumber in tWH_BSM:
    # BSM case: run with modified param card
    mod_paramcard_name = ''
    if runArgs.runNumber in tWH_yt_minus1:
        mod_paramcard_name = 'param_card_yt_minus1.dat'
    elif runArgs.runNumber in tWH_yt_plus2:
        mod_paramcard_name = 'param_card_yt_plus2.dat'
    else:
        raise RuntimeError("No modified param card instuction found for %i ."%runArgs.runNumber)

    mod_paramcard = subprocess.Popen(['get_files','-data',mod_paramcard_name]).communicate()
    if not os.access(mod_paramcard_name, os.R_OK):
        print 'ERROR: Could not get param card'
        raise RuntimeError("parameter card '%s' missing!"%mod_paramcard_name)
    param_card_loc=mod_paramcard_name


    
######
generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,
         madspin_card_loc=madspin_card_loc,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)



###### 
#MadSpin DR hack: needed to properly handle diagram removal in madspin 
msfile=process_dir+'/Cards/madspin_card.dat'
mstilde=process_dir+'/Cards/madspin_card.dat~'
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir: True' in line:
            line = line.replace('#',' ')
        myfile.write(line)
os.remove(mstilde)


MadSpin_DR_hack = 'do_MadSpin_'+DR_mode+'.py'
subprocess.Popen(['get_files','-data',MadSpin_DR_hack]).communicate()
if not os.access(MadSpin_DR_hack, os.R_OK):
    raise RuntimeError("Could not get_file required for MadSpin DR hack: '%s"%MadSpin_DR_hack)

full_madspin_dir = process_dir+'/'+madspin_dir
do_hack_MS = subprocess.Popen(['python', MadSpin_DR_hack, full_madspin_dir], stdout=subprocess.PIPE, cwd=".")
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(["make", "clean"], stdout=subprocess.PIPE, cwd=process_dir)
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The first compilation attempt fails, with errors written to the log
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The second compilation attempt works, producing a new tarfile in the process directory
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(['bin/aMCatNLO', 'decay_events', runName, '-f'], stdout=subprocess.PIPE, cwd=process_dir)
do_hack_MS.communicate()



###### 
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,lhe_version=3,saveProcDir=True)



###### 
evgenConfig.description = 'aMcAtNlo tHW'
evgenConfig.keywords+=['Higgs', 'tHiggs']
evgenConfig.contact = ['liza.mijovic@cern.ch', 'a.hasib@cern.ch']
evgenConfig.inputfilecheck = outputDS
runArgs.inputGeneratorFile=outputDS


###### 
## shower settings: if you change these to another shower eg Hw++,
## make sure you update subtraction term ('parton_shower'  :'PYTHIA8') 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py") 
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
## don't include shower weights, see ATLMCPROD-6135
#include("MC15JobOptions/Pythia8_ShowerWeights.py")


