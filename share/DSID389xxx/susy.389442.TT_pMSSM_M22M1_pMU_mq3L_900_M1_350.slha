#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16441014E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04030254E+01   # W+
        25     1.23644358E+02   # h
        35     3.00009439E+03   # H
        36     2.99999997E+03   # A
        37     3.00088965E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04050676E+03   # ~d_L
   2000001     3.03542794E+03   # ~d_R
   1000002     3.03959806E+03   # ~u_L
   2000002     3.03644881E+03   # ~u_R
   1000003     3.04050676E+03   # ~s_L
   2000003     3.03542794E+03   # ~s_R
   1000004     3.03959806E+03   # ~c_L
   2000004     3.03644881E+03   # ~c_R
   1000005     1.00294156E+03   # ~b_1
   2000005     3.03438943E+03   # ~b_2
   1000006     1.00151408E+03   # ~t_1
   2000006     3.02025852E+03   # ~t_2
   1000011     3.00624225E+03   # ~e_L
   2000011     3.00213050E+03   # ~e_R
   1000012     3.00485281E+03   # ~nu_eL
   1000013     3.00624225E+03   # ~mu_L
   2000013     3.00213050E+03   # ~mu_R
   1000014     3.00485281E+03   # ~nu_muL
   1000015     2.98551524E+03   # ~tau_1
   2000015     3.02199406E+03   # ~tau_2
   1000016     3.00459392E+03   # ~nu_tauL
   1000021     2.35026099E+03   # ~g
   1000022     3.51541055E+02   # ~chi_10
   1000023     7.33470955E+02   # ~chi_20
   1000025    -2.99534716E+03   # ~chi_30
   1000035     2.99629231E+03   # ~chi_40
   1000024     7.33634362E+02   # ~chi_1+
   1000037     2.99680802E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99883613E-01   # N_11
  1  2    -7.19035736E-04   # N_12
  1  3     1.50347339E-02   # N_13
  1  4    -2.49005471E-03   # N_14
  2  1     1.15406480E-03   # N_21
  2  2     9.99586649E-01   # N_22
  2  3    -2.76638014E-02   # N_23
  2  4     7.74041786E-03   # N_24
  3  1    -8.85781382E-03   # N_31
  3  2     1.40974530E-02   # N_32
  3  3     7.06878737E-01   # N_33
  3  4     7.07138779E-01   # N_34
  4  1    -1.23680183E-02   # N_41
  4  2     2.50454915E-02   # N_42
  4  3     7.06633655E-01   # N_43
  4  4    -7.07028028E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99233943E-01   # U_11
  1  2    -3.91347396E-02   # U_12
  2  1     3.91347396E-02   # U_21
  2  2     9.99233943E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99940033E-01   # V_11
  1  2    -1.09512960E-02   # V_12
  2  1     1.09512960E-02   # V_21
  2  2     9.99940033E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98761784E-01   # cos(theta_t)
  1  2    -4.97483550E-02   # sin(theta_t)
  2  1     4.97483550E-02   # -sin(theta_t)
  2  2     9.98761784E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99906289E-01   # cos(theta_b)
  1  2     1.36898947E-02   # sin(theta_b)
  2  1    -1.36898947E-02   # -sin(theta_b)
  2  2     9.99906289E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06962827E-01   # cos(theta_tau)
  1  2     7.07250706E-01   # sin(theta_tau)
  2  1    -7.07250706E-01   # -sin(theta_tau)
  2  2     7.06962827E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01791116E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64410142E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44082861E+02   # higgs               
         4     7.70358433E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64410142E+03  # The gauge couplings
     1     3.62481675E-01   # gprime(Q) DRbar
     2     6.37156423E-01   # g(Q) DRbar
     3     1.02427678E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64410142E+03  # The trilinear couplings
  1  1     2.56622657E-06   # A_u(Q) DRbar
  2  2     2.56626312E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64410142E+03  # The trilinear couplings
  1  1     7.06904162E-07   # A_d(Q) DRbar
  2  2     7.07023577E-07   # A_s(Q) DRbar
  3  3     1.51827337E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64410142E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.44229834E-07   # A_mu(Q) DRbar
  3  3     1.45937604E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64410142E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49478480E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64410142E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16806884E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64410142E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07466905E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64410142E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.54888767E+04   # M^2_Hd              
        22    -9.02722486E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40906099E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.38964682E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48176005E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48176005E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51823995E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51823995E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.53889619E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.10573937E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.53567076E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.05375531E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.86047134E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.55974196E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.97568785E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.99225361E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.12106160E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.39134086E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.54730321E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48102880E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.91126393E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.32677830E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.99142737E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.89319643E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.60766084E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19661535E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89506910E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.52832187E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.33636769E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.25332326E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.57632909E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29157001E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.91271058E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.22044114E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.05147823E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.68277408E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.08331950E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55673708E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.58366031E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.27964788E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11414369E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.76547923E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.26828458E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15004218E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57753378E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.91926293E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.05648426E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.96876419E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42246400E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68806076E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.17340873E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55473493E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.33386752E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.25373218E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.10846896E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.00333255E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.27505532E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65077226E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48155474E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.45201075E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.86023278E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.32381649E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55184390E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.68277408E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.08331950E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55673708E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.58366031E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.27964788E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11414369E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.76547923E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.26828458E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15004218E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57753378E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.91926293E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.05648426E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.96876419E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42246400E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68806076E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.17340873E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55473493E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.33386752E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.25373218E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.10846896E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.00333255E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.27505532E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65077226E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48155474E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.45201075E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.86023278E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.32381649E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55184390E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59958774E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05897199E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98446766E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.44858903E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.77715793E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95655989E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.05676047E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52640213E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998785E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21086215E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.64381195E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.37468180E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59958774E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05897199E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98446766E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.44858903E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.77715793E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95655989E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.05676047E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52640213E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998785E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21086215E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.64381195E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.37468180E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58859665E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68139303E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10923971E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20936725E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53482899E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76833430E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.08024299E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.55219304E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.71684574E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15091963E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.76180028E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59968105E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06379310E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97483560E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.92593076E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.12315717E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96137112E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.31202220E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59968105E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06379310E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97483560E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.92593076E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.12315717E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96137112E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.31202220E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59963558E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06370979E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97455192E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.66212702E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.05644574E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96171922E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.89200555E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.95646240E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.98401348E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.81457145E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.03622533E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.51527997E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.41610173E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.32913620E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.22170458E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.85619758E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.74386190E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.59746043E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.44025396E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.99554182E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.58943258E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.61307307E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.19904994E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.19904994E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.45228530E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.81232291E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55721044E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55721044E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29879097E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29879097E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.61262786E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.61262786E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.92264146E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.22991212E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.79901596E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.27014932E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.27014932E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.67121281E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.08013259E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52168574E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.52168574E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.32627366E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.32627366E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.33069695E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.33069695E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.61641386E-03   # h decays
#          BR         NDA      ID1       ID2
     6.89016917E-01    2           5        -5   # BR(h -> b       bb     )
     5.59244668E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.97979548E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.21072204E-04    2           3        -3   # BR(h -> s       sb     )
     1.81782059E-02    2           4        -4   # BR(h -> c       cb     )
     5.78843397E-02    2          21        21   # BR(h -> g       g      )
     1.94718351E-03    2          22        22   # BR(h -> gam     gam    )
     1.19655283E-03    2          22        23   # BR(h -> Z       gam    )
     1.56140476E-01    2          24       -24   # BR(h -> W+      W-     )
     1.90928058E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40001731E+01   # H decays
#          BR         NDA      ID1       ID2
     7.49086430E-01    2           5        -5   # BR(H -> b       bb     )
     1.77621644E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28027688E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.71219417E-04    2           3        -3   # BR(H -> s       sb     )
     2.17597337E-07    2           4        -4   # BR(H -> c       cb     )
     2.17067298E-02    2           6        -6   # BR(H -> t       tb     )
     2.35418756E-05    2          21        21   # BR(H -> g       g      )
     2.04080101E-08    2          22        22   # BR(H -> gam     gam    )
     8.37359630E-09    2          23        22   # BR(H -> Z       gam    )
     3.07012348E-05    2          24       -24   # BR(H -> W+      W-     )
     1.53316936E-05    2          23        23   # BR(H -> Z       Z      )
     8.09522678E-05    2          25        25   # BR(H -> h       h      )
     4.43699234E-23    2          36        36   # BR(H -> A       A      )
     8.03112060E-19    2          23        36   # BR(H -> Z       A      )
     1.79184554E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.06299554E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.93798463E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.34580865E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.66030223E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.62881970E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33642704E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84809659E-01    2           5        -5   # BR(A -> b       bb     )
     1.86071811E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.57904538E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.08043388E-04    2           3        -3   # BR(A -> s       sb     )
     2.28018390E-07    2           4        -4   # BR(A -> c       cb     )
     2.27337790E-02    2           6        -6   # BR(A -> t       tb     )
     6.69488029E-05    2          21        21   # BR(A -> g       g      )
     5.33576192E-08    2          22        22   # BR(A -> gam     gam    )
     6.59723330E-08    2          23        22   # BR(A -> Z       gam    )
     3.20441748E-05    2          23        25   # BR(A -> Z       h      )
     2.60946411E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21804926E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30149924E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.86694834E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03546459E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11378266E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40225641E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49379274E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.12819793E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.99162821E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02693843E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.23909781E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.14206360E-05    2          24        25   # BR(H+ -> W+      h      )
     7.08334142E-14    2          24        36   # BR(H+ -> W+      A      )
     1.87012208E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.37668238E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.09058881E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
