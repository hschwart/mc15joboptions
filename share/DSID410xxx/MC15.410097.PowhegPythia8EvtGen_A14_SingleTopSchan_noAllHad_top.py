#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top s-channel production (top), inclusive, with CT10 and A14_NNPDF23LO'
evgenConfig.keywords    = [ 'top', 'singleTop', 'sChannel' ]
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg single-top s-channel setup
#--------------------------------------------------------------
#if runArgs.trfSubstepName == 'generate' :

include('PowhegControl/PowhegControl_t_sch_Common.py')

PowhegConfig.topdecaymode = 11100 # leptonic W-from-top decays
#PowhegConfig.nEvents *= 3.
PowhegConfig.PDF     = 10800 # CT10
#PowhegConfig.PDF     = 10981 # CT10nlo
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 A14_NNPDF23LO showering + EvtGen
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

# Enable POWHEG LHEF reading in Pythia8
include('MC15JobOptions/Pythia8_Powheg.py')
