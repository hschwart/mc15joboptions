include("MC15JobOptions/MadGraphControl_HVT.py")
evgenConfig.description = "heavyVectorTriplet Spin-1 Wgamma 600 GeV resonance in fully leptonic decay model"
evgenConfig.keywords = ["exotic", "Wgamma", "heavyVectorTriplet", "leptonic", "spin1"]
