# JO for Pythia 8 jet jet with a jet in HGTD region, inspired by JZ1 slice

evgenConfig.description = "Dijet truth jet pt between 20 and 200 GeV, eta > 2.2, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilterAkt6.py")
filtSeq.QCDTruthJetFilter.MinPt = 20*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 200*GeV
filtSeq.QCDTruthJetFilter.MinEta = 2.2
evgenConfig.minevents = 1000

