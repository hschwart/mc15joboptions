import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=2000
ihtmax=-1
HTrange='highHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=50

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Znunu_Ht2000_E_CMS_13TeV"
