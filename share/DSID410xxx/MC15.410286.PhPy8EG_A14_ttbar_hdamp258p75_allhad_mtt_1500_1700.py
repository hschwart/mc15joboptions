#
#  These JO take unfiltered Powheg LHE files as input
#  Filter efficiency: 8.2e-4
#  For standard 16,500 event LHE files, 18 files are required per job to get 200 events
#  Change to 10 events per file, 50 files to get 500 events per job
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5* top mass, A14 tune, allhadronic, 1.1TeV<Mtt<1.3TeV '
evgenConfig.process     = 'SM ttbar'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'jiahang.zhong@cern.ch','mdshapiro@lbl.gov' ]
evgenConfig.minevents      = 5  
 
 
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
 
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
 
 
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
 
if not hasattr( filtSeq, "MassRangeFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import TTbarMassFilter
    filtSeq += TTbarMassFilter()
    pass
 
TTbarMassFilter = filtSeq.TTbarMassFilter
TTbarMassFilter.TopPairMassLowThreshold=1500000.
TTbarMassFilter.TopPairMassHighThreshold=1700000.

filtSeq.Expression = "(not TTbarWToLeptonFilter) and TTbarMassFilter"

