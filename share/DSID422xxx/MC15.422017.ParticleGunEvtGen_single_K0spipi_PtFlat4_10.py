evgenConfig.description = "Single K0s with flat phi, eta in [-3.0, 3.0], and flat pT [4,10] GeV"
evgenConfig.keywords = ["singleParticle", "K0S"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (310)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[4000,10000], eta=[-3.0, 3.0], mass=0.497614e+03)

evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt', 'K0S.dec' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.blackList=[]
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"
genSeq.EvtInclusiveDecay.userDecayFile  = "K0S.dec"
genSeq.EvtInclusiveDecay.isfHerwig=True
