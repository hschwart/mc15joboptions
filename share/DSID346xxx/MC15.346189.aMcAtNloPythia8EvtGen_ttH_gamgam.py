include('MC15JobOptions/MadGraphControl_ttH_gamgam_NLO.py')

genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
]

evgenConfig.inputconfcheck = "TXT"
