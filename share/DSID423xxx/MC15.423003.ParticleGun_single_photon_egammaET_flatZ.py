evgenConfig.description = "Single egamma particles (photons) for pointing studies: egamma ET spectrum and flat z production point (-200 mm, 200 mm)"
evgenConfig.keywords = ["singleParticle","egamma","photon"]	
evgenConfig.contact  = ["ocariz@in2p3.fr", "Bruno.Lenzi@cern.ch"]
evgenConfig.minevents = 5000

include("MC15JobOptions/ParticleGun_Common.py")
include("MC15JobOptions/ParticleGun_egammaET.py") # defines egammaETSampler

topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler.pid = 22
topSeq.ParticleGun.sampler.mom = egammaETSampler(22, PtMax=1e3)
topSeq.ParticleGun.sampler.pos = PG.PosSampler(x=0., y=0., z=[-200., 200.], t=0.)

