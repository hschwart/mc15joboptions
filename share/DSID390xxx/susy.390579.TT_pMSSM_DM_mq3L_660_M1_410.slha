#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12087987E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.37900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.23485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04173840E+01   # W+
        25     1.25342348E+02   # h
        35     4.00001158E+03   # H
        36     3.99999728E+03   # A
        37     4.00100195E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02323927E+03   # ~d_L
   2000001     4.02037265E+03   # ~d_R
   1000002     4.02258322E+03   # ~u_L
   2000002     4.02155365E+03   # ~u_R
   1000003     4.02323927E+03   # ~s_L
   2000003     4.02037265E+03   # ~s_R
   1000004     4.02258322E+03   # ~c_L
   2000004     4.02155365E+03   # ~c_R
   1000005     7.33872549E+02   # ~b_1
   2000005     4.02394207E+03   # ~b_2
   1000006     7.22652058E+02   # ~t_1
   2000006     2.25067671E+03   # ~t_2
   1000011     4.00419555E+03   # ~e_L
   2000011     4.00294948E+03   # ~e_R
   1000012     4.00307902E+03   # ~nu_eL
   1000013     4.00419555E+03   # ~mu_L
   2000013     4.00294948E+03   # ~mu_R
   1000014     4.00307902E+03   # ~nu_muL
   1000015     4.00412139E+03   # ~tau_1
   2000015     4.00800256E+03   # ~tau_2
   1000016     4.00474041E+03   # ~nu_tauL
   1000021     1.97680656E+03   # ~g
   1000022     3.98708977E+02   # ~chi_10
   1000023    -4.49091237E+02   # ~chi_20
   1000025     4.63487951E+02   # ~chi_30
   1000035     2.05352565E+03   # ~chi_40
   1000024     4.46870628E+02   # ~chi_1+
   1000037     2.05368998E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.32835713E-01   # N_11
  1  2    -1.66505679E-02   # N_12
  1  3    -4.14514448E-01   # N_13
  1  4    -3.66449459E-01   # N_14
  2  1    -3.85083317E-02   # N_21
  2  2     2.35978076E-02   # N_22
  2  3    -7.04477031E-01   # N_23
  2  4     7.08288334E-01   # N_24
  3  1     5.52178025E-01   # N_31
  3  2     2.87514342E-02   # N_32
  3  3     5.76058172E-01   # N_33
  3  4     6.02021400E-01   # N_34
  4  1    -1.10092491E-03   # N_41
  4  2     9.99169284E-01   # N_42
  4  3    -6.84597315E-03   # N_43
  4  4    -4.01579820E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.68769054E-03   # U_11
  1  2     9.99953073E-01   # U_12
  2  1    -9.99953073E-01   # U_21
  2  2     9.68769054E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.68018884E-02   # V_11
  1  2    -9.98385469E-01   # V_12
  2  1    -9.98385469E-01   # V_21
  2  2    -5.68018884E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96516544E-01   # cos(theta_t)
  1  2    -8.33953088E-02   # sin(theta_t)
  2  1     8.33953088E-02   # -sin(theta_t)
  2  2     9.96516544E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999040E-01   # cos(theta_b)
  1  2    -1.38564031E-03   # sin(theta_b)
  2  1     1.38564031E-03   # -sin(theta_b)
  2  2     9.99999040E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06077937E-01   # cos(theta_tau)
  1  2     7.08134131E-01   # sin(theta_tau)
  2  1    -7.08134131E-01   # -sin(theta_tau)
  2  2    -7.06077937E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00244029E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20879872E+03  # DRbar Higgs Parameters
         1    -4.37900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43925895E+02   # higgs               
         4     1.62474062E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20879872E+03  # The gauge couplings
     1     3.61626223E-01   # gprime(Q) DRbar
     2     6.35887189E-01   # g(Q) DRbar
     3     1.03189227E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20879872E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.16228739E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20879872E+03  # The trilinear couplings
  1  1     4.28989582E-07   # A_d(Q) DRbar
  2  2     4.29029803E-07   # A_s(Q) DRbar
  3  3     7.66260036E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20879872E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.46358923E-08   # A_mu(Q) DRbar
  3  3     9.55930944E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20879872E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67636238E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20879872E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85918921E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20879872E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02956833E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20879872E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56955688E+07   # M^2_Hd              
        22    -2.07894624E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.23485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40282527E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.25264690E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45341412E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45341412E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54658588E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54658588E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.35283369E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.86712747E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.01399267E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.80423428E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.31464557E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.29420173E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.76623820E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.16783395E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.18962027E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.72887974E-09    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.31927705E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.24713647E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.39501042E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.41165518E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.36386629E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.02976157E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.68797922E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.99602553E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.18513148E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.94466961E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.58741734E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.67025485E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53570359E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79847483E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59983132E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59187562E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60927476E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.15943050E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13578465E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.11511055E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.87720330E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.83848004E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.59016358E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78803270E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.70606023E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26039491E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.51251041E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78297736E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.49259988E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55387316E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52841060E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61175288E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.78337916E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.04678366E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.65187164E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70308301E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45566987E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78823915E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.60564679E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.12444311E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.89764381E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78830197E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.59778491E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58655545E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53058792E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54492866E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.86450120E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.09805206E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.30694988E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.65130778E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85807559E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78803270E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.70606023E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26039491E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.51251041E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78297736E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.49259988E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55387316E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52841060E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61175288E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.78337916E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.04678366E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.65187164E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70308301E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45566987E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78823915E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.60564679E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.12444311E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.89764381E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78830197E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.59778491E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58655545E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53058792E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54492866E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.86450120E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.09805206E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.30694988E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.65130778E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85807559E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13562759E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05145223E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.44437456E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.87417902E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78169703E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.40233391E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57847816E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03734430E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.95110906E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47809749E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.03410324E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.72733461E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13562759E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05145223E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.44437456E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.87417902E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78169703E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.40233391E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57847816E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03734430E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.95110906E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47809749E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.03410324E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.72733461E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07296876E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49751876E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47634744E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.27423332E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40624864E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54228965E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82013557E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06544818E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.55386134E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97633699E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.03413285E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43897749E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.89686414E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88570820E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13668572E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.20952015E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03598608E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.06604476E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78585022E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.23033042E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55536199E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13668572E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.20952015E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03598608E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.06604476E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78585022E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.23033042E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55536199E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45959656E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09709967E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.39699207E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.68814258E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52837105E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54378225E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04193980E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.02258706E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33576958E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33576958E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11193851E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11193851E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10458381E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.93617936E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.74722737E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.62649559E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.67622430E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.57095330E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.51146741E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.92776232E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.57636314E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.00626895E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.11440075E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18061795E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53136862E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18061795E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53136862E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41301484E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51113718E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51113718E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47871607E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01929134E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01929134E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01929117E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.09653376E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.09653376E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.09653376E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.09653376E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.98845147E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.98845147E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.98845147E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.98845147E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.27533953E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.27533953E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.82407975E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.09876879E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.85792474E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.21892989E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.43771184E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.21892989E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.43771184E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.19915059E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.21946471E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.21946471E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.22101738E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.50552927E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.50552927E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.50552015E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.00585177E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.30471614E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.00585177E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.30471614E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.99125281E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.99165763E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.99165763E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.74089302E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     5.98006943E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     5.98006943E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     5.98006949E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.00561504E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.00561504E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.00561504E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.00561504E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.33518066E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.33518066E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.33518066E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.33518066E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.20777110E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.20777110E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.93476675E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.88646917E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.44906581E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.63908552E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.50926052E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.50926052E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.00608837E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.19583480E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.35725178E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.83682370E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.83682370E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.85045159E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.85045159E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98274540E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94093797E-01    2           5        -5   # BR(h -> b       bb     )
     6.53103778E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31199148E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90459117E-04    2           3        -3   # BR(h -> s       sb     )
     2.13032651E-02    2           4        -4   # BR(h -> c       cb     )
     6.95442218E-02    2          21        21   # BR(h -> g       g      )
     2.39143792E-03    2          22        22   # BR(h -> gam     gam    )
     1.61854014E-03    2          22        23   # BR(h -> Z       gam    )
     2.17591366E-01    2          24       -24   # BR(h -> W+      W-     )
     2.74253356E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52811464E+01   # H decays
#          BR         NDA      ID1       ID2
     3.62805250E-01    2           5        -5   # BR(H -> b       bb     )
     5.99771351E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12064622E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50835850E-04    2           3        -3   # BR(H -> s       sb     )
     7.03479748E-08    2           4        -4   # BR(H -> c       cb     )
     7.05033182E-03    2           6        -6   # BR(H -> t       tb     )
     7.74512450E-07    2          21        21   # BR(H -> g       g      )
     1.92049156E-09    2          22        22   # BR(H -> gam     gam    )
     1.81464284E-09    2          23        22   # BR(H -> Z       gam    )
     1.65129537E-06    2          24       -24   # BR(H -> W+      W-     )
     8.25075677E-07    2          23        23   # BR(H -> Z       Z      )
     6.67924792E-06    2          25        25   # BR(H -> h       h      )
    -3.47393081E-24    2          36        36   # BR(H -> A       A      )
     8.33135925E-21    2          23        36   # BR(H -> Z       A      )
     1.86184061E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55888777E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55888777E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.30443065E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.95380656E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.47126951E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.51504698E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.73256563E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.52718211E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.25384348E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.22750177E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.88500659E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.32291696E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.72589586E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.72589586E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42506343E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52749704E+01   # A decays
#          BR         NDA      ID1       ID2
     3.62871209E-01    2           5        -5   # BR(A -> b       bb     )
     5.99840656E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12088960E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50905587E-04    2           3        -3   # BR(A -> s       sb     )
     7.08171000E-08    2           4        -4   # BR(A -> c       cb     )
     7.06527655E-03    2           6        -6   # BR(A -> t       tb     )
     1.45173534E-05    2          21        21   # BR(A -> g       g      )
     6.50469272E-08    2          22        22   # BR(A -> gam     gam    )
     1.59589931E-08    2          23        22   # BR(A -> Z       gam    )
     1.64802047E-06    2          23        25   # BR(A -> Z       h      )
     1.89422976E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55887537E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55887537E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.01066430E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.38147071E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.26125214E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.00600848E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.42755369E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.83343265E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.38427610E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.29462873E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.50695033E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.94650229E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.94650229E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.55011135E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.84598122E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.97546610E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11277842E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.74142482E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19692947E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46246882E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.72032392E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.64311719E-06    2          24        25   # BR(H+ -> W+      h      )
     2.42690839E-14    2          24        36   # BR(H+ -> W+      A      )
     5.50052063E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.81022788E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.89022098E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55471826E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.34951490E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54608883E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.03731330E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.74677694E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.56160860E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
