#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 Wt production (top), inclusive, with CT10 and Perugia2012 tune'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive']
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg Wt setup starting from ATLAS defaults
#--------------------------------------------------------------
if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_Wt_DR_Common.py')

  if hasattr(PowhegConfig, 'topdecaymode'):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 11111 # inclusive W decays
    PowhegConfig.wdecaymode = 11111 # inclusive W decays
  else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode_top = 't > all'
    PowhegConfig.decay_mode_W = 'w > all'
  PowhegConfig.PDF = 10800 # Set the PDF to CT10
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Run EvtGen as afterburner
#--------------------------------------------------------------
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')



