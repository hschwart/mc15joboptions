#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.85454138E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05421420E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414441E+03   # H
        36     2.00000000E+03   # A
        37     2.00173045E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989275E+03   # ~u_L
   2000002     4.99994936E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989275E+03   # ~c_L
   2000004     4.99994936E+03   # ~c_R
   1000005     4.99932891E+03   # ~b_1
   2000005     5.00083032E+03   # ~b_2
   1000006     4.98343236E+03   # ~t_1
   2000006     5.02097545E+03   # ~t_2
   1000011     5.00008193E+03   # ~e_L
   2000011     5.00007596E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008193E+03   # ~mu_L
   2000013     5.00007596E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99958657E+03   # ~tau_1
   2000015     5.00057191E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.84333146E+03   # ~chi_10
   1000023    -1.85174820E+03   # ~chi_20
   1000025     1.86151341E+03   # ~chi_30
   1000035     3.00144470E+03   # ~chi_40
   1000024     1.85102739E+03   # ~chi_1+
   1000037     3.00144440E+03   # ~chi_2+
   1000039     2.33706464E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.96041888E-01   # N_11
  1  2    -1.02639773E-02   # N_12
  1  3    -5.72829356E-01   # N_13
  1  4    -5.62579103E-01   # N_14
  2  1     1.17558916E-02   # N_21
  2  2    -1.59566653E-02   # N_22
  2  3     7.06901108E-01   # N_23
  2  4    -7.07034658E-01   # N_24
  3  1     8.02866965E-01   # N_31
  3  2     8.73615556E-03   # N_32
  3  3     4.14914688E-01   # N_33
  3  4     4.27988454E-01   # N_34
  4  1     7.08780083E-04   # N_41
  4  2    -9.99781834E-01   # N_42
  4  3    -1.77590505E-03   # N_43
  4  4     2.07997261E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.49674289E-03   # U_11
  1  2     9.99996883E-01   # U_12
  2  1     9.99996883E-01   # U_21
  2  2     2.49674289E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.94186674E-02   # V_11
  1  2    -9.99567177E-01   # V_12
  2  1     9.99567177E-01   # V_21
  2  2     2.94186674E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07639456E-01   # cos(theta_t)
  1  2    -7.06573705E-01   # sin(theta_t)
  2  1     7.06573705E-01   # -sin(theta_t)
  2  2     7.07639456E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.81383484E-01   # cos(theta_b)
  1  2     7.31926600E-01   # sin(theta_b)
  2  1    -7.31926600E-01   # -sin(theta_b)
  2  2    -6.81383484E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04962298E-01   # cos(theta_tau)
  1  2     7.09244780E-01   # sin(theta_tau)
  2  1    -7.09244780E-01   # -sin(theta_tau)
  2  2    -7.04962298E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90197579E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52001301E+02   # vev(Q)              
         4     4.74273428E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52708489E-01   # gprime(Q) DRbar
     2     6.26702431E-01   # g(Q) DRbar
     3     1.08044274E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02700633E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72995967E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79616657E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.85454138E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.82457505E+05   # M^2_Hd              
        22    -8.60500133E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36837479E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.43550024E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.67786164E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.40210630E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.39706482E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.29589829E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.24884437E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.76850351E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.51896462E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.87795114E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.39706482E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.29589829E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.24884437E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.76850351E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.51896462E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.87795114E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.57279552E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.06511010E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.19013612E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.54032503E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.54032503E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.54032503E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.54032503E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.75619998E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.82715252E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.31607937E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.53911641E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.50924878E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.30120066E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.01231552E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.54948867E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.89968210E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.17594635E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.75381586E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.51211989E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.24544531E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.80963301E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.48454489E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.60184947E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.23571375E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.82809655E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.29803217E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.02988363E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.62401757E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.13365215E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.25548055E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.31938843E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.28402758E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.34472594E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.46197562E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.88537593E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.80107762E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.25093934E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.61039715E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.15415019E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09852142E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.92089717E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.26534839E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.69354381E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.68796767E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.15853272E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.37473154E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86858868E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.94011356E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.45213228E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.63270847E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.61841054E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.12928580E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59288928E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09857771E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00021700E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.20738764E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.34259792E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69040393E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.34511746E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38202132E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86910024E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88092579E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.74466055E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.45252492E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.75218154E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.91219678E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89501702E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09852142E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.92089717E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.26534839E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.69354381E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.68796767E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.15853272E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.37473154E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86858868E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.94011356E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.45213228E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.63270847E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.61841054E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.12928580E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59288928E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09857771E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00021700E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.20738764E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.34259792E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69040393E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.34511746E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38202132E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86910024E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88092579E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.74466055E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.45252492E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.75218154E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.91219678E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89501702E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.85875315E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.39525465E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.43838032E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.07561687E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.79249499E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.34416820E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59185539E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.84051320E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.56697811E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.38358761E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.43163554E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.76411843E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.85875315E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.39525465E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.43838032E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.07561687E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.79249499E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.34416820E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59185539E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.84051320E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.56697811E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.38358761E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.43163554E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.76411843E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.34921943E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.78453598E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.89124747E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.13966383E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.68849885E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.26877094E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.38114129E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.80279824E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35721756E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.66761458E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.86756765E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.20073870E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.70387399E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.96137403E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.41194379E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.85855908E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.09785648E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.59214472E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.95547593E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79670866E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.80782052E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.58655814E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.85855908E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.09785648E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.59214472E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.95547593E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79670866E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.80782052E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.58655814E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86094941E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.09276170E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.58997897E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.94715809E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.79437199E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.71583912E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.58188766E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.80495591E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.25998613E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.55993781E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.02325386E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18665212E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18553530E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.18622302E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41998710E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53786024E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48897343E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46786001E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.95222631E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51008368E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.49696074E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.93032271E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.95466092E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51064788E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.53469120E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.67334370E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.26482667E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02984862E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.99556761E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40753938E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82268020E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16245495E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81509144E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.16331171E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.15996680E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.11632709E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.31482577E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.31482577E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.31482577E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.38540457E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.38540457E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.12846827E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.12846827E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01647946E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01647946E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.54157009E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.74098252E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.72989044E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.56896457E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.30478963E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     7.32197157E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.84684558E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.78293884E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.83457257E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.01601139E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55488107E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55359461E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.22975497E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.33734882E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.33734882E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.33734882E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.05002975E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.24458022E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.52457899E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.22833480E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.28920623E-08    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.19795808E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.19723965E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.75991136E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.39245859E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.39245859E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.39245859E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.95007148E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.95007148E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.78445948E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.78445948E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.50017299E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.50017299E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.49687586E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.49687586E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.65565540E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.65565540E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.42009745E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.64128383E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.04193936E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.34183628E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47207015E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47207015E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.59652964E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.47106904E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.84886309E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.60065437E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.13659338E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.97878921E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.20559319E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.25886427E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07559110E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16852354E-01    2           5        -5   # BR(h -> b       bb     )
     6.39056553E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26202483E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79343596E-04    2           3        -3   # BR(h -> s       sb     )
     2.06803091E-02    2           4        -4   # BR(h -> c       cb     )
     6.71482247E-02    2          21        21   # BR(h -> g       g      )
     2.30479647E-03    2          22        22   # BR(h -> gam     gam    )
     1.54136845E-03    2          22        23   # BR(h -> Z       gam    )
     2.01181139E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56806069E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78123533E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48486434E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431049E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71225504E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547670E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666695E-05    2           4        -4   # BR(H -> c       cb     )
     9.96054092E-01    2           6        -6   # BR(H -> t       tb     )
     7.97587862E-04    2          21        21   # BR(H -> g       g      )
     2.73438035E-06    2          22        22   # BR(H -> gam     gam    )
     1.16029325E-06    2          23        22   # BR(H -> Z       gam    )
     3.33324234E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66207643E-04    2          23        23   # BR(H -> Z       Z      )
     9.01544446E-04    2          25        25   # BR(H -> h       h      )
     8.70612629E-24    2          36        36   # BR(H -> A       A      )
     3.48913745E-11    2          23        36   # BR(H -> Z       A      )
     5.69956660E-12    2          24       -37   # BR(H -> W+      H-     )
     5.69956660E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387012E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48762112E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894696E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62255855E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675986E-06    2           3        -3   # BR(A -> s       sb     )
     9.96164129E-06    2           4        -4   # BR(A -> c       cb     )
     9.96983628E-01    2           6        -6   # BR(A -> t       tb     )
     9.43663944E-04    2          21        21   # BR(A -> g       g      )
     3.04155247E-06    2          22        22   # BR(A -> gam     gam    )
     1.35293651E-06    2          23        22   # BR(A -> Z       gam    )
     3.24837552E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74515923E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39462099E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236017E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81139353E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52256897E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45682702E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729343E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403547E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32998324E-04    2          24        25   # BR(H+ -> W+      h      )
     5.44798452E-13    2          24        36   # BR(H+ -> W+      A      )
