#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12074621E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.22485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04180519E+01   # W+
        25     1.25336138E+02   # h
        35     4.00000072E+03   # H
        36     3.99999624E+03   # A
        37     4.00104602E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02328681E+03   # ~d_L
   2000001     4.02054800E+03   # ~d_R
   1000002     4.02263539E+03   # ~u_L
   2000002     4.02107221E+03   # ~u_R
   1000003     4.02328681E+03   # ~s_L
   2000003     4.02054800E+03   # ~s_R
   1000004     4.02263539E+03   # ~c_L
   2000004     4.02107221E+03   # ~c_R
   1000005     2.25139497E+03   # ~b_1
   2000005     4.02425938E+03   # ~b_2
   1000006     7.02027767E+02   # ~t_1
   2000006     2.26172061E+03   # ~t_2
   1000011     4.00386898E+03   # ~e_L
   2000011     4.00373472E+03   # ~e_R
   1000012     4.00275564E+03   # ~nu_eL
   1000013     4.00386898E+03   # ~mu_L
   2000013     4.00373472E+03   # ~mu_R
   1000014     4.00275564E+03   # ~nu_muL
   1000015     4.00536528E+03   # ~tau_1
   2000015     4.00728793E+03   # ~tau_2
   1000016     4.00444177E+03   # ~nu_tauL
   1000021     1.98929711E+03   # ~g
   1000022     9.12596285E+01   # ~chi_10
   1000023    -1.35418993E+02   # ~chi_20
   1000025     1.53240039E+02   # ~chi_30
   1000035     2.06729170E+03   # ~chi_40
   1000024     1.29931705E+02   # ~chi_1+
   1000037     2.06745957E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50944419E-01   # N_11
  1  2     1.41325402E-02   # N_12
  1  3     5.42972874E-01   # N_13
  1  4     3.75583824E-01   # N_14
  2  1    -1.37020146E-01   # N_21
  2  2     2.72490404E-02   # N_22
  2  3    -6.84882896E-01   # N_23
  2  4     7.15135224E-01   # N_24
  3  1     6.45993149E-01   # N_31
  3  2     2.36011981E-02   # N_32
  3  3     4.85917325E-01   # N_33
  3  4     5.88234807E-01   # N_34
  4  1    -9.00467822E-04   # N_41
  4  2     9.99250091E-01   # N_42
  4  3    -4.79775127E-04   # N_43
  4  4    -3.87067749E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.82783103E-04   # U_11
  1  2     9.99999767E-01   # U_12
  2  1    -9.99999767E-01   # U_21
  2  2     6.82783103E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47528923E-02   # V_11
  1  2    -9.98499935E-01   # V_12
  2  1    -9.98499935E-01   # V_21
  2  2    -5.47528923E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.31275271E-02   # cos(theta_t)
  1  2     9.96538918E-01   # sin(theta_t)
  2  1    -9.96538918E-01   # -sin(theta_t)
  2  2    -8.31275271E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999851E-01   # cos(theta_b)
  1  2    -5.45893742E-04   # sin(theta_b)
  2  1     5.45893742E-04   # -sin(theta_b)
  2  2     9.99999851E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03502554E-01   # cos(theta_tau)
  1  2     7.10692730E-01   # sin(theta_tau)
  2  1    -7.10692730E-01   # -sin(theta_tau)
  2  2    -7.03502554E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00317130E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20746211E+03  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43764687E+02   # higgs               
         4     1.61055796E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20746211E+03  # The gauge couplings
     1     3.61996763E-01   # gprime(Q) DRbar
     2     6.36556218E-01   # g(Q) DRbar
     3     1.03105643E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20746211E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14938101E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20746211E+03  # The trilinear couplings
  1  1     4.21347052E-07   # A_d(Q) DRbar
  2  2     4.21386772E-07   # A_s(Q) DRbar
  3  3     7.53925577E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20746211E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.07635382E-08   # A_mu(Q) DRbar
  3  3     9.16962326E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20746211E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68393765E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20746211E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80652927E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20746211E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04139964E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20746211E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58677369E+07   # M^2_Hd              
        22    -2.82721959E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.22485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40574945E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.23865573E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.98713757E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.25165631E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.20939926E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.99580548E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.86962962E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.25321826E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.11471627E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.30234092E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.48234506E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.77053385E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.96710179E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.73301548E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.11510730E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.46690197E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.34008745E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.93583242E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.64834645E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.16331455E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.18840491E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.26016697E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.60471199E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.59397129E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.20710847E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.62963156E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64312629E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.82786895E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.70968144E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41657688E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.99344793E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58034057E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.82111005E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14608218E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.10271818E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.31122268E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.58742445E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.23973229E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76299043E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45442484E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.43783942E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88425345E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81677751E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29980061E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62126993E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51750724E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58986548E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.17943270E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05741636E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34902859E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49221224E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43657946E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76317979E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16648227E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.50359606E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.63197091E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82145122E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.24206933E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65296168E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51975750E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52223116E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.30063970E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.76062765E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.13267641E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.50529410E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85290615E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76299043E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45442484E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.43783942E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88425345E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81677751E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29980061E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62126993E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51750724E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58986548E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.17943270E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05741636E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34902859E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49221224E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43657946E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76317979E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16648227E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.50359606E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.63197091E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82145122E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.24206933E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65296168E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51975750E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52223116E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.30063970E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.76062765E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.13267641E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.50529410E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85290615E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12672676E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.79088323E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.32220307E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.86620237E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76923909E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.80224083E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55182551E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08367625E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64376293E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87663226E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.16856948E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.36931621E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12672676E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.79088323E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.32220307E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.86620237E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76923909E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.80224083E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55182551E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08367625E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64376293E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87663226E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.16856948E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.36931621E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10111616E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23526320E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01768169E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66677149E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38251872E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41985911E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77169251E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10926859E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.07138180E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38396131E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42765382E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41010648E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.25443273E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82701850E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12782183E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00292843E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.69215726E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.07761122E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77205703E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08616522E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52947019E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12782183E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00292843E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.69215726E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.07761122E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77205703E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08616522E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52947019E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46202696E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.06493698E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.14484379E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.49323848E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50704779E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.84838647E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00084757E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.46358577E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33719613E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33719613E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11240644E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11240644E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10079486E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69513180E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.18872696E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46975796E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.22064011E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.44045250E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.74687807E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40197476E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.72406181E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.19591398E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18492729E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53690026E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18492729E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53690026E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37602672E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52337690E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52337690E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48038885E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04476228E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04476228E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04476197E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.86270127E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.86270127E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.86270127E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.86270127E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.28757030E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.28757030E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.28757030E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.28757030E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.80009595E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.80009595E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43599375E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.59947590E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.80132992E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.53560979E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.57259807E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.53560979E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.57259807E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.31308819E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.03785367E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.03785367E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.02883488E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.10109748E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.10109748E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.10109383E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.14474151E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.26788160E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.14474151E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.26788160E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.60100196E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.12528613E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.12528613E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.00728744E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.24768026E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.24768026E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.24768039E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.36613427E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.36613427E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.36613427E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.36613427E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.12199081E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.12199081E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.12199081E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.12199081E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.03332203E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.03332203E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69401091E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39169532E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.29453951E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.36350622E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40891649E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40891649E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.90832258E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.17517280E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.30892438E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.57604926E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.57604926E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.03610965E-03   # h decays
#          BR         NDA      ID1       ID2
     5.99752612E-01    2           5        -5   # BR(h -> b       bb     )
     6.44624825E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28197621E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84094591E-04    2           3        -3   # BR(h -> s       sb     )
     2.10207459E-02    2           4        -4   # BR(h -> c       cb     )
     6.85089107E-02    2          21        21   # BR(h -> g       g      )
     2.35541727E-03    2          22        22   # BR(h -> gam     gam    )
     1.59630005E-03    2          22        23   # BR(h -> Z       gam    )
     2.14549377E-01    2          24       -24   # BR(h -> W+      W-     )
     2.70418623E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.39840171E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37900944E-01    2           5        -5   # BR(H -> b       bb     )
     6.14180554E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.17159368E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56862140E-04    2           3        -3   # BR(H -> s       sb     )
     7.20591649E-08    2           4        -4   # BR(H -> c       cb     )
     7.22182848E-03    2           6        -6   # BR(H -> t       tb     )
     1.07122097E-06    2          21        21   # BR(H -> g       g      )
     2.94387791E-10    2          22        22   # BR(H -> gam     gam    )
     1.85329272E-09    2          23        22   # BR(H -> Z       gam    )
     2.08623572E-06    2          24       -24   # BR(H -> W+      W-     )
     1.04239523E-06    2          23        23   # BR(H -> Z       Z      )
     7.46319591E-06    2          25        25   # BR(H -> h       h      )
    -1.83590766E-24    2          36        36   # BR(H -> A       A      )
    -1.81182290E-20    2          23        36   # BR(H -> Z       A      )
     1.87138599E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66971246E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66971246E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40881032E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.65121533E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70974744E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42437607E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.48602001E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.11566856E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12510732E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.40843738E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22069269E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.08882231E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.16257918E-07    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.16257918E-07    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.39822477E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37938141E-01    2           5        -5   # BR(A -> b       bb     )
     6.14204989E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.17167837E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56914009E-04    2           3        -3   # BR(A -> s       sb     )
     7.25129534E-08    2           4        -4   # BR(A -> c       cb     )
     7.23446836E-03    2           6        -6   # BR(A -> t       tb     )
     1.48650046E-05    2          21        21   # BR(A -> g       g      )
     4.07558735E-08    2          22        22   # BR(A -> gam     gam    )
     1.63430665E-08    2          23        22   # BR(A -> Z       gam    )
     2.08194991E-06    2          23        25   # BR(A -> Z       h      )
     1.87471316E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66978732E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66978732E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.97424318E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.28326571E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34944761E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.90143600E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.38837427E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.77865722E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.40085452E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.28934290E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.67213649E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.36912520E-07    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.36912520E-07    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.38047024E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37249907E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.16393477E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17941632E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43839642E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23467941E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.54013260E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42679444E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.09119651E-06    2          24        25   # BR(H+ -> W+      h      )
     3.11814042E-14    2          24        36   # BR(H+ -> W+      A      )
     4.73324425E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.52610823E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.25998771E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67748840E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.93772664E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57667222E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.92758071E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.12887671E-08    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
