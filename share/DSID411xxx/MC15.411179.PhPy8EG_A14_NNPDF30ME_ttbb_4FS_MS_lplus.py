#--------------------------------------------------------------
# General settings
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG-BOX-RES/OpenLoops+Pythia8+EvtGen ttbb (4FS), ME NNPDF30_nlo_as_0118_nf_4 PDF, mur=[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)], hdamp=HT/2, A14 NNPDF23 LO tune, single-lepton positive charge channel, decays with MadSpin'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'bbbar', '1lepton' ]
evgenConfig.contact     = [ 'tpelzer@cern.ch' ]
evgenConfig.minevents   = 500

#--------------------------------------------------------------
# Powheg
#--------------------------------------------------------------
### ttbb Powheg fragment
include('PowhegControl/PowhegControl_ttbb_Common.py')
### to improve integration (not relevant for generation) - NB: integration was done with ATHENA_PROC_NUMBER=5
PowhegConfig.ncall1       = "200000"
PowhegConfig.ncall2       = "200000"
PowhegConfig.ncall2rm     = "400000"
### decay mode handled by MadSpin
PowhegConfig.decay_mode   = "t t~ > all [MadSpin]"
### muR/muF functional forms as in  Eur. Phys. J. C 78 (2018) 502 arxiv:1802.00426
PowhegConfig.runningscales = 1 # mur=[mT(top)*mT(tbar)*mT(b)*mT(bbar)]**(1/4), muf=1/2*[mT(top)+mT(tbar)+mT(b)+mT(bbar)+mT(gluon)]
### internal reweighting
## factorisation and normalisation scales - factor 2 up & down plus factor 4 down to be able to use factor 0.5 as central value
PowhegConfig.mu_F         = [ 1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0, 0.25, 0.5,  0.25, 0.25, 1.0  ] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [ 1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5, 0.5,  0.25, 0.25, 1.0,  0.25 ] # List of renormalisation scales
## PDF sets - 4FS PDFs, NNPDF30_nlo_as_0118_nf_4 as nominal, 6 alternative central values, error sets of NNPDF30_nlo_as_0118_nf_4 and PDF4LHC15_nlo_nf4_30
PowhegConfig.PDF          = [ 260400, 13191, 25410, 92000, 265400, 266400, 320500 ] # central values of NNPDF30_nlo_as_0118_nf_4, CT14nlo_NF4, MMHT2014nlo68cl_nf4, PDF4LHC15_nlo_nf4_30, NNPDF30_nlo_as_0117_nf_4, NNPDF30_nlo_as_0119_nf_4, NNPDF31_nlo_as_0118_nf_4
PowhegConfig.PDF.extend(range(260401, 260501)) # NNPDF30_nlo_as_0118_nf_4 error set
PowhegConfig.PDF.extend(range(92001, 92031)) # PDF4LHC15_nlo_nf4_30 error set

### MadSpin configuration
# single lepton plus
PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > l+ vl", "decay t~ > w- b~, w- > j j"]
# Parameters are hardcoded to avoid madspin picking up it's own defaults.
# Lines taken from t-channel single-top (4fs) Powheg joboption
PowhegConfig.alphaem_inv  = 1.323489e+02
PowhegConfig.G_F          = 1.166370e-05
PowhegConfig.alphaqcd     = 1.184000e-01
PowhegConfig.mass_W       = 80.399
PowhegConfig.mass_Z       = 9.118760e+01
PowhegConfig.mass_H       = 125.0
# use BWcutoff of 50 to be consistent with ttbar
PowhegConfig.bwcutoff     = 50

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8
#--------------------------------------------------------------
### standard Pythia8 A14 tune + EvtGen
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
### main31 routine for Powheg matching, with Powheg:NFinal = 4 for ttbb ME
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 4' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]
