#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871783E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04014110E+01   # W+
        25     1.24857705E+02   # h
        35     3.00008105E+03   # H
        36     2.99999996E+03   # A
        37     3.00089540E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04197652E+03   # ~d_L
   2000001     3.03688061E+03   # ~d_R
   1000002     3.04107147E+03   # ~u_L
   2000002     3.03781096E+03   # ~u_R
   1000003     3.04197652E+03   # ~s_L
   2000003     3.03688061E+03   # ~s_R
   1000004     3.04107147E+03   # ~c_L
   2000004     3.03781096E+03   # ~c_R
   1000005     1.05173282E+03   # ~b_1
   2000005     3.03579456E+03   # ~b_2
   1000006     1.04799338E+03   # ~t_1
   2000006     3.01868375E+03   # ~t_2
   1000011     3.00623373E+03   # ~e_L
   2000011     3.00223214E+03   # ~e_R
   1000012     3.00484683E+03   # ~nu_eL
   1000013     3.00623373E+03   # ~mu_L
   2000013     3.00223214E+03   # ~mu_R
   1000014     3.00484683E+03   # ~nu_muL
   1000015     2.98557944E+03   # ~tau_1
   2000015     3.02191619E+03   # ~tau_2
   1000016     3.00455212E+03   # ~nu_tauL
   1000021     2.35219690E+03   # ~g
   1000022     2.50873974E+02   # ~chi_10
   1000023     5.28715683E+02   # ~chi_20
   1000025    -2.99470642E+03   # ~chi_30
   1000035     2.99547013E+03   # ~chi_40
   1000024     5.28878870E+02   # ~chi_1+
   1000037     2.99602568E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886574E-01   # N_11
  1  2    -8.46170870E-04   # N_12
  1  3     1.49067251E-02   # N_13
  1  4    -1.97823331E-03   # N_14
  2  1     1.25738576E-03   # N_21
  2  2     9.99622535E-01   # N_22
  2  3    -2.68333128E-02   # N_23
  2  4     5.76011363E-03   # N_24
  3  1    -9.12653738E-03   # N_31
  3  2     1.49113751E-02   # N_32
  3  3     7.06862064E-01   # N_33
  3  4     7.07135334E-01   # N_34
  4  1    -1.19149134E-02   # N_41
  4  2     2.30590870E-02   # N_42
  4  3     7.06685068E-01   # N_43
  4  4    -7.07051998E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99279170E-01   # U_11
  1  2    -3.79623533E-02   # U_12
  2  1     3.79623533E-02   # U_21
  2  2     9.99279170E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966785E-01   # V_11
  1  2    -8.15035006E-03   # V_12
  2  1     8.15035006E-03   # V_21
  2  2     9.99966785E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447802E-01   # cos(theta_t)
  1  2    -5.56954817E-02   # sin(theta_t)
  2  1     5.56954817E-02   # -sin(theta_t)
  2  2     9.98447802E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99905999E-01   # cos(theta_b)
  1  2     1.37110599E-02   # sin(theta_b)
  2  1    -1.37110599E-02   # -sin(theta_b)
  2  2     9.99905999E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06952553E-01   # cos(theta_tau)
  1  2     7.07260976E-01   # sin(theta_tau)
  2  1    -7.07260976E-01   # -sin(theta_tau)
  2  2     7.06952553E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01756622E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68717834E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43928362E+02   # higgs               
         4     7.73063560E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68717834E+03  # The gauge couplings
     1     3.62569398E-01   # gprime(Q) DRbar
     2     6.37937935E-01   # g(Q) DRbar
     3     1.02371525E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68717834E+03  # The trilinear couplings
  1  1     2.71028082E-06   # A_u(Q) DRbar
  2  2     2.71031830E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68717834E+03  # The trilinear couplings
  1  1     7.15595565E-07   # A_d(Q) DRbar
  2  2     7.15724110E-07   # A_s(Q) DRbar
  3  3     1.59234933E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68717834E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.50122858E-07   # A_mu(Q) DRbar
  3  3     1.51916531E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68717834E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50443254E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68717834E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14824288E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68717834E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06750427E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68717834E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.37814095E+04   # M^2_Hd              
        22    -9.03137659E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41259074E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.13866962E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47654563E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47654563E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52345437E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52345437E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.12475082E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.83367832E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.07001736E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.74661481E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10624641E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.91840365E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.98321021E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.00426516E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.36785304E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.00438905E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67839852E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59443500E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11801280E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.99885390E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.00904652E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.47376175E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.32533360E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17958849E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.96438709E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.38975415E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.54966851E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.48518146E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.54805647E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31242882E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.83940057E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.13390824E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.93293165E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.87258517E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98790562E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60795738E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.10547843E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.48775438E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.21647657E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.24493168E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.11568570E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15320947E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59782357E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.41343941E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.22910446E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.02343663E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40217369E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.87766555E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.09326787E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60578773E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.13228102E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.15097248E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21077958E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.05678464E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.12249330E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64798082E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54631899E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.86680657E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.35423161E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.51767455E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54536733E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.87258517E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98790562E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60795738E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.10547843E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.48775438E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.21647657E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.24493168E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.11568570E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15320947E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59782357E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.41343941E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.22910446E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.02343663E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40217369E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.87766555E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.09326787E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60578773E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.13228102E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.15097248E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21077958E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.05678464E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.12249330E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64798082E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54631899E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.86680657E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.35423161E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.51767455E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54536733E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.81233844E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01356313E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99978546E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.76853067E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.32672686E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98665092E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.22910703E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54811233E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998489E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.50576235E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.11823693E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.91548194E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.81233844E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01356313E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99978546E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.76853067E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.32672686E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98665092E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.22910703E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54811233E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998489E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.50576235E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.11823693E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.91548194E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70759873E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56956615E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14666753E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28376631E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.65042569E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65429136E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11842276E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54414619E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.71274023E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22678435E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.75836055E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.81255630E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01907610E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98949249E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.85121108E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.10243819E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99143122E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.45691917E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.81255630E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01907610E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98949249E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.85121108E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.10243819E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99143122E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.45691917E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81247844E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01899416E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98922286E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.51724062E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.03445176E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99176157E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.12569476E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.85600915E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85922716E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.73603528E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.00917933E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.05788573E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.54044037E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.35394197E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.34708456E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.93067389E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.45548178E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.04419342E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49558066E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.88515171E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.52934710E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.28999639E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.31105518E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.31105518E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.30611079E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.25819281E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54456980E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54456980E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.23915095E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.23915095E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.21597666E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.21597666E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.79441857E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.07923289E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.24152523E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.39991356E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.39991356E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.61025383E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.70327119E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.50989027E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.50989027E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.26965951E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.26965951E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.74706413E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.74706413E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.76357335E-03   # h decays
#          BR         NDA      ID1       ID2
     6.71511402E-01    2           5        -5   # BR(h -> b       bb     )
     5.47224500E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93719629E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.11229002E-04    2           3        -3   # BR(h -> s       sb     )
     1.77555931E-02    2           4        -4   # BR(h -> c       cb     )
     5.75821082E-02    2          21        21   # BR(h -> g       g      )
     1.96692047E-03    2          22        22   # BR(h -> gam     gam    )
     1.29521176E-03    2          22        23   # BR(h -> Z       gam    )
     1.72977918E-01    2          24       -24   # BR(h -> W+      W-     )
     2.15834484E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.41029893E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39872382E-01    2           5        -5   # BR(H -> b       bb     )
     1.76325990E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.23446565E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.65594245E-04    2           3        -3   # BR(H -> s       sb     )
     2.15980462E-07    2           4        -4   # BR(H -> c       cb     )
     2.15454340E-02    2           6        -6   # BR(H -> t       tb     )
     2.61246781E-05    2          21        21   # BR(H -> g       g      )
     1.39353558E-08    2          22        22   # BR(H -> gam     gam    )
     8.31804540E-09    2          23        22   # BR(H -> Z       gam    )
     2.95318483E-05    2          24       -24   # BR(H -> W+      W-     )
     1.47477185E-05    2          23        23   # BR(H -> Z       Z      )
     7.87242831E-05    2          25        25   # BR(H -> h       h      )
     5.80743053E-24    2          36        36   # BR(H -> A       A      )
     4.48162703E-19    2          23        36   # BR(H -> Z       A      )
     2.08064541E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08632165E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03754882E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.79539221E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.68060514E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.36960065E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33115437E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83944604E-01    2           5        -5   # BR(A -> b       bb     )
     1.86808836E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60510479E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.11244024E-04    2           3        -3   # BR(A -> s       sb     )
     2.28921565E-07    2           4        -4   # BR(A -> c       cb     )
     2.28238269E-02    2           6        -6   # BR(A -> t       tb     )
     6.72139855E-05    2          21        21   # BR(A -> g       g      )
     3.78736025E-08    2          22        22   # BR(A -> gam     gam    )
     6.62152268E-08    2          23        22   # BR(A -> Z       gam    )
     3.11702559E-05    2          23        25   # BR(A -> Z       h      )
     2.62786886E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21593829E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31034640E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92451991E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04088530E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10487312E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38975055E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44957504E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07117695E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96564115E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02159206E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18199450E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.99232776E-05    2          24        25   # BR(H+ -> W+      h      )
     7.27746288E-14    2          24        36   # BR(H+ -> W+      A      )
     1.93016568E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.99779391E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.78272528E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
