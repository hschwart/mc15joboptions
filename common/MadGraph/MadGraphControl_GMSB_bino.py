
def get_mg_variations(m_stau, m_neutralino, xqcut=None):
  if xqcut is None:
    xqcut = 500  # default
  return xqcut


from MadGraphControl.MadGraphUtils import *

# keep the output directory for testing?!
keepOutput = False

# get some parameters from the jobOptions and config and build the SLHA name
import re
plist = re.search(r'.+GMSB_(.+)\.py', runArgs.jobConfig[0]).group(1).split('_')
lambdaVal = float(plist[2])
tanbVal = float(plist[4])
massVal = float(plist[6])
lifetime = float(-1.) # -1 is for stable (lifetimes not yet implemented in simulation preInclude.GMSB.py)
slha_file = 'susy.%s.GMSB_bino_lambda_%03d_tanb_%02d_mass_%02d.slha'  % (runArgs.runNumber, int(lambdaVal), int(tanbVal), int(massVal))

# get more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 2.0
if evt_multiplier > 0:
  if runArgs.maxEvents > 0:
    nevts = runArgs.maxEvents * evt_multiplier
  else:
    nevts = 5000 * evt_multiplier

# MadGraph5 systematics variations
  process = '''
  generate p p > ta1- svt~, svt~ > n1 vt~ $ susystrong @1
  add process p p > ta1+ svt, svt > n1 vt $ susystrong @1
  '''

# set number of jets done in madgraph
njets = 0

# choose pdf set
pdlabel = 'nn23lo1'
lhaid = 247000

# set beam energy (overwrite by job settings)
beamEnergy = 6500.
if hasattr(runArgs, 'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy * 0.5

# set random seed (overwrite by job settings)
rand_seed = 1234
if hasattr(runArgs, "randomSeed"):
  rand_seed = runArgs.randomSeed

if not os.environ.has_key('MADGRAPH_DATA'):
  os.environ['MADGRAPH_DATA'] = os.getcwd()
  mglog.warning('Setting your MADGRAPH_DATA environmental variable to the working directory')

include("/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/Pythia8/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/Pythia8/Pythia8_MadGraph.py")

# generate the new process
full_proc = """
import model mssm-full
""" + helpful_definitions() + """
# Specify process(es) to run

""" + process + """
# Output processes to MadEvent directory
output -f
"""

thedir = new_process(card_loc = full_proc)
if 1 == thedir:
  mglog.error('Error in process generation!')
mglog.info('Using process directory ' + thedir)

# grab param_card and move new masses into place
from PyJobTransformsCore.trfutil import get_files
get_files(slha_file, keepDir=False, errorIfNotFound=True)
include('/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/SUSYMetadata.py')
(m_stau, m_neutralino) = mass_extract(slha_file, ['1000015', '1000022'])
mglog.info('stau mass = ' + m_stau + ' neutralino mass = ' + m_neutralino)

#if lifetime != -1:
  # remove stau1 and neutralino decay from slha file
#  remove_decay(slha_file, '1000015')
#  remove_decay(slha_file, '1000022')

build_param_card(param_card_old = slha_file, param_card_new = 'param_card.dat')

xqcut = None
xqcut= get_mg_variations(abs(float(m_stau)), abs(float(m_neutralino)), xqcut)
mglog.info("MG5 params: %s"%(xqcut))

# grab run_card and move into place
extras = {'ktdurham': xqcut,
          'lhe_version': '2.0',
          'cut_decays': 'F',
          'pdlabel': pdlabel,
          'lhaid': lhaid,
          'drjj': 0.0,
          'ickkw': 0
          }
build_run_card(run_card_old = get_default_runcard(proc_dir = thedir),
               run_card_new = 'run_card.dat',
               xqcut = xqcut,
               nevts = nevts,
               rand_seed = rand_seed,
               beamEnergy = beamEnergy,
           #    scalefact = scalefact,
           #    alpsfact = alpsfact,
               extras = extras)

if generate(run_card_loc = 'run_card.dat', param_card_loc = 'param_card.dat', mode = 0, njobs = 1, run_name = 'Test', proc_dir = thedir):
  mglog.error('Error generating events!')

# move output files to appropriate place, with appropriate name
the_spot = arrange_output(run_name = 'Test', proc_dir = thedir, outputDS = 'madgraph_OTF._00001.events.tar.gz')
if the_spot == '':
  mglog.error('Error arranging output dataset!')

mglog.info('Removing process directory...')
shutil.rmtree(thedir, ignore_errors=True)

mglog.info('All done generating events!!')

outputDS = the_spot

if xqcut < 0 or outputDS is None or '' == outputDS:
  evgenLog.warning('Looks like something went wrong with the MadGraph generation - bailing out!')
  raise RuntimeError('Error in MadGraph generation')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
  evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
  njobs = os.environ.pop('ATHENA_PROC_NUMBER')
  # Try to modify the opts underfoot
  if not hasattr(opts, 'nprocs'):
    mglog.warning('Did not see option!')
  else:
    opts.nprocs = 0
  mglog.info(opts)

runArgs.qcut = xqcut
runArgs.inputGeneratorFile = outputDS
# runArgs.gentype = production
#if mg_syst_mod:
#  runArgs.syst_mod = mg_syst_mod
#elif py8_syst_mod:
#  runArgs.syst_mod = py8_syst_mod

# Pythia8 setup
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "1000015:mayDecay = false",
			    "1000022:mayDecay = false"
                           ]

if njets > 0:
  genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on", # PMG HAVE A LOOK
                              "Merging:nJetMax = " + str(njets),
                              "Merging:doKTMerging = on",
                              "Merging:TMS = " + str(xqcut),
                              "Merging:ktType = 1",
                              "Merging:Dparameter = 0.4",
                              "1000024:spinType = 1",
                              "1000022:spinType = 1"
                             ]
genSeq.Pythia8.Commands += ["Merging:Process = guess"]

# configuration for EvgenJobTransforms
evgenLog.info('EVNT generation of GMSB bino')
evgenConfig.contact  = [ "revital.kopeliansky@cern.ch" ]
evgenConfig.keywords += ['SUSY','GMSB']
evgenConfig.description = 'GMSB bino'
evgenConfig.generators += ["EvtGen"]

if lifetime != 0:
  evgenConfig.specialConfig = 'GMSBIndex=2;GMSBStau=%s*GeV;GMSBneutralino=%s*GeV;preInclude=SimulationJobOptions/preInclude.GMSBSpecial.py' % (m_stau, m_neutralino)

if not hasattr(runArgs, 'inputGeneratorFile'):
  print 'ERROR: something wasnt write in file name transfer from the fragment.'
  runArgs.inputGeneratorFile = 'madgraph.*._events.tar.gz'
evgenConfig.inputfilecheck = runArgs.inputGeneratorFile.split('._0')[0]

# testSeq.TestHepMC.MaxVtxDisp = 1e16 # in mm
# testSeq.TestHepMC.MaxTransVtxDisp = 1e16 # in mm
testSeq.TestHepMC.MaxNonG4Energy=14000000.
bonus_file = open('pdg_extras.dat','w')
#bonus_file.write('2000039\n')
#bonus_file.write('1000015 stau '+m_stau+' (GeV/c) stau 1\n')
#bonus_file.write('-1000015 Anti-stau '+m_stau+' (GeV/c) stau -1\n')
bonus_file.write('1000015 Stau1 '+m_stau+' (MeV/c) fermion Stau -1\n')
bonus_file.write('-1000015 Anti-stau1 '+m_stau+' (MeV/c) fermion Stau 1\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

import os
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)

# clean up
del keepOutput, plist, lambdaVal, tanbVal, massVal, lifetime, slha_file, evt_multiplier, nevts, njets, pdlabel, lhaid, beamEnergy, rand_seed, full_proc, thedir, m_stau, m_neutralino, xqcut, extras, the_spot, outputDS
