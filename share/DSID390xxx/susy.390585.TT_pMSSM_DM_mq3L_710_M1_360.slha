#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12001220E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.54959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.91900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181476E+01   # W+
        25     1.25653963E+02   # h
        35     4.00000974E+03   # H
        36     3.99999693E+03   # A
        37     4.00101541E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02301009E+03   # ~d_L
   2000001     4.02022988E+03   # ~d_R
   1000002     4.02235636E+03   # ~u_L
   2000002     4.02124195E+03   # ~u_R
   1000003     4.02301009E+03   # ~s_L
   2000003     4.02022988E+03   # ~s_R
   1000004     4.02235636E+03   # ~c_L
   2000004     4.02124195E+03   # ~c_R
   1000005     7.73596002E+02   # ~b_1
   2000005     4.02383894E+03   # ~b_2
   1000006     7.57849927E+02   # ~t_1
   2000006     2.07101081E+03   # ~t_2
   1000011     4.00405667E+03   # ~e_L
   2000011     4.00314686E+03   # ~e_R
   1000012     4.00294246E+03   # ~nu_eL
   1000013     4.00405667E+03   # ~mu_L
   2000013     4.00314686E+03   # ~mu_R
   1000014     4.00294246E+03   # ~nu_muL
   1000015     4.00435414E+03   # ~tau_1
   2000015     4.00789851E+03   # ~tau_2
   1000016     4.00462817E+03   # ~nu_tauL
   1000021     1.97469769E+03   # ~g
   1000022     3.49215727E+02   # ~chi_10
   1000023    -4.02898510E+02   # ~chi_20
   1000025     4.16190970E+02   # ~chi_30
   1000035     2.05339992E+03   # ~chi_40
   1000024     4.00597389E+02   # ~chi_1+
   1000037     2.05356441E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.47485386E-01   # N_11
  1  2    -1.52651904E-02   # N_12
  1  3    -4.01688276E-01   # N_13
  1  4    -3.46672789E-01   # N_14
  2  1    -4.34000471E-02   # N_21
  2  2     2.40530439E-02   # N_22
  2  3    -7.03952935E-01   # N_23
  2  4     7.08511222E-01   # N_24
  3  1     5.29040484E-01   # N_31
  3  2     2.84271466E-02   # N_32
  3  3     5.85715389E-01   # N_33
  3  4     6.13388577E-01   # N_34
  4  1    -1.05904040E-03   # N_41
  4  2     9.99189833E-01   # N_42
  4  3    -5.85459757E-03   # N_43
  4  4    -3.98030118E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.28549510E-03   # U_11
  1  2     9.99965675E-01   # U_12
  2  1    -9.99965675E-01   # U_21
  2  2     8.28549510E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.63005806E-02   # V_11
  1  2    -9.98413864E-01   # V_12
  2  1    -9.98413864E-01   # V_21
  2  2    -5.63005806E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94808816E-01   # cos(theta_t)
  1  2    -1.01761582E-01   # sin(theta_t)
  2  1     1.01761582E-01   # -sin(theta_t)
  2  2     9.94808816E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999225E-01   # cos(theta_b)
  1  2    -1.24498972E-03   # sin(theta_b)
  2  1     1.24498972E-03   # -sin(theta_b)
  2  2     9.99999225E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05951275E-01   # cos(theta_tau)
  1  2     7.08260402E-01   # sin(theta_tau)
  2  1    -7.08260402E-01   # -sin(theta_tau)
  2  2    -7.05951275E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00261894E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20012204E+03  # DRbar Higgs Parameters
         1    -3.91900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43872208E+02   # higgs               
         4     1.61939228E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20012204E+03  # The gauge couplings
     1     3.61633798E-01   # gprime(Q) DRbar
     2     6.35958994E-01   # g(Q) DRbar
     3     1.03202464E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20012204E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14469164E-06   # A_c(Q) DRbar
  3  3     2.54959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20012204E+03  # The trilinear couplings
  1  1     4.21572406E-07   # A_d(Q) DRbar
  2  2     4.21611870E-07   # A_s(Q) DRbar
  3  3     7.53934138E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20012204E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.30260517E-08   # A_mu(Q) DRbar
  3  3     9.39716124E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20012204E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69039341E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20012204E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85711254E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20012204E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03150745E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20012204E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57326222E+07   # M^2_Hd              
        22    -1.46977677E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40310873E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.04331677E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43878232E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43878232E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56121768E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56121768E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.39292483E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.41732690E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.13659153E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.26999558E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.17608599E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19870843E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.56061172E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15966087E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.24385249E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.29485368E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.88389578E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.60900844E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.12412315E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.33190726E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.84882781E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.24223606E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.54014427E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.74997459E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.74676451E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66869891E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53762308E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80353864E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61494067E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.19191291E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62379476E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.36279692E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13416485E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.68535749E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.28022254E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.82941118E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.48927189E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78631642E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.81786518E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14417709E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.41613671E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78868944E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42793519E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.56527996E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52662069E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61031707E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.93851270E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02787287E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.52533395E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.42953071E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45258712E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78651857E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.66174556E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.91988056E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.41372435E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79387204E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.17563168E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59770614E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52880145E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54332804E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.02709565E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.68050719E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.97779339E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.94055520E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85724436E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78631642E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.81786518E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14417709E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.41613671E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78868944E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42793519E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.56527996E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52662069E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61031707E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.93851270E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02787287E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.52533395E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.42953071E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45258712E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78651857E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.66174556E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.91988056E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.41372435E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79387204E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.17563168E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59770614E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52880145E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54332804E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.02709565E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.68050719E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.97779339E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.94055520E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85724436E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13873680E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.10066998E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.97129961E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.44443492E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77982537E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.90609587E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57436858E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04775029E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.19534940E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87741313E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.78587027E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.19509972E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13873680E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.10066998E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.97129961E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.44443492E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77982537E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.90609587E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57436858E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04775029E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.19534940E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87741313E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.78587027E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.19509972E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08231730E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.59106585E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44783973E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.19437754E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40349708E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51839182E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81443637E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07650835E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.65807967E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05266185E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.93294437E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43352252E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95588468E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87459879E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13980054E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.24862095E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19350252E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.72421981E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78370943E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18675372E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55144507E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13980054E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.24862095E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19350252E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.72421981E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78370943E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18675372E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55144507E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46494841E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13194249E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08197932E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.37622541E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52506803E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58884809E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03566233E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.31762594E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33545696E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33545696E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183409E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183409E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10541789E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.82265741E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.72251541E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.58767474E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.83640833E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.42943822E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.67022978E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.22165401E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.74036818E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.26877996E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.15424834E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17930300E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52963630E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17930300E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52963630E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42350014E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50700549E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50700549E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47729706E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01108539E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01108539E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01108520E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.98488177E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.98488177E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.98488177E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.98488177E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.61627838E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.61627838E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.61627838E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.61627838E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.84347443E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.84347443E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.64033844E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.49186413E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.67599410E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.79400560E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.77031398E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.79400560E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.77031398E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.39716679E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.97751900E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.97751900E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.97609958E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.03556800E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.03556800E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.03555686E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.95964330E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.73030009E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.95964330E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.73030009E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.74946514E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.77246488E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.77246488E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.59793987E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.54297681E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.54297681E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.54297684E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.49385755E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.49385755E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.49385755E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.49385755E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.49793755E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.49793755E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.49793755E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.49793755E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.40560489E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.40560489E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.82136534E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.72518706E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.41054216E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.85372668E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.66546064E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.66546064E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.79929830E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.39440680E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.44830327E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82367686E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.82367686E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.83180571E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.83180571E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03118344E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90091450E-01    2           5        -5   # BR(h -> b       bb     )
     6.46910399E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29005324E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85570049E-04    2           3        -3   # BR(h -> s       sb     )
     2.10893938E-02    2           4        -4   # BR(h -> c       cb     )
     6.88939882E-02    2          21        21   # BR(h -> g       g      )
     2.39024090E-03    2          22        22   # BR(h -> gam     gam    )
     1.64379062E-03    2          22        23   # BR(h -> Z       gam    )
     2.22324537E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81609848E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53165633E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60529510E-01    2           5        -5   # BR(H -> b       bb     )
     5.99386959E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11928711E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50675103E-04    2           3        -3   # BR(H -> s       sb     )
     7.03079229E-08    2           4        -4   # BR(H -> c       cb     )
     7.04631775E-03    2           6        -6   # BR(H -> t       tb     )
     7.41588302E-07    2          21        21   # BR(H -> g       g      )
     1.64559975E-09    2          22        22   # BR(H -> gam     gam    )
     1.81253776E-09    2          23        22   # BR(H -> Z       gam    )
     1.74077264E-06    2          24       -24   # BR(H -> W+      W-     )
     8.69783360E-07    2          23        23   # BR(H -> Z       Z      )
     6.92643182E-06    2          25        25   # BR(H -> h       h      )
    -5.76301250E-25    2          36        36   # BR(H -> A       A      )
     1.34117482E-20    2          23        36   # BR(H -> Z       A      )
     1.85209829E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57548353E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57548353E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.25096781E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.74144359E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.40881113E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.58507983E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.59588424E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.44107282E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.18877221E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.16618610E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.19802780E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.59906100E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.97223247E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.97223247E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.39437590E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53130525E+01   # A decays
#          BR         NDA      ID1       ID2
     3.60577948E-01    2           5        -5   # BR(A -> b       bb     )
     5.99427623E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11942922E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50732824E-04    2           3        -3   # BR(A -> s       sb     )
     7.07683382E-08    2           4        -4   # BR(A -> c       cb     )
     7.06041169E-03    2           6        -6   # BR(A -> t       tb     )
     1.45073589E-05    2          21        21   # BR(A -> g       g      )
     6.21850247E-08    2          22        22   # BR(A -> gam     gam    )
     1.59501260E-08    2          23        22   # BR(A -> Z       gam    )
     1.73721294E-06    2          23        25   # BR(A -> Z       h      )
     1.87371805E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57547624E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57547624E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.95338118E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.32149768E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.19399618E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.12221829E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.89866898E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.65134065E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.31869213E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.49586475E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.65319444E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.22657395E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.22657395E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54785677E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.79855345E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.97791457E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11364414E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.71107106E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19741941E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46347679E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.69073132E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.73395995E-06    2          24        25   # BR(H+ -> W+      h      )
     2.59933416E-14    2          24        36   # BR(H+ -> W+      A      )
     5.77280578E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.62365576E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.71123431E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57292326E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.08640775E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56192959E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.08410352E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.89336685E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.21770473E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
