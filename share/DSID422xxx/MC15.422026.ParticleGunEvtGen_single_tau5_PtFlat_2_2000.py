evgenConfig.description = "Single tau (5-prong) with flat eta-phi and flat pT in [2, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "tau"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-15, 15)
# flat pT
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[2000, 2000000], eta=[-5.5, 5.5])

# Use EvtGen to decay the tau (5-prong)
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt', 'tau5.dec' ]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
genSeq += EvtInclusiveDecay()
genSeq.EvtInclusiveDecay.blackList=[]
genSeq.EvtInclusiveDecay.OutputLevel = 3
genSeq.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
genSeq.EvtInclusiveDecay.decayFile = "inclusive.dec"
genSeq.EvtInclusiveDecay.userDecayFile   = "tau5.dec"

print "CHECK ON TAUS BLACKLISTED", genSeq.EvtInclusiveDecay.blackList
