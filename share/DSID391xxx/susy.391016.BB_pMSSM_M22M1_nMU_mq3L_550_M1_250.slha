#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12808430E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04016682E+01   # W+
        25     1.25894580E+02   # h
        35     3.00016881E+03   # H
        36     2.99999997E+03   # A
        37     3.00109831E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02616153E+03   # ~d_L
   2000001     3.02089125E+03   # ~d_R
   1000002     3.02524849E+03   # ~u_L
   2000002     3.02378465E+03   # ~u_R
   1000003     3.02616153E+03   # ~s_L
   2000003     3.02089125E+03   # ~s_R
   1000004     3.02524849E+03   # ~c_L
   2000004     3.02378465E+03   # ~c_R
   1000005     6.34145065E+02   # ~b_1
   2000005     3.02148655E+03   # ~b_2
   1000006     6.30603118E+02   # ~t_1
   2000006     3.02073676E+03   # ~t_2
   1000011     3.00703065E+03   # ~e_L
   2000011     3.00021839E+03   # ~e_R
   1000012     3.00564240E+03   # ~nu_eL
   1000013     3.00703065E+03   # ~mu_L
   2000013     3.00021839E+03   # ~mu_R
   1000014     3.00564240E+03   # ~nu_muL
   1000015     2.98625695E+03   # ~tau_1
   2000015     3.02208985E+03   # ~tau_2
   1000016     3.00602886E+03   # ~nu_tauL
   1000021     2.33245514E+03   # ~g
   1000022     2.52737035E+02   # ~chi_10
   1000023     5.29008012E+02   # ~chi_20
   1000025    -3.00481080E+03   # ~chi_30
   1000035     3.00497049E+03   # ~chi_40
   1000024     5.29168936E+02   # ~chi_1+
   1000037     3.00582924E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891112E-01   # N_11
  1  2     7.59891562E-05   # N_12
  1  3    -1.47484112E-02   # N_13
  1  4    -4.93229470E-04   # N_14
  2  1     3.15480071E-04   # N_21
  2  2     9.99645652E-01   # N_22
  2  3     2.64350546E-02   # N_23
  2  4     3.10787027E-03   # N_24
  3  1    -1.00783180E-02   # N_31
  3  2     1.64967116E-02   # N_32
  3  3    -7.06837848E-01   # N_33
  3  4     7.07111408E-01   # N_34
  4  1     1.07746304E-02   # N_41
  4  2    -2.08907450E-02   # N_42
  4  3     7.06727620E-01   # N_43
  4  4     7.07095152E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99301183E-01   # U_11
  1  2     3.73784062E-02   # U_12
  2  1    -3.73784062E-02   # U_21
  2  2     9.99301183E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990350E-01   # V_11
  1  2    -4.39316601E-03   # V_12
  2  1    -4.39316601E-03   # V_21
  2  2    -9.99990350E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98628289E-01   # cos(theta_t)
  1  2    -5.23597212E-02   # sin(theta_t)
  2  1     5.23597212E-02   # -sin(theta_t)
  2  2     9.98628289E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99725253E-01   # cos(theta_b)
  1  2    -2.34396782E-02   # sin(theta_b)
  2  1     2.34396782E-02   # -sin(theta_b)
  2  2     9.99725253E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06930719E-01   # cos(theta_tau)
  1  2     7.07282800E-01   # sin(theta_tau)
  2  1    -7.07282800E-01   # -sin(theta_tau)
  2  2    -7.06930719E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00130660E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28084305E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44385958E+02   # higgs               
         4     1.13431496E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28084305E+03  # The gauge couplings
     1     3.61623221E-01   # gprime(Q) DRbar
     2     6.37439580E-01   # g(Q) DRbar
     3     1.02987709E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28084305E+03  # The trilinear couplings
  1  1     1.53599769E-06   # A_u(Q) DRbar
  2  2     1.53602083E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28084305E+03  # The trilinear couplings
  1  1     5.48600057E-07   # A_d(Q) DRbar
  2  2     5.48664449E-07   # A_s(Q) DRbar
  3  3     1.12437275E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28084305E+03  # The trilinear couplings
  1  1     2.47920457E-07   # A_e(Q) DRbar
  2  2     2.47932641E-07   # A_mu(Q) DRbar
  3  3     2.51365074E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28084305E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56656088E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28084305E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.93944161E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28084305E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01911062E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28084305E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.65449642E+04   # M^2_Hd              
        22    -9.12536181E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41037329E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.02891070E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48038747E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48038747E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51961253E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51961253E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.26447412E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.58193588E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.04180641E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21004366E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.38389885E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.82889658E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.68930931E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.73237463E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.88899668E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71960343E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63276596E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21135260E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.22395328E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.13904610E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.86095390E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.59862294E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.92507322E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.80114709E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.07694063E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.02305309E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -8.87324006E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.29418394E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.60340066E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.07896256E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.57379662E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88270500E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97010623E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59236106E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.76124452E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.27615876E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.18641954E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.27382473E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.16151812E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19731261E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56429640E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48696016E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.00724748E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.38100779E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43570339E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88787088E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.95794349E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59133963E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.34289068E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.05436250E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18074723E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.00742992E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.16833235E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68142823E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45425682E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.23365699E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.21473728E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.96629591E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55457426E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88270500E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97010623E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59236106E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.76124452E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.27615876E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.18641954E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.27382473E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.16151812E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19731261E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56429640E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48696016E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.00724748E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.38100779E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43570339E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88787088E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.95794349E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59133963E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.34289068E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.05436250E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18074723E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.00742992E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.16833235E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68142823E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45425682E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.23365699E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.21473728E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.96629591E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55457426E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80596480E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01332784E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99772698E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.08233863E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.30853381E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98894517E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.69529307E-10    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53866949E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.47962478E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.80596480E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01332784E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99772698E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.08233863E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.30853381E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98894517E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.69529307E-10    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53866949E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.47962478E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.69832664E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56455868E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14688008E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28856124E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64455059E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64620361E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11963424E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.07399832E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.72708030E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23396753E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.66083259E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80612131E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01226249E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99389814E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.82156951E-11    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.65131390E-11    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99383936E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.80612131E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01226249E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99389814E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.82156951E-11    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.65131390E-11    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99383936E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80699812E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01216318E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99364267E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.03391571E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.15349782E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99419414E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     4.62329953E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.70889564E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.42631028E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.64547006E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.73226100E-14    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.73226100E-14    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     8.22025086E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.78135095E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.86248481E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.63205345E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.00622520E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.61867294E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.03934535E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.96065465E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.73337532E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01696463E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.54941665E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.61603046E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.61603046E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.15639168E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.24462216E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.35005083E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.35005083E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.02004366E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.02004366E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     7.61054168E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     7.61054168E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     7.61054168E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     7.61054168E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     9.63059265E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     9.63059265E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.63536655E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.99291438E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.23929569E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.67462887E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.67462887E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.06140461E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.78503455E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.32413500E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.32413500E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.09142761E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.09142761E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.40769304E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     9.40769304E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.40769304E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     9.40769304E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.03909820E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.03909820E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.72833230E-03   # h decays
#          BR         NDA      ID1       ID2
     5.48146032E-01    2           5        -5   # BR(h -> b       bb     )
     7.00433998E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.47951466E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.25550351E-04    2           3        -3   # BR(h -> s       sb     )
     2.28376527E-02    2           4        -4   # BR(h -> c       cb     )
     7.51137598E-02    2          21        21   # BR(h -> g       g      )
     2.60103622E-03    2          22        22   # BR(h -> gam     gam    )
     1.81419618E-03    2          22        23   # BR(h -> Z       gam    )
     2.47306869E-01    2          24       -24   # BR(h -> W+      W-     )
     3.13635527E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.03843946E+01   # H decays
#          BR         NDA      ID1       ID2
     9.07588593E-01    2           5        -5   # BR(H -> b       bb     )
     6.15791538E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.17729173E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.67371094E-04    2           3        -3   # BR(H -> s       sb     )
     7.49387054E-08    2           4        -4   # BR(H -> c       cb     )
     7.47562052E-03    2           6        -6   # BR(H -> t       tb     )
     4.92610319E-06    2          21        21   # BR(H -> g       g      )
     4.59487884E-08    2          22        22   # BR(H -> gam     gam    )
     3.06548078E-09    2          23        22   # BR(H -> Z       gam    )
     6.53045683E-07    2          24       -24   # BR(H -> W+      W-     )
     3.26120287E-07    2          23        23   # BR(H -> Z       Z      )
     4.98191521E-06    2          25        25   # BR(H -> h       h      )
    -4.01753996E-25    2          36        36   # BR(H -> A       A      )
     3.71697321E-18    2          23        36   # BR(H -> Z       A      )
     7.28246661E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.76081730E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.63925515E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.36762964E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.14887240E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.25359465E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.95241261E+01   # A decays
#          BR         NDA      ID1       ID2
     9.27339928E-01    2           5        -5   # BR(A -> b       bb     )
     6.29163560E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.22456889E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.73223253E-04    2           3        -3   # BR(A -> s       sb     )
     7.70997292E-08    2           4        -4   # BR(A -> c       cb     )
     7.68695983E-03    2           6        -6   # BR(A -> t       tb     )
     2.26373608E-05    2          21        21   # BR(A -> g       g      )
     6.04968378E-08    2          22        22   # BR(A -> gam     gam    )
     2.23019719E-08    2          23        22   # BR(A -> Z       gam    )
     6.64639015E-07    2          23        25   # BR(A -> Z       h      )
     8.30062145E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.92835432E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.14760205E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.53507936E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.31448129E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47835249E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.76575547E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.03863050E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.46144438E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19805064E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46477542E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.29538292E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.09913658E-07    2          24        25   # BR(H+ -> W+      h      )
     4.87443038E-14    2          24        36   # BR(H+ -> W+      A      )
     4.53976175E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.22830753E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.03994324E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
