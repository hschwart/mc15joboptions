from MadGraphControl.MadGraphUtils import *

nevents=10*(runArgs.maxEvents)
mode=0

dosml=False
dodil=False

do4FS=False
do5FS=False

title_str = str(runArgs.jobConfig[0])  # get the main JO name string
for s in title_str.split("_"):
    ss=s.replace(".py","")
    if ss=='sml':
        dosml=True
    if ss=='dil':
        dodil=True
if (not dosml and not dodil) or (dosml and dodil):
    raise RuntimeError("No decay mode was identified, check jobOption name %s"%title_str)

for s in title_str.split("_"):
    ss=s.replace("FS","")
    if ss.isdigit() and int(ss)==4:
        do4FS=True
    if ss.isdigit() and int(ss)==5:
        do5FS=True
if (not do4FS and not do5FS) or (do4FS and do5FS):
    raise RuntimeError("Flavour scheme does not recognised, check jobOption name %s"%title_str)


fcard = open('proc_card_mg5.dat','w')
if do4FS:
    fcard.write("""
    import model ehsm
    define p = g u c d s u~ c~ d~ s~
    generate p p > Ws- > t t~ b b~
    output -f""")
    fcard.close()
if do5FS:
    fcard.write("""
    import model ehsm
    define p = g u c d s u~ c~ d~ s~ b b~
    generate p p > Ws- > t t~ b
    output -f""")
    fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
           'cut_decays' :'F',
           'pdlabel'    :"'lhapdf'",
           'lhaid'      :'247000',
           'use_syst'   : 'False'
	 }

process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#------------------------------------------------------------------------------------
# JOB OPTION NAME MUST CONTAIN THE MASS WE WANT TO SIMULATE IN FORMAT LIKE: *_M700_*
#------------------------------------------------------------------------------------
mWs=0 
for s in title_str.split("_"):
    ss=s.replace("M","")
    if ss.isdigit():
        mWs = int(ss)
if mWs==0:
        raise RuntimeError("Wstar mass does not set, check joOption name %s"%title_str)


masses = {'4000023':str(mWs)+'  #  MWs'}

build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card.dat',masses=masses)    
print_cards()

runName='run_01'

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir,run_name=runName)
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  


#### Shower 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = 'MadGraph_Wstar'
evgenConfig.contact = ['Mihail Chizhov < Mihail.Chizhov@cern.ch>']
evgenConfig.keywords+=['top','bottom']
#evgenConfig.inputfilecheck = runName
#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
runArgs.inputGeneratorFile=outputDS


# JOB OPTION NAME MUST CONTAIN THE DECAY MODE WE WANT TO SIMULATE IN FORMAT LIKE: *_sml.py or *_dil.py

if dosml:
#    evgenConfig.keywords+=['semileptonic']
    include('MC15JobOptions/TTbarWToLeptonFilter.py')                # lep filter   
    filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 
    filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

if dodil:
#    evgenConfig.keywords+=['dileptonic']
    include('MC15JobOptions/TTbarWToLeptonFilter.py')                # lep filter   
    filtSeq.TTbarWToLeptonFilter.NumLeptons = 2 
    filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
    evgenConfig.minevents=2000