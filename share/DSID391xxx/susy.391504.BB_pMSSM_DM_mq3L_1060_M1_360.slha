#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13999111E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04173567E+01   # W+
        25     1.25655384E+02   # h
        35     4.00000890E+03   # H
        36     3.99999718E+03   # A
        37     4.00106810E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02852875E+03   # ~d_L
   2000001     4.02463187E+03   # ~d_R
   1000002     4.02787749E+03   # ~u_L
   2000002     4.02532241E+03   # ~u_R
   1000003     4.02852875E+03   # ~s_L
   2000003     4.02463187E+03   # ~s_R
   1000004     4.02787749E+03   # ~c_L
   2000004     4.02532241E+03   # ~c_R
   1000005     1.12083904E+03   # ~b_1
   2000005     4.02682853E+03   # ~b_2
   1000006     1.09796654E+03   # ~t_1
   2000006     1.88154705E+03   # ~t_2
   1000011     4.00508154E+03   # ~e_L
   2000011     4.00350317E+03   # ~e_R
   1000012     4.00396856E+03   # ~nu_eL
   1000013     4.00508154E+03   # ~mu_L
   2000013     4.00350317E+03   # ~mu_R
   1000014     4.00396856E+03   # ~nu_muL
   1000015     4.00435689E+03   # ~tau_1
   2000015     4.00781880E+03   # ~tau_2
   1000016     4.00516495E+03   # ~nu_tauL
   1000021     1.98339332E+03   # ~g
   1000022     3.47835919E+02   # ~chi_10
   1000023    -4.01641501E+02   # ~chi_20
   1000025     4.15213603E+02   # ~chi_30
   1000035     2.05153012E+03   # ~chi_40
   1000024     3.99255989E+02   # ~chi_1+
   1000037     2.05169409E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40420897E-01   # N_11
  1  2    -1.55942848E-02   # N_12
  1  3    -4.09457729E-01   # N_13
  1  4    -3.54674361E-01   # N_14
  2  1    -4.35070691E-02   # N_21
  2  2     2.40390989E-02   # N_22
  2  3    -7.03938179E-01   # N_23
  2  4     7.08519793E-01   # N_24
  3  1     5.40183981E-01   # N_31
  3  2     2.81533225E-02   # N_32
  3  3     5.80328958E-01   # N_33
  3  4     6.08791391E-01   # N_34
  4  1    -1.05719357E-03   # N_41
  4  2     9.99192839E-01   # N_42
  4  3    -5.80603561E-03   # N_43
  4  4    -3.97346430E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.21676678E-03   # U_11
  1  2     9.99966242E-01   # U_12
  2  1    -9.99966242E-01   # U_21
  2  2     8.21676678E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62037453E-02   # V_11
  1  2    -9.98419320E-01   # V_12
  2  1    -9.98419320E-01   # V_21
  2  2    -5.62037453E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86802582E-01   # cos(theta_t)
  1  2    -1.61927960E-01   # sin(theta_t)
  2  1     1.61927960E-01   # -sin(theta_t)
  2  2     9.86802582E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999176E-01   # cos(theta_b)
  1  2    -1.28374426E-03   # sin(theta_b)
  2  1     1.28374426E-03   # -sin(theta_b)
  2  2     9.99999176E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06046342E-01   # cos(theta_tau)
  1  2     7.08165632E-01   # sin(theta_tau)
  2  1    -7.08165632E-01   # -sin(theta_tau)
  2  2    -7.06046342E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00266386E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39991114E+03  # DRbar Higgs Parameters
         1    -3.89990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43497703E+02   # higgs               
         4     1.60696059E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39991114E+03  # The gauge couplings
     1     3.62160753E-01   # gprime(Q) DRbar
     2     6.36053603E-01   # g(Q) DRbar
     3     1.02835793E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39991114E+03  # The trilinear couplings
  1  1     1.59796616E-06   # A_u(Q) DRbar
  2  2     1.59798165E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39991114E+03  # The trilinear couplings
  1  1     5.89329121E-07   # A_d(Q) DRbar
  2  2     5.89383464E-07   # A_s(Q) DRbar
  3  3     1.05262191E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39991114E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.29266877E-07   # A_mu(Q) DRbar
  3  3     1.30595682E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39991114E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65490020E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39991114E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84631986E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39991114E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03308389E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39991114E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57438348E+07   # M^2_Hd              
        22    -9.82084945E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40356080E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.02594619E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37590890E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37590890E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62409110E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62409110E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.28924500E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09435797E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.24805359E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.55844567E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09914276E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05710083E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.98820966E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19053923E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.04085252E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.31998481E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.56065493E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.18092172E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.24499309E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.33370731E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.65872680E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.94600661E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.40589138E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89893752E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65747368E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.56189395E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82452993E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62634615E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.21823180E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66631521E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.41546103E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12462033E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.60024126E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.16891595E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.61003775E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.56097047E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77781754E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.79370061E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14659200E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46983728E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83600934E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.44634859E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65985499E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51211718E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59963169E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.91569552E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.04431958E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.60774121E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.46394083E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44661166E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77801764E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.65554432E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.99047770E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.76102933E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84122801E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16413177E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.69247995E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51429727E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53264469E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.02152595E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.72441453E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.19426349E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.03462279E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85563224E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77781754E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.79370061E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14659200E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46983728E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83600934E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.44634859E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65985499E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51211718E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59963169E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.91569552E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.04431958E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.60774121E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.46394083E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44661166E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77801764E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.65554432E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.99047770E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.76102933E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84122801E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16413177E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.69247995E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51429727E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53264469E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.02152595E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.72441453E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.19426349E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.03462279E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85563224E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14619981E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08135442E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.69934370E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65066536E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77940314E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.78062496E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57349515E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05399102E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07647043E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88685229E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.90465487E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.18206503E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14619981E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08135442E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.69934370E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65066536E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77940314E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.78062496E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57349515E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05399102E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07647043E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88685229E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.90465487E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.18206503E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08909009E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55494869E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44493723E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23264321E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40310542E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51171741E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81363722E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08303998E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.61205987E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05083367E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98278250E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43226686E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95740104E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87206729E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14725436E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23120330E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19537961E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91175556E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78330006E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17049609E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55066233E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14725436E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23120330E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19537961E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91175556E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78330006E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17049609E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55066233E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47214902E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11634120E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08386291E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.54683173E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52468932E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58510639E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03493704E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.37805451E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33545455E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33545455E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183305E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183305E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10542480E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.49700960E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.36348951E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.28726245E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.16667112E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.80819280E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.93523088E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.58837586E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.63083797E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.72288353E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.33980429E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.48126874E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17913641E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52973054E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17913641E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52973054E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42232025E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50902641E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50902641E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47938291E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01569654E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01569654E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01569641E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.25128247E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.25128247E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.25128247E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.25128247E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.50428148E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.50428148E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.50428148E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.50428148E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.99883458E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.99883458E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.73470079E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.26277114E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.53124713E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.40282715E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.26500221E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.40282715E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.26500221E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.93616941E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.86295574E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.86295574E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.86200319E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.80614986E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.80614986E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.80614200E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.39972955E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.30282196E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.39972955E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.30282196E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.04510810E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.90471651E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.90471651E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.72492127E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.80764740E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.80764740E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.80764743E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.87983410E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.87983410E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.87983410E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.87983410E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.62659461E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.62659461E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.62659461E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.62659461E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.53066581E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.53066581E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.49528335E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.75640171E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.67537639E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.35962270E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.58389745E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.58389745E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.19631509E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.10279068E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.40032090E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.63284617E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.63284617E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.63326079E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.63326079E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.03717765E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90583853E-01    2           5        -5   # BR(h -> b       bb     )
     6.45968811E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28671997E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84862101E-04    2           3        -3   # BR(h -> s       sb     )
     2.10582720E-02    2           4        -4   # BR(h -> c       cb     )
     6.88423260E-02    2          21        21   # BR(h -> g       g      )
     2.38553945E-03    2          22        22   # BR(h -> gam     gam    )
     1.64150965E-03    2          22        23   # BR(h -> Z       gam    )
     2.22053983E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81241009E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50668787E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57665870E-01    2           5        -5   # BR(H -> b       bb     )
     6.02104550E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12889585E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51811659E-04    2           3        -3   # BR(H -> s       sb     )
     7.06279680E-08    2           4        -4   # BR(H -> c       cb     )
     7.07839291E-03    2           6        -6   # BR(H -> t       tb     )
     5.84296000E-07    2          21        21   # BR(H -> g       g      )
     3.06560745E-09    2          22        22   # BR(H -> gam     gam    )
     1.82011198E-09    2          23        22   # BR(H -> Z       gam    )
     1.77191468E-06    2          24       -24   # BR(H -> W+      W-     )
     8.85343547E-07    2          23        23   # BR(H -> Z       Z      )
     7.03274168E-06    2          25        25   # BR(H -> h       h      )
    -2.37240222E-24    2          36        36   # BR(H -> A       A      )
     4.37968953E-20    2          23        36   # BR(H -> Z       A      )
     1.85466585E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58527206E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58527206E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.31674655E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.78019488E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.45901065E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.54941630E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.66567898E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.55613107E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.24189036E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.21083964E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.13918083E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.03469542E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.00859797E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.22480966E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.22480966E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.25970070E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50606177E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57732187E-01    2           5        -5   # BR(A -> b       bb     )
     6.02175839E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12914624E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51882364E-04    2           3        -3   # BR(A -> s       sb     )
     7.10927916E-08    2           4        -4   # BR(A -> c       cb     )
     7.09278174E-03    2           6        -6   # BR(A -> t       tb     )
     1.45738700E-05    2          21        21   # BR(A -> g       g      )
     6.23097826E-08    2          22        22   # BR(A -> gam     gam    )
     1.60210458E-08    2          23        22   # BR(A -> Z       gam    )
     1.76838136E-06    2          23        25   # BR(A -> Z       h      )
     1.87624917E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58534893E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58534893E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.00878994E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.36940049E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23589408E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.08233212E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.11435851E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.77018787E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.38007409E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.54776127E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.58022016E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.81311481E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.81311481E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51865281E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.74541599E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.00962802E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12485724E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.67706311E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20376986E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47654170E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.65791173E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.76638225E-06    2          24        25   # BR(H+ -> W+      h      )
     3.35870101E-14    2          24        36   # BR(H+ -> W+      A      )
     5.69958576E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.56378310E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.83145798E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58393534E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.32404040E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57276622E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.07173571E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.35376289E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.11048018E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
