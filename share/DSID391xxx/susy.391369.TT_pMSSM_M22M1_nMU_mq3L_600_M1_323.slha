#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13385346E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.22990000E+02   # M_1(MX)             
         2     6.45990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04033228E+01   # W+
        25     1.25677018E+02   # h
        35     3.00015357E+03   # H
        36     2.99999992E+03   # A
        37     3.00109555E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02860603E+03   # ~d_L
   2000001     3.02345939E+03   # ~d_R
   1000002     3.02769568E+03   # ~u_L
   2000002     3.02608623E+03   # ~u_R
   1000003     3.02860603E+03   # ~s_L
   2000003     3.02345939E+03   # ~s_R
   1000004     3.02769568E+03   # ~c_L
   2000004     3.02608623E+03   # ~c_R
   1000005     6.90889344E+02   # ~b_1
   2000005     3.02354825E+03   # ~b_2
   1000006     6.87691038E+02   # ~t_1
   2000006     3.02146863E+03   # ~t_2
   1000011     3.00682007E+03   # ~e_L
   2000011     3.00047389E+03   # ~e_R
   1000012     3.00543404E+03   # ~nu_eL
   1000013     3.00682007E+03   # ~mu_L
   2000013     3.00047389E+03   # ~mu_R
   1000014     3.00543404E+03   # ~nu_muL
   1000015     2.98622418E+03   # ~tau_1
   2000015     3.02189543E+03   # ~tau_2
   1000016     3.00573086E+03   # ~nu_tauL
   1000021     2.33558806E+03   # ~g
   1000022     3.26308981E+02   # ~chi_10
   1000023     6.78348941E+02   # ~chi_20
   1000025    -3.00320650E+03   # ~chi_30
   1000035     3.00349865E+03   # ~chi_40
   1000024     6.78507712E+02   # ~chi_1+
   1000037     3.00430296E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890055E-01   # N_11
  1  2    -2.83962369E-05   # N_12
  1  3    -1.48034069E-02   # N_13
  1  4    -8.58207876E-04   # N_14
  2  1     4.29772998E-04   # N_21
  2  2     9.99629283E-01   # N_22
  2  3     2.68512934E-02   # N_23
  2  4     4.48541162E-03   # N_24
  3  1    -9.85760327E-03   # N_31
  3  2     1.58182920E-02   # N_32
  3  3    -7.06852212E-01   # N_33
  3  4     7.07115662E-01   # N_34
  4  1     1.10689265E-02   # N_41
  4  2    -2.21602440E-02   # N_42
  4  3     7.06696411E-01   # N_43
  4  4     7.07083153E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99278912E-01   # U_11
  1  2     3.79691363E-02   # U_12
  2  1    -3.79691363E-02   # U_21
  2  2     9.99278912E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99979894E-01   # V_11
  1  2    -6.34131057E-03   # V_12
  2  1    -6.34131057E-03   # V_21
  2  2    -9.99979894E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98614385E-01   # cos(theta_t)
  1  2    -5.26242346E-02   # sin(theta_t)
  2  1     5.26242346E-02   # -sin(theta_t)
  2  2     9.98614385E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99729075E-01   # cos(theta_b)
  1  2    -2.32760950E-02   # sin(theta_b)
  2  1     2.32760950E-02   # -sin(theta_b)
  2  2     9.99729075E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06942466E-01   # cos(theta_tau)
  1  2     7.07271058E-01   # sin(theta_tau)
  2  1    -7.07271058E-01   # -sin(theta_tau)
  2  2    -7.06942466E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00116757E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33853465E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44303708E+02   # higgs               
         4     1.12022041E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33853465E+03  # The gauge couplings
     1     3.61769488E-01   # gprime(Q) DRbar
     2     6.36930905E-01   # g(Q) DRbar
     3     1.02888709E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33853465E+03  # The trilinear couplings
  1  1     1.69236246E-06   # A_u(Q) DRbar
  2  2     1.69238731E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33853465E+03  # The trilinear couplings
  1  1     6.08354178E-07   # A_d(Q) DRbar
  2  2     6.08423848E-07   # A_s(Q) DRbar
  3  3     1.23123867E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33853465E+03  # The trilinear couplings
  1  1     2.65243652E-07   # A_e(Q) DRbar
  2  2     2.65256572E-07   # A_mu(Q) DRbar
  3  3     2.68874881E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33853465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55616900E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33853465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.88735025E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33853465E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01370624E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33853465E+03  # The soft SUSY breaking masses at the scale Q
         1     3.22990000E+02   # M_1(Q)              
         2     6.45990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -5.26915658E+04   # M^2_Hd              
        22    -9.10765046E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40805067E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.82700288E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48027901E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48027901E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51972099E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51972099E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.98837074E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.19552279E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.04477210E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19624126E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.40428125E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.04021882E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.11538402E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.02488527E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.90671161E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71642605E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62792237E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19885375E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.79432845E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.42020781E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.79792188E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.52300393E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.94369607E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     7.67644098E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.58531362E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.50100032E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -2.00858688E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.36514258E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.45678927E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.05593995E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.53855974E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.74588668E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.04133650E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55997477E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.02700875E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.07499949E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.12147899E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.88120061E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.25813252E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18600346E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55730926E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.66292017E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.15325743E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.10728661E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44269038E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.75114072E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.04220760E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55883188E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.76963981E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.25499401E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.11583498E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.49721307E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.26490906E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67580031E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42953008E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.57320861E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.33234624E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14384371E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55704690E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.74588668E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.04133650E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55997477E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.02700875E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.07499949E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.12147899E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.88120061E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.25813252E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18600346E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55730926E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.66292017E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.15325743E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.10728661E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44269038E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.75114072E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.04220760E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55883188E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.76963981E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.25499401E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.11583498E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.49721307E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.26490906E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67580031E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42953008E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.57320861E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.33234624E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14384371E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55704690E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.65761086E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04482785E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98752487E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.99908745E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.07509413E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.96764724E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.67899578E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52540200E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999830E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.70348107E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.65761086E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04482785E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98752487E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.99908745E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.07509413E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.96764724E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.67899578E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52540200E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999830E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.70348107E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.61591354E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64404507E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.12057896E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.23537597E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.56516049E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.72644616E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.09308284E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.41033408E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.86952767E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.18023798E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     8.02180817E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.65766423E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04451691E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98289363E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.33958262E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.44807546E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97258945E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.51023655E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.65766423E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04451691E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98289363E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.33958262E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.44807546E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97258945E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.51023655E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.65842466E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.04440785E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98263139E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.28714041E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.91465226E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97296074E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.59616588E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.52208199E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.39889163E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.66826004E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.08571077E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.77674267E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.88272051E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.62494012E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.04400914E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.52172648E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.01154480E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.98845520E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.54329336E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07667679E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.81466908E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.60936358E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.60936358E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.80773048E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.97382114E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.36387292E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.36387292E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.87885449E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.87885449E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.63434871E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.63434871E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.63434871E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.63434871E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.27326541E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.27326541E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.44871772E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.63003211E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.96724513E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.66766585E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.66766585E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12726416E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.09714189E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.33571270E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.33571270E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.94781139E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.94781139E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.10968998E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.10968998E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     4.10968998E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.10968998E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     6.85416550E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.85416550E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.69113605E-03   # h decays
#          BR         NDA      ID1       ID2
     5.50832946E-01    2           5        -5   # BR(h -> b       bb     )
     7.06227599E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.50003415E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30078309E-04    2           3        -3   # BR(h -> s       sb     )
     2.30356979E-02    2           4        -4   # BR(h -> c       cb     )
     7.55441128E-02    2          21        21   # BR(h -> g       g      )
     2.60806878E-03    2          22        22   # BR(h -> gam     gam    )
     1.79791891E-03    2          22        23   # BR(h -> Z       gam    )
     2.43935536E-01    2          24       -24   # BR(h -> W+      W-     )
     3.08428779E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.93643944E+01   # H decays
#          BR         NDA      ID1       ID2
     9.05566085E-01    2           5        -5   # BR(H -> b       bb     )
     6.31744652E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.23369812E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.74297976E-04    2           3        -3   # BR(H -> s       sb     )
     7.68758885E-08    2           4        -4   # BR(H -> c       cb     )
     7.66886621E-03    2           6        -6   # BR(H -> t       tb     )
     5.78601178E-06    2          21        21   # BR(H -> g       g      )
     5.56568813E-08    2          22        22   # BR(H -> gam     gam    )
     3.14701842E-09    2          23        22   # BR(H -> Z       gam    )
     6.36315293E-07    2          24       -24   # BR(H -> W+      W-     )
     3.17765457E-07    2          23        23   # BR(H -> Z       Z      )
     5.10140568E-06    2          25        25   # BR(H -> h       h      )
     8.00904846E-26    2          36        36   # BR(H -> A       A      )
     2.45210658E-18    2          23        36   # BR(H -> Z       A      )
     6.70821755E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.78420067E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.35163439E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.30421386E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.18014687E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.22080506E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.85177106E+01   # A decays
#          BR         NDA      ID1       ID2
     9.25473393E-01    2           5        -5   # BR(A -> b       bb     )
     6.45602739E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.28269382E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.80362201E-04    2           3        -3   # BR(A -> s       sb     )
     7.91142392E-08    2           4        -4   # BR(A -> c       cb     )
     7.88780953E-03    2           6        -6   # BR(A -> t       tb     )
     2.32288444E-05    2          21        21   # BR(A -> g       g      )
     6.16618256E-08    2          22        22   # BR(A -> gam     gam    )
     2.28912170E-08    2          23        22   # BR(A -> Z       gam    )
     6.47770383E-07    2          23        25   # BR(A -> Z       h      )
     8.33544406E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.01245599E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.16402025E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.55780323E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.21740940E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47577685E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.89846013E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08555163E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.44496029E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22562513E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52150501E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.28090014E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.92629686E-07    2          24        25   # BR(H+ -> W+      h      )
     4.92539096E-14    2          24        36   # BR(H+ -> W+      A      )
     4.48692010E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.57809327E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.05179160E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
