#--------------------------------------------------------------                                                                                                       # Configuration for EvgenJobTransforms                                                                                                                                #--------------------------------------------------------------                                                                                                        
evgenConfig.generators  += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description  = 'MG5_aMC@NLO+Herwigpp ttbar allhadronic, UE-EE-5 tune, with EvtGen'
evgenConfig.keywords    += [ 'SM', 'top', 'allHadronic']
evgenConfig.contact      = [ 'maria.moreno.llacer@cern.ch' ]

#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")


include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0 #(all had)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

