include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


#--------------------------------------------------------------
# Higgs at Pythia 8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [  #'SLHA:useDecayTable = off',
                              '25:onMode = off', #decay of higgs
                              '25:addChannel = 1 0.002 100 100443 22',
                              '100443:onMode = off',
                              #'100443:oneChannel 1 1 100 13 -13'
                              '100443:onIfMatch = 13 -13'
                           ]

evgenConfig.process     = 'ttH H->Psi2sGamma'
evgenConfig.description = 'aMcAtNloPythia8 ttH allhad, H to Psi2s'
evgenConfig.keywords    = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs', 'photon' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact = ["Will Heidorn <william.dale.heidorn@cern.ch>"]

