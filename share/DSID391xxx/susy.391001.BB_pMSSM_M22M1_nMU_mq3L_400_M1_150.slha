#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10889267E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03991771E+01   # W+
        25     1.26601760E+02   # h
        35     3.00021366E+03   # H
        36     3.00000014E+03   # A
        37     3.00110767E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01685441E+03   # ~d_L
   2000001     3.01129076E+03   # ~d_R
   1000002     3.01593465E+03   # ~u_L
   2000002     3.01525681E+03   # ~u_R
   1000003     3.01685441E+03   # ~s_L
   2000003     3.01129076E+03   # ~s_R
   1000004     3.01593465E+03   # ~c_L
   2000004     3.01525681E+03   # ~c_R
   1000005     4.55178795E+02   # ~b_1
   2000005     3.01402139E+03   # ~b_2
   1000006     4.49871355E+02   # ~t_1
   2000006     3.01819042E+03   # ~t_2
   1000011     3.00766448E+03   # ~e_L
   2000011     2.99915498E+03   # ~e_R
   1000012     3.00627226E+03   # ~nu_eL
   1000013     3.00766448E+03   # ~mu_L
   2000013     2.99915498E+03   # ~mu_R
   1000014     3.00627226E+03   # ~nu_muL
   1000015     2.98637804E+03   # ~tau_1
   2000015     3.02261803E+03   # ~tau_2
   1000016     3.00701429E+03   # ~nu_tauL
   1000021     2.32055629E+03   # ~g
   1000022     1.52158600E+02   # ~chi_10
   1000023     3.21493488E+02   # ~chi_20
   1000025    -3.01055495E+03   # ~chi_30
   1000035     3.01057478E+03   # ~chi_40
   1000024     3.21653312E+02   # ~chi_1+
   1000037     3.01149615E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891906E-01   # N_11
  1  2     3.82471092E-04   # N_12
  1  3    -1.46979781E-02   # N_13
  1  4     3.31787395E-07   # N_14
  2  1     1.67194379E-06   # N_21
  2  2     9.99657774E-01   # N_22
  2  3     2.61268860E-02   # N_23
  2  4     1.31202748E-03   # N_24
  3  1     1.03962952E-02   # N_31
  3  2    -1.93999162E-02   # N_32
  3  3     7.06762931E-01   # N_33
  3  4     7.07107997E-01   # N_34
  4  1    -1.03968211E-02   # N_41
  4  2     1.75451577E-02   # N_42
  4  3    -7.06815049E-01   # N_43
  4  4     7.07104348E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99317621E-01   # U_11
  1  2     3.69363326E-02   # U_12
  2  1    -3.69363326E-02   # U_21
  2  2     9.99317621E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998282E-01   # V_11
  1  2    -1.85359408E-03   # V_12
  2  1    -1.85359408E-03   # V_21
  2  2    -9.99998282E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98657301E-01   # cos(theta_t)
  1  2    -5.18034281E-02   # sin(theta_t)
  2  1     5.18034281E-02   # -sin(theta_t)
  2  2     9.98657301E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99716257E-01   # cos(theta_b)
  1  2    -2.38202748E-02   # sin(theta_b)
  2  1     2.38202748E-02   # -sin(theta_b)
  2  2     9.99716257E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06901873E-01   # cos(theta_tau)
  1  2     7.07311630E-01   # sin(theta_tau)
  2  1    -7.07311630E-01   # -sin(theta_tau)
  2  2    -7.06901873E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00194406E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.08892665E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44729706E+02   # higgs               
         4     1.20249295E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.08892665E+03  # The gauge couplings
     1     3.61081777E-01   # gprime(Q) DRbar
     2     6.38348418E-01   # g(Q) DRbar
     3     1.03349231E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.08892665E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.06321627E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.08892665E+03  # The trilinear couplings
  1  1     3.79268029E-07   # A_d(Q) DRbar
  2  2     3.79313773E-07   # A_s(Q) DRbar
  3  3     7.90297318E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.08892665E+03  # The trilinear couplings
  1  1     1.82044833E-07   # A_e(Q) DRbar
  2  2     1.82053858E-07   # A_mu(Q) DRbar
  3  3     1.84618745E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.08892665E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.60152029E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.08892665E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.06339470E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.08892665E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02660166E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.08892665E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -7.66322798E+04   # M^2_Hd              
        22    -9.18888923E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41450762E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.51865284E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48055015E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48055015E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51944985E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51944985E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.64526194E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.37051734E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.66294827E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.25006127E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.23252563E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.46392426E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.95627366E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.74941281E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.85381514E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72833509E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64556559E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.24155393E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.52289019E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.00938728E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.99061272E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.79712922E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.83158625E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     7.43068647E-09    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.05543427E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.13556758E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.91878091E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.13275756E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.65663913E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02661184E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.87709841E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61886088E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.08530103E-10    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.41046181E-10    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.23985557E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.25447114E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.08251255E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.22373463E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55765241E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.28417837E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.09025913E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.07327508E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44234758E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03172062E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.82720981E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61821202E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.64928349E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.36686538E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.23415389E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.18992415E-09    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.08936189E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69816608E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44147287E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.22153085E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.58619080E-12    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.50370043E-12    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55585271E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02661184E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.87709841E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61886088E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.08530103E-10    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.41046181E-10    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.23985557E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.25447114E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.08251255E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.22373463E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55765241E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.28417837E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.09025913E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.07327508E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44234758E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03172062E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.82720981E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61821202E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.64928349E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.36686538E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.23415389E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.18992415E-09    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.08936189E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69816608E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44147287E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.22153085E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.58619080E-12    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.50370043E-12    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55585271E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.95864662E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81434508E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00760265E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01096285E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54752522E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.74623260E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.95864662E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81434508E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00760265E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01096285E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54752522E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.74623260E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78086084E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48141165E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17408360E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34450475E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72395964E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56218601E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14712160E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.52523968E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.59784110E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.29060072E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.04323505E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.95892343E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.78257247E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00592451E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.01581824E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.95892343E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.78257247E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00592451E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.01581824E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96027542E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.78167139E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00567433E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.01615853E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.38295801E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.01503483E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.49437454E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61075660E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.96450660E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.96450660E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     9.87759421E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.06106856E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.06106856E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.30485197E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.71148449E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.81100827E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.57007664E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.85597869E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.27678274E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.25829934E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.74170066E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.01168460E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.28971947E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.57702375E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.59037218E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.59037218E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.76882669E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.13086010E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.37922508E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37922508E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.41035415E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.41035415E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.06049709E-11    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     4.06049709E-11    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.76681301E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.76681301E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.06049709E-11    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     4.06049709E-11    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.76681301E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.76681301E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.53429184E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.53429184E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     3.11561190E-10    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     3.11561190E-10    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     3.11561190E-10    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     3.11561190E-10    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     2.13002960E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     2.13002960E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.02383556E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.18530613E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.11801171E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.52343924E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.52343924E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14847113E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.95297577E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25835834E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25835834E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.32385074E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.32385074E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.02782862E-11    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.02782862E-11    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.72705707E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.72705707E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.02782862E-11    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.02782862E-11    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.72705707E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.72705707E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.31811085E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.31811085E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.66803612E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.66803612E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.66803612E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.66803612E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.82756386E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.82756386E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.84267066E-03   # h decays
#          BR         NDA      ID1       ID2
     5.38299164E-01    2           5        -5   # BR(h -> b       bb     )
     6.83593263E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.41986684E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.12347666E-04    2           3        -3   # BR(h -> s       sb     )
     2.22582690E-02    2           4        -4   # BR(h -> c       cb     )
     7.37352610E-02    2          21        21   # BR(h -> g       g      )
     2.58508337E-03    2          22        22   # BR(h -> gam     gam    )
     1.87152307E-03    2          22        23   # BR(h -> Z       gam    )
     2.58964182E-01    2          24       -24   # BR(h -> W+      W-     )
     3.31728569E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.37343908E+01   # H decays
#          BR         NDA      ID1       ID2
     9.13950256E-01    2           5        -5   # BR(H -> b       bb     )
     5.68630863E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.01054285E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.46893866E-04    2           3        -3   # BR(H -> s       sb     )
     6.92170248E-08    2           4        -4   # BR(H -> c       cb     )
     6.90484815E-03    2           6        -6   # BR(H -> t       tb     )
     2.81005362E-06    2          21        21   # BR(H -> g       g      )
     3.17647417E-08    2          22        22   # BR(H -> gam     gam    )
     2.82483796E-09    2          23        22   # BR(H -> Z       gam    )
     7.51881853E-07    2          24       -24   # BR(H -> W+      W-     )
     3.75477360E-07    2          23        23   # BR(H -> Z       Z      )
     4.76882198E-06    2          25        25   # BR(H -> h       h      )
    -3.42187143E-25    2          36        36   # BR(H -> A       A      )
     1.10218293E-17    2          23        36   # BR(H -> Z       A      )
     7.41851630E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.53076892E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.70955974E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.29839164E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.04418647E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.23258866E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.28437188E+01   # A decays
#          BR         NDA      ID1       ID2
     9.32934296E-01    2           5        -5   # BR(A -> b       bb     )
     5.80415099E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.05220622E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52053536E-04    2           3        -3   # BR(A -> s       sb     )
     7.11259352E-08    2           4        -4   # BR(A -> c       cb     )
     7.09136352E-03    2           6        -6   # BR(A -> t       tb     )
     2.08833852E-05    2          21        21   # BR(A -> g       g      )
     5.30655932E-08    2          22        22   # BR(A -> gam     gam    )
     2.05651533E-08    2          23        22   # BR(A -> Z       gam    )
     7.64418078E-07    2          23        25   # BR(A -> Z       h      )
     7.85787959E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.64141308E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.92901752E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.38660305E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.64290764E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48643795E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.35791870E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.89442937E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.51319132E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.11330690E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.29043029E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.34095227E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.06612002E-07    2          24        25   # BR(H+ -> W+      h      )
     4.72229946E-14    2          24        36   # BR(H+ -> W+      A      )
     4.37185483E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.94773026E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.96212392E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
