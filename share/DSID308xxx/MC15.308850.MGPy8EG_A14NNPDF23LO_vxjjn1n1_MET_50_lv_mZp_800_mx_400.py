model="LightVector"
mDM1 = 400.
mDM2 = 1600.
mZp = 800.
mHD = 125.
widthZp = 3.810994e+00
widthN2 = 5.255678e+01
filteff = 9.980040e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
