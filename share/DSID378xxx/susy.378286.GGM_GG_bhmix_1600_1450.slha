#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05415454E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414320E+03   # H
        36     2.00000000E+03   # A
        37     2.00175873E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013260E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989271E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013260E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989271E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99949104E+03   # ~b_1
   2000005     5.00066825E+03   # ~b_2
   1000006     4.98751137E+03   # ~t_1
   2000006     5.01691750E+03   # ~t_2
   1000011     5.00008198E+03   # ~e_L
   2000011     5.00007593E+03   # ~e_R
   1000012     4.99984209E+03   # ~nu_eL
   1000013     5.00008198E+03   # ~mu_L
   2000013     5.00007593E+03   # ~mu_R
   1000014     4.99984209E+03   # ~nu_muL
   1000015     4.99969305E+03   # ~tau_1
   2000015     5.00046546E+03   # ~tau_2
   1000016     4.99984209E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     1.44323191E+03   # ~chi_10
   1000023    -1.45200038E+03   # ~chi_20
   1000025     1.46129284E+03   # ~chi_30
   1000035     3.00150230E+03   # ~chi_40
   1000024     1.45119252E+03   # ~chi_1+
   1000037     3.00150202E+03   # ~chi_2+
   1000039     1.16148254E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.05059610E-01   # N_11
  1  2    -7.45709979E-03   # N_12
  1  3    -5.69505366E-01   # N_13
  1  4    -5.56337037E-01   # N_14
  2  1     1.49895231E-02   # N_21
  2  2    -1.73906230E-02   # N_22
  2  3     7.06822928E-01   # N_23
  2  4    -7.07017842E-01   # N_24
  3  1     7.96038871E-01   # N_31
  3  2     6.68138793E-03   # N_32
  3  3     4.19568228E-01   # N_33
  3  4     4.36165079E-01   # N_34
  4  1     5.46087435E-04   # N_41
  4  2    -9.99798638E-01   # N_42
  4  3    -5.24299036E-03   # N_43
  4  4     1.93622285E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.40436091E-03   # U_11
  1  2     9.99972587E-01   # U_12
  2  1     9.99972587E-01   # U_21
  2  2     7.40436091E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.73832004E-02   # V_11
  1  2    -9.99625010E-01   # V_12
  2  1     9.99625010E-01   # V_21
  2  2     2.73832004E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07787434E-01   # cos(theta_t)
  1  2    -7.06425473E-01   # sin(theta_t)
  2  1     7.06425473E-01   # -sin(theta_t)
  2  2     7.07787434E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.74116063E-01   # cos(theta_b)
  1  2     7.38625435E-01   # sin(theta_b)
  2  1    -7.38625435E-01   # -sin(theta_b)
  2  2    -6.74116063E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04336161E-01   # cos(theta_tau)
  1  2     7.09866588E-01   # sin(theta_tau)
  2  1    -7.09866588E-01   # -sin(theta_tau)
  2  2    -7.04336161E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196040E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51922774E+02   # vev(Q)              
         4     4.52502125E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52751785E-01   # gprime(Q) DRbar
     2     6.26977688E-01   # g(Q) DRbar
     3     1.08402852E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02664554E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72742104E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79698010E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.45402667E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.96079448E+05   # M^2_Hd              
        22    -7.42523009E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36960247E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.38278977E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.65560221E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.47293719E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.36603504E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.98437659E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.16838014E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.77402180E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.07231315E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.54487430E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.36603504E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.98437659E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.16838014E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.77402180E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.07231315E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.54487430E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.53444815E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.02360471E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.11172611E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.11840941E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.11840941E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.11840941E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.11840941E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.11447922E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68919337E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.35694059E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.66478742E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.33281061E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.32833656E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.66159026E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.59663412E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.23298480E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.16711422E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.79893592E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.62830049E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.12047382E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.86480109E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.23660357E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.61837709E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.50145899E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.90786989E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.80688447E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.04238334E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.42846624E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.12531711E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.86184026E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.38576902E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.56517812E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.36588988E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.57247364E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.83650444E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.62804731E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27284828E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.26168164E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.18458241E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33669760E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.63535618E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.44080789E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.64506974E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.31455976E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.01673928E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.62817647E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.98047950E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.18757406E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.49499112E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.15495416E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.57586049E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.94540629E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59282323E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33673776E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.93520296E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.76644672E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.37533052E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.31637609E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.43425530E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.63370264E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98095263E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12081624E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.85520402E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.36083132E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.64249377E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.53321344E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89499940E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33669760E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.63535618E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.44080789E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.64506974E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.31455976E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.01673928E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.62817647E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.98047950E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.18757406E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.49499112E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.15495416E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.57586049E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.94540629E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59282323E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33673776E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.93520296E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.76644672E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.37533052E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.31637609E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.43425530E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.63370264E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98095263E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12081624E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.85520402E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.36083132E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.64249377E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.53321344E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89499940E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.91933894E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.23921975E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.50524035E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.15747743E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73742008E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.15842373E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.48011414E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07422398E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.67161268E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.24839945E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.32613747E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.45622713E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.91933894E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.23921975E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.50524035E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.15747743E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73742008E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.15842373E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.48011414E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07422398E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.67161268E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.24839945E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.32613747E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.45622713E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.49637833E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.95508078E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.73394360E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.27200838E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.58837208E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.00667513E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.17979803E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.06799038E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50526676E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.82579499E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.14035561E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.32984515E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60712673E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     8.47404920E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21735552E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.91914948E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.81067296E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.74476330E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.09042218E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.74053708E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.42291661E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.47580577E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.91914948E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.81067296E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.74476330E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.09042218E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.74053708E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.42291661E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.47580577E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92184362E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.80439304E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.74131037E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.08941673E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.73801011E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.76374382E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.47075511E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.11229920E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.28518290E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.54201187E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03993138E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18067710E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17963849E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.29222864E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.98915312E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53371589E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48666507E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46342406E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.91805372E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52438958E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.46120550E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.43143263E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.09380305E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50917096E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.39702600E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.92695762E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.40996636E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01467212E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91625872E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40209358E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81553186E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17690990E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80856334E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14652922E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.14345587E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.18772416E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.28103504E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.28103504E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.28103504E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.95222828E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.95222828E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.65074286E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.65074286E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.51854382E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.51854382E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.77129928E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.01879962E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.14863661E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.97318079E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.11718784E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     9.89972607E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32103918E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.14395364E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.31934473E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.30247856E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.33988060E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.33817615E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.90261675E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.86135243E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.86135243E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.86135243E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.17215850E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.10756904E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.71798133E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.09352416E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.38144625E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.37524360E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.45638019E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.87351010E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.87351010E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.87351010E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.62307277E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.62307277E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.47514588E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.47514588E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.41018842E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.41018842E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.40723077E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.40723077E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.65738087E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.65738087E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.98933777E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.20742525E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.23183104E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.06632288E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46672416E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46672416E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.16293169E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.90296395E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.10053877E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     8.32637405E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.62803675E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.98902270E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.74758610E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.74664539E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07763443E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17026630E-01    2           5        -5   # BR(h -> b       bb     )
     6.38733382E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26088093E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79101234E-04    2           3        -3   # BR(h -> s       sb     )
     2.06699886E-02    2           4        -4   # BR(h -> c       cb     )
     6.71156287E-02    2          21        21   # BR(h -> g       g      )
     2.30310941E-03    2          22        22   # BR(h -> gam     gam    )
     1.54056292E-03    2          22        23   # BR(h -> Z       gam    )
     2.01097814E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56677383E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78120742E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48179913E-03    2           5        -5   # BR(H -> b       bb     )
     2.46433227E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71233203E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548669E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666923E-05    2           4        -4   # BR(H -> c       cb     )
     9.96056314E-01    2           6        -6   # BR(H -> t       tb     )
     7.97610457E-04    2          21        21   # BR(H -> g       g      )
     2.73783806E-06    2          22        22   # BR(H -> gam     gam    )
     1.16012896E-06    2          23        22   # BR(H -> Z       gam    )
     3.32858863E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65975568E-04    2          23        23   # BR(H -> Z       Z      )
     9.03057667E-04    2          25        25   # BR(H -> h       h      )
     8.79022894E-24    2          36        36   # BR(H -> A       A      )
     3.48402838E-11    2          23        36   # BR(H -> Z       A      )
     5.35991703E-12    2          24       -37   # BR(H -> W+      H-     )
     5.35991703E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82385664E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48457366E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895555E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62258894E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676387E-06    2           3        -3   # BR(A -> s       sb     )
     9.96167640E-06    2           4        -4   # BR(A -> c       cb     )
     9.96987141E-01    2           6        -6   # BR(A -> t       tb     )
     9.43667270E-04    2          21        21   # BR(A -> g       g      )
     3.02532973E-06    2          22        22   # BR(A -> gam     gam    )
     1.35274022E-06    2          23        22   # BR(A -> Z       gam    )
     3.24383325E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74518841E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38787334E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237596E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81144936E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51825044E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45685116E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729823E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404007E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32543198E-04    2          24        25   # BR(H+ -> W+      h      )
     5.90781983E-13    2          24        36   # BR(H+ -> W+      A      )
