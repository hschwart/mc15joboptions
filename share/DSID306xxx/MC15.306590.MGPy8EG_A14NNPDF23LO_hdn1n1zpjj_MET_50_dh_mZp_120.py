model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 120.
mHD = 120.
widthZp = 4.774635e-01
widthhd = 4.758041e-02
filteff = 6.858711e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
