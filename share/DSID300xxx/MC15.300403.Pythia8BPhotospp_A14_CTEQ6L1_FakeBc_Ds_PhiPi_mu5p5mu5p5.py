##################################################################
# Job options for EvtGen generation of B+ -> K*+(K0Pi+)mu3.5mu3.5.
##################################################################

evgenConfig.description = "Fake B_c+->J/psi(mu5p5mu5p5) D_s+ (->Phi(K+K-) pi+) production"
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

# dirty hack below: let the B+ be B_c+
# NOT for physics studies!
genSeq.Pythia8B.Commands += ['521:name = FakeB_c+'] 
genSeq.Pythia8B.Commands += ['521:antiName = FakeB_c-'] 
genSeq.Pythia8B.Commands += ['521:m0 = 6.2756'] 
genSeq.Pythia8B.Commands += ['521:tau0 = 1.35500e-01'] 

# also redefine the mass of 523 which can only decay into 521
genSeq.Pythia8B.Commands += ['523:name = FakeB_c*+'] 
genSeq.Pythia8B.Commands += ['523:antiName = FakeB_c*-']
genSeq.Pythia8B.Commands += ['523:m0 = 6.3216']

# disable excited B decays to 521 and 523 in order to avoid energy 
# conservation problems
genSeq.Pythia8B.Commands += ['545:offIfAny = 521 523']
genSeq.Pythia8B.Commands += ['535:offIfAny = 521 523']
genSeq.Pythia8B.Commands += ['525:offIfAny = 521 523']
genSeq.Pythia8B.Commands += ['515:offIfAny = 521 523']

genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.QuarkPtCut                = 0.
genSeq.Pythia8B.AntiQuarkPtCut            = 9.
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.NHadronizationLoops       = 4

genSeq.Pythia8B.Commands += ['521:addChannel = 2 1.0 0 443 431']

genSeq.Pythia8B.Commands += ['443:onMode = off' ]
#genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.Commands += ['443:onIfMatch = 13 -13']

genSeq.Pythia8B.Commands += ['431:onMode = 3' ]
genSeq.Pythia8B.Commands += ['431:onIfMatch = 333 211']

genSeq.Pythia8B.SignalPDGCodes = [ 521, 443, 13, -13, 431, 211, 333, 321, -321 ]

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.TriggerStatePtCut  = [ 5.5 ]
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]


