#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15008912E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04012393E+01   # W+
        25     1.23994899E+02   # h
        35     3.00014004E+03   # H
        36     2.99999981E+03   # A
        37     3.00091674E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03539219E+03   # ~d_L
   2000001     3.03029202E+03   # ~d_R
   1000002     3.03447915E+03   # ~u_L
   2000002     3.03162107E+03   # ~u_R
   1000003     3.03539219E+03   # ~s_L
   2000003     3.03029202E+03   # ~s_R
   1000004     3.03447915E+03   # ~c_L
   2000004     3.03162107E+03   # ~c_R
   1000005     8.45327659E+02   # ~b_1
   2000005     3.02946718E+03   # ~b_2
   1000006     8.43641400E+02   # ~t_1
   2000006     3.01611844E+03   # ~t_2
   1000011     3.00635708E+03   # ~e_L
   2000011     3.00181400E+03   # ~e_R
   1000012     3.00496451E+03   # ~nu_eL
   1000013     3.00635708E+03   # ~mu_L
   2000013     3.00181400E+03   # ~mu_R
   1000014     3.00496451E+03   # ~nu_muL
   1000015     2.98566368E+03   # ~tau_1
   2000015     3.02201894E+03   # ~tau_2
   1000016     3.00482921E+03   # ~nu_tauL
   1000021     2.34368332E+03   # ~g
   1000022     2.51390770E+02   # ~chi_10
   1000023     5.27975141E+02   # ~chi_20
   1000025    -2.99765811E+03   # ~chi_30
   1000035     2.99840087E+03   # ~chi_40
   1000024     5.28138181E+02   # ~chi_1+
   1000037     2.99898333E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886512E-01   # N_11
  1  2    -8.47350639E-04   # N_12
  1  3     1.49107373E-02   # N_13
  1  4    -1.97876545E-03   # N_14
  2  1     1.25913777E-03   # N_21
  2  2     9.99621686E-01   # N_22
  2  3    -2.68634670E-02   # N_23
  2  4     5.76657571E-03   # N_24
  3  1    -9.12895960E-03   # N_31
  3  2     1.49281463E-02   # N_32
  3  3     7.06861624E-01   # N_33
  3  4     7.07135388E-01   # N_34
  4  1    -1.19180650E-02   # N_41
  4  2     2.30849979E-02   # N_42
  4  3     7.06684278E-01   # N_43
  4  4    -7.07051889E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99277549E-01   # U_11
  1  2    -3.80049995E-02   # U_12
  2  1     3.80049995E-02   # U_21
  2  2     9.99277549E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966711E-01   # V_11
  1  2    -8.15949339E-03   # V_12
  2  1     8.15949339E-03   # V_21
  2  2     9.99966711E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98824873E-01   # cos(theta_t)
  1  2    -4.84651739E-02   # sin(theta_t)
  2  1     4.84651739E-02   # -sin(theta_t)
  2  2     9.98824873E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99913277E-01   # cos(theta_b)
  1  2     1.31696044E-02   # sin(theta_b)
  2  1    -1.31696044E-02   # -sin(theta_b)
  2  2     9.99913277E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06943998E-01   # cos(theta_tau)
  1  2     7.07269527E-01   # sin(theta_tau)
  2  1    -7.07269527E-01   # -sin(theta_tau)
  2  2     7.06943998E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01836594E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.50089122E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44257866E+02   # higgs               
         4     7.41866924E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.50089122E+03  # The gauge couplings
     1     3.62176559E-01   # gprime(Q) DRbar
     2     6.37794166E-01   # g(Q) DRbar
     3     1.02634118E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.50089122E+03  # The trilinear couplings
  1  1     2.13098930E-06   # A_u(Q) DRbar
  2  2     2.13102080E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.50089122E+03  # The trilinear couplings
  1  1     5.62908663E-07   # A_d(Q) DRbar
  2  2     5.63011043E-07   # A_s(Q) DRbar
  3  3     1.24758086E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.50089122E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.18540182E-07   # A_mu(Q) DRbar
  3  3     1.19954728E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.50089122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51796980E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.50089122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14711379E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.50089122E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06595406E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.50089122E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.47161017E+04   # M^2_Hd              
        22    -9.05848421E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41196167E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.17674552E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48222866E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48222866E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51777134E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51777134E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.72480451E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.50058183E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.74227578E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.00766603E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01574130E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.42810687E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.46547557E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.96273236E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.79614129E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.31329977E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.57724483E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49816032E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.95747656E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.48990044E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.05124775E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.77701882E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.91785641E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.21998849E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.90870962E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.02419516E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.51497668E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.46675584E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -4.22955031E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28492336E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.92907447E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.23052606E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.07946475E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88266839E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.95122263E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60092311E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.99174075E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76673023E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.20240948E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.41743458E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.13715437E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17334337E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58294125E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.39771690E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.70463605E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.25792745E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41705615E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88783613E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.05613493E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59874981E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.32227757E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.39880321E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.19670286E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.22728903E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.14398168E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66711849E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.50156119E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.81833199E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.02924715E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.30723565E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54984315E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88266839E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.95122263E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60092311E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.99174075E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76673023E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.20240948E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.41743458E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.13715437E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17334337E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58294125E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.39771690E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.70463605E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.25792745E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41705615E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88783613E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.05613493E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59874981E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.32227757E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.39880321E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.19670286E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.22728903E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.14398168E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66711849E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.50156119E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.81833199E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.02924715E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.30723565E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54984315E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.81071094E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01177712E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00038615E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01214917E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.37481521E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98783647E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.21358945E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54444843E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.51029892E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.47251109E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.44263693E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.81071094E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01177712E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00038615E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01214917E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.37481521E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98783647E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.21358945E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54444843E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.51029892E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.47251109E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.44263693E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70505890E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56513136E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14815131E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28671733E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64787718E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64973530E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11998138E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.22750466E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.37618590E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22988365E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.39293384E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.81092780E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01729228E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99008149E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.04450200E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.41452943E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99262614E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.71918228E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.81092780E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01729228E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99008149E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.04450200E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.41452943E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99262614E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.71918228E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81107214E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01720665E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98981646E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.93294264E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.19387543E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99296703E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     9.77625229E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.77602621E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.38953614E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.75697479E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.15814953E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.63932621E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.09775082E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.21174939E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.91868426E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.43329904E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.45337210E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.00893819E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49910618E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.40853619E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.43785528E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.98456599E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.89184393E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.89184393E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.71513003E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.12137237E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61215739E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61215739E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27889071E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27889071E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.81421031E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.81421031E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.32798447E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.51926117E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.10792615E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.96187111E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.96187111E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.50562936E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.33801102E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.58308526E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.58308526E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30547361E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30547361E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.63515630E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.63515630E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.70323853E-03   # h decays
#          BR         NDA      ID1       ID2
     6.86970946E-01    2           5        -5   # BR(h -> b       bb     )
     5.50580456E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94910949E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.14315875E-04    2           3        -3   # BR(h -> s       sb     )
     1.78832971E-02    2           4        -4   # BR(h -> c       cb     )
     5.73527279E-02    2          21        21   # BR(h -> g       g      )
     1.93343216E-03    2          22        22   # BR(h -> gam     gam    )
     1.21297691E-03    2          22        23   # BR(h -> Z       gam    )
     1.59379710E-01    2          24       -24   # BR(h -> W+      W-     )
     1.95996374E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39795216E+01   # H decays
#          BR         NDA      ID1       ID2
     7.45769349E-01    2           5        -5   # BR(H -> b       bb     )
     1.77886664E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28964736E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72368555E-04    2           3        -3   # BR(H -> s       sb     )
     2.17961134E-07    2           4        -4   # BR(H -> c       cb     )
     2.17430282E-02    2           6        -6   # BR(H -> t       tb     )
     2.83458717E-05    2          21        21   # BR(H -> g       g      )
     2.67831388E-08    2          22        22   # BR(H -> gam     gam    )
     8.36859247E-09    2          23        22   # BR(H -> Z       gam    )
     3.20281675E-05    2          24       -24   # BR(H -> W+      W-     )
     1.59943396E-05    2          23        23   # BR(H -> Z       Z      )
     8.36743267E-05    2          25        25   # BR(H -> h       h      )
     3.83470087E-23    2          36        36   # BR(H -> A       A      )
     4.20103233E-18    2          23        36   # BR(H -> Z       A      )
     2.10503518E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09635213E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.04970797E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.86572096E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.90824381E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.94074271E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33026533E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83786990E-01    2           5        -5   # BR(A -> b       bb     )
     1.86933674E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60951875E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.11786157E-04    2           3        -3   # BR(A -> s       sb     )
     2.29074547E-07    2           4        -4   # BR(A -> c       cb     )
     2.28390794E-02    2           6        -6   # BR(A -> t       tb     )
     6.72589068E-05    2          21        21   # BR(A -> g       g      )
     3.78345666E-08    2          22        22   # BR(A -> gam     gam    )
     6.62575191E-08    2          23        22   # BR(A -> Z       gam    )
     3.35319312E-05    2          23        25   # BR(A -> Z       h      )
     2.63604818E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21735702E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31442124E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.94189871E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01816420E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10280982E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44309699E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63819506E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.05797177E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07648457E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04439612E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.17551549E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.38781826E-05    2          24        25   # BR(H+ -> W+      h      )
     8.37633239E-14    2          24        36   # BR(H+ -> W+      A      )
     1.97621259E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.07866855E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.30498100E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
