mDM=1000
l=100

include("MC15JobOptions/MadGraphControl_monoHiggs_xxhhs.py")

evgenConfig.description = "xxhhs EFT Model for MonoHiggs(h->bb) with lambda=0.1 and mDM="+str(mDM)+"GeV"
evgenConfig.keywords = [ "Higgs", "BSMHiggs", "BSM"]
evgenConfig.contact = ['Nikola Whallon <alokin@uw.edu>']
