#JO for Pythia8 jet jet Power law
evgenConfig.description= "Dijet truth jets, with power law"
evgenConfig.keywords= ["QCD", "jets", "SM"]
evgenConfig.generators += ["Pythia8"]
evgenConfig.contact = ["Harinder Singh Bawa <bawa@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
	"PhaseSpace:pTHatMin = 20.0",
	"PhaseSpace:bias2Selection = on",
	"PhaseSpace:bias2SelectionPow = 5.0"]
