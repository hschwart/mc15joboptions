model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 190.
mHD = 125.
widthZp = 7.559856e-01
widthhd = 1.145416e-01
filteff = 7.616146e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
