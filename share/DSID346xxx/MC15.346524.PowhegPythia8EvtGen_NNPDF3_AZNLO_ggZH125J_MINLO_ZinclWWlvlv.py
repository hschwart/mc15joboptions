#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 gg->ZH->ZWW->Zinclvlv production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
#--------------------------------------------------------------
# Higgs->WW->lvlv at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->ZH->Zinclvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'ada.farilla@cern.ch' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents   = 2000
evgenConfig.process = "gg->ZH, H->WW, Z->inclusive"
