#--------------------------------------------------------------
# Author 1: S. Ask (Cambrdige U.), 21 May 2012
# Author 2: D. Hayden (RHUL.), 3rd August 2012
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 8 TeV by Nikos Tsirintanis, Mihail Chizhov 18th September 2013
# Modifications for Wprime->e,mu flat sample at sqrt(s) = 13 TeV by Nikos Tsirintanis, Mihail Chizhov 16th January 2015
# Modifications for Wprime->tau flat sample at sqrt(s) = 13 , Will Davey, Christos Vergis June 2018
#--------------------------------------------------------------

# EVGEN configuration
evgenConfig.description = "Wprime->tauhad + nu (Flat) production with A14 NNPDF23LO tune"
evgenConfig.process = "W' -> tau + nu"
evgenConfig.contact = ["Christos Vergis <christos.vergis@cern.ch>"] 
evgenConfig.keywords    = [ 'BSM','exotic', 'SSM', 'Wprime', 'heavyBoson', 'resonance', 'tau', 'neutrino' ]
evgenConfig.generators += [ 'Pythia8' ]

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  # create W' bosons
                            "PhaseSpace:mHatMin = 25.0",        # minimum inv.mass cut
                            "34:onMode = off",                  # switch off all W' decays
                            "34:onIfAny = 15,16",               # switch on W'->taunu decays
                            "34:m0 = 1000.0",
			    "15:offIfAny = 12 14",              #turn off leptonic tau decays
			    "-15:offIfAny = 12 14"]             #turn off leptonic atau decays


genSeq.Pythia8.UserHook = "WprimeFlat"
genSeq.Pythia8.UserModes += ["WprimeFlat:EnergyMode = 13"]
