model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 600.
mHD = 125.
widthZp = 2.842817e+00
widthhd = 1.142243e+00
filteff = 8.652016e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
