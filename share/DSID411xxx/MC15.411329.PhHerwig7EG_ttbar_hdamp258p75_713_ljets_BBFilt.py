#Based on the JobOptions of 411082 and 411233 

#Provide config information
evgenConfig.generators += [ 'Powheg', 'Herwig7', 'EvtGen']
evgenConfig.tune        = 'H7.1-Default'
evgenConfig.description = 'PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, one lepton filter, BB filter, ME NNPDF30 NLO, from DSID 410450 LHE files'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'nedaa.asbah@cern.ch', 'aknue@cern.ch', 'tpelzer@cern.ch']
evgenConfig.minevents   = 1000
evgenConfig.inputFilesPerJob = 30

#--------------------------------------------------------------
# l+jets filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 1
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Herwig7.1.3 H7.1-Default tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# BB filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBBFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusBBFilter)"
