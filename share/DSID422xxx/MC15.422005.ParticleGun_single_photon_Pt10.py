evgenConfig.description = "Single photon with flat phi, eta in [-3.0, 3.0], and pT = 10 GeV"
evgenConfig.keywords = ["singleParticle", "photon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (22)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=10000, eta=[-3.0, 3.0])
