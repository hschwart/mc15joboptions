evgenConfig.description = "Starlight gamma + gamma UPC -> ee at 5020 GeV, breakup mode 5 (no selection)"
evgenConfig.keywords = ["2photon"]
evgenConfig.contact = ["angerami@cern.ch","peter.steinberg@bnl.gov"]

if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

include("MC15JobOptions/Starlight_Common.py")

genSeq.Starlight.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beam1Gamma 2705",   #Gamma of the colliding ion1, for sqrt(nn)=5.02 TeV
     "beam2Gamma 2705",   #Gamma of the colliding ion2, for sqrt(nn)=5.02 TeV
     "maxW 4", #Max value of w
     "minW 0.2", #Min value of w
     "nmbWBins 500", #Bins n w
     "maxRapidity 3", #max y
     "nmbRapidityBins 100", #Bins n y
     #Use ATLAS filter instead of putting final-state pT/eta cuts in STARlight
     #Generation is equally efficient but STARlight_i does not keep track of effect of cuts on cross sections
     "accCutPt 0", #Cut in pT? 0 = (no, 1 = yes)
     "accCutEta 0", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 13", #Channel of interest
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 1", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]




include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 100.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2
