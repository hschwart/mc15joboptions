#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16440925E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03970523E+01   # W+
        25     1.23807208E+02   # h
        35     3.00008346E+03   # H
        36     2.99999996E+03   # A
        37     3.00088781E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04056607E+03   # ~d_L
   2000001     3.03542507E+03   # ~d_R
   1000002     3.03965141E+03   # ~u_L
   2000002     3.03646232E+03   # ~u_R
   1000003     3.04056607E+03   # ~s_L
   2000003     3.03542507E+03   # ~s_R
   1000004     3.03965141E+03   # ~c_L
   2000004     3.03646232E+03   # ~c_R
   1000005     9.98726757E+02   # ~b_1
   2000005     3.03442044E+03   # ~b_2
   1000006     9.97292610E+02   # ~t_1
   2000006     3.02055374E+03   # ~t_2
   1000011     3.00631350E+03   # ~e_L
   2000011     3.00212583E+03   # ~e_R
   1000012     3.00491615E+03   # ~nu_eL
   1000013     3.00631350E+03   # ~mu_L
   2000013     3.00212583E+03   # ~mu_R
   1000014     3.00491615E+03   # ~nu_muL
   1000015     2.98570682E+03   # ~tau_1
   2000015     3.02189893E+03   # ~tau_2
   1000016     3.00466611E+03   # ~nu_tauL
   1000021     2.35026442E+03   # ~g
   1000022     1.50430674E+02   # ~chi_10
   1000023     3.20792026E+02   # ~chi_20
   1000025    -2.99547510E+03   # ~chi_30
   1000035     2.99605198E+03   # ~chi_40
   1000024     3.20954139E+02   # ~chi_1+
   1000037     2.99669939E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888290E-01   # N_11
  1  2    -1.15184893E-03   # N_12
  1  3     1.48288681E-02   # N_13
  1  4    -1.47821706E-03   # N_14
  2  1     1.54775738E-03   # N_21
  2  2     9.99644582E-01   # N_22
  2  3    -2.63232340E-02   # N_23
  2  4     3.92443580E-03   # N_24
  3  1    -9.41916934E-03   # N_31
  3  2     1.58514932E-02   # N_32
  3  3     7.06842131E-01   # N_33
  3  4     7.07130972E-01   # N_34
  4  1    -1.15017784E-02   # N_41
  4  2     2.14035699E-02   # N_42
  4  3     7.06725826E-01   # N_43
  4  4    -7.07070154E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99306093E-01   # U_11
  1  2    -3.72469047E-02   # U_12
  2  1     3.72469047E-02   # U_21
  2  2     9.99306093E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984575E-01   # V_11
  1  2    -5.55424227E-03   # V_12
  2  1     5.55424227E-03   # V_21
  2  2     9.99984575E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98761748E-01   # cos(theta_t)
  1  2    -4.97490777E-02   # sin(theta_t)
  2  1     4.97490777E-02   # -sin(theta_t)
  2  2     9.98761748E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99908509E-01   # cos(theta_b)
  1  2     1.35267745E-02   # sin(theta_b)
  2  1    -1.35267745E-02   # -sin(theta_b)
  2  2     9.99908509E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06931741E-01   # cos(theta_tau)
  1  2     7.07281778E-01   # sin(theta_tau)
  2  1    -7.07281778E-01   # -sin(theta_tau)
  2  2     7.06931741E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01772294E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64409252E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44070948E+02   # higgs               
         4     7.79655558E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64409252E+03  # The gauge couplings
     1     3.62481264E-01   # gprime(Q) DRbar
     2     6.39057891E-01   # g(Q) DRbar
     3     1.02429040E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64409252E+03  # The trilinear couplings
  1  1     2.55099452E-06   # A_u(Q) DRbar
  2  2     2.55103338E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64409252E+03  # The trilinear couplings
  1  1     6.54364428E-07   # A_d(Q) DRbar
  2  2     6.54489751E-07   # A_s(Q) DRbar
  3  3     1.48402321E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64409252E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.43371043E-07   # A_mu(Q) DRbar
  3  3     1.45120335E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64409252E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49534455E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64409252E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14231599E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64409252E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05749622E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64409252E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.23404964E+04   # M^2_Hd              
        22    -9.02821658E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41774742E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.41400038E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48175977E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48175977E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51824023E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51824023E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.88899538E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.35498769E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.14009819E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.72440304E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.91596594E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.65947383E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.34775503E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.72291059E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.15389545E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.37491487E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.54041239E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48464402E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.92285677E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.71585017E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.48245556E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.43564120E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.41611324E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19102637E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.98147261E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.20006602E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.27290968E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.23293438E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.22426640E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30384149E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.85082945E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.15621516E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.96849779E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.01582682E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.88741772E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64057168E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.16823078E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.76797332E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28138330E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.34939684E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01916985E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16400504E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60489950E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.78358052E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.18419691E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.71781592E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39509642E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02086647E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.03021846E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63800334E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.17185852E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.01979230E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27563536E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.37560692E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02605332E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65440421E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.57013029E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07740431E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.21059372E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.65396811E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54298581E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.01582682E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.88741772E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64057168E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.16823078E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.76797332E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28138330E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.34939684E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01916985E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16400504E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60489950E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.78358052E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.18419691E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.71781592E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39509642E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02086647E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.03021846E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63800334E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.17185852E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.01979230E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27563536E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.37560692E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02605332E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65440421E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.57013029E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07740431E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.21059372E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.65396811E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54298581E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96804020E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.81045317E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01124303E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.76076740E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.16485531E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00771126E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.48232086E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56127907E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997643E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35348157E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.74696028E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.17300560E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96804020E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.81045317E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01124303E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.76076740E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.16485531E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00771126E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.48232086E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56127907E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997643E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35348157E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.74696028E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.17300560E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79323380E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48928395E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17387696E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33683909E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73383392E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57198305E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14636095E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.40054465E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56542511E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28119985E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.59557638E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96833963E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.88508668E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99899815E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.43884564E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.32641502E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01249303E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.66098267E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96833963E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.88508668E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99899815E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.43884564E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.32641502E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01249303E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.66098267E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96833640E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.88426392E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99873954E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.15543352E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.86461240E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01281639E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.75474590E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.66077099E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.98192859E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68574783E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.05449603E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.31177456E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.44475948E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.31269049E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.26381586E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.74799295E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.92044513E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.88875362E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51112464E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.00637525E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.42891122E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.78375343E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.22852285E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.22852285E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.90360448E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.67871824E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55838245E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55838245E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24675587E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24675587E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.51007545E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.51007545E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.91967087E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.68857541E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.65512745E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.31021204E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.31021204E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49632804E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.08967542E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.53105559E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.53105559E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27419787E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27419787E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.99568347E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.99568347E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.64338203E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87098933E-01    2           5        -5   # BR(h -> b       bb     )
     5.56689026E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.97074179E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.19039391E-04    2           3        -3   # BR(h -> s       sb     )
     1.80917771E-02    2           4        -4   # BR(h -> c       cb     )
     5.77732462E-02    2          21        21   # BR(h -> g       g      )
     1.94719472E-03    2          22        22   # BR(h -> gam     gam    )
     1.20738739E-03    2          22        23   # BR(h -> Z       gam    )
     1.58214360E-01    2          24       -24   # BR(h -> W+      W-     )
     1.93820852E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40352049E+01   # H decays
#          BR         NDA      ID1       ID2
     7.48691114E-01    2           5        -5   # BR(H -> b       bb     )
     1.77177688E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.26457966E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.69292169E-04    2           3        -3   # BR(H -> s       sb     )
     2.17037258E-07    2           4        -4   # BR(H -> c       cb     )
     2.16508566E-02    2           6        -6   # BR(H -> t       tb     )
     2.36542209E-05    2          21        21   # BR(H -> g       g      )
     5.78481770E-09    2          22        22   # BR(H -> gam     gam    )
     8.34656434E-09    2          23        22   # BR(H -> Z       gam    )
     3.01042056E-05    2          24       -24   # BR(H -> W+      W-     )
     1.50335373E-05    2          23        23   # BR(H -> Z       Z      )
     8.07472303E-05    2          25        25   # BR(H -> h       h      )
     2.28336865E-23    2          36        36   # BR(H -> A       A      )
     3.88864663E-19    2          23        36   # BR(H -> Z       A      )
     2.30443635E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11560013E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.14835454E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.18894511E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.66460315E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.54343847E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33850892E+01   # A decays
#          BR         NDA      ID1       ID2
     7.85137445E-01    2           5        -5   # BR(A -> b       bb     )
     1.85782399E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.56881248E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.06786575E-04    2           3        -3   # BR(A -> s       sb     )
     2.27663735E-07    2           4        -4   # BR(A -> c       cb     )
     2.26984194E-02    2           6        -6   # BR(A -> t       tb     )
     6.68446725E-05    2          21        21   # BR(A -> g       g      )
     3.11371849E-08    2          22        22   # BR(A -> gam     gam    )
     6.58019555E-08    2          23        22   # BR(A -> Z       gam    )
     3.14505595E-05    2          23        25   # BR(A -> Z       h      )
     2.60905442E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.20551008E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30010758E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.89736448E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03521219E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11329635E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40284064E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49585844E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.12508554E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.99284258E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02718827E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.23613287E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.07265187E-05    2          24        25   # BR(H+ -> W+      h      )
     7.01248979E-14    2          24        36   # BR(H+ -> W+      A      )
     1.99009140E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.74913289E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.10247024E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
