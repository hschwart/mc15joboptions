#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.75439966E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.75000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05420059E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414355E+03   # H
        36     2.00000000E+03   # A
        37     2.00173763E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989274E+03   # ~u_L
   2000002     4.99994936E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989274E+03   # ~c_L
   2000004     4.99994936E+03   # ~c_R
   1000005     4.99936959E+03   # ~b_1
   2000005     5.00078965E+03   # ~b_2
   1000006     4.98445429E+03   # ~t_1
   2000006     5.01995847E+03   # ~t_2
   1000011     5.00008194E+03   # ~e_L
   2000011     5.00007595E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008194E+03   # ~mu_L
   2000013     5.00007595E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99961318E+03   # ~tau_1
   2000015     5.00054530E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.74330208E+03   # ~chi_10
   1000023    -1.75180342E+03   # ~chi_20
   1000025     1.76144693E+03   # ~chi_30
   1000035     3.00145408E+03   # ~chi_40
   1000024     1.75107011E+03   # ~chi_1+
   1000037     3.00145379E+03   # ~chi_2+
   1000039     1.99181706E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.98767450E-01   # N_11
  1  2    -9.39578276E-03   # N_12
  1  3    -5.71701796E-01   # N_13
  1  4    -5.60844289E-01   # N_14
  2  1     1.24259545E-02   # N_21
  2  2    -1.62924125E-02   # N_22
  2  3     7.06885507E-01   # N_23
  2  4    -7.07031140E-01   # N_24
  3  1     8.00826264E-01   # N_31
  3  2     8.09694790E-03   # N_32
  3  3     4.16487988E-01   # N_33
  3  4     4.30290007E-01   # N_34
  4  1     6.56048422E-04   # N_41
  4  2    -9.99790336E-01   # N_42
  4  3    -2.77358441E-03   # N_43
  4  4     2.02771013E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.90916077E-03   # U_11
  1  2     9.99992359E-01   # U_12
  2  1     9.99992359E-01   # U_21
  2  2     3.90916077E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.86787218E-02   # V_11
  1  2    -9.99588681E-01   # V_12
  2  1     9.99588681E-01   # V_21
  2  2     2.86787218E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07670147E-01   # cos(theta_t)
  1  2    -7.06542966E-01   # sin(theta_t)
  2  1     7.06542966E-01   # -sin(theta_t)
  2  2     7.07670147E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.79878818E-01   # cos(theta_b)
  1  2     7.33324480E-01   # sin(theta_b)
  2  1    -7.33324480E-01   # -sin(theta_b)
  2  2    -6.79878818E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04833245E-01   # cos(theta_tau)
  1  2     7.09373031E-01   # sin(theta_tau)
  2  1    -7.09373031E-01   # -sin(theta_tau)
  2  2    -7.04833245E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196486E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.75000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51980881E+02   # vev(Q)              
         4     4.67841046E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52718421E-01   # gprime(Q) DRbar
     2     6.26765637E-01   # g(Q) DRbar
     3     1.08213115E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02681354E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72898212E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79637134E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.75439966E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.79764364E+05   # M^2_Hd              
        22    -8.29566846E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36865661E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.28550602E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.25153933E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.10484234E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.90459914E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.29050376E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.78322466E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.27086674E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.51688498E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.42000855E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.90459914E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.29050376E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.78322466E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.27086674E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.51688498E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.42000855E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.21469676E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     8.57829443E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.79607110E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.39636673E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.39636673E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.39636673E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.39636673E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.91509605E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68766166E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.13055146E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.43441378E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.42543152E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.11357786E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.84556762E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.63627961E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.04733457E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.01929137E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.58127047E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.49568417E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.18665057E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.63736307E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.36777442E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67119659E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.36734318E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.77832282E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.08369829E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.95532564E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.52700903E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.10231181E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.06038940E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.38120349E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.41989283E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.31784492E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.44971824E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.77510543E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.70706989E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.22658283E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.42125542E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20820542E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22274664E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.94074228E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.27366131E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63278967E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.48259280E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.07508470E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.96409201E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.93086043E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.06683117E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.42231914E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.10895884E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.52955928E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.08213219E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60475098E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22279603E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.70703087E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.26636768E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.31592213E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.48477140E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.99762087E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.97064503E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.93134549E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00561130E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.66442274E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.57389500E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.51708620E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.33995451E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89816915E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22274664E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.94074228E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.27366131E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63278967E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.48259280E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.07508470E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.96409201E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.93086043E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.06683117E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.42231914E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.10895884E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.52955928E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.08213219E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60475098E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22279603E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.70703087E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.26636768E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.31592213E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.48477140E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.99762087E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.97064503E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.93134549E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00561130E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.66442274E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.57389500E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.51708620E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.33995451E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89816915E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.87483028E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.62727559E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.52325264E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.09741313E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77763128E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.59890387E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56161582E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.90284278E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.59857288E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.54560139E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.39987923E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.29067171E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.87483028E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.62727559E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.52325264E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.09741313E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77763128E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.59890387E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56161582E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.90284278E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.59857288E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.54560139E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.39987923E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.29067171E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.38845342E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.83256101E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.85743573E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.17559377E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.66076883E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.88973847E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32532917E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.79535808E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.39663326E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.71261115E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.02230925E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.23645306E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.67674855E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     6.62530539E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.35733884E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.87463745E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.29139016E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.83583913E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.02129809E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78148963E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.60540910E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55663201E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.87463745E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.29139016E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.83583913E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.02129809E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78148963E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.60540910E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55663201E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.87710877E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.28598611E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.83340326E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.02042084E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77910044E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.71901356E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.55185656E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.88414387E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.25787993E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.55520987E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.02824981E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18507620E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18398146E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.21694669E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.96936565E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53673509E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48821935E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46657147E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.94291032E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51418305E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.77567954E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.23079447E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.99354716E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51043409E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.49601875E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.94186779E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.73585543E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02501607E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.97297956E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40624589E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82098306E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16617195E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81355055E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.15933042E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.15605393E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.13456477E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.30681128E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.30681128E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.30681128E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.52533009E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.52533009E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.17511011E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17511011E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.06218528E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.06218528E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.60616715E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.19510627E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.82216127E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.88826530E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     9.74792913E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     7.77731864E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.04395723E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.19862787E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.04264482E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.23707126E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.69178298E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.69042199E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.34634973E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.60664039E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.60664039E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.60664039E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.82198693E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.94921744E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.31361934E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.93349937E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.13046420E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.12976934E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.15529988E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.25764622E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.25764622E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.25764622E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.85934229E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.85934229E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.69828572E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.69828572E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.19774573E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.19774573E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.19453555E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.19453555E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.37691268E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.37691268E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.96949442E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.03714493E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     9.18392448E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.84043060E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47040101E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47040101E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.86581093E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.59862472E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.34411717E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.27144607E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.34820450E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.07078257E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.55043187E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.79996757E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07632219E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16917021E-01    2           5        -5   # BR(h -> b       bb     )
     6.38939852E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26161176E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79256091E-04    2           3        -3   # BR(h -> s       sb     )
     2.06766303E-02    2           4        -4   # BR(h -> c       cb     )
     6.71365403E-02    2          21        21   # BR(h -> g       g      )
     2.30427208E-03    2          22        22   # BR(h -> gam     gam    )
     1.54108435E-03    2          22        23   # BR(h -> Z       gam    )
     2.01149049E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56760012E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78121694E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48372981E-03    2           5        -5   # BR(H -> b       bb     )
     2.46432502E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71230643E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548337E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666819E-05    2           4        -4   # BR(H -> c       cb     )
     9.96055294E-01    2           6        -6   # BR(H -> t       tb     )
     7.97594683E-04    2          21        21   # BR(H -> g       g      )
     2.73505560E-06    2          22        22   # BR(H -> gam     gam    )
     1.16025972E-06    2          23        22   # BR(H -> Z       gam    )
     3.32993545E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66042743E-04    2          23        23   # BR(H -> Z       Z      )
     9.01964117E-04    2          25        25   # BR(H -> h       h      )
     8.68569340E-24    2          36        36   # BR(H -> A       A      )
     3.48552590E-11    2          23        36   # BR(H -> Z       A      )
     5.60532203E-12    2          24       -37   # BR(H -> W+      H-     )
     5.60532203E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386454E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48648943E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895052E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62257114E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676152E-06    2           3        -3   # BR(A -> s       sb     )
     9.96165584E-06    2           4        -4   # BR(A -> c       cb     )
     9.96985084E-01    2           6        -6   # BR(A -> t       tb     )
     9.43665322E-04    2          21        21   # BR(A -> g       g      )
     3.03847243E-06    2          22        22   # BR(A -> gam     gam    )
     1.35289262E-06    2          23        22   # BR(A -> Z       gam    )
     3.24514588E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74516401E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39212726E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236593E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81141388E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52097297E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45683697E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729541E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403877E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32669961E-04    2          24        25   # BR(H+ -> W+      h      )
     5.56192260E-13    2          24        36   # BR(H+ -> W+      A      )
