from MadGraphControl.MadGraphUtils import *

###---------------------------------------------------------------------------###
###                        Configurable Settings                              ###
###---------------------------------------------------------------------------###

# General settings
nevents          = int(1.1*runArgs.maxEvents)
name             = 'SMttzNLO_nunu'
process          = 'pp>ttz'
keywords         = ['SM', 'NLO', 'ttZ']
gridpack_mode    = False
gridpack_dir     = None
mode             = 0
maxjetflavor     = 5
run_name         = 'run_01'
lhe_version      = 3
beam_energy      = -999
if hasattr(runArgs,'ecmEnergy'):
    beam_energy  = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# PDFs 
pdflabel         = 'lhapdf'
lhaid            = 303400 #NNPDF31_nlo_as_0118

# MadSpin Settings 
breit_wig_cut    = 15 #controls how offshell you allow particles to be, in GeV
madspin_card_loc = 'madspin_card.dat'

# Parton showering
syst             = "False"
parton_shower    = "HERWIGPP"


###---------------------------------------------------------------------------###
###                         Setup MadGraph Cards                              ###
###---------------------------------------------------------------------------###

# MadGraph run card

generate_line = """
generate p p > t t~ z [QCD]
"""

definitions = """
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""

madspin_decays = definitions + """
decay t > w+ b, w+ > all
decay t~ > w- b~, w- > all
"""

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
""" + generate_line + """
output -f \n
""")
fcard.close()


# Madspin card

mscard = open(madspin_card_loc,'w')
mscard.write('set max_weight_ps_point 500 \n')  # number of PS to estimate the maximum for each event
mscard.write('set Nevents_for_max_weigth 500 \n')
mscard.write('set BW_cut ' + str(breit_wig_cut) + '\n')
mscard.write('set seed '   + str(runArgs.randomSeed) + '\n') 
mscard.write('decay t > w+ b, w+ > all all \n')
mscard.write('decay t~ > w- b~, w- > all all \n')
mscard.write('decay w+ > all all \n')
mscard.write('decay w- > all all \n')
mscard.write('decay z > vl vl~ \n')
#mscard.write('import Events/' + run_name + '/events.lhe.gz\m')
#mscard.write(madspin_decays)
mscard.write('launch \n')
mscard.close()


# Parameter card 

get_param_file = subprocess.Popen(['get_files',
                                   '-data', 
                                   'aMcAtNlo_param_card_loop_sm-no_b_mass.dat'])
get_param_file.wait()
import os, shutil
if not os.path.exists('aMcAtNlo_param_card_loop_sm-no_b_mass.dat'):
    raise RuntimeError("Cannot find aMcAtNlo_param_card_loop_sm-no_b_mass.dat")


# run_card.dat

extras = {'lhe_version'           :'3.0',
          'lhaid'                 :lhaid,
          'pdlabel'               :pdflabel,
          'maxjetflavor'          :maxjetflavor,
          'parton_shower'         :parton_shower,
          'reweight_scale'        :syst,
          'reweight_pdf'          :syst,
          'dynamical_scale_choice':'3',
}

process_dir = new_process()
build_run_card(run_card_old = get_default_runcard(proc_dir=process_dir),
               run_card_new = 'run_card.dat',
               nevts        = nevents, 
               rand_seed    = runArgs.randomSeed, 
               beamEnergy   = beam_energy, 
               extras       = extras)

# Print cards

print_cards()

###---------------------------------------------------------------------------###
###                         Actually run everything                           ###
###---------------------------------------------------------------------------###

generate(run_card_loc     = 'run_card.dat', 
         madspin_card_loc = madspin_card_loc, 
         param_card_loc   = 'aMcAtNlo_param_card_loop_sm-no_b_mass.dat', 
         mode             = mode, 
         proc_dir         = process_dir,
         nevents          = nevents,
         random_seed      = runArgs.randomSeed,
         run_name         = run_name)

# run

stringy  = 'madgraph.' + str(runArgs.runNumber) + '.MadGraph_' + str(name)
#try:
outputDS = arrange_output(proc_dir    = process_dir,
                          run_name    = run_name,
                          outputDS    = stringy + '._00001.events.tar.gz',
                          lhe_version = lhe_version,
                          saveProcDir = True)
#else:
#    raise RuntimeError("Error: while arranging output.")
opts.nprocs = 0

# run the parton shower

# ttZ inclusive in ttbar, Z->nunu, and Herwig 7.0.4
evgenConfig.generators    += ["aMcAtNlo", "Herwig7", "EvtGen"]
evgenConfig.description    = 'aMcAtNlo_' + str(name) + '_ttZnunu'
evgenConfig.tune           = "H7-UE-MMHT" #H7.1-Default
evgenConfig.keywords      += keywords
evgenConfig.contact        = ["jhowarth@cern.ch","baptiste.ravina@cern.ch"]
runArgs.inputGeneratorFile = outputDS

# Striping PDF and scale factor weights

include("MC15JobOptions/Herwig7_701_StripWeights.py")

# initialize Herwig7 generator configuration for showering of LHE files

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7

Herwig7Config.me_pdf_commands(order = "NLO", 
                              name  = "NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename = runArgs.inputGeneratorFile, 
                                   me_pdf_order = "NLO")
Herwig7Config.add_commands("set /Herwig/Generators/LHCGenerator:DebugLevel 0")

# add EvtGen
#include("MC15JobOptions/Herwig71_EvtGen.py")
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
""")

# run Herwig7
Herwig7Config.run()
