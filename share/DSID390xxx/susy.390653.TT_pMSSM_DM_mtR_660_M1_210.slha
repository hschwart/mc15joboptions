#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12073643E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.46900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.22485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188683E+01   # W+
        25     1.25330968E+02   # h
        35     4.00000597E+03   # H
        36     3.99999660E+03   # A
        37     4.00102339E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02327459E+03   # ~d_L
   2000001     4.02053911E+03   # ~d_R
   1000002     4.02262451E+03   # ~u_L
   2000002     4.02106900E+03   # ~u_R
   1000003     4.02327459E+03   # ~s_L
   2000003     4.02053911E+03   # ~s_R
   1000004     4.02262451E+03   # ~c_L
   2000004     4.02106900E+03   # ~c_R
   1000005     2.25145986E+03   # ~b_1
   2000005     4.02432645E+03   # ~b_2
   1000006     7.03834772E+02   # ~t_1
   2000006     2.26181921E+03   # ~t_2
   1000011     4.00386246E+03   # ~e_L
   2000011     4.00370141E+03   # ~e_R
   1000012     4.00275140E+03   # ~nu_eL
   1000013     4.00386246E+03   # ~mu_L
   2000013     4.00370141E+03   # ~mu_R
   1000014     4.00275140E+03   # ~nu_muL
   1000015     4.00495823E+03   # ~tau_1
   2000015     4.00763063E+03   # ~tau_2
   1000016     4.00443034E+03   # ~nu_tauL
   1000021     1.98929051E+03   # ~g
   1000022     1.97729122E+02   # ~chi_10
   1000023    -2.57509506E+02   # ~chi_20
   1000025     2.69966516E+02   # ~chi_30
   1000035     2.06717802E+03   # ~chi_40
   1000024     2.54225625E+02   # ~chi_1+
   1000037     2.06734552E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.60797511E-01   # N_11
  1  2    -1.24579178E-02   # N_12
  1  3    -4.02987879E-01   # N_13
  1  4    -3.10601377E-01   # N_14
  2  1    -7.12471458E-02   # N_21
  2  2     2.56230319E-02   # N_22
  2  3    -7.00170084E-01   # N_23
  2  4     7.09950109E-01   # N_24
  3  1     5.03935093E-01   # N_31
  3  2     2.67957025E-02   # N_32
  3  3     5.89367762E-01   # N_33
  3  4     6.30854225E-01   # N_34
  4  1    -9.54712864E-04   # N_41
  4  2     9.99234833E-01   # N_42
  4  3    -2.87463224E-03   # N_43
  4  4    -3.89945402E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.07041015E-03   # U_11
  1  2     9.99991716E-01   # U_12
  2  1    -9.99991716E-01   # U_21
  2  2     4.07041015E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.51588830E-02   # V_11
  1  2    -9.98477590E-01   # V_12
  2  1    -9.98477590E-01   # V_21
  2  2    -5.51588830E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.33243350E-02   # cos(theta_t)
  1  2     9.96522481E-01   # sin(theta_t)
  2  1    -9.96522481E-01   # -sin(theta_t)
  2  2    -8.33243350E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999405E-01   # cos(theta_b)
  1  2    -1.09087105E-03   # sin(theta_b)
  2  1     1.09087105E-03   # -sin(theta_b)
  2  2     9.99999405E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05386885E-01   # cos(theta_tau)
  1  2     7.08822504E-01   # sin(theta_tau)
  2  1    -7.08822504E-01   # -sin(theta_tau)
  2  2    -7.05386885E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00284202E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20736426E+03  # DRbar Higgs Parameters
         1    -2.46900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43828106E+02   # higgs               
         4     1.61555114E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20736426E+03  # The gauge couplings
     1     3.61864718E-01   # gprime(Q) DRbar
     2     6.35780510E-01   # g(Q) DRbar
     3     1.03105453E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20736426E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14815364E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20736426E+03  # The trilinear couplings
  1  1     4.21682341E-07   # A_d(Q) DRbar
  2  2     4.21722146E-07   # A_s(Q) DRbar
  3  3     7.54808794E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20736426E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.23759891E-08   # A_mu(Q) DRbar
  3  3     9.33242352E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20736426E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68179407E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20736426E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83512822E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20736426E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03654609E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20736426E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58229274E+07   # M^2_Hd              
        22    -7.38924032E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.22485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40228979E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.23411591E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.59408222E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.33897735E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.09416836E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.14975539E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.92217852E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.20247039E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.16330855E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23630440E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.69170501E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.79058496E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     5.00061935E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.77579003E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.12407931E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.49339602E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.35245872E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.87917864E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.13579604E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.26357594E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.64191374E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.28466391E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.55406357E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.63446548E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.21758151E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.66503505E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64502225E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.59642722E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79044944E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60748446E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.18486659E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62788806E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.23320393E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13570927E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.49298679E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     5.78623334E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.13584118E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.00411008E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76232896E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.00243792E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.28549623E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.31319882E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80694719E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33856215E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60170107E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52051170E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58944339E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.16169416E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.84208324E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.42074741E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.80043750E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43891348E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76251947E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.71868595E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.34384636E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.15046739E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81180505E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.90733690E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63358957E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52275029E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52210024E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08630835E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.41855620E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.70850939E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.30850168E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85354214E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76232896E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.00243792E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.28549623E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.31319882E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80694719E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33856215E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60170107E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52051170E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58944339E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.16169416E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.84208324E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.42074741E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.80043750E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43891348E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76251947E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.71868595E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.34384636E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.15046739E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81180505E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.90733690E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63358957E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52275029E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52210024E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08630835E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.41855620E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.70850939E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.30850168E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85354214E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11807840E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.17136983E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.14112852E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.03213257E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77010818E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.69708707E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55399790E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07357482E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.41780638E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.06441068E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.53154458E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.93221646E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11807840E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.17136983E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.14112852E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.03213257E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77010818E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.69708707E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55399790E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07357482E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.41780638E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.06441068E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.53154458E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.93221646E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09047113E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.73789037E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29042047E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.11591114E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38792539E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46471905E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78275914E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09050780E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.77036192E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.46297066E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.82831970E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41110009E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.14705853E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82921537E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11917537E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29598912E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.24084749E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.45708793E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77327433E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11446503E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53147463E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11917537E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29598912E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.24084749E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.45708793E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77327433E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11446503E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53147463E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44982350E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17227142E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.02693755E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.12707858E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51005183E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.78241758E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00645775E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.86543698E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33505683E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33505683E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11169717E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11169717E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10649201E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66939551E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.25940709E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46752289E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     4.08917777E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.41188102E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.98274818E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40298942E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.80110546E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.01243042E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17752281E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52752229E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17752281E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52752229E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43716350E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50324984E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50324984E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47706828E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00444105E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00444105E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00444078E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.16043508E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.16043508E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.16043508E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.16043508E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.72014864E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.72014864E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.72014864E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.72014864E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     6.30491553E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     6.30491553E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.81543252E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.27121598E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.19232534E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.02694115E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.32920723E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.02694115E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.32920723E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.28192931E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.02468905E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.02468905E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.01192543E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.10603480E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.10603480E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.10602694E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.34918426E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.75029576E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.34918426E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.75029576E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.83226776E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.01473479E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.01473479E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.56340032E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.02534847E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.02534847E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.02534857E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.46888708E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.46888708E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.46888708E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.46888708E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.89624080E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.89624080E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.89624080E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.89624080E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.59969901E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.59969901E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66826129E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.98044808E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.42413075E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.45883337E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40574182E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40574182E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.15336116E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.02405346E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.15890648E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.61080706E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.61080706E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.02281960E-03   # h decays
#          BR         NDA      ID1       ID2
     5.98620875E-01    2           5        -5   # BR(h -> b       bb     )
     6.46642653E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28911956E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85614702E-04    2           3        -3   # BR(h -> s       sb     )
     2.10894981E-02    2           4        -4   # BR(h -> c       cb     )
     6.87186562E-02    2          21        21   # BR(h -> g       g      )
     2.36521209E-03    2          22        22   # BR(h -> gam     gam    )
     1.60088532E-03    2          22        23   # BR(h -> Z       gam    )
     2.15112299E-01    2          24       -24   # BR(h -> W+      W-     )
     2.71137826E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.42564655E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47386052E-01    2           5        -5   # BR(H -> b       bb     )
     6.11097455E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16069259E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55572686E-04    2           3        -3   # BR(H -> s       sb     )
     7.16879732E-08    2           4        -4   # BR(H -> c       cb     )
     7.18462745E-03    2           6        -6   # BR(H -> t       tb     )
     9.14700287E-07    2          21        21   # BR(H -> g       g      )
     7.76626234E-11    2          22        22   # BR(H -> gam     gam    )
     1.84674154E-09    2          23        22   # BR(H -> Z       gam    )
     1.89350241E-06    2          24       -24   # BR(H -> W+      W-     )
     9.46095384E-07    2          23        23   # BR(H -> Z       Z      )
     7.04455673E-06    2          25        25   # BR(H -> h       h      )
    -9.83916313E-25    2          36        36   # BR(H -> A       A      )
     1.25566801E-20    2          23        36   # BR(H -> Z       A      )
     1.86722718E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63560424E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63560424E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.40671204E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.09150461E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.37792369E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.56116423E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.12613447E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.67622162E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.24734714E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.96173268E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.76595477E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.62859163E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.18135730E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.18135730E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.42526892E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47436102E-01    2           5        -5   # BR(A -> b       bb     )
     6.11143324E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16085307E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55633354E-04    2           3        -3   # BR(A -> s       sb     )
     7.21514929E-08    2           4        -4   # BR(A -> c       cb     )
     7.19840619E-03    2           6        -6   # BR(A -> t       tb     )
     1.47909042E-05    2          21        21   # BR(A -> g       g      )
     5.27027899E-08    2          22        22   # BR(A -> gam     gam    )
     1.62639102E-08    2          23        22   # BR(A -> Z       gam    )
     1.88967574E-06    2          23        25   # BR(A -> Z       h      )
     1.87024879E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63574348E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63574348E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.08303967E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.35997407E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.13271204E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19305134E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.91671795E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.66163043E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.39238394E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.11694462E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.62256350E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.22186296E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.22186296E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41598929E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.54114045E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12347591E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16511106E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.54632683E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22657609E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52346145E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.53165055E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.89503433E-06    2          24        25   # BR(H+ -> W+      h      )
     2.77305112E-14    2          24        36   # BR(H+ -> W+      A      )
     6.26455708E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.55518170E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.63784231E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.64065939E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.35421092E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61175899E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14252189E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.47382598E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
