# JO for Pythia 8 jet jet with large-R jet filtering

evgenConfig.description = "Dijet truth jet filtered with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 250."]

## Truth jet filter common config for all JZx and JZxW
include("MC15JobOptions/JetFilterAkt10.py")
filtSeq.QCDTruthJetFilter.MinPt = 400*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 7000*GeV

evgenConfig.minevents = 500
