##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+ 
##############################################################
evgenConfig.description = "Exclusive Bplus->Jpsi_mu3p5mu3p5_Kplus production"
evgenConfig.keywords = ["exclusive","Bplus","Jpsi","2muon"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")

include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 7.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

genSeq.Pythia8B.Commands += ['521:onIfMatch = 443 321']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:onIfMatch = -13 13']
genSeq.Pythia8B.SignalPDGCodes = [   521,   443,   -13,    13,   321 ]
genSeq.Pythia8B.SignalPtCuts   = [   0.0,   0.0,   0.0,   0.0,   0.5 ] # no cuts on muons here -- apply trigger-like selection below
genSeq.Pythia8B.SignalEtaCuts  = [ 102.5, 102.5, 102.5, 102.5,   2.6 ] # no cuts on muons here -- apply trigger-like selection below

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [ 2 ]
