evgenConfig.description = "Single photon with flat eta-phi and flat pT = [5, 1000] GeV"
evgenConfig.keywords = ["singleParticle", "photon"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 22
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[5000.0, 1000000.0], eta=[-4.1,4.1])

