#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.95468939E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.95000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05422740E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414382E+03   # H
        36     2.00000000E+03   # A
        37     2.00172503E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989275E+03   # ~u_L
   2000002     4.99994935E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989275E+03   # ~c_L
   2000004     4.99994935E+03   # ~c_R
   1000005     4.99928837E+03   # ~b_1
   2000005     5.00087085E+03   # ~b_2
   1000006     4.98241300E+03   # ~t_1
   2000006     5.02198744E+03   # ~t_2
   1000011     5.00008192E+03   # ~e_L
   2000011     5.00007597E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008192E+03   # ~mu_L
   2000013     5.00007597E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99955995E+03   # ~tau_1
   2000015     5.00059852E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.94336186E+03   # ~chi_10
   1000023    -1.95169700E+03   # ~chi_20
   1000025     1.96158498E+03   # ~chi_30
   1000035     3.00143955E+03   # ~chi_40
   1000024     1.95098255E+03   # ~chi_1+
   1000037     3.00143924E+03   # ~chi_2+
   1000039     2.71971391E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.93054557E-01   # N_11
  1  2    -1.12958631E-02   # N_12
  1  3    -5.74111891E-01   # N_13
  1  4    -5.64406088E-01   # N_14
  2  1     1.11543735E-02   # N_21
  2  2    -1.56344330E-02   # N_22
  2  3     7.06914858E-01   # N_23
  2  4    -7.07037855E-01   # N_24
  3  1     8.05084638E-01   # N_31
  3  2     9.49855963E-03   # N_32
  3  3     4.13118029E-01   # N_33
  3  4     4.25537305E-01   # N_34
  4  1     7.73867929E-04   # N_41
  4  2    -9.99768846E-01   # N_42
  4  3    -6.43246097E-04   # N_43
  4  4     2.14765457E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -8.93031495E-04   # U_11
  1  2     9.99999601E-01   # U_12
  2  1     9.99999601E-01   # U_21
  2  2     8.93031495E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.03769062E-02   # V_11
  1  2    -9.99538515E-01   # V_12
  2  1     9.99538515E-01   # V_21
  2  2     3.03769062E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07612026E-01   # cos(theta_t)
  1  2    -7.06601175E-01   # sin(theta_t)
  2  1     7.06601175E-01   # -sin(theta_t)
  2  2     7.07612026E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.82726338E-01   # cos(theta_b)
  1  2     7.30674173E-01   # sin(theta_b)
  2  1    -7.30674173E-01   # -sin(theta_b)
  2  2    -6.82726338E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05077756E-01   # cos(theta_tau)
  1  2     7.09130001E-01   # sin(theta_tau)
  2  1    -7.09130001E-01   # -sin(theta_tau)
  2  2    -7.05077756E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196940E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.95000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52020124E+02   # vev(Q)              
         4     4.79257353E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52699119E-01   # gprime(Q) DRbar
     2     6.26642999E-01   # g(Q) DRbar
     3     1.08044248E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02698040E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73031594E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79597585E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.95468939E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.20747677E+06   # M^2_Hd              
        22    -8.95718195E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36810966E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.33802441E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.30400965E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     7.85935231E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.93422065E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.06601059E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.90774006E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.26847325E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.34270896E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.47942726E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.93422065E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.06601059E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.90774006E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.26847325E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.34270896E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.47942726E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.25565581E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     8.90437018E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.88982870E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.78196620E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.78196620E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.78196620E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.78196620E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.72589097E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.74688933E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.07259419E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.34026798E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.52894309E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.05778558E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.05086214E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.62026577E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.87050042E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.99802285E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.52699347E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.42488079E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.25544770E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.58300901E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.50378327E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67078629E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.22697015E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.72623680E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.26769658E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.94518860E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.63359373E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.09998376E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.27555067E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.35196077E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.27299371E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.29612208E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.39785693E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.78057638E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.80605224E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.21126406E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.62133379E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.19383249E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09830188E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.47380948E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.18427903E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.65846790E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.68748221E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.19216616E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.37370985E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86951171E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.93734885E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.38974127E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.90129351E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.54403107E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.34807008E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60657362E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09835965E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.73096325E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.02958429E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.28926687E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69009599E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.03040314E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38145686E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.87001709E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88023386E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.57998529E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.26257750E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.55344715E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.47272020E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89865302E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09830188E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.47380948E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.18427903E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.65846790E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.68748221E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.19216616E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.37370985E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86951171E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.93734885E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.38974127E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.90129351E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.54403107E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.34807008E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60657362E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09835965E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.73096325E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.02958429E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.28926687E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69009599E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.03040314E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38145686E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.87001709E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88023386E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.57998529E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.26257750E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.55344715E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.47272020E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89865302E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.84212555E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.15088846E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.32222373E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.05288412E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.80803132E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.87726129E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.62355561E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77586125E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.53246682E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.24580334E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.46628396E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.41488314E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.84212555E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.15088846E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.32222373E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.05288412E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.80803132E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.87726129E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.62355561E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77586125E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.53246682E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.24580334E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.46628396E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.41488314E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.30853459E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.73327820E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.91943620E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.10108803E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.71814072E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.71570973E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.44085788E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.10601993E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.31637104E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.61936930E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.52321207E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.16205297E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.73305271E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.26490460E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.47073690E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.84193020E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.89774897E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.37070377E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.68176276E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.81268343E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     9.11428988E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.61788040E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.84193020E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.89774897E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.37070377E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.68176276E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.81268343E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     9.11428988E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.61788040E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.84423660E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.89296646E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.36878135E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.67391178E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.81040262E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.72192080E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.61332157E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.72544036E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.26692947E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.56484105E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.01774579E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18828647E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18714595E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.15287792E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.92818098E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53895774E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48968654E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46926131E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.96332871E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.50576152E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.29099710E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.75006187E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.91280159E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51132197E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.57587644E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.42679421E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     9.13770551E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.03467658E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.01766068E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40879759E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82433032E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15873342E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81658621E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.16717945E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.16376651E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.09810237E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.32260982E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.32260982E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.32260982E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.38351377E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.38351377E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.12783801E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.12783801E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01404251E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01404251E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.47229848E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.34244880E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.64468696E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.26669226E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     8.87936170E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     6.94104840E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.35208089E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.43599949E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.34053470E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.83476369E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.44167252E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.44044868E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.13379557E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.11199750E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.11199750E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.11199750E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.28216954E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.54525146E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.74021308E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.52849660E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.81145786E-06    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.26666601E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.26592478E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.03792158E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.52969640E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.52969640E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.52969640E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.04711636E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.04711636E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.87685021E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.87685021E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.82365215E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.82365215E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.82026647E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.82026647E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.95490271E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.95490271E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.92827313E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.82068713E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.17853353E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.80119436E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.47399282E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.47399282E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.41650540E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.32990971E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.39732418E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.10479422E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     9.80232063E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.96553986E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     9.51736043E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     8.00769791E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07525278E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16824604E-01    2           5        -5   # BR(h -> b       bb     )
     6.39108386E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26220831E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79382492E-04    2           3        -3   # BR(h -> s       sb     )
     2.06820436E-02    2           4        -4   # BR(h -> c       cb     )
     6.71536109E-02    2          21        21   # BR(h -> g       g      )
     2.30508609E-03    2          22        22   # BR(h -> gam     gam    )
     1.54150363E-03    2          22        23   # BR(h -> Z       gam    )
     2.01193971E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56827389E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78122625E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48536263E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431779E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71228085E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548006E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666719E-05    2           4        -4   # BR(H -> c       cb     )
     9.96054314E-01    2           6        -6   # BR(H -> t       tb     )
     7.97582363E-04    2          21        21   # BR(H -> g       g      )
     2.73377911E-06    2          22        22   # BR(H -> gam     gam    )
     1.16033453E-06    2          23        22   # BR(H -> Z       gam    )
     3.33130675E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66111132E-04    2          23        23   # BR(H -> Z       Z      )
     9.01119622E-04    2          25        25   # BR(H -> h       h      )
     8.68075663E-24    2          36        36   # BR(H -> A       A      )
     3.48667315E-11    2          23        36   # BR(H -> Z       A      )
     5.75682037E-12    2          24       -37   # BR(H -> W+      H-     )
     5.75682037E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82387128E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48811038E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894622E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62255593E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675952E-06    2           3        -3   # BR(A -> s       sb     )
     9.96163827E-06    2           4        -4   # BR(A -> c       cb     )
     9.96983325E-01    2           6        -6   # BR(A -> t       tb     )
     9.43663658E-04    2          21        21   # BR(A -> g       g      )
     3.04422889E-06    2          22        22   # BR(A -> gam     gam    )
     1.35298058E-06    2          23        22   # BR(A -> Z       gam    )
     3.24648343E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74515208E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39570354E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49235818E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81138649E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52326181E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45682466E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729296E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403742E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32802359E-04    2          24        25   # BR(H+ -> W+      h      )
     5.36321350E-13    2          24        36   # BR(H+ -> W+      A      )
