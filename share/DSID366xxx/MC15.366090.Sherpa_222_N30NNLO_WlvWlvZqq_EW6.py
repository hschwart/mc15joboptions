include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "WWZ (-> lvlv qq) +Oj@NLO+1,2j@LO."
evgenConfig.keywords = ["SM", "triboson", "multilepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 10000
evgenConfig.inputconfcheck = "222_N30NNLO_WlvWlvZqq"

Sherpa_iRunCard="""
(run){
  BEAM_1=2212; BEAM_2=2212

  %scales, tags for scale variations
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  %tags for process setup
  NJET:=2; LJET:=3; QCUT:=30;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;

  %decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[23,1,-1]=2
  HDH_STATUS[23,2,-2]=2
  HDH_STATUS[23,3,-3]=2
  HDH_STATUS[23,4,-4]=2
  HDH_STATUS[23,5,-5]=2
  HDH_STATUS[24,12,-11]=2
  HDH_STATUS[24,14,-13]=2
  HDH_STATUS[24,16,-15]=2
  HDH_STATUS[-24,-12,11]=2
  HDH_STATUS[-24,-14,13]=2
  HDH_STATUS[-24,-16,15]=2
}(run);

(processes){
  Process 901 901 -> 24 -24 23 901{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5};
  End process;
}(processes);
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[23]=0", "WIDTH[24]=0" ]

Sherpa_iNCores = 24
Sherpa_i_OpenLoopsLibs = [ "ppvvv" ]

