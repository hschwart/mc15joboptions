# Filename: FlipLHE.py
# Description:
# Author: Fabian Wilk
# Created: Mon Jun 13 17:52:44 2016
#
# (c) by Fabian Wilk
#
# This file is licensed under a Creative Commons Attribution-ShareAlike 4.0
# International License.
#
# You should have received a copy of the license along with this work.
# If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
# For use with Powheg+Pythia8

from __future__ import print_function
from __future__ import absolute_import

import argparse
import os
import re
import sys

# This code is stored in TopControl and needs to be pulled to this directory
import ColourFlow_LHE as lhe


def expand_path(path):
    return os.path.abspath(
        os.path.normpath(
            os.path.expanduser(
                os.path.expandvars(
                    path))))


def make_parser():
    parser = argparse.ArgumentParser(
        description=r"""
        Produce colour-flipped MC from ttbar signal MC.
        """
    )

    parser.add_argument('INPUT', help="Input file name.")
    parser.add_argument('OUTPUT', help="Output file name.")

    return parser


class ColourStringFlipper(object):
    def __init__(self, fIn, fOut):
        self.fIn = fIn
        self.fOut = fOut

    def __call__(self):
        self.copy_header()
        while self.process_event():
            pass

        self.copy_footer()


    def copy_header(self):
        rx = re.compile(r'<event>\n')

        last_pos = self.fIn.tell()
        line = self.fIn.readline()
        matched = False

        while line:
            if rx.match(line):
                self.fIn.seek(last_pos)
                matched = True
                break

            else:
                self.fOut.write(line)
                last_pos = self.fIn.tell()
                line = self.fIn.readline()

        if not matched:
            raise RuntimeError("failure to find header")


    def process_event(self):
        rx_start = re.compile('<event>\n')
        rx_end = re.compile('</event>\n')

        last_pos = self.fIn.tell()
        line = self.fIn.readline()

        if not rx_start.match(line):
            self.fIn.seek(last_pos)
            return False

        # Acquire all content lines.
        line = self.fIn.readline()

        data = []
        matched = False

        while line:
            if rx_end.match(line):
                matched = True
                break

            else:
                data.append(line)
                last_pos = self.fIn.tell()
                line = self.fIn.readline()

        if not matched:
            raise RuntimeError("failure to find event object")

        event = lhe.Event.from_lines(data)

        # Find hadronically decaying top, do the flip for each hadronically
        # decaying top.
        top_had = None

        num_signal_top = False
        top_index = []
        wboson_index = []

        for p in event:
            # Find top quarks.
            if p.pdg_id not in (6, -6):
                continue

            # Ascertain that they decay as t -> Wq.
            p_W = None
            p_q = None

            top_children = list(p.daughters())

            if len(top_children) == 2:
                if all(abs(da.pdg_id) in range(1, 6) + [24]
                    for da in top_children):
                    d0, d1 = top_children

                if abs(d0.pdg_id) == 24:
                    if p_W is not None:
                        raise RuntimeError("invalid dual-W top decay")
                    p_W = d0
                else:
                    p_q = d0

                if abs(d1.pdg_id) == 24:
                    if p_W is not None:
                        raise RuntimeError("invalid dual-W top decay")
                    p_W = d1
                else:
                    p_q = d1

            if p_W is None or p_q is None:
                raise RuntimeError("top does not decay as t -> Wq")

            # Select only hadronically decaying W bosons.
            W_children = list(p_W.daughters())

            top_index.append(p.index())
            wboson_index.append(p_W.index())

            if not all(abs(da.pdg_id) in range(1, 6)
                    for da in W_children):
                continue

            # Ensure that there are exactly two daughters.
            if len(W_children) != 2:
                raise RuntimeError("W boson does not decay to two quarks")

            # => We have a hadronically decaying top-quark and its W daughter.
            num_signal_top += 1

            # Find the W daughter that has the same PDG id sign as the t -> Wq q-quark.
            if self.perform_colour_flip:
                if 0 < W_children[0].pdg_id * p_q.pdg_id:
                    # Both positive or both negative
                    p_W_q = W_children[0]
                else:
                    p_W_q = W_children[1]

                p_q.colour0, p_W_q.colour0 = p_W_q.colour0, p_q.colour0
                p_q.colour1, p_W_q.colour1 = p_W_q.colour1, p_q.colour1

        # Remove all-hadronic.
        if self.remove_allhadronic and 1 < num_signal_top:
            return True

        # Remove di-lepton
        if self.remove_dilepton and num_signal_top < 1:
            return True

        # Remove the W and/or top
        if (self.remove_wboson or self.remove_top):
            rm_indices = set()
            if self.remove_wboson:
                rm_indices.update(wboson_index)
            if self.remove_top:
                rm_indices.update(top_index)

            for i in reversed(sorted(rm_indices)):
                del event[i]

        self.fOut.write('<event>\n')
        self.fOut.write(event.to_string())
        self.fOut.write('</event>\n')

        return True


    def copy_footer(self):
        rx = re.compile('</LesHouchesEvents>\n')

        last_pos = self.fIn.tell()
        line = self.fIn.readline()

        if rx.match(line):
            while line:
                self.fOut.write(line)
                line = self.fIn.readline()

        else:
            raise RuntimeError("failure to find footer")


if __name__ == '__main__':
    parser = make_parser()
    args = parser.parse_args()

    args.INPUT = expand_path(args.INPUT)
    args.OUTPUT = expand_path(args.OUTPUT)

    if args.INPUT == args.OUTPUT:
        print("input and output file must be distinct", file=sys.stderr)
        syst.exit(1)

    try:
        fIn = open(args.INPUT, 'r')
        fOut = open(args.OUTPUT, 'w')

        flipper = ColourStringFlipper(fIn=fIn, fOut=fOut)
        flipper.perform_colour_flip = True
        flipper.remove_allhadronic  = True
        flipper.remove_dilepton     = False
        flipper.remove_wboson       = True
        flipper.remove_top          = True

        flipper()


    finally:
        fIn.close()
        fOut.close()
