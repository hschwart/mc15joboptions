#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13973048E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49990000E+02   # M_1(MX)             
         2     6.99990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04042981E+01   # W+
        25     1.24157519E+02   # h
        35     3.00017788E+03   # H
        36     2.99999968E+03   # A
        37     3.00093685E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03120950E+03   # ~d_L
   2000001     3.02623178E+03   # ~d_R
   1000002     3.03029729E+03   # ~u_L
   2000002     3.02777128E+03   # ~u_R
   1000003     3.03120950E+03   # ~s_L
   2000003     3.02623178E+03   # ~s_R
   1000004     3.03029729E+03   # ~c_L
   2000004     3.02777128E+03   # ~c_R
   1000005     7.42358092E+02   # ~b_1
   2000005     3.02551624E+03   # ~b_2
   1000006     7.40347857E+02   # ~t_1
   2000006     3.01168481E+03   # ~t_2
   1000011     3.00629102E+03   # ~e_L
   2000011     3.00157186E+03   # ~e_R
   1000012     3.00490159E+03   # ~nu_eL
   1000013     3.00629102E+03   # ~mu_L
   2000013     3.00157186E+03   # ~mu_R
   1000014     3.00490159E+03   # ~nu_muL
   1000015     2.98556714E+03   # ~tau_1
   2000015     3.02205652E+03   # ~tau_2
   1000016     3.00484952E+03   # ~nu_tauL
   1000021     2.33863864E+03   # ~g
   1000022     3.52629688E+02   # ~chi_10
   1000023     7.31406571E+02   # ~chi_20
   1000025    -2.99928751E+03   # ~chi_30
   1000035     3.00021259E+03   # ~chi_40
   1000024     7.31567302E+02   # ~chi_1+
   1000037     3.00073489E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99883636E-01   # N_11
  1  2    -7.19829918E-04   # N_12
  1  3     1.50331427E-02   # N_13
  1  4    -2.49023589E-03   # N_14
  2  1     1.15529104E-03   # N_21
  2  2     9.99585742E-01   # N_22
  2  3    -2.76939070E-02   # N_23
  2  4     7.74963691E-03   # N_24
  3  1    -8.85653354E-03   # N_31
  3  2     1.41122335E-02   # N_32
  3  3     7.06878403E-01   # N_33
  3  4     7.07138834E-01   # N_34
  4  1    -1.23669691E-02   # N_41
  4  2     2.50733078E-02   # N_42
  4  3     7.06632844E-01   # N_43
  4  4    -7.07027872E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99232275E-01   # U_11
  1  2    -3.91772963E-02   # U_12
  2  1     3.91772963E-02   # U_21
  2  2     9.99232275E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99939890E-01   # V_11
  1  2    -1.09643329E-02   # V_12
  2  1     1.09643329E-02   # V_21
  2  2     9.99939890E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98856806E-01   # cos(theta_t)
  1  2    -4.78025220E-02   # sin(theta_t)
  2  1     4.78025220E-02   # -sin(theta_t)
  2  2     9.98856806E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99915423E-01   # cos(theta_b)
  1  2     1.30056467E-02   # sin(theta_b)
  2  1    -1.30056467E-02   # -sin(theta_b)
  2  2     9.99915423E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06951181E-01   # cos(theta_tau)
  1  2     7.07262347E-01   # sin(theta_tau)
  2  1    -7.07262347E-01   # -sin(theta_tau)
  2  2     7.06951181E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01883287E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39730478E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44421947E+02   # higgs               
         4     7.04749809E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39730478E+03  # The gauge couplings
     1     3.61936330E-01   # gprime(Q) DRbar
     2     6.36955278E-01   # g(Q) DRbar
     3     1.02793535E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39730478E+03  # The trilinear couplings
  1  1     1.84310222E-06   # A_u(Q) DRbar
  2  2     1.84312871E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39730478E+03  # The trilinear couplings
  1  1     5.02471833E-07   # A_d(Q) DRbar
  2  2     5.02558484E-07   # A_s(Q) DRbar
  3  3     1.08685576E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39730478E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.01694864E-07   # A_mu(Q) DRbar
  3  3     1.02880540E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39730478E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53430473E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39730478E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15412168E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39730478E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07350605E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39730478E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49990000E+02   # M_1(Q)              
         2     6.99990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.69477798E+04   # M^2_Hd              
        22    -9.08150219E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40811565E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.62163771E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48241709E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48241709E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51758291E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51758291E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.46007417E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.40690607E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.93093929E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02860057E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.24862681E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.62907608E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.29551619E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.24014515E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.27712414E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.60360333E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50583550E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.97742574E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.04630296E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.62079026E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.79209739E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.23739571E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83264937E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.26408794E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.03303232E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.79031499E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     9.11486577E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27333099E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.98146767E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.28738793E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.16315128E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.69268326E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.03370300E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.54817241E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30037852E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.09688820E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.09701780E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.84257006E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.29447204E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17354203E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55853116E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.90077691E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.15139449E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.41953879E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44146680E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.69801764E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.12324986E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.54617652E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.21593846E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.30922902E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.09135497E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.49301543E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.30123266E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67322438E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42460015E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.39569967E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.31397468E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.39365816E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55753941E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.69268326E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.03370300E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.54817241E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30037852E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.09688820E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.09701780E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.84257006E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.29447204E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17354203E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55853116E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.90077691E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.15139449E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.41953879E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44146680E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.69801764E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.12324986E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.54617652E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.21593846E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.30922902E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.09135497E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.49301543E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.30123266E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67322438E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42460015E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.39569967E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.31397468E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.39365816E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55753941E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59869632E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05587988E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98550040E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01954417E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.79171909E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95861956E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.41137525E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52125135E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998785E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21446783E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86748456E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.28966255E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59869632E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05587988E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98550040E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01954417E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.79171909E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95861956E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.41137525E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52125135E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998785E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21446783E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86748456E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.28966255E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58576771E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.67385123E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.11175793E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.21439084E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53192651E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76065738E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.08285320E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.12006157E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.25484266E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15612547E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.26458672E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59879760E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06069672E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97586153E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.72121892E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.37674679E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96344170E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.22252739E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59879760E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06069672E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97586153E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.72121892E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.37674679E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96344170E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.22252739E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59905326E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06060197E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97558558E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.68937725E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.30204741E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96380744E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.96191741E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.92023677E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.62815492E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.81704015E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.21108652E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.84448869E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.90706383E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.16150796E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.72969256E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.27377182E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.73436956E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.53209429E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.44679057E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.64295871E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.47393675E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.22667321E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.70692344E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.70692344E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.80152280E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.68302283E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64083096E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64083096E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.30282422E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.30282422E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.02289391E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.02289391E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.56691371E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.62831289E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.67433898E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.77081249E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.77081249E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.54340594E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.62049520E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.60987903E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.60987903E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.32979012E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.32979012E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.13794231E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.13794231E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.74851852E-03   # h decays
#          BR         NDA      ID1       ID2
     6.86365154E-01    2           5        -5   # BR(h -> b       bb     )
     5.46148838E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93341491E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10873696E-04    2           3        -3   # BR(h -> s       sb     )
     1.77314471E-02    2           4        -4   # BR(h -> c       cb     )
     5.70868347E-02    2          21        21   # BR(h -> g       g      )
     1.92460714E-03    2          22        22   # BR(h -> gam     gam    )
     1.21957895E-03    2          22        23   # BR(h -> Z       gam    )
     1.60634380E-01    2          24       -24   # BR(h -> W+      W-     )
     1.98188997E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39292655E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44264618E-01    2           5        -5   # BR(H -> b       bb     )
     1.78530638E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.31241674E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.75163336E-04    2           3        -3   # BR(H -> s       sb     )
     2.18790597E-07    2           4        -4   # BR(H -> c       cb     )
     2.18257785E-02    2           6        -6   # BR(H -> t       tb     )
     3.08772242E-05    2          21        21   # BR(H -> g       g      )
     5.81804673E-08    2          22        22   # BR(H -> gam     gam    )
     8.39041178E-09    2          23        22   # BR(H -> Z       gam    )
     3.34913542E-05    2          24       -24   # BR(H -> W+      W-     )
     1.67250369E-05    2          23        23   # BR(H -> Z       Z      )
     8.57101752E-05    2          25        25   # BR(H -> h       h      )
    -6.90280662E-24    2          36        36   # BR(H -> A       A      )
     1.43003464E-17    2          23        36   # BR(H -> Z       A      )
     1.80981574E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.06751587E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.02757252E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.38772407E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.03412089E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.16547431E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32439821E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82839128E-01    2           5        -5   # BR(A -> b       bb     )
     1.87761787E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63879880E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.15382361E-04    2           3        -3   # BR(A -> s       sb     )
     2.30089344E-07    2           4        -4   # BR(A -> c       cb     )
     2.29402562E-02    2           6        -6   # BR(A -> t       tb     )
     6.75568666E-05    2          21        21   # BR(A -> g       g      )
     5.36421577E-08    2          22        22   # BR(A -> gam     gam    )
     6.65861236E-08    2          23        22   # BR(A -> Z       gam    )
     3.50907390E-05    2          23        25   # BR(A -> Z       h      )
     2.64131783E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22850202E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31738331E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.95017856E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00629411E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09589415E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47193197E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.74014850E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.01371156E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13639599E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05672182E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13581376E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.62556249E-05    2          24        25   # BR(H+ -> W+      h      )
     9.45289281E-14    2          24        36   # BR(H+ -> W+      A      )
     1.92720838E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.45713382E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.41669528E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
