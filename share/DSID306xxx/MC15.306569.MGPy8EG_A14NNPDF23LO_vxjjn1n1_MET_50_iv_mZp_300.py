model="InelasticVectorEFT"
mDM1 = 150.
mDM2 = 600.
mZp = 300.
mHD = 125.
widthZp = 1.193662e+00
widthN2 = 9.862922e-02
filteff = 9.633911e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
