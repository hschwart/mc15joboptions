model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 60.
mHD = 60.
widthZp = 2.387215e-01
widthhd = 2.353933e-02
filteff = 3.561254e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
