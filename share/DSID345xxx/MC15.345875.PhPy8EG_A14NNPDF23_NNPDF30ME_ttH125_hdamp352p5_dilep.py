evgenConfig.description = 'POWHEG+Pythia8 ttH production with Powheg hdamp=352.5=1.5*(m_top+m_antitop+m_H)/2, A14 tune, di-lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 345674 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'Higgs']
evgenConfig.contact     = [ 'mamolla@cern.ch']
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
