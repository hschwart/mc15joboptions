model="InelasticVectorEFT"
mDM1 = 60.
mDM2 = 240.
mZp = 120.
mHD = 125.
widthZp = 4.774635e+01
widthN2 = 7.883540e+08
filteff = 9.074410e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
