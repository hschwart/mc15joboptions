##############################################################
# bb->J/psi(mu4mu4)X  
##############################################################
evgenConfig.description = "Inclusive bb->J/psi(mu4mu4)"
evgenConfig.keywords = ["inclusive","bottom","Jpsi","2muon"]
evgenConfig.minevents = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3

# Quark cuts
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 4.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->mumu
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

# Number of repeat-hadronization loops
genSeq.Pythia8B.NHadronizationLoops = 2

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [4.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]

