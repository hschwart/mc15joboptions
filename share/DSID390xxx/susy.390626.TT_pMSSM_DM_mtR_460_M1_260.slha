#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90337211E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.05959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.76900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.80485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04207659E+01   # W+
        25     1.24567114E+02   # h
        35     4.00000475E+03   # H
        36     3.99999537E+03   # A
        37     4.00099671E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01278731E+03   # ~d_L
   2000001     4.01224548E+03   # ~d_R
   1000002     4.01213248E+03   # ~u_L
   2000002     4.01286975E+03   # ~u_R
   1000003     4.01278731E+03   # ~s_L
   2000003     4.01224548E+03   # ~s_R
   1000004     4.01213248E+03   # ~c_L
   2000004     4.01286975E+03   # ~c_R
   1000005     1.82878560E+03   # ~b_1
   2000005     4.01858211E+03   # ~b_2
   1000006     4.48334158E+02   # ~t_1
   2000006     1.84269408E+03   # ~t_2
   1000011     4.00167722E+03   # ~e_L
   2000011     4.00354592E+03   # ~e_R
   1000012     4.00056388E+03   # ~nu_eL
   1000013     4.00167722E+03   # ~mu_L
   2000013     4.00354592E+03   # ~mu_R
   1000014     4.00056388E+03   # ~nu_muL
   1000015     4.00392417E+03   # ~tau_1
   2000015     4.00904389E+03   # ~tau_2
   1000016     4.00315313E+03   # ~nu_tauL
   1000021     1.96368956E+03   # ~g
   1000022     2.42493624E+02   # ~chi_10
   1000023    -2.86853091E+02   # ~chi_20
   1000025     3.06526942E+02   # ~chi_30
   1000035     2.06408137E+03   # ~chi_40
   1000024     2.83991593E+02   # ~chi_1+
   1000037     2.06424499E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.79855220E-01   # N_11
  1  2    -1.67718599E-02   # N_12
  1  3    -4.77311792E-01   # N_13
  1  4    -4.04620803E-01   # N_14
  2  1    -6.07448572E-02   # N_21
  2  2     2.53365239E-02   # N_22
  2  3    -7.01604228E-01   # N_23
  2  4     7.09520705E-01   # N_24
  3  1     6.23004754E-01   # N_31
  3  2     2.50482409E-02   # N_32
  3  3     5.29067880E-01   # N_33
  3  4     5.75608235E-01   # N_34
  4  1    -9.87252940E-04   # N_41
  4  2     9.99224375E-01   # N_42
  4  3    -3.48411630E-03   # N_43
  4  4    -3.92114188E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.93254374E-03   # U_11
  1  2     9.99987835E-01   # U_12
  2  1    -9.99987835E-01   # U_21
  2  2     4.93254374E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54651286E-02   # V_11
  1  2    -9.98460625E-01   # V_12
  2  1    -9.98460625E-01   # V_21
  2  2    -5.54651286E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01256890E-01   # cos(theta_t)
  1  2     9.94860313E-01   # sin(theta_t)
  2  1    -9.94860313E-01   # -sin(theta_t)
  2  2    -1.01256890E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999420E-01   # cos(theta_b)
  1  2    -1.07703281E-03   # sin(theta_b)
  2  1     1.07703281E-03   # -sin(theta_b)
  2  2     9.99999420E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05370296E-01   # cos(theta_tau)
  1  2     7.08839012E-01   # sin(theta_tau)
  2  1    -7.08839012E-01   # -sin(theta_tau)
  2  2    -7.05370296E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00258295E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.03372107E+02  # DRbar Higgs Parameters
         1    -2.76900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44601087E+02   # higgs               
         4     1.62322222E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.03372107E+02  # The gauge couplings
     1     3.60886915E-01   # gprime(Q) DRbar
     2     6.35242854E-01   # g(Q) DRbar
     3     1.03766203E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.03372107E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.72350232E-07   # A_c(Q) DRbar
  3  3     2.05959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.03372107E+02  # The trilinear couplings
  1  1     2.11173401E-07   # A_d(Q) DRbar
  2  2     2.11193780E-07   # A_s(Q) DRbar
  3  3     3.76184100E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.03372107E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.73045985E-08   # A_mu(Q) DRbar
  3  3     4.77861539E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.03372107E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74207525E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.03372107E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86782371E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.03372107E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03210018E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.03372107E+02  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57891510E+07   # M^2_Hd              
        22    -7.02878465E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.80485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39983632E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71911950E+01   # gluino decays
#          BR         NDA      ID1       ID2
     9.74837796E-03    2     1000005        -5   # BR(~g -> ~b_1  bb)
     9.74837796E-03    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90251622E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     4.90251622E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.52410167E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.52366364E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.54763364E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.76877774E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     8.34681950E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.22357210E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.35343971E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.30542332E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.86228387E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.19548003E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.15338905E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.87541512E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.04842073E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.84494567E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.98143625E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.34168560E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.62887630E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.66396867E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81572633E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55485856E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.32776826E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66785915E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.05024715E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12648090E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.90025349E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.45553337E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.19142528E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.59450735E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74083957E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.51605810E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.68821705E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80221544E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83595684E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42913702E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65965432E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51165819E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57059644E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.42232960E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.07067980E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.17507715E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.00472364E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43818834E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74101986E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.39269446E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.19739844E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.60876326E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84100106E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.29399204E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.69207774E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51391607E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.50389339E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.93386979E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.40543138E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.67795429E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.84201130E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85334114E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74083957E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.51605810E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.68821705E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80221544E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83595684E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42913702E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65965432E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51165819E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57059644E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.42232960E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.07067980E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.17507715E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.00472364E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43818834E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74101986E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.39269446E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.19739844E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.60876326E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84100106E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.29399204E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.69207774E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51391607E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.50389339E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.93386979E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.40543138E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.67795429E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.84201130E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85334114E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11119992E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.31256146E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.29987838E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.32859825E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77370735E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.48702451E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56149798E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05592796E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.09220987E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.68542700E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.87093056E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.30174006E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11119992E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.31256146E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.29987838E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.32859825E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77370735E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.48702451E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56149798E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05592796E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.09220987E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.68542700E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.87093056E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.30174006E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07627330E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27150554E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35292911E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.55820794E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39320739E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48291711E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79349451E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07823334E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.22794660E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.32108493E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.36378876E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41938692E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10798272E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84597095E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11230897E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08289405E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.82900180E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.51562803E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77705703E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14267989E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53876930E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11230897E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08289405E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.82900180E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.51562803E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77705703E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14267989E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53876930E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44232578E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.79719691E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.65474724E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.99014691E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51480268E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.74205760E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01570970E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.45981656E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33665260E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33665260E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11223090E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11223090E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10223301E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.92698621E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.09419971E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.89542492E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.62693185E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.12637302E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.22149871E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.07916845E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.44177286E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.06888014E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.97256615E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.51587516E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18595318E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53803159E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18595318E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53803159E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38353506E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52492319E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52492319E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48530283E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04653923E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04653923E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04653893E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.23847756E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.23847756E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.23847756E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.23847756E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.12826326E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.12826326E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.12826326E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.12826326E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.54298516E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.54298516E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.17534283E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.56391671E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.43436509E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.64790094E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.12381545E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.64790094E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.12381545E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.02451644E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.76376911E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.76376911E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.74748882E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     9.78190127E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     9.78190127E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     9.78184572E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.65510711E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.14655313E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.65510711E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.14655313E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.23084045E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.92010260E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.92010260E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.69647930E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.83384006E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.83384006E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.83384031E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.11765587E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.11765587E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.11765587E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.11765587E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.72546782E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.72546782E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.72546782E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.72546782E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.61280695E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.61280695E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.92547248E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.82519277E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.25764694E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.86957980E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.07517235E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.07517235E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.45343653E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.51887153E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.68086909E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.02443419E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.02443419E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30883018E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     2.30883018E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.95280254E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.95280254E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.91576068E-03   # h decays
#          BR         NDA      ID1       ID2
     6.10438876E-01    2           5        -5   # BR(h -> b       bb     )
     6.60195229E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33713035E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.96391117E-04    2           3        -3   # BR(h -> s       sb     )
     2.15597697E-02    2           4        -4   # BR(h -> c       cb     )
     6.87765254E-02    2          21        21   # BR(h -> g       g      )
     2.37415933E-03    2          22        22   # BR(h -> gam     gam    )
     1.53597369E-03    2          22        23   # BR(h -> Z       gam    )
     2.03251330E-01    2          24       -24   # BR(h -> W+      W-     )
     2.53137389E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49781646E+01   # H decays
#          BR         NDA      ID1       ID2
     3.53859369E-01    2           5        -5   # BR(H -> b       bb     )
     6.03075544E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13232905E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52217780E-04    2           3        -3   # BR(H -> s       sb     )
     7.07395846E-08    2           4        -4   # BR(H -> c       cb     )
     7.08957914E-03    2           6        -6   # BR(H -> t       tb     )
     5.51059114E-07    2          21        21   # BR(H -> g       g      )
     8.73407694E-10    2          22        22   # BR(H -> gam     gam    )
     1.82518752E-09    2          23        22   # BR(H -> Z       gam    )
     1.73293352E-06    2          24       -24   # BR(H -> W+      W-     )
     8.65866647E-07    2          23        23   # BR(H -> Z       Z      )
     6.55984105E-06    2          25        25   # BR(H -> h       h      )
     1.96877162E-24    2          36        36   # BR(H -> A       A      )
     1.92238604E-20    2          23        36   # BR(H -> Z       A      )
     1.85473405E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60994830E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60994830E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.80470284E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.85296665E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.75467621E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.06450405E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.35390621E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.66596788E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.71631787E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.95438554E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.52558406E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.37319621E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.33600962E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.98127133E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.98127133E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     6.03810042E-08    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49756694E+01   # A decays
#          BR         NDA      ID1       ID2
     3.53901234E-01    2           5        -5   # BR(A -> b       bb     )
     6.03106047E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13243523E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52271472E-04    2           3        -3   # BR(A -> s       sb     )
     7.12026159E-08    2           4        -4   # BR(A -> c       cb     )
     7.10373867E-03    2           6        -6   # BR(A -> t       tb     )
     1.45963921E-05    2          21        21   # BR(A -> g       g      )
     5.44972665E-08    2          22        22   # BR(A -> gam     gam    )
     1.60553279E-08    2          23        22   # BR(A -> Z       gam    )
     1.72945170E-06    2          23        25   # BR(A -> Z       h      )
     1.85965102E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61003236E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61003236E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.40224600E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.10691432E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.44506764E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.57846251E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.76972820E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.69815641E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.95106430E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.93884845E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.50817398E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.11974019E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.11974019E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49206193E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.65218859E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03861705E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13510705E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.61739761E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20957926E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48849352E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60002252E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.73309157E-06    2          24        25   # BR(H+ -> W+      h      )
     2.41220137E-14    2          24        36   # BR(H+ -> W+      A      )
     4.94120213E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.53189089E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.79234259E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61369384E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.38209256E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59183063E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.05728022E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.26923705E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.11614675E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
