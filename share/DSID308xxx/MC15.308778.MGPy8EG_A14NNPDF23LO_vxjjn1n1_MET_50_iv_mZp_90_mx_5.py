model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 120.
mZp = 90.
mHD = 125.
widthZp = 3.580954e-01
widthN2 = 8.998220e-04
filteff = 3.441156e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
