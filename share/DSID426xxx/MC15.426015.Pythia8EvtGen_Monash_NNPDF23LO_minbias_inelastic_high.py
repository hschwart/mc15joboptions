evgenConfig.description = "High-pT inelastic minimum bias events for pile-up, with the A2 Monash tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.process  = "minBias"
evgenConfig.generators =  ["Pythia8"]

evgenConfig.saveJets = True


include ("MC15JobOptions/Pythia8_Monash_NNPDF23LO_EvtGen_Common.py")


genSeq.Pythia8.Commands += \
    ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_MinbiasHigh.py")

evgenConfig.minevents = 1000
