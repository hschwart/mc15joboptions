#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11037939E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.16900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.19485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04208337E+01   # W+
        25     1.25201121E+02   # h
        35     4.00001064E+03   # H
        36     3.99999693E+03   # A
        37     4.00098261E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02005322E+03   # ~d_L
   2000001     4.01799713E+03   # ~d_R
   1000002     4.01940294E+03   # ~u_L
   2000002     4.01850851E+03   # ~u_R
   1000003     4.02005322E+03   # ~s_L
   2000003     4.01799713E+03   # ~s_R
   1000004     4.01940294E+03   # ~c_L
   2000004     4.01850851E+03   # ~c_R
   1000005     2.22027493E+03   # ~b_1
   2000005     4.02275485E+03   # ~b_2
   1000006     6.11930146E+02   # ~t_1
   2000006     2.23047142E+03   # ~t_2
   1000011     4.00315183E+03   # ~e_L
   2000011     4.00360184E+03   # ~e_R
   1000012     4.00204207E+03   # ~nu_eL
   1000013     4.00315183E+03   # ~mu_L
   2000013     4.00360184E+03   # ~mu_R
   1000014     4.00204207E+03   # ~nu_muL
   1000015     4.00409001E+03   # ~tau_1
   2000015     4.00849693E+03   # ~tau_2
   1000016     4.00399297E+03   # ~nu_tauL
   1000021     1.98343958E+03   # ~g
   1000022     3.89300477E+02   # ~chi_10
   1000023    -4.28078527E+02   # ~chi_20
   1000025     4.51741572E+02   # ~chi_30
   1000035     2.06769903E+03   # ~chi_40
   1000024     4.25726279E+02   # ~chi_1+
   1000037     2.06786637E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.29558315E-01   # N_11
  1  2     2.08585966E-02   # N_12
  1  3     5.05012814E-01   # N_13
  1  4     4.60729467E-01   # N_14
  2  1    -3.95013207E-02   # N_21
  2  2     2.37929022E-02   # N_22
  2  3    -7.04317126E-01   # N_23
  2  4     7.08386144E-01   # N_24
  3  1     6.82776030E-01   # N_31
  3  2     2.52694242E-02   # N_32
  3  3     4.98857341E-01   # N_33
  3  4     5.33216374E-01   # N_34
  4  1    -1.09684313E-03   # N_41
  4  2     9.99179800E-01   # N_42
  4  3    -6.38718662E-03   # N_43
  4  4    -3.99715878E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.03869363E-03   # U_11
  1  2     9.99959150E-01   # U_12
  2  1    -9.99959150E-01   # U_21
  2  2     9.03869363E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.65380372E-02   # V_11
  1  2    -9.98400446E-01   # V_12
  2  1    -9.98400446E-01   # V_21
  2  2    -5.65380372E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.08098967E-02   # cos(theta_t)
  1  2     9.96729532E-01   # sin(theta_t)
  2  1    -9.96729532E-01   # -sin(theta_t)
  2  2    -8.08098967E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998285E-01   # cos(theta_b)
  1  2    -1.85202512E-03   # sin(theta_b)
  2  1     1.85202512E-03   # -sin(theta_b)
  2  2     9.99998285E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06091533E-01   # cos(theta_tau)
  1  2     7.08120574E-01   # sin(theta_tau)
  2  1    -7.08120574E-01   # -sin(theta_tau)
  2  2    -7.06091533E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00235015E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10379391E+03  # DRbar Higgs Parameters
         1    -4.16900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44126337E+02   # higgs               
         4     1.62901796E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10379391E+03  # The gauge couplings
     1     3.61470360E-01   # gprime(Q) DRbar
     2     6.35040369E-01   # g(Q) DRbar
     3     1.03304644E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10379391E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.34186394E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10379391E+03  # The trilinear couplings
  1  1     3.45014217E-07   # A_d(Q) DRbar
  2  2     3.45046753E-07   # A_s(Q) DRbar
  3  3     6.16209211E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10379391E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.72412378E-08   # A_mu(Q) DRbar
  3  3     7.80268484E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10379391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69758708E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10379391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88052948E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10379391E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02946403E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10379391E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57057072E+07   # M^2_Hd              
        22    -2.00207427E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.19485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39894111E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.42514562E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.95616618E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.20891722E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.09596469E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.18314358E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.65116547E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.05625749E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26833107E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.20419661E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.18019967E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.29780388E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.83544237E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.63200273E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.28466985E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.20028793E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.59522418E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.43451579E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.33602570E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.58055023E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.74510310E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.13420138E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.55831345E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64531929E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64741989E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82490921E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53405735E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.73329608E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66380454E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.44376231E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12658730E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.34552920E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.79812753E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.23851679E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.13579260E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75851641E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.19882300E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.29512260E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.06571179E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79380163E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.52523361E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57553398E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52466635E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58588709E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.94789093E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.60891108E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.56555690E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.68910911E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44779396E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75870710E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20069806E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.45880592E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     9.37230171E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79916835E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.41223853E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.60820171E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52689661E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51976758E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.68932463E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.24555958E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.69202769E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.62103604E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85596182E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75851641E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.19882300E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.29512260E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.06571179E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79380163E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.52523361E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57553398E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52466635E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58588709E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.94789093E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.60891108E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.56555690E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.68910911E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44779396E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75870710E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20069806E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.45880592E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     9.37230171E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79916835E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.41223853E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.60820171E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52689661E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51976758E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.68932463E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.24555958E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.69202769E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.62103604E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85596182E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10057848E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.90513363E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.66490770E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.64858080E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77627567E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.27225463E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56751700E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03592880E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.33902723E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.55894078E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64537675E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.61317396E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10057848E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.90513363E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.66490770E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.64858080E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77627567E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.27225463E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56751700E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03592880E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.33902723E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.55894078E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64537675E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.61317396E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05620790E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.96547357E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.49942701E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.83001557E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39692266E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.56257205E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80138830E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04990253E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.93738625E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.03270673E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67189029E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42810527E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95481885E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86386563E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10168853E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.65959206E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.08325002E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.66119587E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78033331E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.23454083E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54440999E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10168853E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.65959206E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.08325002E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.66119587E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78033331E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.23454083E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54440999E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.42574527E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.75027382E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.81280741E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03417497E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52034344E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65427475E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02597140E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.53358988E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33765584E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33765584E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11256502E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11256502E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09955829E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.64377594E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.48472768E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46312353E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.47499192E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40145576E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.44767633E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39177242E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.88466118E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.11700438E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19040371E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54428382E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19040371E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54428382E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34730072E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54203680E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54203680E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49358632E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.08134232E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.08134232E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.08134216E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.19748648E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.19748648E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.19748648E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.19748648E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.39916411E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.39916411E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.39916411E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.39916411E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.38771334E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.38771334E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.92388353E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.35493423E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.15704127E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.91337479E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.25947086E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.91337479E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.25947086E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.21444067E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.68835620E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.68835620E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.71334192E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.87990385E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.87990385E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.87981884E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.29359667E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.97551006E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.29359667E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.97551006E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.05341006E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.82519148E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.82519148E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.60671111E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36435439E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36435439E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36435442E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26903418E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26903418E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26903418E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26903418E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.23004979E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.23004979E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.23004979E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.23004979E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.13357974E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.13357974E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.64233158E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.56805379E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.61877096E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.89154676E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40171927E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40171927E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.95607253E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.25582130E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.65922479E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.72359286E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.72359286E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     3.98842521E-03   # h decays
#          BR         NDA      ID1       ID2
     5.98990782E-01    2           5        -5   # BR(h -> b       bb     )
     6.51413654E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30601469E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89298826E-04    2           3        -3   # BR(h -> s       sb     )
     2.12536356E-02    2           4        -4   # BR(h -> c       cb     )
     6.91392422E-02    2          21        21   # BR(h -> g       g      )
     2.37577296E-03    2          22        22   # BR(h -> gam     gam    )
     1.59627332E-03    2          22        23   # BR(h -> Z       gam    )
     2.13873650E-01    2          24       -24   # BR(h -> W+      W-     )
     2.69093781E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47641903E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60012376E-01    2           5        -5   # BR(H -> b       bb     )
     6.05432909E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14066412E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53203629E-04    2           3        -3   # BR(H -> s       sb     )
     7.10094651E-08    2           4        -4   # BR(H -> c       cb     )
     7.11662691E-03    2           6        -6   # BR(H -> t       tb     )
     7.13709070E-07    2          21        21   # BR(H -> g       g      )
     1.87184825E-09    2          22        22   # BR(H -> gam     gam    )
     1.83405028E-09    2          23        22   # BR(H -> Z       gam    )
     1.62166622E-06    2          24       -24   # BR(H -> W+      W-     )
     8.10271534E-07    2          23        23   # BR(H -> Z       Z      )
     6.33982117E-06    2          25        25   # BR(H -> h       h      )
    -5.44992278E-24    2          36        36   # BR(H -> A       A      )
     7.58296564E-20    2          23        36   # BR(H -> Z       A      )
     1.87376968E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56536049E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56536049E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.74690696E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.20083291E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.82210054E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.90771757E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.85495614E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.79494198E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.89730048E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.21030722E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.69952570E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.14344127E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.34853972E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.34853972E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.47576715E+01   # A decays
#          BR         NDA      ID1       ID2
     3.60080842E-01    2           5        -5   # BR(A -> b       bb     )
     6.05507332E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14092558E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53275888E-04    2           3        -3   # BR(A -> s       sb     )
     7.14861077E-08    2           4        -4   # BR(A -> c       cb     )
     7.13202208E-03    2           6        -6   # BR(A -> t       tb     )
     1.46545001E-05    2          21        21   # BR(A -> g       g      )
     6.41934971E-08    2          22        22   # BR(A -> gam     gam    )
     1.61194363E-08    2          23        22   # BR(A -> Z       gam    )
     1.61847373E-06    2          23        25   # BR(A -> Z       h      )
     1.90122417E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56539243E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56539243E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.37861738E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.66786309E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.55017101E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.31099855E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.76907501E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.20685114E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.13151873E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.36815196E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.12431104E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.56150408E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.56150408E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47879039E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.76594343E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05322335E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14027148E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.69020066E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21250555E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49451383E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.67123719E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.61934286E-06    2          24        25   # BR(H+ -> W+      h      )
     2.23477934E-14    2          24        36   # BR(H+ -> W+      A      )
     4.18251205E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.20108067E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.33614211E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.56667001E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     8.01300710E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55725130E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.83662574E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.51695276E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
