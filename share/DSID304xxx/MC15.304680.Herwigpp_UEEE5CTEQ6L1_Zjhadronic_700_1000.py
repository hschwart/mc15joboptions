evgenConfig.description = "Herwig Z->had  700 < pT < 1000 GeV"
evgenConfig.keywords = ["SM","Z","jets"]
evgenConfig.process = "Z + jets (Z -> qqbar)"
evgenConfig.contact  = ["craig.sawyer@cern.ch"]
	
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEZJet
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:ZDecay Quarks

set /Herwig/Cuts/ZBosonKtCut:MinKT 700.*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 1000.*GeV
"""

genSeq.Herwigpp.Commands += cmds.splitlines()
