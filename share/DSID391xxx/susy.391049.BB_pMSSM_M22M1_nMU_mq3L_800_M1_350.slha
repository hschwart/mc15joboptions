#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15475444E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04029095E+01   # W+
        25     1.25104720E+02   # h
        35     3.00007730E+03   # H
        36     2.99999996E+03   # A
        37     3.00108923E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03696849E+03   # ~d_L
   2000001     3.03183645E+03   # ~d_R
   1000002     3.03606161E+03   # ~u_L
   2000002     3.03361150E+03   # ~u_R
   1000003     3.03696849E+03   # ~s_L
   2000003     3.03183645E+03   # ~s_R
   1000004     3.03606161E+03   # ~c_L
   2000004     3.03361150E+03   # ~c_R
   1000005     9.00323845E+02   # ~b_1
   2000005     3.03027497E+03   # ~b_2
   1000006     8.97818107E+02   # ~t_1
   2000006     3.02345484E+03   # ~t_2
   1000011     3.00654037E+03   # ~e_L
   2000011     3.00135487E+03   # ~e_R
   1000012     3.00515468E+03   # ~nu_eL
   1000013     3.00654037E+03   # ~mu_L
   2000013     3.00135487E+03   # ~mu_R
   1000014     3.00515468E+03   # ~nu_muL
   1000015     2.98616363E+03   # ~tau_1
   2000015     3.02167631E+03   # ~tau_2
   1000016     3.00516046E+03   # ~nu_tauL
   1000021     2.34583975E+03   # ~g
   1000022     3.52569140E+02   # ~chi_10
   1000023     7.35008438E+02   # ~chi_20
   1000025    -2.99800675E+03   # ~chi_30
   1000035     2.99836439E+03   # ~chi_40
   1000024     7.35171319E+02   # ~chi_1+
   1000037     2.99914295E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889532E-01   # N_11
  1  2    -5.59732880E-05   # N_12
  1  3    -1.48301100E-02   # N_13
  1  4    -9.93920608E-04   # N_14
  2  1     4.61741592E-04   # N_21
  2  2     9.99622144E-01   # N_22
  2  3     2.70233776E-02   # N_23
  2  4     5.00923566E-03   # N_24
  3  1    -9.78016384E-03   # N_31
  3  2     1.55699578E-02   # N_32
  3  3    -7.06857409E-01   # N_33
  3  4     7.07117054E-01   # N_34
  4  1     1.11829497E-02   # N_41
  4  2    -2.26526348E-02   # N_42
  4  3     7.06684093E-01   # N_43
  4  4     7.07078066E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99269614E-01   # U_11
  1  2     3.82130708E-02   # U_12
  2  1    -3.82130708E-02   # U_21
  2  2     9.99269614E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974921E-01   # V_11
  1  2    -7.08212849E-03   # V_12
  2  1    -7.08212849E-03   # V_21
  2  2    -9.99974921E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98536054E-01   # cos(theta_t)
  1  2    -5.40901919E-02   # sin(theta_t)
  2  1     5.40901919E-02   # -sin(theta_t)
  2  2     9.98536054E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99723972E-01   # cos(theta_b)
  1  2    -2.34942505E-02   # sin(theta_b)
  2  1     2.34942505E-02   # -sin(theta_b)
  2  2     9.99723972E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06955687E-01   # cos(theta_tau)
  1  2     7.07257843E-01   # sin(theta_tau)
  2  1    -7.07257843E-01   # -sin(theta_tau)
  2  2    -7.06955687E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00125289E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54754444E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44017419E+02   # higgs               
         4     1.05697971E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.54754444E+03  # The gauge couplings
     1     3.62254834E-01   # gprime(Q) DRbar
     2     6.36940269E-01   # g(Q) DRbar
     3     1.02562722E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54754444E+03  # The trilinear couplings
  1  1     2.28884491E-06   # A_u(Q) DRbar
  2  2     2.28887855E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54754444E+03  # The trilinear couplings
  1  1     8.19411705E-07   # A_d(Q) DRbar
  2  2     8.19505006E-07   # A_s(Q) DRbar
  3  3     1.64907680E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54754444E+03  # The trilinear couplings
  1  1     3.45616615E-07   # A_e(Q) DRbar
  2  2     3.45633424E-07   # A_mu(Q) DRbar
  3  3     3.50330632E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54754444E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52159029E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54754444E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.80112430E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54754444E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01261177E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54754444E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.27469193E+04   # M^2_Hd              
        22    -9.05508987E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40811019E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.92167360E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47950752E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47950752E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52049248E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52049248E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.07894085E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.28805880E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.07119412E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14847859E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.62737824E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.34068344E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.72004393E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04827086E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.97063319E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69105983E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61000783E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.15445233E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.22300585E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.87546835E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.12453165E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.35856326E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.03847556E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.06055426E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.90858582E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.76390459E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -6.26383352E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.50520996E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.11248780E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.01062370E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.46830277E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.68376421E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.10852864E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55195288E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.35303072E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.17731692E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.10538481E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.18296024E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.28157615E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16342841E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56857115E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.05324177E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.71269898E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.24520524E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43142832E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68902400E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.11291144E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55077834E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.97085066E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.88410812E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.09974455E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.60640102E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.28834320E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65926037E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45889570E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.67831835E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.50275389E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.20528853E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55411029E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.68376421E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.10852864E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55195288E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.35303072E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.17731692E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.10538481E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.18296024E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.28157615E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16342841E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56857115E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.05324177E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.71269898E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.24520524E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43142832E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68902400E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.11291144E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55077834E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.97085066E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.88410812E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.09974455E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.60640102E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.28834320E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65926037E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45889570E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.67831835E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.50275389E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.20528853E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55411029E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59587453E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06116318E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98216955E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.08586520E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.64213462E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95666700E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.38029727E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52384466E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999805E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93751271E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.89102802E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.10213781E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59587453E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06116318E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98216955E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.08586520E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.64213462E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95666700E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.38029727E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52384466E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999805E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93751271E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.89102802E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.10213781E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58348149E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68447358E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10715790E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20836851E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53394024E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76731202E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07947043E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.33833179E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.14280224E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15283636E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.33073612E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59587802E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06106345E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97731232E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.40023034E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.78444050E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96162413E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40471847E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59587802E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06106345E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97731232E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.40023034E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.78444050E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96162413E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40471847E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59621700E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06096559E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97703886E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.40539747E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.79372386E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96198530E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.01510735E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.36475656E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.91505224E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.34070482E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61791740E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.90219048E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.08205956E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.99735102E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.91877720E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.41555094E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.23080939E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.37337955E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.62662045E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.93305565E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.16169884E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.11791337E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.90307300E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.90307300E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.21204129E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.98190259E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34222939E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34222939E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.58322902E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.58322902E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.46345635E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.46345635E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.84182549E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.98306861E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97011963E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.96664333E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.96664333E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.22294342E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.46591340E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31030748E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31030748E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.65160563E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.65160563E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.46211097E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.46211097E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.61892442E-03   # h decays
#          BR         NDA      ID1       ID2
     5.60442723E-01    2           5        -5   # BR(h -> b       bb     )
     7.17056007E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53839438E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.38689885E-04    2           3        -3   # BR(h -> s       sb     )
     2.34091914E-02    2           4        -4   # BR(h -> c       cb     )
     7.61340654E-02    2          21        21   # BR(h -> g       g      )
     2.60928191E-03    2          22        22   # BR(h -> gam     gam    )
     1.74307802E-03    2          22        23   # BR(h -> Z       gam    )
     2.33861108E-01    2          24       -24   # BR(h -> W+      W-     )
     2.93024230E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.62543566E+01   # H decays
#          BR         NDA      ID1       ID2
     8.98712883E-01    2           5        -5   # BR(H -> b       bb     )
     6.85920635E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.42525145E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.97821715E-04    2           3        -3   # BR(H -> s       sb     )
     8.34716006E-08    2           4        -4   # BR(H -> c       cb     )
     8.32682640E-03    2           6        -6   # BR(H -> t       tb     )
     9.80370471E-06    2          21        21   # BR(H -> g       g      )
     6.57690760E-08    2          22        22   # BR(H -> gam     gam    )
     3.41450721E-09    2          23        22   # BR(H -> Z       gam    )
     7.13151021E-07    2          24       -24   # BR(H -> W+      W-     )
     3.56135926E-07    2          23        23   # BR(H -> Z       Z      )
     5.65457142E-06    2          25        25   # BR(H -> h       h      )
    -2.06129145E-24    2          36        36   # BR(H -> A       A      )
     1.89933971E-19    2          23        36   # BR(H -> Z       A      )
     6.89992256E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.07644814E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.44728172E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.44088030E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24864435E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.18388539E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.54530154E+01   # A decays
#          BR         NDA      ID1       ID2
     9.19049434E-01    2           5        -5   # BR(A -> b       bb     )
     7.01411135E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.48001869E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.04597794E-04    2           3        -3   # BR(A -> s       sb     )
     8.59531798E-08    2           4        -4   # BR(A -> c       cb     )
     8.56966227E-03    2           6        -6   # BR(A -> t       tb     )
     2.52368351E-05    2          21        21   # BR(A -> g       g      )
     6.62792110E-08    2          22        22   # BR(A -> gam     gam    )
     2.48682511E-08    2          23        22   # BR(A -> Z       gam    )
     7.26521109E-07    2          23        25   # BR(A -> Z       h      )
     8.94836674E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.35346654E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.46993474E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.75684915E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.89553200E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46646597E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.38582069E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.25787044E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.38537066E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.32689283E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.72984523E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.22863683E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.62337673E-07    2          24        25   # BR(H+ -> W+      h      )
     5.17939837E-14    2          24        36   # BR(H+ -> W+      A      )
     4.78059344E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.19920928E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08114962E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
