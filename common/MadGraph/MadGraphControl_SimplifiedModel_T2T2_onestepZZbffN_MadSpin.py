include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

masses['2000006'] = float(runArgs.jobConfig[0].split('_')[4])
masses['1000006'] = float(runArgs.jobConfig[0].split('_')[5])
masses['1000022'] = float(runArgs.jobConfig[0].split('_')[6].split('.')[0])
if masses['1000022']<0.5: masses['1000022']=0.5
gentype = str(runArgs.jobConfig[0].split('_')[2])
decaytype = str(runArgs.jobConfig[0].split('_')[3])

#--------------------------------------------------------------
# MadGraph options
# 
bwcutoff = 15
extras['bwcutoff']=bwcutoff # to allow very low-mass W* and Z*
extras['event_norm']='sum'
extras['use_syst']='F'

#MadGraph 2.6.X uses 5-flav for merging
#force 4-flav merging as use a 4-flav scheme
extras['pdgs_for_merging_cut']='1, 2, 3, 4, 21, 1000001, 1000002, 1000003, 1000004, 1000021, 2000001, 2000002, 2000003, 2000004'



process = '''
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g

generate p p > t2 t2~ $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @1
add process p p > t2 t2~ j $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @2
add process p p > t2 t2~ j j $ go susylq susylq~ b1 b2 t1 b1~ b2~ t1~ @3
'''
njets = 2
evt_multiplier = 10



evgenLog.info('Registered generation of stop2 pair production, stop2 to Z+stop1, stop1->t+LSP; grid point '+str(runArgs.runNumber)+' decoded into mass point ' + str(masses['2000006']))

#--------------------------------------------------------------
# Madspin configuration
#
if 'MS' in runArgs.jobConfig[0]:
    evgenLog.info('Running w/ MadSpin option')
    madspin_card='madspin_card_test.dat'

    mscard = open(madspin_card,'w')

    mscard.write("""#************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    #Some options (uncomment to apply)
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event   
    #  
    set seed %i 
    set spinmode none 
    # specify the decay for the final state particles

    decay t2 > t1 z, t1 > n1 fu fd~ b
    decay t2~ > t1~ z, t1~ > n1 fu~ fd b~
    
    #
    #
    # running the actual code
    launch"""%runArgs.randomSeed)

    mscard.close()

include ( 'MC15JobOptions/MultiElecMuTauFilter.py' )
filtSeq.MultiElecMuTauFilter.MinPt  = 10000.
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
filtSeq.Expression = "MultiElecMuTauFilter"

evgenConfig.contact  = [ "federico.meloni@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel', 'stop']
evgenConfig.description = 'stop2 direct pair production, st2->Z+st1, st1->t+LSP in simplified model, m_stop2 = %s GeV, m_stop1 = %s GeV, m_N1 = %s GeV'%(masses['2000006'],masses['1000006'],masses['1000022'])

include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t2,2000006}{t2~,-2000006}"]
