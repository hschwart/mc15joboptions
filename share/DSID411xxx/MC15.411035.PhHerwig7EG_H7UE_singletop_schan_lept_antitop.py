#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7+EvtGen single-top-quark s-channel production (anti-top), inclusive, H7UE tune, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'sChannel', 'inclusive' ]
evgenConfig.contact     = [ 'cescobar@cern.ch', 'kevin.finelli@cern.ch', 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += [ 'Powheg','Herwig7', 'EvtGen' ]
evgenConfig.minevents   = 1000
evgenConfig.inputfilecheck = 'TXT'
evgenConfig.tune = "MMHT2014"

#--------------------------------------------------------------
# Herwig7 showering with the H7UE MMHT2014 tune
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
""")

# run Herwig7
Herwig7Config.run()
