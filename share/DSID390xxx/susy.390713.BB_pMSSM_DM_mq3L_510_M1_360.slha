#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10033907E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.94900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04187102E+01   # W+
        25     1.24910919E+02   # h
        35     4.00000812E+03   # H
        36     3.99999642E+03   # A
        37     4.00098962E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01652581E+03   # ~d_L
   2000001     4.01506971E+03   # ~d_R
   1000002     4.01586671E+03   # ~u_L
   2000002     4.01631374E+03   # ~u_R
   1000003     4.01652581E+03   # ~s_L
   2000003     4.01506971E+03   # ~s_R
   1000004     4.01586671E+03   # ~c_L
   2000004     4.01631374E+03   # ~c_R
   1000005     5.55281737E+02   # ~b_1
   2000005     4.02027949E+03   # ~b_2
   1000006     5.40475678E+02   # ~t_1
   2000006     2.02393357E+03   # ~t_2
   1000011     4.00279471E+03   # ~e_L
   2000011     4.00287628E+03   # ~e_R
   1000012     4.00167647E+03   # ~nu_eL
   1000013     4.00279471E+03   # ~mu_L
   2000013     4.00287628E+03   # ~mu_R
   1000014     4.00167647E+03   # ~nu_muL
   1000015     4.00409878E+03   # ~tau_1
   2000015     4.00830494E+03   # ~tau_2
   1000016     4.00392705E+03   # ~nu_tauL
   1000021     1.96405583E+03   # ~g
   1000022     3.50771366E+02   # ~chi_10
   1000023    -4.05134348E+02   # ~chi_20
   1000025     4.18056539E+02   # ~chi_30
   1000035     2.05618981E+03   # ~chi_40
   1000024     4.02922715E+02   # ~chi_1+
   1000037     2.05635391E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.57902818E-01   # N_11
  1  2    -1.47536772E-02   # N_12
  1  3    -3.89791620E-01   # N_13
  1  4    -3.34436207E-01   # N_14
  2  1    -4.32468977E-02   # N_21
  2  2     2.40642427E-02   # N_22
  2  3    -7.03975823E-01   # N_23
  2  4     7.08497465E-01   # N_24
  3  1     5.11987630E-01   # N_31
  3  2     2.88266405E-02   # N_32
  3  3     5.93671108E-01   # N_33
  3  4     6.20155067E-01   # N_34
  4  1    -1.06182279E-03   # N_41
  4  2     9.99185802E-01   # N_42
  4  3    -5.92858533E-03   # N_43
  4  4    -3.98930869E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.39019570E-03   # U_11
  1  2     9.99964802E-01   # U_12
  2  1    -9.99964802E-01   # U_21
  2  2     8.39019570E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.64281327E-02   # V_11
  1  2    -9.98406664E-01   # V_12
  2  1    -9.98406664E-01   # V_21
  2  2    -5.64281327E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95933682E-01   # cos(theta_t)
  1  2    -9.00894059E-02   # sin(theta_t)
  2  1     9.00894059E-02   # -sin(theta_t)
  2  2     9.95933682E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999230E-01   # cos(theta_b)
  1  2    -1.24096713E-03   # sin(theta_b)
  2  1     1.24096713E-03   # -sin(theta_b)
  2  2     9.99999230E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05861258E-01   # cos(theta_tau)
  1  2     7.08350114E-01   # sin(theta_tau)
  2  1    -7.08350114E-01   # -sin(theta_tau)
  2  2    -7.05861258E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00241135E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00339072E+03  # DRbar Higgs Parameters
         1    -3.94900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44384138E+02   # higgs               
         4     1.62934414E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00339072E+03  # The gauge couplings
     1     3.61037744E-01   # gprime(Q) DRbar
     2     6.35728109E-01   # g(Q) DRbar
     3     1.03613591E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00339072E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.51278482E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00339072E+03  # The trilinear couplings
  1  1     2.77335775E-07   # A_d(Q) DRbar
  2  2     2.77362181E-07   # A_s(Q) DRbar
  3  3     4.94566215E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00339072E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.17009666E-08   # A_mu(Q) DRbar
  3  3     6.23233516E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00339072E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71913651E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00339072E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86623901E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00339072E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02902369E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00339072E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57190299E+07   # M^2_Hd              
        22    -1.67356649E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40206555E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.99222484E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45164890E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45164890E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54835110E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54835110E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.24806688E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.68485224E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     5.31514776E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14604512E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.48937497E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.18185514E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.62013163E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.34440606E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.50545149E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.11754680E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.33978984E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.34799610E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.26615085E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.15594039E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.57790876E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66675375E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51887539E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78817025E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.61339869E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.19681611E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.59250496E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.37243311E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14053349E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.74679446E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.37946176E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.99741839E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.22156424E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78154940E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.87511916E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.15756906E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.35159299E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76919090E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.45154661E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.52627218E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53251927E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60821918E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.02139305E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.01685231E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.42333323E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.42326440E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45451018E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78175070E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.69963569E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.90263453E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.96366564E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77438547E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.20531024E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.55874321E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53471632E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54135390E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04862067E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.65154154E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.71147716E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.92273241E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85775792E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78154940E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.87511916E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.15756906E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.35159299E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76919090E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.45154661E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.52627218E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53251927E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60821918E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.02139305E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.01685231E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.42333323E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.42326440E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45451018E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78175070E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.69963569E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.90263453E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.96366564E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77438547E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.20531024E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.55874321E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53471632E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54135390E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04862067E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.65154154E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.71147716E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.92273241E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85775792E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12787852E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.13081153E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.24085859E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.14064940E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77988355E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.09716203E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57452902E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04078511E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.37260231E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.86383205E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.60875315E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.21525655E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12787852E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.13081153E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.24085859E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.14064940E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77988355E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.09716203E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57452902E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04078511E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.37260231E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.86383205E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.60875315E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.21525655E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07290544E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64632484E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45014860E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.13857955E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40336634E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.52517824E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81419658E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06798735E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72968731E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05268530E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.85885533E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43447112E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95194522E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87652319E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12895118E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.27566635E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19083326E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.45103350E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78375305E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.20816484E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55148728E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12895118E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.27566635E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19083326E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.45103350E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78375305E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.20816484E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55148728E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45414760E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.15623666E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.07935210E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.12796600E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52512515E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.59311626E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03573645E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.34118894E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33538920E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33538920E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11181174E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11181174E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10559813E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.46112461E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.83738457E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.60736252E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.72563804E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.20169531E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.19927830E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.05489581E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.83633074E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.07756713E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.27070138E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.92850813E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17867699E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52851601E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17867699E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52851601E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43023005E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50268832E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50268832E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47353724E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00185132E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00185132E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00185105E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.62350862E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.62350862E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.62350862E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.62350862E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.41170014E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.41170014E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.41170014E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.41170014E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     9.36371908E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     9.36371908E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.87712708E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.66131264E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.70352008E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.56382636E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.76598678E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.56382636E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.76598678E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.44310425E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.20397430E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.20397430E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.20251415E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.48950313E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.48950313E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.48948805E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.08351544E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.59254014E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.08351544E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.59254014E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.32224586E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.51083659E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.51083659E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.35328781E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.01974446E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.01974446E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.01974450E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.80135008E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.80135008E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.80135008E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.80135008E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.26710463E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.26710463E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.26710463E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.26710463E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.18440287E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.18440287E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.46002062E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.80129760E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.00979171E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.71131272E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.05013062E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.05013062E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.33831028E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.15410388E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.17495185E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88524174E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.88524174E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.89653915E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.89653915E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.92989387E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01421205E-01    2           5        -5   # BR(h -> b       bb     )
     6.59595749E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33499253E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.95671248E-04    2           3        -3   # BR(h -> s       sb     )
     2.15299362E-02    2           4        -4   # BR(h -> c       cb     )
     6.96867476E-02    2          21        21   # BR(h -> g       g      )
     2.39071320E-03    2          22        22   # BR(h -> gam     gam    )
     1.57846088E-03    2          22        23   # BR(h -> Z       gam    )
     2.10366874E-01    2          24       -24   # BR(h -> W+      W-     )
     2.63373173E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.56226501E+01   # H decays
#          BR         NDA      ID1       ID2
     3.62689301E-01    2           5        -5   # BR(H -> b       bb     )
     5.96088465E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10762443E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49295622E-04    2           3        -3   # BR(H -> s       sb     )
     6.99152017E-08    2           4        -4   # BR(H -> c       cb     )
     7.00695888E-03    2           6        -6   # BR(H -> t       tb     )
     5.91863916E-07    2          21        21   # BR(H -> g       g      )
     2.18756929E-09    2          22        22   # BR(H -> gam     gam    )
     1.80427097E-09    2          23        22   # BR(H -> Z       gam    )
     1.62679329E-06    2          24       -24   # BR(H -> W+      W-     )
     8.12833192E-07    2          23        23   # BR(H -> Z       Z      )
     6.54554029E-06    2          25        25   # BR(H -> h       h      )
    -1.86028350E-24    2          36        36   # BR(H -> A       A      )
     6.24034030E-20    2          23        36   # BR(H -> Z       A      )
     1.84921158E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56276558E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56276558E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.15166011E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.68909272E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.33247979E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.64105042E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.10714835E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.27540054E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11169628E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.11024638E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.28900142E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.29618061E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.26867648E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.26867648E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44511720E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.56220072E+01   # A decays
#          BR         NDA      ID1       ID2
     3.62719043E-01    2           5        -5   # BR(A -> b       bb     )
     5.96098002E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10765650E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49340090E-04    2           3        -3   # BR(A -> s       sb     )
     7.03752447E-08    2           4        -4   # BR(A -> c       cb     )
     7.02119356E-03    2           6        -6   # BR(A -> t       tb     )
     1.44267779E-05    2          21        21   # BR(A -> g       g      )
     6.20728926E-08    2          22        22   # BR(A -> gam     gam    )
     1.58630841E-08    2          23        22   # BR(A -> Z       gam    )
     1.62344061E-06    2          23        25   # BR(A -> Z       h      )
     1.87104357E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56267222E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56267222E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.86952918E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.25767254E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.13030725E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.18504134E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.14528850E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.47893814E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.22966391E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.42657638E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.76784053E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.50907413E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.50907413E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.58140088E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.83788318E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94194912E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10092764E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73624208E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19021623E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44865754E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.71487802E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.61962769E-06    2          24        25   # BR(H+ -> W+      h      )
     2.27857214E-14    2          24        36   # BR(H+ -> W+      A      )
     5.89199638E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.71215431E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.53834649E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55941602E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.74643247E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54865235E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.10411927E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.73675860E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.47493875E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
