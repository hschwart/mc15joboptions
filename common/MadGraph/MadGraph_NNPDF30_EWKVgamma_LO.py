from MadGraphControl.MadGraphUtils import *
import fileinput
import shutil
import subprocess
import os


mode=0
nJobs=1
gridpack_mode=True
gridpack_dir='madevent/'
cluster_type=None 
cluster_queue=None 
cluster_nb_retry=5

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot                                                                                                                                                                                           
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = njobs
    nJobs=int(njobs)
    mode=2
    print opts

name = runArgs.jobConfig[0]
thisDSID = int(name.split(".")[1])
runName = str(thisDSID)

isZnn = [363266]
isZee = [363267]
isZmm = [363268]
isZtt = [363269]
isWen = [363270]
isWmn = [363271]
isWtn = [363272]
isZll = [999999]

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

if(thisDSID in isZnn):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate p p > vl vl~ a j j QCD=0
   output -f""")
   fcard.close()
elif(thisDSID in isZll):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   define lp = e+ mu+ ta+
   define lm = e- mu- ta-
   generate  p p > lp lm a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isZee):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > e+ e- a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isZmm):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > mu+ mu- a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isZtt):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > ta+ ta- a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isWen):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > e+ ve a j j QCD=0 
   add process p p > e- ve~ a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isWmn):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > mu+ vm a j j QCD=0 
   add process p p > mu- vm~ a j j QCD=0 
   output -f""")
   fcard.close()
elif(thisDSID in isWtn):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > ta+ vt a j j QCD=0 
   add process p p > ta- vt~ a j j QCD=0 
   output -f""")
   fcard.close()
else:
   print "Unknown DSID"
   raise RuntimeError("DSID not known")

name = "EWKVg"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack="madevent/")
maxjetflavor = 5
parton_shower='PYTHIA8'

extras = { 'lhe_version'   :'3.0',
           'pdlabel'      : "'lhapdf'",
           'lhaid'         : 315000, #NNPDF31_lo_as_0118
           'maxjetflavor'  : maxjetflavor,
           'ptj'           : 10.0,
           'ptb'           : 10.0,
           'drll':"0.1",
           'drjl':"0.1",
           'drjj':"0.1",
           'etaj'          : -1,
           'draa':"0.0",
           'dral':"0.1",
           'etaa'          : '3.0',
           'ptgmin'        : 10.0,
           'epsgamma'      :'0.1',
           'R0gamma'       :'0.1',
           'xn'            :'2',
           'isoEM'         :'T',
           'bwcutoff'      :'15',
         }

madspin_card_loc=None
if (thisDSID in isZnn) or (thisDSID in isZll) or (thisDSID in isZee)or (thisDSID in isZmm)or (thisDSID in isZtt):
    mllcut=10.0
    extras['mmll'] = mllcut

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
    
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,random_seed=runArgs.randomSeed,required_accuracy=0.001,nevents=nevents)

outputDS=arrange_output(run_name=runName, proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)
runArgs.inputGeneratorFile=outputDS

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on',
]
