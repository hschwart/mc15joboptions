evgenConfig.generators = ["MadGraph", "Herwig7", "EvtGen"]
#evgenConfig.inputfilecheck = 'munugamma2j'
evgenConfig.contact = ['Evgeny Soldatov <Evgeny.Soldatov@cern.ch>']
evgenConfig.keywords = ['SM','W','photon','muon','diboson','VBS']
evgenConfig.description = 'MadGraph W->muvg plus two EWK jets'

name='munugamma2j'

from Herwig7_i.Herwig7_iConf import Herwig7 
from Herwig7_i.Herwig7ConfigLHEF import Hw7ConfigLHEF

genSeq += Herwig7()
Herwig7Config = Hw7ConfigLHEF(genSeq, runArgs)

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
inputGeneratorFile = (runArgs.inputGeneratorFile.split('/')[-1]).replace('tar.gz.1', 'events')
Herwig7Config.lhef_mg5amc_commands(lhe_filename=inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen

include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()


