# joboption for Double Higgs production (h1 h1) in gluon-fusion, with top-quark mass effects, in the 2HDM
# (arXiv:1407.0281)

from MadGraphControl.MadGraphUtils import *
import tarfile, shutil, os, glob

cwd = os.getcwd()

mode=0

run_number = runArgs.runNumber
if (run_number > run_number_max):
    log.fatal('Run number out of validity range for this generation. run_number_max '+str(run_number_max))
    raise RunTimeError('Run number too high.')

imass = run_number - run_number_min + offset


def numbers_to_mass(argument):
    switcher = {  0:  260.,
                  1:  275.,
                  2:  300.,
                  3:  325.,
                  4:  350.,
                  5:  400.,
                  6:  450.,
                  7:  500.,
                  8:  600.,
                  9:  700.,
                 10:  750.,
                 11:  800.,
                 12:  900.,
                 13: 1000.,
                 14: 1100.,
                 15: 1200.,
                 16: 1300.,
                 17: 1400.,
                 18: 1500.,
                 19: 1600.,
                 20: 1800.,
                 21: 2000.,
                 22: 2250.,
                 23: 2500.,
                 24: 2750.,
                 25: 3000.,
                 26: 3250.,
                 27: 3500.,
                 28: 3750.,
                 29: 4000.,
                 30: 4500.,
                 31: 5000. }

    return switcher.get(argument, "nothing")

# Setting higgs masses
m_h2 = numbers_to_mass(imass)
higgsMasses = { 'm_h1' : '1.250900e+02',
                'm_h2' : str(m_h2) }

# Setting decay widths
decays = { 'Wh2' : '1.000000e-03' }

# Setting renormalisation scale in param_card
scale = m_h2 / 2.
loop = { 'MU_R' : str(scale) }
parameters = { 'decay' : decays,
               'loop' : loop,
               'mass' : higgsMasses }

# setting some parameters for run_card.dat
extras = { 'lhe_version' : '3.0',
           'pdlabel' : 'lhapdf',
           'lhaid' : '11000',
           'PDF_set_min': '11001',
           'PDF_set_max': '11052',
           'parton_shower' : 'HERWIGPP',
           'muR_ref_fixed' : str(scale),
           'muF1_ref_fixed' : str(scale),
           'muF2_ref_fixed' : str(scale),
           'QES_ref_fixed' : str(scale) }

# Generating di-higgs through Heavy Higgs resonance with MadGraph
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_optimized_output True
set complex_mass_scheme False
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
import model 2HDMCP_EFT
generate p p > h1 h1 [real=QCD]
output""")
fcard.close()

beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# Setting the number of events and add protection to avoid crashing with maxEvents=-1
nevents = 5000 * safefactor
if evgenConfig.minevents > 0 :
   nevents = evgenConfig.minevents * safefactor

if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents * safefactor

# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
build_run_card(run_card_old='run_card.template.dat', run_card_new='run_card.dat', nevts=nevents,
        rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, extras=extras, xqcut=0.0)

# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Higgs masses are set by 'higgsMasses'
build_param_card(param_card_old='param_card.dat', param_card_new='param_card_new.dat', params=parameters)

print_cards()

runName='run_01'

process_dir = 'PROCNLO_2HDMCP_EFT_0'

# fetch and compile additional files
shutil.copytree(os.environ['MADPATH'] + '/vendor/CutTools', 'vendors/CutTools')
shutil.copytree(os.environ['MADPATH'] + '/vendor/StdHEP', 'vendors/StdHEP')
shutil.copytree(os.environ['MADPATH'] + '/vendor/IREGI', 'vendors/IREGI')
os.chdir(cwd + '/vendors/CutTools')
os.system('make clean && make -j1')
os.chdir(cwd + '/vendors/StdHEP')
os.system('make clean && make -j1')
os.chdir(cwd + '/vendors/IREGI/src')
os.system('make clean && make')
os.chdir(cwd + '/code/Source/DHELAS')
os.system('make clean && make')
os.chdir(cwd + '/code/Source/MODEL')
os.system('make clean && make')
os.chdir(cwd + '/code/SubProcesses/P0_gg_h1h1')
os.system('make clean && make reweight_FT4')
os.chdir(cwd + '/code/SubProcesses/P1_gg_h1h1g')
os.system('make clean && make reweight_FT4')
os.chdir(cwd + '/code/SubProcesses/P2_ug_h1h1u')
os.system('make clean && make reweight_FT4')
os.chdir(cwd)

# generate the events
generate(run_card_loc='run_card.dat', param_card_loc='param_card_new.dat', mode=mode, proc_dir=process_dir, run_name=runName)

# do the reweighting
os.chdir(cwd + '/' + process_dir + '/Events/' + runName)
os.system('gunzip events.lhe.gz')

#first step, reweight the S events
os.chdir(cwd + '/code/SubProcesses/P0_gg_h1h1')
shutil.copy(cwd + '/' + process_dir + '/Events/' + runName + '/events.lhe', '.')
os.system('echo -e events.lhe "\n 1\n"  | ./reweight_FT4')

#second step, reweight the gg H events
os.chdir(cwd + '/code/SubProcesses/P1_gg_h1h1g')
shutil.copy(cwd + '/code/SubProcesses/P0_gg_h1h1/events.lhe.rwgt1', '.')
os.system('echo -e events.lhe.rwgt1 "\n 1\n"  | ./reweight_FT4')
os.remove('events.lhe.rwgt1')

#third step, reweight the qq H events
os.chdir(cwd + '/code/SubProcesses/P2_ug_h1h1u')
shutil.copy(cwd + '/code/SubProcesses/P1_gg_h1h1g/events.lhe.rwgt1.rwgt2', '.')
os.system('echo -e events.lhe.rwgt1.rwgt2 "\n 1\n"  | ./reweight_FT4')
os.remove('events.lhe.rwgt1.rwgt2')

#finally compute scale and PDF uncertainties
os.chdir(cwd + '/' +process_dir + '/SubProcesses/P0_gg_h1h1/')
shutil.copy(cwd + '/code/SubProcesses/P2_ug_h1h1u/events.lhe.rwgt1.rwgt2.rwgt3', '.')
os.system('echo -e events.lhe.rwgt1.rwgt2.rwgt3 "\n 1\n"  | ./reweight_xsec_events')
os.remove('events.lhe.rwgt1.rwgt2.rwgt3')

shutil.move('events.lhe.rwgt1.rwgt2.rwgt3.rwgt', 'events.lhe.rwgt')

os.chdir(cwd + '/' +process_dir + '/Events/' + runName)
shutil.copy(cwd + '/' +process_dir + '/SubProcesses/P0_gg_h1h1/events.lhe.rwgt', '.')

# calculate xsec
os.chdir(cwd + '/' + process_dir + '/Events/' + runName)
shutil.move(cwd + '/' + process_dir + '/SubProcesses/P0_gg_h1h1/rwgt.dat', '.')
shutil.copy(cwd + '/' + process_dir + '/Events/xsec.py', '.')
os.system('python xsec.py > xsec_after_reweighting.dat')
os.system('python xsec.py')

# finalise and cleanup cruft
os.chdir(cwd + '/' + process_dir + '/Events/' + runName)
os.system('rename .lhe .lhe.before_rwgt *.lhe')
os.system('rename .lhe.rwgt .lhe *.lhe.rwgt')
os.remove(cwd + '/' + process_dir + '/SubProcesses/P0_gg_h1h1/events.lhe.rwgt')
os.chdir(cwd + '/code/SubProcesses/P0_gg_h1h1')
for file in glob.glob("events*"):
    os.remove(file)
os.chdir(cwd + '/code/SubProcesses/P1_gg_h1h1g/')
for file in glob.glob("events*"):
    os.remove(file)
os.chdir(cwd + '/code/SubProcesses/P2_ug_h1h1u/')
for file in glob.glob("events*"):
    os.remove(file)

os.chdir(cwd + '/' + process_dir + '/Events/' + runName)
os.system('gzip events.lhe')
os.chdir(cwd)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
## To modify Higgs BR
cmds = """
create ThePEG::ParticleData h2
setup h2 35  h2 """
cmds += str(m_h2)
cmds += """ 1.0 10.0 1.973269631e-13 2 3 2 0
set /Herwig/EventHandlers/LHEReader:AllowedToReOpen 0
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
"""

from Herwigpp_i import config as hw
genSeq.Herwigpp.Commands += cmds.splitlines()
genSeq.Herwigpp.Commands += cmdsps.splitlines()
del cmds
del cmdsps

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description += ", h2 is a " +str(m_h2)+ " scalar."

evgenConfig.contact = ['Biagio Di Micco <biagio.di.micco@cern.ch>', 'Julian Wollrath <wollrath@cern.ch>']

evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
