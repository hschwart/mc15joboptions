# based on the JobOptions MC15.361596 and MC15.429306
# using LHE files from mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.evgen.TXT.e4711

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox/Herwig7 Diboson ZZ->llqq production mllmin20 mqqmin20"
evgenConfig.keywords       = ['electroweak', 'diboson', 'ZZ', '2lepton', '2jet']
evgenConfig.contact        = ['james.robinson@cern.ch', 'christian.johnson@cern.ch','carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'daniel.rauch@desy.de']
evgenConfig.minevents      = 5000
evgenConfig.inputfilecheck = "TXT"

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()
