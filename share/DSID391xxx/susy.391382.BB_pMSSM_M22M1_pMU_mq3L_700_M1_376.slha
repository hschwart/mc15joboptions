#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14500254E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.75990000E+02   # M_1(MX)             
         2     7.51990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04045941E+01   # W+
        25     1.24017955E+02   # h
        35     3.00016312E+03   # H
        36     2.99999973E+03   # A
        37     3.00092865E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03331640E+03   # ~d_L
   2000001     3.02833997E+03   # ~d_R
   1000002     3.03240543E+03   # ~u_L
   2000002     3.02975808E+03   # ~u_R
   1000003     3.03331640E+03   # ~s_L
   2000003     3.02833997E+03   # ~s_R
   1000004     3.03240543E+03   # ~c_L
   2000004     3.02975808E+03   # ~c_R
   1000005     7.96647677E+02   # ~b_1
   2000005     3.02755669E+03   # ~b_2
   1000006     7.94811835E+02   # ~t_1
   2000006     3.01382107E+03   # ~t_2
   1000011     3.00625339E+03   # ~e_L
   2000011     3.00169680E+03   # ~e_R
   1000012     3.00486456E+03   # ~nu_eL
   1000013     3.00625339E+03   # ~mu_L
   2000013     3.00169680E+03   # ~mu_R
   1000014     3.00486456E+03   # ~nu_muL
   1000015     2.98552643E+03   # ~tau_1
   2000015     3.02204328E+03   # ~tau_2
   1000016     3.00476589E+03   # ~nu_tauL
   1000021     2.34124609E+03   # ~g
   1000022     3.78601588E+02   # ~chi_10
   1000023     7.84546518E+02   # ~chi_20
   1000025    -2.99838555E+03   # ~chi_30
   1000035     2.99936717E+03   # ~chi_40
   1000024     7.84707673E+02   # ~chi_1+
   1000037     2.99987241E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99882726E-01   # N_11
  1  2    -6.98047976E-04   # N_12
  1  3     1.50716308E-02   # N_13
  1  4    -2.62531399E-03   # N_14
  2  1     1.14093713E-03   # N_21
  2  2     9.99574216E-01   # N_22
  2  3    -2.79518653E-02   # N_23
  2  4     8.29324501E-03   # N_24
  3  1    -8.78872948E-03   # N_31
  3  2     1.39101530E-02   # N_32
  3  3     7.06882508E-01   # N_33
  3  4     7.07139580E-01   # N_34
  4  1    -1.24895995E-02   # N_41
  4  2     2.56399454E-02   # N_42
  4  3     7.06617760E-01   # N_43
  4  4    -7.07020470E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99217916E-01   # U_11
  1  2    -3.95418226E-02   # U_12
  2  1     3.95418226E-02   # U_21
  2  2     9.99217916E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99931163E-01   # V_11
  1  2    -1.17332413E-02   # V_12
  2  1     1.17332413E-02   # V_21
  2  2     9.99931163E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98841790E-01   # cos(theta_t)
  1  2    -4.81152632E-02   # sin(theta_t)
  2  1     4.81152632E-02   # -sin(theta_t)
  2  2     9.98841790E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99913696E-01   # cos(theta_b)
  1  2     1.31377529E-02   # sin(theta_b)
  2  1    -1.31377529E-02   # -sin(theta_b)
  2  2     9.99913696E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06956384E-01   # cos(theta_tau)
  1  2     7.07257146E-01   # sin(theta_tau)
  2  1    -7.07257146E-01   # -sin(theta_tau)
  2  2     7.06956384E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01866221E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.45002539E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44347957E+02   # higgs               
         4     7.18232293E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.45002539E+03  # The gauge couplings
     1     3.62060065E-01   # gprime(Q) DRbar
     2     6.36845718E-01   # g(Q) DRbar
     3     1.02710348E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.45002539E+03  # The trilinear couplings
  1  1     1.99283017E-06   # A_u(Q) DRbar
  2  2     1.99285836E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.45002539E+03  # The trilinear couplings
  1  1     5.49514594E-07   # A_d(Q) DRbar
  2  2     5.49607736E-07   # A_s(Q) DRbar
  3  3     1.17848779E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.45002539E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10294963E-07   # A_mu(Q) DRbar
  3  3     1.11579505E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.45002539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52552353E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.45002539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15953045E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.45002539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07572891E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.45002539E+03  # The soft SUSY breaking masses at the scale Q
         1     3.75990000E+02   # M_1(Q)              
         2     7.51990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.66527518E+04   # M^2_Hd              
        22    -9.06863062E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40761831E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.39297734E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48232228E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48232228E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51767772E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51767772E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.11397972E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.31826577E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.81734227E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01999761E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.28517038E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.74827803E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.53641378E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.78056275E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.30080438E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.59317861E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50118311E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.96465412E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.56014287E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.59129329E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.08706707E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.23036470E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.82915199E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.28849238E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.28629425E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.22132450E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.02497969E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27540245E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.97823860E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.28287722E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.15317697E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.63335104E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.07374048E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.53446311E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52148096E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.19240766E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.06961241E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.56163498E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.33518615E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16648994E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55715344E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.82598864E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.13097030E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.16393562E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44284456E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.63872177E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.16093319E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.53249827E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.58878445E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.91903804E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.06395995E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.06642443E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.34192833E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66841259E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.41918415E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.18160007E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.59181231E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.01295461E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55808102E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.63335104E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.07374048E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.53446311E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52148096E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.19240766E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.06961241E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.56163498E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.33518615E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16648994E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55715344E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.82598864E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.13097030E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.16393562E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44284456E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.63872177E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.16093319E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.53249827E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.58878445E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.91903804E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.06395995E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.06642443E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.34192833E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66841259E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.41918415E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.18160007E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.59181231E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.01295461E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55808102E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.53541211E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07100435E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98044158E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.44801780E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.46898406E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.94855385E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.92911802E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51585531E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998832E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.16706194E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.87894600E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.87873950E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.53541211E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07100435E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98044158E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.44801780E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.46898406E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.94855385E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.92911802E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51585531E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998832E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.16706194E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.87894600E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.87873950E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.55083935E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.71067208E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.09946320E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.18986472E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.49806173E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.79815681E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07031880E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.23110433E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.37206787E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.13112498E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.39083867E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.53547618E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07572919E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97088658E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.27441109E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.91185728E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.95338415E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.04057940E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.53547618E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07572919E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97088658E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.27441109E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.91185728E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.95338415E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.04057940E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.53566153E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.07563520E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97060474E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.20569291E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.73718114E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.95375262E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.37405214E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.29713364E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.50967097E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.83557122E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.17986485E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.30351798E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.98648242E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.19234871E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.80531852E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.38157764E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.09119169E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.67682847E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.43231715E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.52242530E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.51316116E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.37946676E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.78347870E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.78347870E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.74207002E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.61054495E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.62679976E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.62679976E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.30978940E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.30978940E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.44050358E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.44050358E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44848019E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.56042363E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.60127041E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.84762951E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.84762951E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.58651357E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.80309276E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.59417868E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.59417868E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.33705908E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.33705908E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.50394846E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.50394846E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.71560061E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87327187E-01    2           5        -5   # BR(h -> b       bb     )
     5.49304253E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94459072E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.13339710E-04    2           3        -3   # BR(h -> s       sb     )
     1.78390783E-02    2           4        -4   # BR(h -> c       cb     )
     5.72597262E-02    2          21        21   # BR(h -> g       g      )
     1.92930617E-03    2          22        22   # BR(h -> gam     gam    )
     1.21250797E-03    2          22        23   # BR(h -> Z       gam    )
     1.59288173E-01    2          24       -24   # BR(h -> W+      W-     )
     1.96057970E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39412738E+01   # H decays
#          BR         NDA      ID1       ID2
     7.45230254E-01    2           5        -5   # BR(H -> b       bb     )
     1.78376014E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30694959E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.74492476E-04    2           3        -3   # BR(H -> s       sb     )
     2.18586354E-07    2           4        -4   # BR(H -> c       cb     )
     2.18054016E-02    2           6        -6   # BR(H -> t       tb     )
     2.96253874E-05    2          21        21   # BR(H -> g       g      )
     5.38115054E-08    2          22        22   # BR(H -> gam     gam    )
     8.38916639E-09    2          23        22   # BR(H -> Z       gam    )
     3.29671324E-05    2          24       -24   # BR(H -> W+      W-     )
     1.64632495E-05    2          23        23   # BR(H -> Z       Z      )
     8.47756875E-05    2          25        25   # BR(H -> h       h      )
     6.55574499E-24    2          36        36   # BR(H -> A       A      )
     9.05688184E-18    2          23        36   # BR(H -> Z       A      )
     1.71081357E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.05669604E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.53420564E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.22312184E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.97207252E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.08926682E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32661896E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83219941E-01    2           5        -5   # BR(A -> b       bb     )
     1.87447478E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62768562E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14017427E-04    2           3        -3   # BR(A -> s       sb     )
     2.29704180E-07    2           4        -4   # BR(A -> c       cb     )
     2.29018548E-02    2           6        -6   # BR(A -> t       tb     )
     6.74437766E-05    2          21        21   # BR(A -> g       g      )
     5.89799607E-08    2          22        22   # BR(A -> gam     gam    )
     6.64780488E-08    2          23        22   # BR(A -> Z       gam    )
     3.45143029E-05    2          23        25   # BR(A -> Z       h      )
     2.62665952E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22812019E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31012463E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92031049E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01247106E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09963009E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45684432E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.68680225E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.03762152E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10504736E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05027240E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.15731985E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.52936643E-05    2          24        25   # BR(H+ -> W+      h      )
     8.98890332E-14    2          24        36   # BR(H+ -> W+      A      )
     1.89153435E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.32561984E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.35700819E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
