#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 0.75*top mass, A14 tune, at least one lepton + at least one soft muon filter, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'simone.amoroso@cern.ch','marco.vanadia@cern.ch']
evgenConfig.minevents = 1000

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
PowhegConfig.topdecaymode = 22222                                         # Inclusive
PowhegConfig.hdamp        = 129.375                                        # 0.75 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118 - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260001, 260101))                          # Include the NNPDF error set
#PowhegConfig.PDF.extend(range(25201, 25251))                            # Include the MMHT2014nlo68clas118 error set
PowhegConfig.PDF.extend(range(90901, 90931))                            # Include the PDF4LHC15_nlo_30 error set

#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Define a weight group configuration for scale variations with different PDFs
# Nominal mu_F = mu_R = 1.0 is not required as this is captured by the PDF variation above
PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',                   parameter_values=[ 2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',                 parameter_values=[ 0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',                   parameter_values=[ 1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',                 parameter_values=[ 1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',          parameter_values=[ 0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',              parameter_values=[ 2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',            parameter_values=[ 0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',            parameter_values=[ 2.0, 0.5, 25200] )

## Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',                   parameter_values=[ 2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',                 parameter_values=[ 0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',                   parameter_values=[ 1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',                 parameter_values=[ 1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',          parameter_values=[ 0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',              parameter_values=[ 2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',            parameter_values=[ 0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',            parameter_values=[ 2.0, 0.5, 13165] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC15_NLO_30',              parameter_values=[ 2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC15_NLO_30',            parameter_values=[ 0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC15_NLO_30',              parameter_values=[ 1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC15_NLO_30',            parameter_values=[ 1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC15_NLO_30',     parameter_values=[ 0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC15_NLO_30',         parameter_values=[ 2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC15_NLO_30',       parameter_values=[ 0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC15_NLO_30',       parameter_values=[ 2.0, 0.5, 90900] )

# Scale variations, NNPDF30_nlo_as_0117
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF_NLO_0117',                   parameter_values=[ 2.0, 1.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF_NLO_0117',                 parameter_values=[ 0.5, 1.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF_NLO_0117',                   parameter_values=[ 1.0, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF_0117',                     parameter_values=[ 1.0, 0.5, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF_NLO_0117',          parameter_values=[ 0.5, 0.5, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF_NLO_0117',              parameter_values=[ 2.0, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF_NLO_0117',            parameter_values=[ 0.5, 2.0, 265000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF_NLO_0117',            parameter_values=[ 2.0, 0.5, 265000] )

# Scale variations, NNPDF30_nlo_as_0119
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF_NLO_0119',                  parameter_values=[ 2.0, 1.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF_NLO_0119',                 parameter_values=[ 0.5, 1.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF_NLO_0119',                   parameter_values=[ 1.0, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF_NLO_0119',                 parameter_values=[ 1.0, 0.5, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF_NLO_0119',          parameter_values=[ 0.5, 0.5, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF_NLO_0119',              parameter_values=[ 2.0, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF_NLO_0119',            parameter_values=[ 0.5, 2.0, 266000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF_NLO_0119',            parameter_values=[ 2.0, 0.5, 266000] )

# Scale variations, NNPDF31_nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF31_NLO_0118',                  parameter_values=[ 2.0, 1.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF31_NLO_0118',                 parameter_values=[ 0.5, 1.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF31_NLO_0118',                   parameter_values=[ 1.0, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF31_NLO_0118',                 parameter_values=[ 1.0, 0.5, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF31_NLO_0118',          parameter_values=[ 0.5, 0.5, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF31_NLO_0118',              parameter_values=[ 2.0, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF31_NLO_0118',            parameter_values=[ 0.5, 2.0, 303400] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF31_NLO_0118',            parameter_values=[ 2.0, 0.5, 303400] )


PowhegConfig.nEvents     *= 15. # to compensate for the filter efficiency
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]
#rB value fitted at LEP=1.05
genSeq.Pythia8.Commands += [ 'StringZ:rFactB = 1.05' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 20000.

from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter

filtSeq += TTbarWToLeptonFilter("onetWlep")
filtSeq.onetWlep.NumLeptons = 1
filtSeq.onetWlep.Ptcut = 0.

filtSeq += TTbarWToLeptonFilter("twotWlep")
filtSeq.twotWlep.NumLeptons = 2
filtSeq.twotWlep.Ptcut = 0.

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("Wmu")
filtSeq.Wmu.PDGParent  = [24]
filtSeq.Wmu.PDGChild = [13]

filtSeq += ParentChildFilter("Wetau")
filtSeq.Wetau.PDGParent  = [24]
filtSeq.Wetau.PDGChild = [11,15]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("OneMuonFilter")
filtSeq.OneMuonFilter.Ptcut = 3250.
filtSeq.OneMuonFilter.Etacut = 2.8
filtSeq.OneMuonFilter.NMuons = 1

filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 2.8
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 3250.
filtSeq.ThreeMuonsFilter.Etacut = 2.8
filtSeq.ThreeMuonsFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter("ElectronFilter")
filtSeq.ElectronFilter.Ptcut = 20000.
filtSeq.ElectronFilter.Etacut = 2.8

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 20000.
filtSeq.MuonFilter.Etacut = 2.8

filtSeq.Expression="TTbarWToLeptonFilter and (ElectronFilter or MuonFilter) and ((onetWlep and ((Wmu and TwoMuonsFilter) or (Wetau and OneMuonFilter))) or (twotWlep and ( ((not Wmu) and OneMuonFilter) or (Wmu and Wetau and TwoMuonsFilter) or (Wmu and (not Wetau) and ThreeMuonsFilter) )) )"


