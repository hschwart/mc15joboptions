import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=70
ihtmax=140
HTrange='lowHT'
include('MC15JobOptions/MadGraphControl_Wjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=25

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Wtaunu_Ht70_140_13TeV"
