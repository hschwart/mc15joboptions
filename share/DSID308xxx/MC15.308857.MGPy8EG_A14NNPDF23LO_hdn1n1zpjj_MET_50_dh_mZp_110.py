model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 110.
mHD = 110.
widthZp = 4.376743e-01
widthhd = 4.358638e-02
filteff = 6.639225E-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
