evgenConfig.description = "Herwig Z->had  pT > 1400 GeV"
evgenConfig.keywords = ["SM","Z","jets"]
evgenConfig.process = "Z + jets (Z -> qqbar)"
evgenConfig.contact  = ["craig.sawyer@cern.ch"]
	
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEZJet
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:ZDecay Quarks

set /Herwig/Cuts/ZBosonKtCut:MinKT 1400.*GeV
"""

genSeq.Herwigpp.Commands += cmds.splitlines()
