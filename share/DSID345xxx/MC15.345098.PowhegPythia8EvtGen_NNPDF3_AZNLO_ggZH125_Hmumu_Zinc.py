#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z-> Zinc + Hmumu production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# H->mumu decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z production: Z->all, H->mumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs","2muon" ]
evgenConfig.contact     = [ 'paul.daniel.thompson@cern.ch' ]
evgenConfig.inputfilecheck = 'TXT'
evgenConfig.process = "gg->ZH, H->mumu, Z->all"
evgenConfig.minevents   = 2000
