#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.34300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.35000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05412040E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401669E+03   # H
        36     2.00000000E+03   # A
        37     2.00150792E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994927E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994927E+03   # ~c_R
   1000005     4.99953288E+03   # ~b_1
   2000005     5.00062679E+03   # ~b_2
   1000006     4.98853206E+03   # ~t_1
   2000006     5.01589817E+03   # ~t_2
   1000011     5.00008219E+03   # ~e_L
   2000011     5.00007610E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008219E+03   # ~mu_L
   2000013     5.00007610E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99971931E+03   # ~tau_1
   2000015     5.00043958E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.30109638E+03   # ~chi_10
   1000023    -1.35008360E+03   # ~chi_20
   1000025     1.38829401E+03   # ~chi_30
   1000035     3.00369322E+03   # ~chi_40
   1000024     1.34641977E+03   # ~chi_1+
   1000037     3.00369079E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.20513517E-01   # N_11
  1  2    -3.15913894E-02   # N_12
  1  3     4.91631433E-01   # N_13
  1  4    -4.88017203E-01   # N_14
  2  1     3.24167859E-03   # N_21
  2  2    -3.56751607E-03   # N_22
  2  3    -7.06989089E-01   # N_23
  2  4    -7.07208026E-01   # N_24
  3  1     6.93432190E-01   # N_31
  3  2     3.46419789E-02   # N_32
  3  3    -5.07471384E-01   # N_33
  3  4     5.10318063E-01   # N_34
  4  1     1.24965759E-03   # N_41
  4  2    -9.98893983E-01   # N_42
  4  3    -3.06228074E-02   # N_43
  4  4     3.56579834E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.32754883E-02   # U_11
  1  2     9.99063177E-01   # U_12
  2  1     9.99063177E-01   # U_21
  2  2     4.32754883E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.03972615E-02   # V_11
  1  2     9.98729251E-01   # V_12
  2  1     9.98729251E-01   # V_21
  2  2     5.03972615E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07840162E-01   # cos(theta_t)
  1  2     7.06372639E-01   # sin(theta_t)
  2  1    -7.06372639E-01   # -sin(theta_t)
  2  2     7.07840162E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.71446609E-01   # cos(theta_b)
  1  2     7.41052934E-01   # sin(theta_b)
  2  1    -7.41052934E-01   # -sin(theta_b)
  2  2     6.71446609E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04112102E-01   # cos(theta_tau)
  1  2     7.10088831E-01   # sin(theta_tau)
  2  1    -7.10088831E-01   # -sin(theta_tau)
  2  2     7.04112102E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202351E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.35000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52181257E+02   # vev(Q)              
         4     3.38971804E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52770937E-01   # gprime(Q) DRbar
     2     6.27099640E-01   # g(Q) DRbar
     3     1.08619148E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02514325E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71747382E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79793779E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.34300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.00524168E+06   # M^2_Hd              
        22    -7.16473921E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37014986E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.21010636E-06   # gluino decays
#          BR         NDA      ID1       ID2
     2.18903413E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.24656991E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.38453353E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.19945200E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.61003619E-11    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.35898394E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     3.60304560E-05    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.02519714E-11    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     9.15143241E-10    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.19945200E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.61003619E-11    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.35898394E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     3.60304560E-05    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.02519714E-11    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     9.15143241E-10    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.13995094E-05    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.39335277E-08    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.56906798E-11    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.58105961E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.58105961E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.58105961E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.58105961E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.33758905E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.19286364E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.47982543E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.73562002E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.00435814E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.95921500E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.99003689E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.66380809E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.25777686E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.26709710E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.37485186E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.55936879E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.37776042E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.90714579E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.73496677E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67788093E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.57776047E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.17204083E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.28363819E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.11442386E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.27873250E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01117335E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.57853391E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.53950700E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.70775277E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.22375449E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.31921109E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.63287263E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.63592266E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32937451E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.29950261E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.12758477E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43939922E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.21756468E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.83454117E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.78988317E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.16359625E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.38640596E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.32808590E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02232315E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29248785E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.09332826E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.19022164E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.90008441E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.96558183E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60065425E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43944181E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.87181082E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.16127832E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.00603773E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.16700472E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.49706012E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.33319512E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02274720E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22387106E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.39489923E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07989904E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.89687686E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.64308679E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89708108E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43939922E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.21756468E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.83454117E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.78988317E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.16359625E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.38640596E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.32808590E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02232315E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29248785E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.09332826E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.19022164E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.90008441E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.96558183E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60065425E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43944181E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.87181082E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.16127832E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.00603773E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.16700472E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.49706012E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.33319512E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02274720E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22387106E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.39489923E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07989904E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.89687686E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.64308679E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89708108E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.93526875E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.08939978E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.74183779E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02386097E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71207289E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.14700756E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43363867E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.13118324E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.24186707E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.04927239E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.75802059E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.41061012E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.93526875E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.08939978E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.74183779E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02386097E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71207289E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.14700756E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43363867E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.13118324E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.24186707E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.04927239E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.75802059E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.41061012E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.53771724E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.74469091E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47478737E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.55690882E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55094448E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.42309613E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.10733271E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.41733102E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.53738510E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59874125E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.52127911E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.62908015E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.58622325E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.47931228E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.17795475E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.93534697E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10548207E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.66442993E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.17005008E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.71921961E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.91155169E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.42901136E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.93534697E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10548207E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.66442993E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.17005008E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.71921961E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.91155169E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.42901136E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.93810986E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10444251E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.66286476E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.16330762E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71666255E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.84777599E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42392013E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22292828E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.05853234E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.27271513E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.25700552E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09090633E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09087691E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08264287E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.70829618E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53186372E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11517142E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46285271E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35309017E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53694635E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.56186781E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     7.53596679E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99144637E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89710617E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.11447457E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.46037996E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.79691419E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.09550460E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     6.69478770E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.64399730E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.16612128E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.50994561E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15849704E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.50974044E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37957518E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.44843833E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.44831859E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.41464394E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.88671107E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.88671107E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.88671107E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.66603035E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.66603035E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.09870574E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.09870574E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.55343485E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.55343485E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.53045891E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.53045891E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.75619290E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.75619290E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.15071104E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.84676314E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.25658352E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.06582725E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.36806082E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.67760425E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17770816E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.40590290E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.57088027E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.41326094E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.80431510E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.47340075E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.47337238E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.61497907E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.86096168E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.86096168E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.86096168E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.31104938E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.99245343E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.28834448E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.99180764E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.58842192E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.83422713E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.83388477E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.73723715E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36482257E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36482257E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36482257E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31398666E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31398666E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30657889E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30657889E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37994905E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37994905E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37981016E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37981016E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.34100097E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.34100097E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.70831633E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.70447831E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51583008E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.79891855E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46431203E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46431203E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14136187E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.42580538E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38534691E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.81257320E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.74841987E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.96792209E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     7.37821466E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.18402237E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08510536E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17711257E-01    2           5        -5   # BR(h -> b       bb     )
     6.37577266E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25678871E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78233885E-04    2           3        -3   # BR(h -> s       sb     )
     2.06320125E-02    2           4        -4   # BR(h -> c       cb     )
     6.69924075E-02    2          21        21   # BR(h -> g       g      )
     2.30413486E-03    2          22        22   # BR(h -> gam     gam    )
     1.53772743E-03    2          22        23   # BR(h -> Z       gam    )
     2.00740026E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56207959E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78103099E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47091731E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427086E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71211493E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546850E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668043E-05    2           4        -4   # BR(H -> c       cb     )
     9.96063901E-01    2           6        -6   # BR(H -> t       tb     )
     7.97666169E-04    2          21        21   # BR(H -> g       g      )
     2.71142849E-06    2          22        22   # BR(H -> gam     gam    )
     1.16005936E-06    2          23        22   # BR(H -> Z       gam    )
     3.34728932E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66907981E-04    2          23        23   # BR(H -> Z       Z      )
     9.03526200E-04    2          25        25   # BR(H -> h       h      )
     7.43133973E-24    2          36        36   # BR(H -> A       A      )
     2.98380922E-11    2          23        36   # BR(H -> Z       A      )
     6.91044780E-12    2          24       -37   # BR(H -> W+      H-     )
     6.91044780E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382319E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47382050E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897689E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266438E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677381E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176356E-06    2           4        -4   # BR(A -> c       cb     )
     9.96995864E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675527E-04    2          21        21   # BR(A -> g       g      )
     3.17290922E-06    2          22        22   # BR(A -> gam     gam    )
     1.35263702E-06    2          23        22   # BR(A -> Z       gam    )
     3.26255561E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472495E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36377662E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237210E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143572E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50282843E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693527E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731505E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402201E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34373892E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73796089E-13    2          24        36   # BR(H+ -> W+      A      )
