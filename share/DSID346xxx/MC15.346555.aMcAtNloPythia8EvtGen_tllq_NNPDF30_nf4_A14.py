evgenConfig.description = 'aMC@NLO+Pythia8  single top + Z production with Pythia A14 tune 4l filter'
evgenConfig.keywords    = [ 'SM','singleTop','tZ','lepton','NLO']
evgenConfig.contact     = ['syed.haider.abidi@cern.ch' ]

#Common MG_Control file
include("MC15JobOptions/MadGraphControl_tllq_NLO_Pythia8_A14.py")

#Below is showering
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut    = 8000.
filtSeq.MultiLeptonFilter.Etacut   = 3.
filtSeq.MultiLeptonFilter.NLeptons = 3

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt           = 4000.
filtSeq.FourLeptonMassFilter.MaxEta          = 4.
filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu     = False
filtSeq.FourLeptonMassFilter.AllowSameCharge = False
