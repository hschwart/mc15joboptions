#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10035004E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.06900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181452E+01   # W+
        25     1.24911699E+02   # h
        35     4.00000664E+03   # H
        36     3.99999608E+03   # A
        37     4.00100359E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01653473E+03   # ~d_L
   2000001     4.01508295E+03   # ~d_R
   1000002     4.01587519E+03   # ~u_L
   2000002     4.01632673E+03   # ~u_R
   1000003     4.01653473E+03   # ~s_L
   2000003     4.01508295E+03   # ~s_R
   1000004     4.01587519E+03   # ~c_L
   2000004     4.01632673E+03   # ~c_R
   1000005     5.52181969E+02   # ~b_1
   2000005     4.02022888E+03   # ~b_2
   1000006     5.37443867E+02   # ~t_1
   2000006     2.02382972E+03   # ~t_2
   1000011     4.00280050E+03   # ~e_L
   2000011     4.00293664E+03   # ~e_R
   1000012     4.00168145E+03   # ~nu_eL
   1000013     4.00280050E+03   # ~mu_L
   2000013     4.00293664E+03   # ~mu_R
   1000014     4.00168145E+03   # ~nu_muL
   1000015     4.00442847E+03   # ~tau_1
   2000015     4.00806375E+03   # ~tau_2
   1000016     4.00393900E+03   # ~nu_tauL
   1000021     1.96406297E+03   # ~g
   1000022     2.52240025E+02   # ~chi_10
   1000023    -3.16925522E+02   # ~chi_20
   1000025     3.26743907E+02   # ~chi_30
   1000035     2.05620990E+03   # ~chi_40
   1000024     3.14314752E+02   # ~chi_1+
   1000037     2.05637416E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.91096095E-01   # N_11
  1  2    -1.16266138E-02   # N_12
  1  3    -3.56591858E-01   # N_13
  1  4    -2.80454661E-01   # N_14
  2  1    -5.75089725E-02   # N_21
  2  2     2.50017112E-02   # N_22
  2  3    -7.02262964E-01   # N_23
  2  4     7.09150450E-01   # N_24
  3  1     4.50154959E-01   # N_31
  3  2     2.84064729E-02   # N_32
  3  3     6.16159242E-01   # N_33
  3  4     6.45679002E-01   # N_34
  4  1    -9.89837672E-04   # N_41
  4  2     9.99216097E-01   # N_42
  4  3    -4.09430031E-03   # N_43
  4  4    -3.93630326E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.79570342E-03   # U_11
  1  2     9.99983205E-01   # U_12
  2  1    -9.99983205E-01   # U_21
  2  2     5.79570342E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.56797974E-02   # V_11
  1  2    -9.98448677E-01   # V_12
  2  1    -9.98448677E-01   # V_21
  2  2    -5.56797974E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95948346E-01   # cos(theta_t)
  1  2    -8.99271488E-02   # sin(theta_t)
  2  1     8.99271488E-02   # -sin(theta_t)
  2  2     9.95948346E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999541E-01   # cos(theta_b)
  1  2    -9.58123055E-04   # sin(theta_b)
  2  1     9.58123055E-04   # -sin(theta_b)
  2  2     9.99999541E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05476620E-01   # cos(theta_tau)
  1  2     7.08733193E-01   # sin(theta_tau)
  2  1    -7.08733193E-01   # -sin(theta_tau)
  2  2    -7.05476620E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00263264E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00350044E+03  # DRbar Higgs Parameters
         1    -3.06900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44347876E+02   # higgs               
         4     1.62401120E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00350044E+03  # The gauge couplings
     1     3.61088575E-01   # gprime(Q) DRbar
     2     6.36006437E-01   # g(Q) DRbar
     3     1.03613480E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00350044E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.51586019E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00350044E+03  # The trilinear couplings
  1  1     2.76699950E-07   # A_d(Q) DRbar
  2  2     2.76726539E-07   # A_s(Q) DRbar
  3  3     4.93916377E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00350044E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.10888898E-08   # A_mu(Q) DRbar
  3  3     6.17070331E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00350044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72117513E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00350044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84838636E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00350044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03215465E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00350044E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57798973E+07   # M^2_Hd              
        22    -1.05615933E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40331819E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.00525544E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45185632E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45185632E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54814368E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54814368E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.20016683E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.48948972E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.73421498E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.08737106E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.68892424E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16418835E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.99805554E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20359381E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.03267777E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.38169211E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.47854041E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.09991364E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.30377671E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.70660623E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.36167975E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.21674109E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.26557896E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.31560002E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66587101E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51263618E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77479536E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63082126E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.99556802E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.57935451E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18305764E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14545522E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.04425330E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.38511069E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.33004167E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.81675465E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78186706E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.13691445E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.72027217E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.11286796E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77289583E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.35235965E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53363453E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53132475E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60898006E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.36848467E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.81160474E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.10914668E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.97425776E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45042496E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78206739E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81689521E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11145037E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.78093551E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77783660E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.79946325E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.56568569E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53352844E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54159089E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13949726E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.72546716E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.89314421E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.75487399E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85664621E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78186706E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.13691445E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.72027217E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.11286796E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77289583E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.35235965E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53363453E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53132475E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60898006E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.36848467E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.81160474E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.10914668E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.97425776E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45042496E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78206739E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81689521E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11145037E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.78093551E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77783660E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.79946325E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.56568569E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53352844E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54159089E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13949726E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.72546716E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.89314421E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.75487399E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85664621E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13435081E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.24549703E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96855891E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.08888741E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77694048E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.40964795E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56803593E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05791954E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.94934309E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.29569773E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.01769458E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.35776861E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13435081E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.24549703E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96855891E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.08888741E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77694048E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.40964795E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56803593E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05791954E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.94934309E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.29569773E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.01769458E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.35776861E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08825752E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.85710646E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35713755E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.55666357E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39886904E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.47768384E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80487601E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08703807E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.97944708E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25028063E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60477120E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42602590E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05422516E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85930524E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13542621E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.36458363E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68551857E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.61484498E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78034690E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14498877E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54527990E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13542621E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.36458363E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68551857E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.61484498E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78034690E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14498877E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54527990E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46422052E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.23577685E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52642586E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.36803715E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51991676E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.66372051E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02586637E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.74556975E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33471793E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33471793E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11158469E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11158469E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10739475E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.48887594E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.82944868E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.70101021E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.71957032E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.23650782E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.25669483E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.09560331E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.23736435E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.11465730E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.39235027E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.77479789E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17596512E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52491264E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17596512E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52491264E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45226298E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49392173E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49392173E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47066349E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98448475E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98448475E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98448440E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.96995850E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.96995850E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.96995850E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.96995850E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.56653558E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.56653558E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.56653558E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.56653558E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.27775994E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.27775994E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.99687470E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.07549370E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.28255908E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11799601E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.44670853E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11799601E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.44670853E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.39870316E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.29040003E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.29040003E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.28119829E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.63717238E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.63717238E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.63716181E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.08062225E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.29170083E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.08062225E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.29170083E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     8.05972214E-07    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.21258678E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.21258678E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.90709171E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.42356141E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.42356141E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.42356144E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.48198249E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.48198249E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.48198249E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.48198249E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.49398171E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.49398171E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.49398171E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.49398171E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.35242953E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.35242953E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.48782780E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.76484315E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.77415872E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.08501442E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.08498881E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.08498881E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.60324468E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.42295227E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.21610653E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88173895E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.88173895E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.89301014E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.89301014E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.94391014E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02782425E-01    2           5        -5   # BR(h -> b       bb     )
     6.57313827E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32691441E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93955258E-04    2           3        -3   # BR(h -> s       sb     )
     2.14535240E-02    2           4        -4   # BR(h -> c       cb     )
     6.94505839E-02    2          21        21   # BR(h -> g       g      )
     2.38182144E-03    2          22        22   # BR(h -> gam     gam    )
     1.57292954E-03    2          22        23   # BR(h -> Z       gam    )
     2.09654403E-01    2          24       -24   # BR(h -> W+      W-     )
     2.62462833E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52164877E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55749709E-01    2           5        -5   # BR(H -> b       bb     )
     6.00472828E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12312648E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51129260E-04    2           3        -3   # BR(H -> s       sb     )
     7.04356885E-08    2           4        -4   # BR(H -> c       cb     )
     7.05912246E-03    2           6        -6   # BR(H -> t       tb     )
     7.08374767E-07    2          21        21   # BR(H -> g       g      )
     6.85079905E-10    2          22        22   # BR(H -> gam     gam    )
     1.81571815E-09    2          23        22   # BR(H -> Z       gam    )
     1.75097909E-06    2          24       -24   # BR(H -> W+      W-     )
     8.74883049E-07    2          23        23   # BR(H -> Z       Z      )
     6.86823093E-06    2          25        25   # BR(H -> h       h      )
     5.12845113E-24    2          36        36   # BR(H -> A       A      )
     1.31144031E-20    2          23        36   # BR(H -> Z       A      )
     1.85079715E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60339140E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60339140E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.95881270E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.15682622E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.11569749E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.81745195E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.67967994E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.01460017E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.55385863E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.01963918E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.04692440E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.03554511E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.46989759E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.46989759E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42714718E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52142868E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55789532E-01    2           5        -5   # BR(A -> b       bb     )
     6.00499734E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12321994E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51181281E-04    2           3        -3   # BR(A -> s       sb     )
     7.08949133E-08    2           4        -4   # BR(A -> c       cb     )
     7.07303982E-03    2           6        -6   # BR(A -> t       tb     )
     1.45333105E-05    2          21        21   # BR(A -> g       g      )
     5.65709305E-08    2          22        22   # BR(A -> gam     gam    )
     1.59786506E-08    2          23        22   # BR(A -> Z       gam    )
     1.74742130E-06    2          23        25   # BR(A -> Z       h      )
     1.85868374E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60344589E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60344589E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70052165E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.02431415E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.29291405E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.45618108E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.36603775E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07634266E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.05462507E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.84008167E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.16361631E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.58418802E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.58418802E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53010766E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.70850812E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99708321E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12042170E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65344209E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20125947E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47137703E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63471567E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74660835E-06    2          24        25   # BR(H+ -> W+      h      )
     2.47033281E-14    2          24        36   # BR(H+ -> W+      A      )
     6.57396953E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.83092106E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.08137970E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60301608E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.09059136E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58427479E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.22121014E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.58566506E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.03925342E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
