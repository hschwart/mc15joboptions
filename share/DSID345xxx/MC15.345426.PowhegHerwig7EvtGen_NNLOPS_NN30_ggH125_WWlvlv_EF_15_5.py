#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "gg->H->WW*->lvlv"
evgenConfig.description = "POWHEG+HERWIG7+EVTGEN, ggH H->WW->lvlv"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WW", "mH125" ]
evgenConfig.contact     = [ 'kathrin.becker@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents   = 1000
evgenConfig.inputfilecheck = "TXT"


#--------------------------------------------------------------
# Herwig 7 showering setup
# Include fragments that remove the weight lines for Herwig showering
#--------------------------------------------------------------

include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")

from Herwig7_i import config as hw

genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

## only consider H->WW->lvlv decays
genSeq.Herwig7.Commands += [
  '## force H->WW decays',
  'do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;',
  'do /Herwig/Particles/h0:PrintDecayModes', # print out decays modes and branching ratios to the terminal/log.generate
  'do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;',
  'do /Herwig/Particles/W+:PrintDecayModes', 
  'do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;',
  'do /Herwig/Particles/W-:PrintDecayModes' 
]

#--------------------------------------------------------------
# Dilepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"


