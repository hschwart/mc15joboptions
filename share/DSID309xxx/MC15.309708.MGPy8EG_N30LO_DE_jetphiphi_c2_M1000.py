# Inverse of M scale
invMscale=.00100000000000000000

# Wilson coefficients
c1=0.000000e+00
c2=1.000000e+00

evt_multiplier=1
filter_string="MET150"

include("MC15JobOptions/MadGraphControl_DarkEnergy_jetphiphi.py")
