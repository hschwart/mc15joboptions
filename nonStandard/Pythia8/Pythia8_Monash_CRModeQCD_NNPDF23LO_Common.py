## Config for Py8 Monash tune 

include("MC15JobOptions/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
   "Tune:pp=14", # Monash
   "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed", # Equivalent to "13": NNPDF2.3 QCD+QED LO alpha_s(M_Z) = 0.130. 
   "ParticleDecays:limitTau0 = off", # Want Pythia to decay K0S and Lambda particles
   ]

# Enable QCD-inspired colour reconnection
genSeq.Pythia8.Commands += [
   "ColourReconnection:mode = 1",
   "BeamRemnants:remnantMode = 1"]

# "Mode 2" tune from https://arxiv.org/abs/1505.01681
# Note: This is "on top of" and modifies some of the Monash tune
genSeq.Pythia8.Commands += [
   "StringPT:sigma = 0.335",
   "StringZ:aLund = 0.36",
   "StringZ:bLund = 0.56",
   "StringFlav:probQQtoQ = 0.078",
   "StringFlav:ProbStoUD = 0.22",
   "StringFlav:probQQ1toQQ0join = 0.0275,0.0275,0.0275,0.0275",
   #
   "MultiPartonInteractions:pT0Ref = 2.15",
   # 
   "BeamRemnants:saturation = 5", # Note: This is the default
   #
   "ColourReconnection:allowDoubleJunRem = off",
   "ColourReconnection:m0 = 0.3", # Note: This is the default
   "ColourReconnection:allowJunctions = on", # Note: This is the default
   "ColourReconnection:junctionCorrection = 1.2", # Note: This is the default
   "ColourReconnection:timeDilationMode = 2", # Note: This is the default
   "ColourReconnection:timeDilationPar = 0.18", # Note: This is the default
   ]

rel = os.popen("echo $AtlasVersion").read()
print "Atlas release " + rel

evgenConfig.tune = "Monash"

# Disable test of maximum vertex displacement due to having disabled limitTau0 in Pythia
testSeq.TestHepMC.MaxVtxDisp = 1000000000
