evgenConfig.description = "MG Z -> e e + 0,1j@LO with ptll>75 MJJ>800,DPHI<2.5 "
evgenConfig.keywords = ["SM", "Z", "jets", "LO" ]
evgenConfig.contact  = [ "schae@cern.ch"]

include("MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_VBF.py")
evgenConfig.minevents=50
evgenConfig.maxeventsstrategy='IGNORE'
