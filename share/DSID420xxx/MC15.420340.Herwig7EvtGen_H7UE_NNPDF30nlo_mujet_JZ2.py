evgenConfig.description = "Herwig7 multijets with pdf NNPDF30nlo, slice JZ2 with muon"
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords+=['QCD', 'jets', 'muon']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 100

include("MC15JobOptions/Herwig7EvtGen_H7UE_NNPDF30nlo_jetjet.py");
include("MC15JobOptions/JetFilter_JZ2.py")

include("MC15JobOptions/LowPtMuonFilter.py")
