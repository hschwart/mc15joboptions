#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90954504E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.86900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.85485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04190391E+01   # W+
        25     1.25534231E+02   # h
        35     4.00000196E+03   # H
        36     3.99999512E+03   # A
        37     4.00101413E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01296254E+03   # ~d_L
   2000001     4.01227326E+03   # ~d_R
   1000002     4.01230580E+03   # ~u_L
   2000002     4.01348647E+03   # ~u_R
   1000003     4.01296254E+03   # ~s_L
   2000003     4.01227326E+03   # ~s_R
   1000004     4.01230580E+03   # ~c_L
   2000004     4.01348647E+03   # ~c_R
   1000005     4.74685623E+02   # ~b_1
   2000005     4.01820279E+03   # ~b_2
   1000006     4.50354539E+02   # ~t_1
   2000006     1.87502095E+03   # ~t_2
   1000011     4.00202986E+03   # ~e_L
   2000011     4.00300068E+03   # ~e_R
   1000012     4.00091390E+03   # ~nu_eL
   1000013     4.00202986E+03   # ~mu_L
   2000013     4.00300068E+03   # ~mu_R
   1000014     4.00091390E+03   # ~nu_muL
   1000015     4.00440297E+03   # ~tau_1
   2000015     4.00834647E+03   # ~tau_2
   1000016     4.00349238E+03   # ~nu_tauL
   1000021     1.95667440E+03   # ~g
   1000022     1.46998675E+02   # ~chi_10
   1000023    -1.96254162E+02   # ~chi_20
   1000025     2.10102103E+02   # ~chi_30
   1000035     2.05756382E+03   # ~chi_40
   1000024     1.92701188E+02   # ~chi_1+
   1000037     2.05772805E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.19606543E-01   # N_11
  1  2    -1.33259110E-02   # N_12
  1  3    -4.59801725E-01   # N_13
  1  4    -3.41540494E-01   # N_14
  2  1    -9.35299583E-02   # N_21
  2  2     2.64552748E-02   # N_22
  2  3    -6.95963886E-01   # N_23
  2  4     7.11467874E-01   # N_24
  3  1     5.65240126E-01   # N_31
  3  2     2.53419506E-02   # N_32
  3  3     5.51555775E-01   # N_33
  3  4     6.12900981E-01   # N_34
  4  1    -9.28628589E-04   # N_41
  4  2     9.99239873E-01   # N_42
  4  3    -1.69414801E-03   # N_43
  4  4    -3.89351183E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.40062854E-03   # U_11
  1  2     9.99997118E-01   # U_12
  2  1    -9.99997118E-01   # U_21
  2  2     2.40062854E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50756481E-02   # V_11
  1  2    -9.98482185E-01   # V_12
  2  1    -9.98482185E-01   # V_21
  2  2    -5.50756481E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94525360E-01   # cos(theta_t)
  1  2    -1.04495494E-01   # sin(theta_t)
  2  1     1.04495494E-01   # -sin(theta_t)
  2  2     9.94525360E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999832E-01   # cos(theta_b)
  1  2    -5.79655046E-04   # sin(theta_b)
  2  1     5.79655046E-04   # -sin(theta_b)
  2  2     9.99999832E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04235704E-01   # cos(theta_tau)
  1  2     7.09966248E-01   # sin(theta_tau)
  2  1    -7.09966248E-01   # -sin(theta_tau)
  2  2    -7.04235704E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00308653E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.09545038E+02  # DRbar Higgs Parameters
         1    -1.86900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44457420E+02   # higgs               
         4     1.61930389E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.09545038E+02  # The gauge couplings
     1     3.60867633E-01   # gprime(Q) DRbar
     2     6.36400415E-01   # g(Q) DRbar
     3     1.03838530E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.09545038E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.91159329E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.09545038E+02  # The trilinear couplings
  1  1     2.16472517E-07   # A_d(Q) DRbar
  2  2     2.16493509E-07   # A_s(Q) DRbar
  3  3     3.87558276E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.09545038E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.75014685E-08   # A_mu(Q) DRbar
  3  3     4.79807537E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.09545038E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.76172993E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.09545038E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83745639E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.09545038E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03637713E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.09545038E+02  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58320052E+07   # M^2_Hd              
        22    -4.23945531E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.85485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40501819E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.25125956E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44176223E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44176223E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55823777E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55823777E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.12469727E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.51575430E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.91788473E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.01734701E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.54901396E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22943361E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.02751055E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.10677263E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.74520848E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.16437436E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.56815696E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.25227478E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.53114937E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.91674631E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.51100472E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.41722147E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.94580059E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.51259732E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66023574E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65413626E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.74140365E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.51275146E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.31186849E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.56052823E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.49832975E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15143901E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.89477458E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.77229394E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.04720781E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.78721405E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77604554E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75232311E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.72005378E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52322857E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77952285E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.29330529E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54682894E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52924880E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60452520E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71619451E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.83016434E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.76297353E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.61463730E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44725277E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77623619E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47423641E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69933471E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.79077967E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78426690E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00573618E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57859314E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53147146E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53697136E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.69582407E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.26022387E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.59972026E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.81889398E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85578426E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77604554E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75232311E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.72005378E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52322857E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77952285E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.29330529E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54682894E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52924880E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60452520E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71619451E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.83016434E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.76297353E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.61463730E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44725277E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77623619E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47423641E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69933471E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.79077967E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78426690E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00573618E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57859314E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53147146E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53697136E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.69582407E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.26022387E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.59972026E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.81889398E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85578426E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13592647E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04491149E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.61499515E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.11610837E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77538739E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.89846858E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56441630E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06666785E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.72372158E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.73739945E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.18889974E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.68568256E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13592647E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04491149E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.61499515E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.11610837E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77538739E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.89846858E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56441630E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06666785E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.72372158E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.73739945E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.18889974E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.68568256E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09602482E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.53017052E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.15747375E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.32175796E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39404159E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43349134E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79493342E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10284926E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.46566511E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.74170745E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.07063341E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42145360E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.18179750E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84989738E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13701791E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17119838E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23106976E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45207675E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77838658E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10266724E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54186999E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13701791E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17119838E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23106976E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45207675E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77838658E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10266724E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54186999E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47021637E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05942989E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92273391E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.02722580E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51556188E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75432395E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01762592E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     6.96652196E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33605474E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33605474E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11202890E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11202890E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10383272E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.70600987E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.85224419E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.13921483E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.73696201E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.13645903E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.25441125E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.02721074E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.68720861E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.00150912E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.44156605E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.19335708E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18147198E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53174022E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18147198E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53174022E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40906353E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50779363E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50779363E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47300436E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01186229E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01186229E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01186185E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.90662626E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.90662626E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.90662626E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.90662626E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.68877073E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.68877073E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.68877073E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.68877073E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.83222597E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.83222597E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.84306798E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.10330656E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.41847694E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.67031378E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.91828607E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.67031378E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.91828607E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.35081466E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.25077203E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.25077203E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.23357148E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.55015042E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.55015042E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.55013966E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.73993186E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.84901419E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.73993186E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.84901419E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.30174523E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.11066945E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.11066945E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.00970584E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.21953822E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.21953822E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.21953828E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.97672097E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.97672097E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.97672097E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.97672097E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.32555682E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.32555682E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.32555682E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.32555682E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.25930262E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.25930262E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.70504843E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.42444262E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.37126846E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.12149871E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.99065846E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.99065846E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.28809238E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.73749016E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.61985299E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89418124E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89418124E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.80799570E-06    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.80799570E-06    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.90066664E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90066664E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04026000E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95487762E-01    2           5        -5   # BR(h -> b       bb     )
     6.44961032E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28315772E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84196597E-04    2           3        -3   # BR(h -> s       sb     )
     2.10258646E-02    2           4        -4   # BR(h -> c       cb     )
     6.76421718E-02    2          21        21   # BR(h -> g       g      )
     2.38506014E-03    2          22        22   # BR(h -> gam     gam    )
     1.62292490E-03    2          22        23   # BR(h -> Z       gam    )
     2.18942222E-01    2          24       -24   # BR(h -> W+      W-     )
     2.76853791E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48897162E+01   # H decays
#          BR         NDA      ID1       ID2
     3.48087112E-01    2           5        -5   # BR(H -> b       bb     )
     6.04046603E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13576248E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52623916E-04    2           3        -3   # BR(H -> s       sb     )
     7.08677837E-08    2           4        -4   # BR(H -> c       cb     )
     7.10242730E-03    2           6        -6   # BR(H -> t       tb     )
     9.01934074E-07    2          21        21   # BR(H -> g       g      )
     1.03853370E-12    2          22        22   # BR(H -> gam     gam    )
     1.82366663E-09    2          23        22   # BR(H -> Z       gam    )
     2.00464174E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00162656E-06    2          23        23   # BR(H -> Z       Z      )
     7.49172188E-06    2          25        25   # BR(H -> h       h      )
     2.70779730E-24    2          36        36   # BR(H -> A       A      )
     5.26867255E-20    2          23        36   # BR(H -> Z       A      )
     1.85387332E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64208674E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64208674E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.83704339E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55001115E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.57635848E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.10387214E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.21824103E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.55516721E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.58572849E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.71047037E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.20152582E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.11862520E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.58044404E-09    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.37491661E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.37491661E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42257310E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48887130E+01   # A decays
#          BR         NDA      ID1       ID2
     3.48119458E-01    2           5        -5   # BR(A -> b       bb     )
     6.04061469E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13581336E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52671116E-04    2           3        -3   # BR(A -> s       sb     )
     7.13154134E-08    2           4        -4   # BR(A -> c       cb     )
     7.11499225E-03    2           6        -6   # BR(A -> t       tb     )
     1.46195166E-05    2          21        21   # BR(A -> g       g      )
     4.67814283E-08    2          22        22   # BR(A -> gam     gam    )
     1.60759301E-08    2          23        22   # BR(A -> Z       gam    )
     2.00047242E-06    2          23        25   # BR(A -> Z       h      )
     1.85506000E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64215409E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64215409E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.45830165E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.92081923E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.27584894E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.68050936E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.89573978E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.42198177E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.78367217E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.23629986E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.80856042E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.61108811E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.61108811E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48358698E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.56012720E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.04797614E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13841619E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55847836E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21145330E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49234901E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54263409E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.00463201E-06    2          24        25   # BR(H+ -> W+      h      )
     2.63675356E-14    2          24        36   # BR(H+ -> W+      A      )
     5.58757797E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.42238952E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.24660103E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.64583184E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.99214604E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59759430E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00325908E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.99850568E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.08606623E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
