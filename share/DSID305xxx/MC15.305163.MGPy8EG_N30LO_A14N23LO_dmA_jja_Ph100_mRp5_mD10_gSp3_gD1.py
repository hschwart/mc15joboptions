model  = 'dmA'
mR     = 500
mDM    = 10000
gSM    = 0.30
gDM    = 1.00
widthR = 19.245608
phminpt= 100.000000
filteff = 0.101500

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")
