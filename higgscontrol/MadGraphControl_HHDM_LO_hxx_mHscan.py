from MadGraphControl.MadGraphUtils import *

#-----------------------------------------------------------------------------------------------
# Preliminaries
#-----------------------------------------------------------------------------------------------
efficiency = 1.0
safety_fac = 1.1
nevents = int(5000*safety_fac/efficiency)

mode=0
dsid_list = range(343285, 343285 + 26)
dsid = runArgs.runNumber

#-----------------------------------------------------------------------------------------------
# Get mass points
#-----------------------------------------------------------------------------------------------

mX = 0.0
mH = 0.0

if dsid < 343299:
    mX = 50.0
    if dsid < 343288:
        mH = 250.0 + (dsid - 343285)*10
    elif dsid < 343293:
        mH = 275.0 + (dsid - 343288)*5
    else:
        mH = 300.0 + (dsid - 343293)*10
else:
    mX = 60.0
    if dsid < 343304:
        mH = 250.0 + (dsid - 343299)*10
    elif dsid < 343305:
        mH = 295.0
    else:
        mH = mH = 300.0 + (dsid - 343305)*10

mass_points = {'35':str(mH), '1000022':str(mX)}

#-----------------------------------------------------------------------------------------------
# Create Process
#-----------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in dsid_list:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    import model HHDM_intMET
    generate p p > h2, h2 > h n1 n1
    output -f""")
    fcard.close()
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#-----------------------------------------------------------------------------------------------
# Card settings
#-----------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'parton_shower':'PYTHIA8' }
build_run_card(run_card_old='MadGraph_run_card_HHDM_def.dat', run_card_new='run_card_new.dat', nevts=nevents, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, extras=extras)
build_param_card(param_card_old='MadGraph_param_card_HHDM.dat',param_card_new='param_card_new.dat',masses=mass_points)

print_cards()

#-----------------------------------------------------------------------------------------------
# Generation
#-----------------------------------------------------------------------------------------------

runName = 'mH' + str(mH) + 'mX' + str(mX)
process_dir = new_process()
generate(run_card_loc='run_card_new.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#-----------------------------------------------------------------------------------------------
# Pythia8 Showering Dependencies
#-----------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

#-----------------------------------------------------------------------------------------------
# EVGEN
#-----------------------------------------------------------------------------------------------

evgenConfig.description = "Higgs production in association with DM through a heavy scalar"
evgenConfig.keywords = ["Higgs", "BSMHiggs", "diphoton"]
evgenConfig.contact = ['Stefan von Buddenbrock <stef.von.b@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#-----------------------------------------------------------------------------------------------
# Pythia8 Showering Commands
#-----------------------------------------------------------------------------------------------

genSeq.Pythia8.Commands += ["1000022:mayDecay no"] #switch off DM decays
genSeq.Pythia8.Commands += ["35:mayDecay no"] #switch off heavy scalar decays
genSeq.Pythia8.Commands += ["1000022:isVisible no"] #make DM invisible
genSeq.Pythia8.Commands += ["25:oneChannel = 1 1 0 22 22"] #gamma gamma decay
