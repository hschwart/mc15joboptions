from MadGraphControl.MadGraphUtils import *

# General settings
nevents=int(1.1*runArgs.maxEvents)
mode=0
cluster_type = 'None'
cluster_queue = 'None'
nJobs=1
gridpack_dir=None
gridpack_mode=False

# MG particle cuts
mllCut=-1
ptjCut=0
ptlCut=0
xptlCut=5
ptaCut=20
drajCut=0.2
dralCut=0.2
etajCut=-1
etabCut=-1
myyCut=100
myyMaxCut=-1
maxjetflavor=5
dyn_scale = '3'

name='bbgamgam_inc'
process="pp > bbyy"
keyword=['SM']

#nevents= 20000
gridpack_mode=False
gridpack_dir='madevent/'

mode=0
cluster_type = 'condor'
cluster_queue = 'None'
nJobs=1

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define bs = b b~
generate p p > a a bs bs QCD=2 QED=6
output -f""")
fcard.close()

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)

lhaid=247000
pdflabel='lhapdf'
#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'   :'3.0',
           'PDLABEL'       :"'lhapdf'",
           'lhaid'         : lhaid,
           'pdlabel'      : "'"+pdflabel+"'",
           'maxjetflavor'  : maxjetflavor,
#           'parton_shower'  :'PYTHIA8',
#           'mll_sf'        : mllCut,
           'pta'           : ptaCut,
           'ptl'           : ptlCut,
           'xptl'          : xptlCut,
           'ptj'           : ptjCut,
           'etal'          : 5.0,
           'etaj'          : 5.0,
           'etaa'          : 5.0,
           'etaj'          : etajCut,
           'etab'          : etabCut,
           'drjj'          : 0.0,
           'drjl'          : 0.0,
           'drll'          : 0.0,
           'draa'          : 0.0,
           'draj'          : 0.2,
           'dral'          : 0.2,
           'mmaa'          : myyCut,
           'mmaamax'       : myyMaxCut,
           'dynamical_scale_choice': dyn_scale,
           'use_syst'      :'T',
           'sys_scalefact' : '1 0.5 2',
           'sys_pdf'       : 'NNPDF23_lo_as_0130_qed'
           }

runName='run_01'

print  "checking process_dir for old card: ", process_dir
print "old run card is:", get_default_runcard(proc_dir=process_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode, njobs=nJobs,grid_pack=gridpack_mode, gridpack_dir=gridpack_dir, proc_dir=process_dir,run_name=runName, nevents=nevents, cluster_type=cluster_type,cluster_queue=cluster_queue, random_seed=runArgs.randomSeed)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

############################
# Shower JOs will go here

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

evgenConfig.generators = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact  = [ "a.hasib@cern.ch", "xifeng.ruan@cern.ch" ]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=keyword
evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
