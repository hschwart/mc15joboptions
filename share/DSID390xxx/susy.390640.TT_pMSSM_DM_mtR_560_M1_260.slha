#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11039296E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.42959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.83900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.19485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04198303E+01   # W+
        25     1.25197432E+02   # h
        35     4.00000694E+03   # H
        36     3.99999642E+03   # A
        37     4.00100337E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02006575E+03   # ~d_L
   2000001     4.01801496E+03   # ~d_R
   1000002     4.01941489E+03   # ~u_L
   2000002     4.01852716E+03   # ~u_R
   1000003     4.02006575E+03   # ~s_L
   2000003     4.01801496E+03   # ~s_R
   1000004     4.01941489E+03   # ~c_L
   2000004     4.01852716E+03   # ~c_R
   1000005     2.22012476E+03   # ~b_1
   2000005     4.02268615E+03   # ~b_2
   1000006     6.05566631E+02   # ~t_1
   2000006     2.23028630E+03   # ~t_2
   1000011     4.00316153E+03   # ~e_L
   2000011     4.00368767E+03   # ~e_R
   1000012     4.00205063E+03   # ~nu_eL
   1000013     4.00316153E+03   # ~mu_L
   2000013     4.00368767E+03   # ~mu_R
   1000014     4.00205063E+03   # ~nu_muL
   1000015     4.00457959E+03   # ~tau_1
   2000015     4.00813335E+03   # ~tau_2
   1000016     4.00401087E+03   # ~nu_tauL
   1000021     1.98344973E+03   # ~g
   1000022     2.44510505E+02   # ~chi_10
   1000023    -2.94463532E+02   # ~chi_20
   1000025     3.11090278E+02   # ~chi_30
   1000035     2.06774974E+03   # ~chi_40
   1000024     2.91545429E+02   # ~chi_1+
   1000037     2.06791731E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.13039358E-01   # N_11
  1  2    -1.54330132E-02   # N_12
  1  3    -4.46900831E-01   # N_13
  1  4    -3.72838398E-01   # N_14
  2  1    -5.99450090E-02   # N_21
  2  2     2.52105221E-02   # N_22
  2  3    -7.01779320E-01   # N_23
  2  4     7.09420053E-01   # N_24
  3  1     5.79113655E-01   # N_31
  3  2     2.59774847E-02   # N_32
  3  3     5.54772326E-01   # N_33
  3  4     5.96808353E-01   # N_34
  4  1    -9.85787535E-04   # N_41
  4  2     9.99225411E-01   # N_42
  4  3    -3.61919636E-03   # N_43
  4  4    -3.91727925E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.12358958E-03   # U_11
  1  2     9.99986874E-01   # U_12
  2  1    -9.99986874E-01   # U_21
  2  2     5.12358958E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54103941E-02   # V_11
  1  2    -9.98463664E-01   # V_12
  2  1    -9.98463664E-01   # V_21
  2  2    -5.54103941E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.05966665E-02   # cos(theta_t)
  1  2     9.96746797E-01   # sin(theta_t)
  2  1    -9.96746797E-01   # -sin(theta_t)
  2  2    -8.05966665E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999221E-01   # cos(theta_b)
  1  2    -1.24819846E-03   # sin(theta_b)
  2  1     1.24819846E-03   # -sin(theta_b)
  2  2     9.99999221E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05570826E-01   # cos(theta_tau)
  1  2     7.08639407E-01   # sin(theta_tau)
  2  1    -7.08639407E-01   # -sin(theta_tau)
  2  2    -7.05570826E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00269439E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10392961E+03  # DRbar Higgs Parameters
         1    -2.83900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44073412E+02   # higgs               
         4     1.62122658E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10392961E+03  # The gauge couplings
     1     3.61548730E-01   # gprime(Q) DRbar
     2     6.35462902E-01   # g(Q) DRbar
     3     1.03305052E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10392961E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.34613415E-07   # A_c(Q) DRbar
  3  3     2.42959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10392961E+03  # The trilinear couplings
  1  1     3.43745604E-07   # A_d(Q) DRbar
  2  2     3.43778254E-07   # A_s(Q) DRbar
  3  3     6.14872882E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10392961E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.60059191E-08   # A_mu(Q) DRbar
  3  3     7.67819903E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10392961E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69966725E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10392961E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85188714E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10392961E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03417071E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10392961E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57975450E+07   # M^2_Hd              
        22    -1.06770677E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.19485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40084712E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.43974523E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.00735210E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.20344947E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.90183000E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.01623342E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.26159163E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.76924513E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.35984200E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.32263933E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.56565323E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.19456509E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.20110123E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     9.64463919E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.57817591E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.26161589E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.32482591E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.36788683E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.42379263E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.58523435E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.52730489E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.84103554E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.11062921E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.48537082E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64407541E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.62192336E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80150651E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.57471248E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.18782196E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63767332E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.22242119E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13358391E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.09214766E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     7.96233485E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.71812678E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.38347929E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75898695E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.69018882E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.67656016E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.61196304E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79940559E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37438701E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58665883E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52283000E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58710408E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.70010126E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.00503975E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.86907270E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.97881695E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44107727E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75917588E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.52590325E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.17090747E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.14560545E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80437821E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.59538124E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61869318E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52507136E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52013397E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.65650496E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.23273808E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.87789079E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.77270874E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85413269E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75898695E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.69018882E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.67656016E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.61196304E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79940559E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37438701E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58665883E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52283000E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58710408E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.70010126E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.00503975E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.86907270E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.97881695E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44107727E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75917588E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.52590325E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.17090747E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.14560545E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80437821E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.59538124E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61869318E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52507136E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52013397E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.65650496E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.23273808E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.87789079E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.77270874E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85413269E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11076156E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02582640E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.04607993E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.45285710E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77139407E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.68519803E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55682069E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06361374E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.62068768E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.58690960E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.34343795E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.27250967E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11076156E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02582640E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.04607993E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.45285710E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77139407E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.68519803E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55682069E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06361374E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.62068768E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.58690960E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.34343795E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.27250967E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08053458E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.45684688E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.36254045E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.38156271E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38984020E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48772402E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78672377E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.07974945E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44268462E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.31629916E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.16377713E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41462629E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10877987E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83640405E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11186795E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17159022E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.79794726E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.70139804E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77474374E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13857555E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53416101E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11186795E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17159022E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.79794726E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.70139804E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77474374E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13857555E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53416101E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44136426E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05994201E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.62661495E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.25339166E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51209260E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.76050227E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01030985E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.18511811E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33589542E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33589542E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11197873E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11197873E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10425172E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66627052E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.37780775E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46522711E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.04902858E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40813729E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.78726888E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39668308E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.60654625E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.56375214E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18174867E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53293039E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18174867E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53293039E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40839869E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51524235E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51524235E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48198174E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02795541E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02795541E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02795517E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.50604422E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.50604422E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.50604422E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.50604422E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.83535130E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.83535130E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.83535130E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.83535130E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.17189621E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.17189621E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.15347302E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.23699057E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.00091855E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.59680949E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.93615577E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.59680949E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.93615577E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.68414440E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.34024200E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.34024200E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.33540305E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.73170670E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.73170670E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.73169835E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.00623183E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.30530867E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.00623183E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.30530867E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.72581027E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.99360415E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.99360415E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.80495885E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     5.98398567E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     5.98398567E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     5.98398577E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.68927559E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.68927559E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.68927559E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.68927559E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.56306059E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.56306059E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.56306059E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.56306059E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.46079901E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.46079901E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66508618E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.74891466E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.46596642E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.25165927E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40395760E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40395760E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.35325961E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.78429762E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.07813504E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.67085114E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.67085114E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.00135969E-03   # h decays
#          BR         NDA      ID1       ID2
     6.00349602E-01    2           5        -5   # BR(h -> b       bb     )
     6.49378059E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29880881E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87771781E-04    2           3        -3   # BR(h -> s       sb     )
     2.11844231E-02    2           4        -4   # BR(h -> c       cb     )
     6.89148850E-02    2          21        21   # BR(h -> g       g      )
     2.36713015E-03    2          22        22   # BR(h -> gam     gam    )
     1.59053420E-03    2          22        23   # BR(h -> Z       gam    )
     2.13127900E-01    2          24       -24   # BR(h -> W+      W-     )
     2.68100668E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44256985E+01   # H decays
#          BR         NDA      ID1       ID2
     3.50949061E-01    2           5        -5   # BR(H -> b       bb     )
     6.09197527E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15397490E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54778093E-04    2           3        -3   # BR(H -> s       sb     )
     7.14608654E-08    2           4        -4   # BR(H -> c       cb     )
     7.16186653E-03    2           6        -6   # BR(H -> t       tb     )
     8.33989747E-07    2          21        21   # BR(H -> g       g      )
     3.11936645E-10    2          22        22   # BR(H -> gam     gam    )
     1.84250126E-09    2          23        22   # BR(H -> Z       gam    )
     1.80886316E-06    2          24       -24   # BR(H -> W+      W-     )
     9.03805123E-07    2          23        23   # BR(H -> Z       Z      )
     6.79736138E-06    2          25        25   # BR(H -> h       h      )
     1.31147927E-24    2          36        36   # BR(H -> A       A      )
     2.90256820E-20    2          23        36   # BR(H -> Z       A      )
     1.86736621E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61981978E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61981978E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.67082394E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.76256010E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64887999E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.30036265E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.19213652E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.22516499E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.52137616E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.03207752E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.98989013E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.26379949E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.23843875E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.23843875E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.44209340E+01   # A decays
#          BR         NDA      ID1       ID2
     3.51005588E-01    2           5        -5   # BR(A -> b       bb     )
     6.09253920E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15417260E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54843041E-04    2           3        -3   # BR(A -> s       sb     )
     7.19284305E-08    2           4        -4   # BR(A -> c       cb     )
     7.17615171E-03    2           6        -6   # BR(A -> t       tb     )
     1.47451777E-05    2          21        21   # BR(A -> g       g      )
     5.55263180E-08    2          22        22   # BR(A -> gam     gam    )
     1.62163451E-08    2          23        22   # BR(A -> Z       gam    )
     1.80524982E-06    2          23        25   # BR(A -> Z       h      )
     1.87315001E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61996679E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61996679E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.29441023E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.09681065E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.36230372E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.85806368E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.05900145E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.27116573E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.71848311E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.96677750E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.00571576E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.30719818E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.30719818E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43544436E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.60323877E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10152764E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15735069E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.58606974E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22218046E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51441824E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.57017974E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.80945620E-06    2          24        25   # BR(H+ -> W+      h      )
     2.50636514E-14    2          24        36   # BR(H+ -> W+      A      )
     5.47003657E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.93546650E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.35033035E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62402968E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.51381757E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60277233E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00242277E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.65337214E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
