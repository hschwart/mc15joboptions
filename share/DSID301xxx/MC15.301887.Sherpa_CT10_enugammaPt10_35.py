include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e nu gamma production with up to three jets in ME+PS and 10<pT_gamma<35 GeV."
evgenConfig.keywords = ["electroweak", "electron", "photon", "neutrino", "SM" ]
evgenConfig.contact  = [ "olivier.arnaez@cern.ch", "frank.siegert@cern.ch", "stefano.manzoni@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.inputconfcheck = "Sherpa_CT10_enugammaPt10_35"
evgenConfig.minevents = 2000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  12 -11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;

  Process 93 93 ->  -12 11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  PT 22  10 35
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""

