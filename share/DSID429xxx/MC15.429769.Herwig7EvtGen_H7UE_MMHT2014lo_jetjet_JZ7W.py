# based on the JobOptions MC15.426047 and MC15.429301

evgenConfig.description = "Herwig7 BuiltinME QCD dijet production JZ7W with MMHT2014lo68cl PDF and H7-UE-MMHT tune."
evgenConfig.generators  = ["Herwig7", "EvtGen"] 
evgenConfig.keywords    = ["QCD", "jets"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', "Haifeng.Li@cern.ch", "daniel.rauch@desy.de"]
evgenConfig.minevents   = 500

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()

import os
if "HERWIG7VER" in os.environ:
  version = os.getenv("HERWIG7VER")
  verh7 = version.split(".")[1]
else:
  verh7 = 0

if int(verh7 == 0):  
   Herwig7Config.add_commands("""
   insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
   set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
   set /Herwig/Cuts/JetKtCut:MinKT 950*GeV
   """)
else:
   Herwig7Config.add_commands("""
   insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
   set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
   set /Herwig/Cuts/JetKtCut:MinKT 950*GeV
   """) 

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

include("MC15JobOptions/JetFilter_JZ7W.py")
