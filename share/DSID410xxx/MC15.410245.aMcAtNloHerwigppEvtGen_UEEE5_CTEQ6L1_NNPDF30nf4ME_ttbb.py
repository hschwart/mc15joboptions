include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

## To modify Higgs BR
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = 'MG5_aMC@NLO_Herwigpp_ttbb'
evgenConfig.keywords +=  [ 'ttbar']


# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
