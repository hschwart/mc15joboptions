#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871856E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.02990000E+02   # M_1(MX)             
         2     1.00590000E+03   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04051352E+01   # W+
        25     1.24700037E+02   # h
        35     3.00002483E+03   # H
        36     2.99999994E+03   # A
        37     3.00108242E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04197525E+03   # ~d_L
   2000001     3.03674109E+03   # ~d_R
   1000002     3.04107295E+03   # ~u_L
   2000002     3.03807293E+03   # ~u_R
   1000003     3.04197525E+03   # ~s_L
   2000003     3.03674109E+03   # ~s_R
   1000004     3.04107295E+03   # ~c_L
   2000004     3.03807293E+03   # ~c_R
   1000005     1.05685845E+03   # ~b_1
   2000005     3.03435924E+03   # ~b_2
   1000006     1.05461387E+03   # ~t_1
   2000006     3.02473600E+03   # ~t_2
   1000011     3.00650624E+03   # ~e_L
   2000011     3.00181613E+03   # ~e_R
   1000012     3.00512406E+03   # ~nu_eL
   1000013     3.00650624E+03   # ~mu_L
   2000013     3.00181613E+03   # ~mu_R
   1000014     3.00512406E+03   # ~nu_muL
   1000015     2.98624686E+03   # ~tau_1
   2000015     3.02154569E+03   # ~tau_2
   1000016     3.00497262E+03   # ~nu_tauL
   1000021     2.35218218E+03   # ~g
   1000022     5.05896015E+02   # ~chi_10
   1000023     1.04504356E+03   # ~chi_20
   1000025    -2.99488790E+03   # ~chi_30
   1000035     2.99556079E+03   # ~chi_40
   1000024     1.04520523E+03   # ~chi_1+
   1000037     2.99623330E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885605E-01   # N_11
  1  2    -1.58834245E-04   # N_12
  1  3    -1.50192238E-02   # N_13
  1  4    -1.78192079E-03   # N_14
  2  1     6.02957905E-04   # N_21
  2  2     9.99556754E-01   # N_22
  2  3     2.85869315E-02   # N_23
  2  4     8.28968692E-03   # N_24
  3  1    -9.35557292E-03   # N_31
  3  2     1.43577634E-02   # N_32
  3  3    -7.06882178E-01   # N_33
  3  4     7.07123691E-01   # N_34
  4  1     1.18695935E-02   # N_41
  4  2    -2.60791971E-02   # N_42
  4  3     7.06593799E-01   # N_43
  4  4     7.07039031E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99182524E-01   # U_11
  1  2     4.04262708E-02   # U_12
  2  1    -4.04262708E-02   # U_21
  2  2     9.99182524E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99931301E-01   # V_11
  1  2    -1.17214891E-02   # V_12
  2  1    -1.17214891E-02   # V_21
  2  2    -9.99931301E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98450642E-01   # cos(theta_t)
  1  2    -5.56445459E-02   # sin(theta_t)
  2  1     5.56445459E-02   # -sin(theta_t)
  2  2     9.98450642E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99720966E-01   # cos(theta_b)
  1  2    -2.36218149E-02   # sin(theta_b)
  2  1     2.36218149E-02   # -sin(theta_b)
  2  2     9.99720966E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06974273E-01   # cos(theta_tau)
  1  2     7.07239265E-01   # sin(theta_tau)
  2  1    -7.07239265E-01   # -sin(theta_tau)
  2  2    -7.06974273E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00133548E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718556E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43885307E+02   # higgs               
         4     1.03101808E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718556E+03  # The gauge couplings
     1     3.62542264E-01   # gprime(Q) DRbar
     2     6.36236567E-01   # g(Q) DRbar
     3     1.02365488E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718556E+03  # The trilinear couplings
  1  1     2.72827796E-06   # A_u(Q) DRbar
  2  2     2.72831583E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718556E+03  # The trilinear couplings
  1  1     9.94496018E-07   # A_d(Q) DRbar
  2  2     9.94602962E-07   # A_s(Q) DRbar
  3  3     1.94649825E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718556E+03  # The trilinear couplings
  1  1     3.88511479E-07   # A_e(Q) DRbar
  2  2     3.88529965E-07   # A_mu(Q) DRbar
  3  3     3.93647056E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718556E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49806527E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718556E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.70377697E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718556E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00311146E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718556E+03  # The soft SUSY breaking masses at the scale Q
         1     5.02990000E+02   # M_1(Q)              
         2     1.00590000E+03   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.17216576E+04   # M^2_Hd              
        22    -9.02232826E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40490311E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.10422664E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47866790E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47866790E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52133210E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52133210E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.46508179E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.67217260E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.27827405E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10695443E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.68742956E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.84837925E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.74922979E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.48759490E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.04402216E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67019131E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59339426E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11056411E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.72346089E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.76499831E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.35001695E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.19492246E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.05675627E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.63540941E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.70330045E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.44442952E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.16586823E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.66784978E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.73229837E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.60745590E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.39138321E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.29092672E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.32577776E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.44923743E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.05042109E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.06324857E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.89974573E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.00072044E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.58775720E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.12818385E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54322136E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.63068802E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.30379669E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.03565044E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.45677784E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.29644696E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.34355224E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.44797101E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.20317288E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.56036010E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.89420067E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.17559158E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.59438454E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63961378E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.37376193E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.31217852E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.48038509E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.42866476E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.56262359E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.29092672E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.32577776E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.44923743E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.05042109E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.06324857E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.89974573E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.00072044E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.58775720E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.12818385E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54322136E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.63068802E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.30379669E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.03565044E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.45677784E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.29644696E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.34355224E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.44797101E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.20317288E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.56036010E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.89420067E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.17559158E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.59438454E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63961378E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.37376193E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.31217852E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.48038509E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.42866476E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.56262359E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.17464853E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.16813870E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.94694011E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.84888530E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.51522626E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.88492052E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.80097884E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.48161437E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999698E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.97495887E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.97151725E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.58753481E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.17464853E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.16813870E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.94694011E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.84888530E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.51522626E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.88492052E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.80097884E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.48161437E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999698E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.97495887E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.97151725E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.58753481E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.34732907E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.94051629E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.02206031E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.03742340E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.30606310E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.02553082E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.99361739E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.82509443E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57658914E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.98032658E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.85039397E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.17438839E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16893750E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.94103529E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.83449293E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.66029027E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.89002694E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.65647362E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.17438839E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16893750E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.94103529E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.83449293E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.66029027E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.89002694E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.65647362E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.17447975E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.16883808E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.94072089E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.63423870E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.60823895E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.89041623E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.45733726E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.18107288E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.30271521E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.28435898E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61100712E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.41960138E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.21897672E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.10966036E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.04771301E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.66996472E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.12429972E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.60378603E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.39621397E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.31364341E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.33894163E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.75899789E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.03323744E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.03323744E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.65516931E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.47319296E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34616447E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34616447E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.28676677E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.28676677E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.64200416E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.64200416E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.23070039E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.40913703E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.46269304E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.09883923E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.09883923E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.41684662E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.23920318E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30696397E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30696397E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.35167284E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.35167284E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.99477190E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.99477190E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.56210901E-03   # h decays
#          BR         NDA      ID1       ID2
     5.66351174E-01    2           5        -5   # BR(h -> b       bb     )
     7.26154753E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.57062432E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.45873787E-04    2           3        -3   # BR(h -> s       sb     )
     2.37206345E-02    2           4        -4   # BR(h -> c       cb     )
     7.66573305E-02    2          21        21   # BR(h -> g       g      )
     2.61517670E-03    2          22        22   # BR(h -> gam     gam    )
     1.70788571E-03    2          22        23   # BR(h -> Z       gam    )
     2.27232368E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82970188E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.41743380E+01   # H decays
#          BR         NDA      ID1       ID2
     8.94501176E-01    2           5        -5   # BR(H -> b       bb     )
     7.27656357E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.57281900E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.15943791E-04    2           3        -3   # BR(H -> s       sb     )
     8.85536712E-08    2           4        -4   # BR(H -> c       cb     )
     8.83379207E-03    2           6        -6   # BR(H -> t       tb     )
     1.38398049E-05    2          21        21   # BR(H -> g       g      )
     9.12931434E-08    2          22        22   # BR(H -> gam     gam    )
     3.62241753E-09    2          23        22   # BR(H -> Z       gam    )
     7.79777092E-07    2          24       -24   # BR(H -> W+      W-     )
     3.89407969E-07    2          23        23   # BR(H -> Z       Z      )
     6.19475308E-06    2          25        25   # BR(H -> h       h      )
     2.24745864E-25    2          36        36   # BR(H -> A       A      )
     9.31024646E-21    2          23        36   # BR(H -> Z       A      )
     4.61181816E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.05146982E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.30396074E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.14863343E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23529067E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.92078068E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.34354740E+01   # A decays
#          BR         NDA      ID1       ID2
     9.14305646E-01    2           5        -5   # BR(A -> b       bb     )
     7.43735221E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.62966634E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.22977632E-04    2           3        -3   # BR(A -> s       sb     )
     9.11397097E-08    2           4        -4   # BR(A -> c       cb     )
     9.08676716E-03    2           6        -6   # BR(A -> t       tb     )
     2.67596597E-05    2          21        21   # BR(A -> g       g      )
     6.16444869E-08    2          22        22   # BR(A -> gam     gam    )
     2.63789475E-08    2          23        22   # BR(A -> Z       gam    )
     7.94073044E-07    2          23        25   # BR(A -> Z       h      )
     8.64602680E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.56282682E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.31788284E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.78367844E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.68895573E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46024871E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.74340225E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.38430256E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.34558018E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.40119417E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.88270698E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.19412402E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.20951877E-07    2          24        25   # BR(H+ -> W+      h      )
     5.30105325E-14    2          24        36   # BR(H+ -> W+      A      )
     4.51275828E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30975627E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06912713E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
