evgenConfig.description = "Single pi0 with flat phi, |eta|<2.5 , pT in [50:500] GeV"
evgenConfig.keywords = ["singleParticle", "pi0" , "egamma" , "performance" ]
evgenConfig.contact  = ["ocariz@in2p3.fr"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[50000,500000], eta=[-2.5,2.5])
