#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.32600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.35000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05425407E+01   # W+
        25     1.25000000E+02   # h
        35     2.00398075E+03   # H
        36     2.00000000E+03   # A
        37     2.00152355E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013293E+03   # ~d_L
   2000001     5.00002540E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994920E+03   # ~u_R
   1000003     5.00013293E+03   # ~s_L
   2000003     5.00002540E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994920E+03   # ~c_R
   1000005     4.99913229E+03   # ~b_1
   2000005     5.00102730E+03   # ~b_2
   1000006     4.97835677E+03   # ~t_1
   2000006     5.02599814E+03   # ~t_2
   1000011     5.00008213E+03   # ~e_L
   2000011     5.00007620E+03   # ~e_R
   1000012     4.99984167E+03   # ~nu_eL
   1000013     5.00008213E+03   # ~mu_L
   2000013     5.00007620E+03   # ~mu_R
   1000014     4.99984167E+03   # ~nu_muL
   1000015     4.99945235E+03   # ~tau_1
   2000015     5.00070653E+03   # ~tau_2
   1000016     4.99984167E+03   # ~nu_tauL
   1000021     2.40000000E+03   # ~g
   1000022     2.28945106E+03   # ~chi_10
   1000023    -2.35006126E+03   # ~chi_20
   1000025     2.37740685E+03   # ~chi_30
   1000035     3.00920335E+03   # ~chi_40
   1000024     2.34092444E+03   # ~chi_1+
   1000037     3.00916545E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.64645206E-01   # N_11
  1  2    -6.98702537E-02   # N_12
  1  3     4.54190743E-01   # N_13
  1  4    -4.51825880E-01   # N_14
  2  1     1.86736392E-03   # N_21
  2  2    -2.89935859E-03   # N_22
  2  3    -7.07056493E-01   # N_23
  2  4    -7.07148657E-01   # N_24
  3  1     6.44404775E-01   # N_31
  3  2     9.45207083E-02   # N_32
  3  3    -5.35944511E-01   # N_33
  3  4     5.37188796E-01   # N_34
  4  1     7.53046031E-03   # N_41
  4  2    -9.93063733E-01   # N_42
  4  3    -8.09034348E-02   # N_43
  4  4     8.49844064E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.13947855E-01   # U_11
  1  2     9.93486732E-01   # U_12
  2  1     9.93486732E-01   # U_21
  2  2     1.13947855E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.19705016E-01   # V_11
  1  2     9.92809503E-01   # V_12
  2  1     9.92809503E-01   # V_21
  2  2     1.19705016E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07527492E-01   # cos(theta_t)
  1  2     7.06685820E-01   # sin(theta_t)
  2  1    -7.06685820E-01   # -sin(theta_t)
  2  2     7.07527492E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.86752056E-01   # cos(theta_b)
  1  2     7.26891748E-01   # sin(theta_b)
  2  1    -7.26891748E-01   # -sin(theta_b)
  2  2     6.86752056E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05432029E-01   # cos(theta_tau)
  1  2     7.08777576E-01   # sin(theta_tau)
  2  1    -7.08777576E-01   # -sin(theta_tau)
  2  2     7.05432029E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90191341E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.35000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52410973E+02   # vev(Q)              
         4     2.81579028E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52678371E-01   # gprime(Q) DRbar
     2     6.26513388E-01   # g(Q) DRbar
     3     1.07754725E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02429136E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71067419E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79689162E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.32600000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.12765754E+06   # M^2_Hd              
        22    -1.04479920E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36753244E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.17014220E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.29702696E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.32301232E-05    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.95611069E-06    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.59180501E-06    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.48205859E-12    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.62173046E-10    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.78721866E-06    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.13495554E-12    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.18959337E-09    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.59180501E-06    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.48205859E-12    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.62173046E-10    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.78721866E-06    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.13495554E-12    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.18959337E-09    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.49011582E-06    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.34361233E-09    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.43864521E-10    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.52393969E-08    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.52393969E-08    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.52393969E-08    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.52393969E-08    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.44161314E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.47369561E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.19336285E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.26999735E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11400624E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.04047837E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.11944085E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.64247134E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.33710909E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     2.57165050E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.06550031E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.63988898E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.32150900E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     7.77319987E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.49435295E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.61338984E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.90069343E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.81720949E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.20851713E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.49631616E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.82048983E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.03606425E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.78662958E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.33936770E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.95747514E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.07650689E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.68823636E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.05003321E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.11956996E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.20208995E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.40885777E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.09293306E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.82095216E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.50231386E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.08192087E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.87763267E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.15403140E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.80837046E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.32772687E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.70145775E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.64971832E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.35014135E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.36228648E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.60097601E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.48961679E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60487201E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.82104479E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.94240488E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.63528906E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.32005950E-05    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.17818328E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.63869184E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.33997327E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.70193474E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60088317E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.05477221E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.50972223E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.12467135E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.83783475E-07    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89820138E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.82095216E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.50231386E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.08192087E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.87763267E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.15403140E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.80837046E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.32772687E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.70145775E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.64971832E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.35014135E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.36228648E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.60097601E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.48961679E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60487201E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.82104479E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.94240488E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.63528906E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.32005950E-05    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.17818328E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.63869184E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.33997327E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.70193474E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60088317E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.05477221E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.50972223E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.12467135E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.83783475E-07    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89820138E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.77819733E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.70657067E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.45694799E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.80073589E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.79500202E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.11252063E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.64300070E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51927942E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.94850541E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.44743243E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.05108439E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.75722556E-05    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.77819733E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.70657067E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.45694799E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.80073589E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.79500202E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.11252063E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.64300070E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51927942E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.94850541E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.44743243E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.05108439E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.75722556E-05    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.15288935E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.52947862E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.92568936E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.97249468E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.78222591E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.11822784E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.59838210E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.67023225E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.15079559E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.41086412E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.17644153E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.02917008E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.82673177E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.07402765E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.68731732E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.77856436E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09844371E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.65529671E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.02748319E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.84231305E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.22749222E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.63367914E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.77856436E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09844371E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.65529671E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.02748319E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.84231305E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.22749222E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.63367914E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.78051283E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09767396E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.65063294E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.02536165E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.84032127E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.29588452E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.62981364E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.58931584E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.57654132E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     8.06754861E-04    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     2.81041181E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.80004219E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     9.36804885E-02    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     9.36785480E-02    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.31346762E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.64857530E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.56482236E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.28828311E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.45474780E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.57591220E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.47558303E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.06238233E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.94474612E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.04229458E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.85615152E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     9.37720180E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
     7.46001253E-04    2     1000039        35   # BR(~chi_10 -> ~G        H)
     3.21870333E-05    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     4.50557328E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00287643E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     7.69742997E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.50555185E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.71427477E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.90819575E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     5.08951837E-04    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.07359248E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.39029076E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.06853792E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.39016314E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.30842694E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.17590178E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.17581901E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15257057E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.34291025E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.34291025E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.34291025E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.15179282E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.15179282E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.58543171E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.58543171E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.71726446E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.71726446E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.71611720E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.71611720E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.43070731E-05    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.43070731E-05    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.65900313E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.03411196E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.19077940E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.94777444E-01    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.73742022E-01    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.71268562E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
     1.02491293E-03    2     1000039        35   # BR(~chi_30 -> ~G        H)
     4.21074480E-05    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     4.70854032E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.79799487E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.90093815E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.89509161E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.43862535E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.10656492E-05    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.10655245E-05    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.93124402E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.74691750E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.74691750E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.74691750E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.04450304E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.82757294E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.93730935E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.82437628E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.89730584E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.78808760E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.78793235E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.74364510E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.57115397E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.57115397E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.57115397E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.60269862E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.60269862E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.53127896E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.53127896E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.20089526E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.20089526E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.20076112E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.20076112E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.16338064E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.16338064E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     2.64798232E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.64225174E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52572194E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.14875850E-05    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.49177692E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.49177692E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.21630219E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.46443403E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.56594413E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.47769979E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     8.14442152E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     3.48671666E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.74674419E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.12076338E-10    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.09006748E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18218348E-01    2           5        -5   # BR(h -> b       bb     )
     6.36782817E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25397665E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77638279E-04    2           3        -3   # BR(h -> s       sb     )
     2.06072854E-02    2           4        -4   # BR(h -> c       cb     )
     6.69098140E-02    2          21        21   # BR(h -> g       g      )
     2.30015194E-03    2          22        22   # BR(h -> gam     gam    )
     1.53593409E-03    2          22        23   # BR(h -> Z       gam    )
     2.00457435E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55897137E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78078174E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46309544E-03    2           5        -5   # BR(H -> b       bb     )
     2.46442546E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71266153E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11554158E-06    2           3        -3   # BR(H -> s       sb     )
     1.00669814E-05    2           4        -4   # BR(H -> c       cb     )
     9.96080439E-01    2           6        -6   # BR(H -> t       tb     )
     7.97634906E-04    2          21        21   # BR(H -> g       g      )
     2.71784654E-06    2          22        22   # BR(H -> gam     gam    )
     1.16052280E-06    2          23        22   # BR(H -> Z       gam    )
     3.31390302E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65243256E-04    2          23        23   # BR(H -> Z       Z      )
     8.99822629E-04    2          25        25   # BR(H -> h       h      )
     7.10002364E-24    2          36        36   # BR(H -> A       A      )
     2.85297730E-11    2          23        36   # BR(H -> Z       A      )
     6.22931131E-12    2          24       -37   # BR(H -> W+      H-     )
     6.22931131E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82378053E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46598514E-03    2           5        -5   # BR(A -> b       bb     )
     2.43900410E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62276057E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678649E-06    2           3        -3   # BR(A -> s       sb     )
     9.96187469E-06    2           4        -4   # BR(A -> c       cb     )
     9.97006987E-01    2           6        -6   # BR(A -> t       tb     )
     9.43686054E-04    2          21        21   # BR(A -> g       g      )
     3.12618522E-06    2          22        22   # BR(A -> gam     gam    )
     1.35310258E-06    2          23        22   # BR(A -> Z       gam    )
     3.23001351E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74469758E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34638697E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49240978E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81156893E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49169894E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45701200E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08733033E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99405543E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.31045221E-04    2          24        25   # BR(H+ -> W+      h      )
     2.88282954E-13    2          24        36   # BR(H+ -> W+      A      )
