#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.90000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375320E+01   # W+
        25     1.25000000E+02   # h
        35     2.00417713E+03   # H
        36     2.00000000E+03   # A
        37     2.00207747E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013325E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989208E+03   # ~u_L
   2000002     4.99994934E+03   # ~u_R
   1000003     5.00013325E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989208E+03   # ~c_L
   2000004     4.99994934E+03   # ~c_R
   1000005     4.99996565E+03   # ~b_1
   2000005     5.00019438E+03   # ~b_2
   1000006     4.99970003E+03   # ~t_1
   2000006     5.00477064E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001288E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     2.80000000E+03   # ~g
   1000022     2.20549617E+02   # ~chi_10
   1000023    -2.50217205E+02   # ~chi_20
   1000025     3.17402170E+02   # ~chi_30
   1000035     3.00226542E+03   # ~chi_40
   1000024     2.47883478E+02   # ~chi_1+
   1000037     3.00226493E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.30446798E-01   # N_11
  1  2    -2.36121096E-02   # N_12
  1  3     6.06647593E-01   # N_13
  1  4    -5.91648004E-01   # N_14
  2  1     1.63095810E-02   # N_21
  2  2    -4.82920974E-03   # N_22
  2  3    -7.05467837E-01   # N_23
  2  4    -7.08537795E-01   # N_24
  3  1     8.47561191E-01   # N_31
  3  2     1.54233135E-02   # N_32
  3  3    -3.66086397E-01   # N_33
  3  4     3.83904805E-01   # N_34
  4  1     4.68663494E-04   # N_41
  4  2    -9.99590550E-01   # N_42
  4  3    -1.65704273E-02   # N_43
  4  4     2.33223686E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.34255110E-02   # U_11
  1  2     9.99725585E-01   # U_12
  2  1     9.99725585E-01   # U_21
  2  2     2.34255110E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.29781243E-02   # V_11
  1  2     9.99456074E-01   # V_12
  2  1     9.99456074E-01   # V_21
  2  2     3.29781243E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.11086217E-01   # cos(theta_t)
  1  2     7.03104823E-01   # sin(theta_t)
  2  1    -7.03104823E-01   # -sin(theta_t)
  2  2     7.11086217E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13897453E-01   # cos(theta_b)
  1  2     8.57851623E-01   # sin(theta_b)
  2  1    -8.57851623E-01   # -sin(theta_b)
  2  2     5.13897453E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89409334E-01   # cos(theta_tau)
  1  2     7.24371983E-01   # sin(theta_tau)
  2  1    -7.24371983E-01   # -sin(theta_tau)
  2  2     6.89409334E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90209818E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51797690E+02   # vev(Q)              
         4     3.82914434E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53062056E-01   # gprime(Q) DRbar
     2     6.28967612E-01   # g(Q) DRbar
     3     1.07511042E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02721493E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72264397E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79953185E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.90000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99421389E+06   # M^2_Hd              
        22    -5.33245291E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37848199E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.75871362E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.52214219E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.61339753E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.03311310E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.87517630E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.55862761E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.14618635E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.62053918E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.91896533E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.48157974E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.87517630E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.55862761E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.14618635E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.62053918E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.91896533E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.48157974E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.99229820E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.70804026E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.19698076E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.54908177E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.23076919E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.65823666E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.59622284E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.59622284E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.59622284E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.59622284E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.30784597E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.30784597E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.57197522E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.30377668E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.41625619E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.03669249E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.47242096E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.35113623E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.93457359E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.18446220E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.47362420E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.91512489E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.42744056E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.60767588E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.63308458E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.48796317E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.25732513E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     4.94327522E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     8.55646295E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.08834428E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.80234360E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.71862488E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.77839536E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -4.49183471E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -5.58438536E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.49446135E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.68459858E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.77698944E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.03850772E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.84277906E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.78407122E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.09166972E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.59492401E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     4.76645804E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.51946464E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.06417622E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.89700874E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.72445667E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.11808845E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.68308316E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.02352224E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.41767319E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.38388316E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.13674173E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.01799847E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.43457185E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.92137297E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.24266677E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.51952627E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.86384484E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.66080860E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.52106972E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.12047757E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.35383103E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.02419289E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.41847977E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.30533399E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.66339227E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.34866576E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.44042283E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.83455795E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.79927029E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.51946464E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.06417622E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.89700874E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.72445667E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.11808845E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.68308316E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.02352224E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.41767319E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.38388316E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.13674173E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.01799847E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.43457185E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.92137297E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.24266677E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.51952627E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.86384484E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.66080860E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.52106972E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.12047757E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.35383103E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.02419289E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.41847977E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.30533399E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.66339227E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.34866576E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.44042283E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.83455795E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.79927029E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03217224E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.85796551E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.20826889E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.55300016E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.64992338E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.08680935E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30407227E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46288188E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.82219697E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.66503521E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.17513709E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.04386042E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03217224E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.85796551E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.20826889E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.55300016E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.64992338E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.08680935E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30407227E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46288188E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.82219697E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.66503521E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.17513709E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.04386042E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.74105489E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.64663664E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29194544E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15250025E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39120568E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94189324E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78463399E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31257549E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.76397305E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.42325463E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.79325916E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.98224729E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.52739073E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.07175139E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.05720692E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03203039E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.67576046E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.26266049E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.36406561E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65242212E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40450622E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30062850E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03203039E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.67576046E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.26266049E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.36406561E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65242212E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40450622E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30062850E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.03523483E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.66871255E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26132744E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.36262550E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.64962183E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.45838554E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29503622E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.11080753E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.15557144E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.35581320E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.31140127E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11860537E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11852152E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09534308E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.18411554E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.11695681E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.41939939E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.06692371E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.44203543E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.12288927E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.06318230E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.08824796E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.05020567E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.05533356E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.03842406E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     8.79173783E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.99610401E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.01885260E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.91908808E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.20593148E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.34940970E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.53060574E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.17671083E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.36713917E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.27773563E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.21062105E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.56699864E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19189640E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.56644818E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.23151936E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.57613758E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.57586375E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49808073E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.13998680E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.13998680E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.13998680E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.89545169E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.89545169E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.67666100E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.67666100E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.31817342E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.31817342E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.25412719E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.25412719E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.03979222E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.03979222E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.14116860E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99908890E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.58137333E-05    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.48974175E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.99337381E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.18415423E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.65327720E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.06078678E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.13297023E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.06323075E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.06323075E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42450634E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.15542970E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.83314466E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.90121439E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.77103486E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.74987223E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.41763289E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     6.23990326E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.75888020E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.08109936E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.08109936E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.10785068E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.68311490E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.51573724E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.97953626E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.05325107E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08327259E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17418309E-01    2           5        -5   # BR(h -> b       bb     )
     6.37877662E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25785200E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78459005E-04    2           3        -3   # BR(h -> s       sb     )
     2.06410666E-02    2           4        -4   # BR(h -> c       cb     )
     6.70228684E-02    2          21        21   # BR(h -> g       g      )
     2.31759589E-03    2          22        22   # BR(h -> gam     gam    )
     1.53821597E-03    2          22        23   # BR(h -> Z       gam    )
     2.00937639E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56322949E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01283866E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38922128E-03    2           5        -5   # BR(H -> b       bb     )
     2.32208120E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20942156E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05109352E-06    2           3        -3   # BR(H -> s       sb     )
     9.48614587E-06    2           4        -4   # BR(H -> c       cb     )
     9.38614597E-01    2           6        -6   # BR(H -> t       tb     )
     7.51631493E-04    2          21        21   # BR(H -> g       g      )
     2.63971295E-06    2          22        22   # BR(H -> gam     gam    )
     1.09206232E-06    2          23        22   # BR(H -> Z       gam    )
     3.17614739E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58374153E-04    2          23        23   # BR(H -> Z       Z      )
     8.53080250E-04    2          25        25   # BR(H -> h       h      )
     8.63781156E-24    2          36        36   # BR(H -> A       A      )
     3.41924752E-11    2          23        36   # BR(H -> Z       A      )
     2.67402991E-12    2          24       -37   # BR(H -> W+      H-     )
     2.67402991E-12    2         -24        37   # BR(H -> W-      H+     )
     7.97799185E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12321264E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.75338384E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.61359407E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.82172958E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.43255791E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.71757459E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05770538E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39203585E-03    2           5        -5   # BR(A -> b       bb     )
     2.29839663E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12566239E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07125128E-06    2           3        -3   # BR(A -> s       sb     )
     9.38757719E-06    2           4        -4   # BR(A -> c       cb     )
     9.39529992E-01    2           6        -6   # BR(A -> t       tb     )
     8.89282987E-04    2          21        21   # BR(A -> g       g      )
     2.65445441E-06    2          22        22   # BR(A -> gam     gam    )
     1.27350660E-06    2          23        22   # BR(A -> Z       gam    )
     3.09541378E-04    2          23        25   # BR(A -> Z       h      )
     6.64195097E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.64651775E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.78134753E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.01356231E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.43300151E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.38253871E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.54049290E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97702688E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23341418E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34745808E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29911230E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41997758E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13945467E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02405571E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41266827E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17257779E-04    2          24        25   # BR(H+ -> W+      h      )
     1.27927345E-12    2          24        36   # BR(H+ -> W+      A      )
     1.63448049E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.41599948E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.16813949E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
