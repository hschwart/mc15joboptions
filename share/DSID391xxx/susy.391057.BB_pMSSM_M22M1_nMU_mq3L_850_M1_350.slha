#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954641E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04026616E+01   # W+
        25     1.24989350E+02   # h
        35     3.00005774E+03   # H
        36     2.99999993E+03   # A
        37     3.00108769E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03871625E+03   # ~d_L
   2000001     3.03357688E+03   # ~d_R
   1000002     3.03781003E+03   # ~u_L
   2000002     3.03518563E+03   # ~u_R
   1000003     3.03871625E+03   # ~s_L
   2000003     3.03357688E+03   # ~s_R
   1000004     3.03781003E+03   # ~c_L
   2000004     3.03518563E+03   # ~c_R
   1000005     9.51012432E+02   # ~b_1
   2000005     3.03168643E+03   # ~b_2
   1000006     9.48692891E+02   # ~t_1
   2000006     3.02385353E+03   # ~t_2
   1000011     3.00649641E+03   # ~e_L
   2000011     3.00152927E+03   # ~e_R
   1000012     3.00511069E+03   # ~nu_eL
   1000013     3.00649641E+03   # ~mu_L
   2000013     3.00152927E+03   # ~mu_R
   1000014     3.00511069E+03   # ~nu_muL
   1000015     2.98614996E+03   # ~tau_1
   2000015     3.02164302E+03   # ~tau_2
   1000016     3.00505781E+03   # ~nu_tauL
   1000021     2.34803644E+03   # ~g
   1000022     3.52379791E+02   # ~chi_10
   1000023     7.35336832E+02   # ~chi_20
   1000025    -2.99692553E+03   # ~chi_30
   1000035     2.99728769E+03   # ~chi_40
   1000024     7.35500064E+02   # ~chi_1+
   1000037     2.99806410E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889523E-01   # N_11
  1  2    -5.59652235E-05   # N_12
  1  3    -1.48307153E-02   # N_13
  1  4    -9.93961166E-04   # N_14
  2  1     4.61675140E-04   # N_21
  2  2     9.99622284E-01   # N_22
  2  3     2.70183856E-02   # N_23
  2  4     5.00831127E-03   # N_24
  3  1    -9.78056431E-03   # N_31
  3  2     1.55670808E-02   # N_32
  3  3    -7.06857471E-01   # N_33
  3  4     7.07117050E-01   # N_34
  4  1     1.11834084E-02   # N_41
  4  2    -2.26484514E-02   # N_42
  4  3     7.06684210E-01   # N_43
  4  4     7.07078076E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99269884E-01   # U_11
  1  2     3.82060177E-02   # U_12
  2  1    -3.82060177E-02   # U_21
  2  2     9.99269884E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974931E-01   # V_11
  1  2    -7.08082316E-03   # V_12
  2  1    -7.08082316E-03   # V_21
  2  2    -9.99974931E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98510365E-01   # cos(theta_t)
  1  2    -5.45623587E-02   # sin(theta_t)
  2  1     5.45623587E-02   # -sin(theta_t)
  2  2     9.98510365E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99721236E-01   # cos(theta_b)
  1  2    -2.36103852E-02   # sin(theta_b)
  2  1     2.36103852E-02   # -sin(theta_b)
  2  2     9.99721236E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06957956E-01   # cos(theta_tau)
  1  2     7.07255575E-01   # sin(theta_tau)
  2  1    -7.07255575E-01   # -sin(theta_tau)
  2  2    -7.06957956E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00132343E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59546409E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43958494E+02   # higgs               
         4     1.04412893E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59546409E+03  # The gauge couplings
     1     3.62357137E-01   # gprime(Q) DRbar
     2     6.36976340E-01   # g(Q) DRbar
     3     1.02493816E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59546409E+03  # The trilinear couplings
  1  1     2.43298651E-06   # A_u(Q) DRbar
  2  2     2.43302192E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59546409E+03  # The trilinear couplings
  1  1     8.69408041E-07   # A_d(Q) DRbar
  2  2     8.69507338E-07   # A_s(Q) DRbar
  3  3     1.74938399E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59546409E+03  # The trilinear couplings
  1  1     3.64738138E-07   # A_e(Q) DRbar
  2  2     3.64755877E-07   # A_mu(Q) DRbar
  3  3     3.69713236E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59546409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51388775E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59546409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.78315617E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59546409E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01277850E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59546409E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.13542824E+04   # M^2_Hd              
        22    -9.04434819E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40828056E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.66853161E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47934103E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47934103E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52065897E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52065897E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.70631733E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     5.54382738E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.02669839E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.41891887E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13650826E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.69082798E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.41640940E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.87238310E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.19041860E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.98904414E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68311278E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60509096E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14219011E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.47251102E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.27889130E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.39196967E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.88014120E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.32074184E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.06349153E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.81302358E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.51824376E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.36194078E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.12505763E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.53787785E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.02435361E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.00032444E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.45212441E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.68158120E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.11819142E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55366036E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.70274662E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.95330275E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.10880029E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.36444590E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.27635643E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15817313E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57242730E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.05974280E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.11838229E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.43544993E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42757214E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68683124E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.12258292E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55248552E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.54753086E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.03808710E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.10315753E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.14570928E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.28312562E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65477238E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46996399E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.69709535E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.92786694E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.75485353E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55300345E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.68158120E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.11819142E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55366036E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.70274662E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.95330275E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.10880029E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.36444590E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.27635643E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15817313E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57242730E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.05974280E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.11838229E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.43544993E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42757214E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68683124E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.12258292E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55248552E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.54753086E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.03808710E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.10315753E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.14570928E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.28312562E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65477238E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46996399E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.69709535E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.92786694E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.75485353E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55300345E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59603671E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06173020E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98198022E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.36433298E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.34847320E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95628922E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.09093368E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52484481E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999804E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93669847E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.24290671E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.02592426E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59603671E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06173020E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98198022E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.36433298E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.34847320E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95628922E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.09093368E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52484481E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999804E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93669847E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.24290671E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.02592426E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58403111E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68584818E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10669913E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20745270E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53448581E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76871373E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07899087E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.45490994E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.25161357E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15187911E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.45647432E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59603846E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06163079E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97712408E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45687949E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.67438626E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96124501E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     7.42093245E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59603846E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06163079E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97712408E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45687949E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.67438626E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96124501E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     7.42093245E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59629209E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06153619E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97684839E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.39932287E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.57071837E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96160139E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.39049699E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.37182720E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.75342801E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.32670955E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.59652285E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.35181291E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.18485780E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03247609E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.01819061E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.53211798E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.23470481E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.37819907E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.62180093E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.77084030E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.18228276E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.18761737E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.00237824E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.00237824E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.38487394E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.01680110E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33226207E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33226207E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.51236864E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.51236864E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.80626552E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.80626552E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.68000232E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.14362170E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.00378132E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.06793357E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.06793357E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.24596114E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.54903825E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.29944231E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.29944231E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.58104302E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.58104302E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.88736968E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.88736968E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.60588302E-03   # h decays
#          BR         NDA      ID1       ID2
     5.62499974E-01    2           5        -5   # BR(h -> b       bb     )
     7.19004391E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.54529738E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.40251680E-04    2           3        -3   # BR(h -> s       sb     )
     2.34764156E-02    2           4        -4   # BR(h -> c       cb     )
     7.62172152E-02    2          21        21   # BR(h -> g       g      )
     2.60858256E-03    2          22        22   # BR(h -> gam     gam    )
     1.73142092E-03    2          22        23   # BR(h -> Z       gam    )
     2.31783890E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89872813E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.55883904E+01   # H decays
#          BR         NDA      ID1       ID2
     8.97204764E-01    2           5        -5   # BR(H -> b       bb     )
     6.98751676E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.47061895E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.03393121E-04    2           3        -3   # BR(H -> s       sb     )
     8.50355216E-08    2           4        -4   # BR(H -> c       cb     )
     8.48283632E-03    2           6        -6   # BR(H -> t       tb     )
     1.10034794E-05    2          21        21   # BR(H -> g       g      )
     6.69870352E-08    2          22        22   # BR(H -> gam     gam    )
     3.47720295E-09    2          23        22   # BR(H -> Z       gam    )
     7.45538678E-07    2          24       -24   # BR(H -> W+      W-     )
     3.72309774E-07    2          23        23   # BR(H -> Z       Z      )
     5.81650362E-06    2          25        25   # BR(H -> h       h      )
    -3.36087404E-24    2          36        36   # BR(H -> A       A      )
     1.66825735E-20    2          23        36   # BR(H -> Z       A      )
     7.02333505E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.15351849E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.50894220E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.48599967E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.25201749E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.14578018E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.48009802E+01   # A decays
#          BR         NDA      ID1       ID2
     9.17533478E-01    2           5        -5   # BR(A -> b       bb     )
     7.14552849E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.52648459E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.10304770E-04    2           3        -3   # BR(A -> s       sb     )
     8.75636078E-08    2           4        -4   # BR(A -> c       cb     )
     8.73022438E-03    2           6        -6   # BR(A -> t       tb     )
     2.57096755E-05    2          21        21   # BR(A -> g       g      )
     6.75052612E-08    2          22        22   # BR(A -> gam     gam    )
     2.53331035E-08    2          23        22   # BR(A -> Z       gam    )
     7.59555381E-07    2          23        25   # BR(A -> Z       h      )
     9.11131465E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.43563018E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.55133233E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.80788588E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.82574923E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46433391E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.50229655E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.29905346E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.37172550E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.35109513E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.77963715E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.21672779E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.92115506E-07    2          24        25   # BR(H+ -> W+      h      )
     5.23740437E-14    2          24        36   # BR(H+ -> W+      A      )
     4.86673426E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.36078858E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08218041E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
