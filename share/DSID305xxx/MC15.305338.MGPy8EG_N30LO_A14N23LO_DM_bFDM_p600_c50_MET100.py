mchi = 50
mphi = 600
gx = 1.66986871893
filter_string = "T"
evt_multiplier = 15
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']
