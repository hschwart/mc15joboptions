#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954655E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04037300E+01   # W+
        25     1.24961711E+02   # h
        35     3.00005986E+03   # H
        36     2.99999993E+03   # A
        37     3.00108699E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03870500E+03   # ~d_L
   2000001     3.03357411E+03   # ~d_R
   1000002     3.03779974E+03   # ~u_L
   2000002     3.03518784E+03   # ~u_R
   1000003     3.03870500E+03   # ~s_L
   2000003     3.03357411E+03   # ~s_R
   1000004     3.03779974E+03   # ~c_L
   2000004     3.03518784E+03   # ~c_R
   1000005     9.52862349E+02   # ~b_1
   2000005     3.03171360E+03   # ~b_2
   1000006     9.50458199E+02   # ~t_1
   2000006     3.02390875E+03   # ~t_2
   1000011     3.00648854E+03   # ~e_L
   2000011     3.00151912E+03   # ~e_R
   1000012     3.00510410E+03   # ~nu_eL
   1000013     3.00648854E+03   # ~mu_L
   2000013     3.00151912E+03   # ~mu_R
   1000014     3.00510410E+03   # ~nu_muL
   1000015     2.98617468E+03   # ~tau_1
   2000015     3.02160669E+03   # ~tau_2
   1000016     3.00505332E+03   # ~nu_tauL
   1000021     2.34803503E+03   # ~g
   1000022     4.02684473E+02   # ~chi_10
   1000023     8.36819032E+02   # ~chi_20
   1000025    -2.99690339E+03   # ~chi_30
   1000035     2.99735931E+03   # ~chi_40
   1000024     8.36981502E+02   # ~chi_1+
   1000037     2.99810207E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888463E-01   # N_11
  1  2    -9.76915375E-05   # N_12
  1  3    -1.48827196E-02   # N_13
  1  4    -1.24793724E-03   # N_14
  2  1     5.13653939E-04   # N_21
  2  2     9.99605095E-01   # N_22
  2  3     2.74431397E-02   # N_23
  2  4     6.02204580E-03   # N_24
  3  1    -9.63724353E-03   # N_31
  3  2     1.51512519E-02   # N_32
  3  3    -7.06866065E-01   # N_33
  3  4     7.07119459E-01   # N_34
  4  1     1.13983302E-02   # N_41
  4  2    -2.36661111E-02   # N_42
  4  3     7.06658153E-01   # N_43
  4  4     7.07067358E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99246704E-01   # U_11
  1  2     3.88075196E-02   # U_12
  2  1    -3.88075196E-02   # U_21
  2  2     9.99246704E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99963751E-01   # V_11
  1  2    -8.51448619E-03   # V_12
  2  1    -8.51448619E-03   # V_21
  2  2    -9.99963751E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98510387E-01   # cos(theta_t)
  1  2    -5.45619561E-02   # sin(theta_t)
  2  1     5.45619561E-02   # -sin(theta_t)
  2  2     9.98510387E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99723618E-01   # cos(theta_b)
  1  2    -2.35093091E-02   # sin(theta_b)
  2  1     2.35093091E-02   # -sin(theta_b)
  2  2     9.99723618E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06962418E-01   # cos(theta_tau)
  1  2     7.07251115E-01   # sin(theta_tau)
  2  1    -7.07251115E-01   # -sin(theta_tau)
  2  2    -7.06962418E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00126705E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59546551E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43967763E+02   # higgs               
         4     1.04738642E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59546551E+03  # The gauge couplings
     1     3.62356329E-01   # gprime(Q) DRbar
     2     6.36680550E-01   # g(Q) DRbar
     3     1.02493327E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59546551E+03  # The trilinear couplings
  1  1     2.43649190E-06   # A_u(Q) DRbar
  2  2     2.43652660E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59546551E+03  # The trilinear couplings
  1  1     8.76980277E-07   # A_d(Q) DRbar
  2  2     8.77078687E-07   # A_s(Q) DRbar
  3  3     1.74908430E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59546551E+03  # The trilinear couplings
  1  1     3.60595881E-07   # A_e(Q) DRbar
  2  2     3.60613298E-07   # A_mu(Q) DRbar
  3  3     3.65463759E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59546551E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51349902E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59546551E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.76678778E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59546551E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00929869E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59546551E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.20590278E+04   # M^2_Hd              
        22    -9.04354281E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40692613E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.65842423E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47924648E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47924648E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52075352E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52075352E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.92366359E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.70588529E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.29411471E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13489783E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.65333173E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.51584173E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.07406356E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.19336518E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.99357496E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68459820E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60457215E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14013825E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.10534341E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.13792861E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.86207139E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.30257211E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.04943058E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.28966823E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.50682221E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.32810360E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -5.81222189E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.56047891E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.98461508E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.93420939E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.44181204E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56672282E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.17277869E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52283398E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.59506798E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.61487680E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.04706854E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.00082581E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.36836857E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15286533E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56184933E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.65640727E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.08857135E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.48774271E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43815005E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57205904E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.18250606E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52161627E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.39321724E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.95138207E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.04145300E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.33772657E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.37509990E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65334460E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43591231E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.03834043E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.84670668E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.88654196E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55640860E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56672282E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.17277869E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52283398E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.59506798E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.61487680E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.04706854E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.00082581E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.36836857E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15286533E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56184933E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.65640727E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.08857135E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.48774271E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43815005E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57205904E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.18250606E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52161627E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.39321724E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.95138207E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.04145300E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.33772657E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.37509990E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65334460E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43591231E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.03834043E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.84670668E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.88654196E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55640860E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47020489E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09070362E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97247287E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.32290236E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.79079068E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93682313E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.26585205E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51181992E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999765E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.32772471E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.09848329E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.03389876E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47020489E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09070362E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97247287E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.32290236E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.79079068E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93682313E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.26585205E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51181992E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999765E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.32772471E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.09848329E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.03389876E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51329591E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75675597E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08315539E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16008864E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46626066E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84024217E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05524399E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.48625988E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.27451431E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10408899E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.48767278E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47012627E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09093822E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96723838E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.41745388E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.41331323E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94182326E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.09693479E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47012627E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09093822E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96723838E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.41745388E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.41331323E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94182326E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.09693479E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47038012E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09083864E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96695433E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.36274440E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.30302139E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94219275E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.41481643E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.35268994E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.71699021E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.32078607E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61754913E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.34563715E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.12666244E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03147431E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.96058688E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.49789087E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.10020582E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.53113409E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.46886591E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.73256806E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.21705636E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.33255345E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.94533266E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.94533266E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.03237699E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.81048120E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34435544E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34435544E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.47941077E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.47941077E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.78453768E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.78453768E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.64412551E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.79579927E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.79896568E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.00944096E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.00944096E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.28360847E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.72297252E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31015460E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31015460E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.54642071E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.54642071E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.90465421E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.90465421E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.59821684E-03   # h decays
#          BR         NDA      ID1       ID2
     5.62480045E-01    2           5        -5   # BR(h -> b       bb     )
     7.20360316E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55009876E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.41294271E-04    2           3        -3   # BR(h -> s       sb     )
     2.35222477E-02    2           4        -4   # BR(h -> c       cb     )
     7.63321133E-02    2          21        21   # BR(h -> g       g      )
     2.61174509E-03    2          22        22   # BR(h -> gam     gam    )
     1.73088744E-03    2          22        23   # BR(h -> Z       gam    )
     2.31541934E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89486918E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.55233433E+01   # H decays
#          BR         NDA      ID1       ID2
     8.97170285E-01    2           5        -5   # BR(H -> b       bb     )
     7.00031699E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.47514480E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.03948870E-04    2           3        -3   # BR(H -> s       sb     )
     8.51893639E-08    2           4        -4   # BR(H -> c       cb     )
     8.49818321E-03    2           6        -6   # BR(H -> t       tb     )
     1.10554162E-05    2          21        21   # BR(H -> g       g      )
     7.36547598E-08    2          22        22   # BR(H -> gam     gam    )
     3.48498303E-09    2          23        22   # BR(H -> Z       gam    )
     7.31626297E-07    2          24       -24   # BR(H -> W+      W-     )
     3.65362202E-07    2          23        23   # BR(H -> Z       Z      )
     5.82704508E-06    2          25        25   # BR(H -> h       h      )
     5.31092450E-25    2          36        36   # BR(H -> A       A      )
     9.72822470E-20    2          23        36   # BR(H -> Z       A      )
     6.28811476E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.08566298E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.14143752E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.36865175E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.25329661E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.11356807E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.47402333E+01   # A decays
#          BR         NDA      ID1       ID2
     9.17422080E-01    2           5        -5   # BR(A -> b       bb     )
     7.15802320E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.53090241E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.10847371E-04    2           3        -3   # BR(A -> s       sb     )
     8.77167220E-08    2           4        -4   # BR(A -> c       cb     )
     8.74549010E-03    2           6        -6   # BR(A -> t       tb     )
     2.57546316E-05    2          21        21   # BR(A -> g       g      )
     6.57763818E-08    2          22        22   # BR(A -> gam     gam    )
     2.53820728E-08    2          23        22   # BR(A -> Z       gam    )
     7.45320279E-07    2          23        25   # BR(A -> Z       h      )
     8.93574231E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.42723213E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.46319383E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.77415239E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.82317704E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46429081E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.50666972E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.30059971E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.37144964E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.35200386E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.78150669E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.21651123E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.78414443E-07    2          24        25   # BR(H+ -> W+      h      )
     5.22412149E-14    2          24        36   # BR(H+ -> W+      A      )
     4.72123221E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.09243879E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08139848E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
