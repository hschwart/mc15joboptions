#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13002422E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.67990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04199378E+01   # W+
        25     1.26070908E+02   # h
        35     4.00000512E+03   # H
        36     3.99999734E+03   # A
        37     4.00103115E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02589752E+03   # ~d_L
   2000001     4.02255164E+03   # ~d_R
   1000002     4.02524863E+03   # ~u_L
   2000002     4.02326236E+03   # ~u_R
   1000003     4.02589752E+03   # ~s_L
   2000003     4.02255164E+03   # ~s_R
   1000004     4.02524863E+03   # ~c_L
   2000004     4.02326236E+03   # ~c_R
   1000005     1.01853068E+03   # ~b_1
   2000005     4.02546229E+03   # ~b_2
   1000006     9.91084760E+02   # ~t_1
   2000006     1.80352356E+03   # ~t_2
   1000011     4.00450883E+03   # ~e_L
   2000011     4.00334599E+03   # ~e_R
   1000012     4.00339956E+03   # ~nu_eL
   1000013     4.00450883E+03   # ~mu_L
   2000013     4.00334599E+03   # ~mu_R
   1000014     4.00339956E+03   # ~nu_muL
   1000015     4.00354466E+03   # ~tau_1
   2000015     4.00854613E+03   # ~tau_2
   1000016     4.00481306E+03   # ~nu_tauL
   1000021     1.97718615E+03   # ~g
   1000022     5.42379195E+02   # ~chi_10
   1000023    -5.80056117E+02   # ~chi_20
   1000025     6.01920524E+02   # ~chi_30
   1000035     2.05209052E+03   # ~chi_40
   1000024     5.78052566E+02   # ~chi_1+
   1000037     2.05225472E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.37059859E-01   # N_11
  1  2     2.30158658E-02   # N_12
  1  3     4.93635687E-01   # N_13
  1  4     4.61017182E-01   # N_14
  2  1    -2.89501079E-02   # N_21
  2  2     2.23599929E-02   # N_22
  2  3    -7.05378888E-01   # N_23
  2  4     7.07885969E-01   # N_24
  3  1     6.75205968E-01   # N_31
  3  2     2.79287246E-02   # N_32
  3  3     5.08594479E-01   # N_33
  3  4     5.33524641E-01   # N_34
  4  1    -1.24737573E-03   # N_41
  4  2     9.99094734E-01   # N_42
  4  3    -9.80245481E-03   # N_43
  4  4    -4.13771550E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.38689322E-02   # U_11
  1  2     9.99903822E-01   # U_12
  2  1    -9.99903822E-01   # U_21
  2  2     1.38689322E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.85232719E-02   # V_11
  1  2    -9.98286044E-01   # V_12
  2  1    -9.98286044E-01   # V_21
  2  2    -5.85232719E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85619948E-01   # cos(theta_t)
  1  2    -1.68977271E-01   # sin(theta_t)
  2  1     1.68977271E-01   # -sin(theta_t)
  2  2     9.85619948E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998234E-01   # cos(theta_b)
  1  2    -1.87936076E-03   # sin(theta_b)
  2  1     1.87936076E-03   # -sin(theta_b)
  2  2     9.99998234E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06372652E-01   # cos(theta_tau)
  1  2     7.07840149E-01   # sin(theta_tau)
  2  1    -7.07840149E-01   # -sin(theta_tau)
  2  2    -7.06372652E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00243966E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30024224E+03  # DRbar Higgs Parameters
         1    -5.67990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43660345E+02   # higgs               
         4     1.61383093E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30024224E+03  # The gauge couplings
     1     3.61841153E-01   # gprime(Q) DRbar
     2     6.35550731E-01   # g(Q) DRbar
     3     1.03007274E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30024224E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36760796E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30024224E+03  # The trilinear couplings
  1  1     5.06339213E-07   # A_d(Q) DRbar
  2  2     5.06385535E-07   # A_s(Q) DRbar
  3  3     9.04069546E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30024224E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13655815E-07   # A_mu(Q) DRbar
  3  3     1.14816225E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30024224E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68370746E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30024224E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.89916782E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30024224E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02726590E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30024224E+03  # The soft SUSY breaking masses at the scale Q
         1     5.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.55704877E+07   # M^2_Hd              
        22    -2.74401140E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40122968E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.66784404E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37074951E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37074951E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62167882E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62167882E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     7.57166432E-04    2     2000006        -6   # BR(~g -> ~t_2  tb)
     7.57166432E-04    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     6.31099286E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.17738875E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.96274092E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.63922956E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.22064077E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05574453E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.26608928E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01710343E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.46030760E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.97695208E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.75800755E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.37696088E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.59833636E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.95434979E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.80139899E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.38856460E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.82184791E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.79881885E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66116799E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.61108498E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.84274031E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54702113E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.30994961E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68995442E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.58042744E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11394091E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.56672121E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.95234184E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     8.21648603E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.44551126E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77736437E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.15572692E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.39476272E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.04401326E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81851247E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.76840277E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62491729E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51765174E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59849727E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.94413332E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.51841300E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.45001075E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.80908391E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.46013327E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77756330E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.24566438E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.53206319E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.42776063E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82440945E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.23975972E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65879728E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51981774E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53316255E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.67251186E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.17751164E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.38479230E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.25297253E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85930908E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77736437E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.15572692E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.39476272E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.04401326E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81851247E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.76840277E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62491729E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51765174E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59849727E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.94413332E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.51841300E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.45001075E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.80908391E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.46013327E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77756330E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.24566438E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.53206319E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.42776063E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82440945E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.23975972E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65879728E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51981774E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53316255E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.67251186E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.17751164E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.38479230E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.25297253E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85930908E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12722604E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.79926166E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.70366945E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.35731631E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78864159E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.89765199E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.59363259E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.00175560E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.45409084E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.36910994E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.53753124E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.81108489E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12722604E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.79926166E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.70366945E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.35731631E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78864159E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.89765199E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.59363259E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.00175560E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.45409084E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.36910994E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.53753124E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.81108489E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.04361428E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.95153821E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.53067740E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.77624278E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41616849E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.62329202E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.84065358E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.03168113E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.95236254E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.81471834E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61186642E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45760857E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.73030588E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92366005E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12827722E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.70853712E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.43541777E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.24248288E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79376445E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.37684553E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56992967E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12827722E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.70853712E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.43541777E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.24248288E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79376445E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.37684553E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56992967E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44463148E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.82025285E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.75513913E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.67136131E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53935846E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.41936035E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.06278895E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.21172625E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33783966E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33783966E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11262637E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11262637E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09906796E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.88747928E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.53319864E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.63894384E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.33719957E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.94545897E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.05083571E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.74041009E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.68896074E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.84396295E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.41063815E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.92930386E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.19044082E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54439004E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19044082E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54439004E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33889953E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.54263001E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.54263001E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49206997E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.08252240E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.08252240E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.08252230E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.69931808E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.69931808E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.69931808E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.69931808E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.23310684E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.23310684E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.23310684E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.23310684E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.41533376E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.41533376E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.36181567E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.37813144E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.10576517E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.34604520E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.27786584E-04    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.34604520E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.27786584E-04    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.04870495E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.93925841E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.93925841E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.97601079E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.35357809E-04    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.35357809E-04    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.35351923E-04    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.27570307E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.95238697E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.27570307E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.95238697E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90608539E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.77266968E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.77266968E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.52167002E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.35389443E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.35389443E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.35389444E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.21152914E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.21152914E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.21152914E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.21152914E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.03837430E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.03837430E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.03837430E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.03837430E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.92933685E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.92933685E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.88587186E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.26221120E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.75438396E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.26695423E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.74274670E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.74274670E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.17236242E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.16424528E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.48814984E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.72023473E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.72023473E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     8.98057097E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     8.98057097E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.71597670E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.71597670E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06796367E-03   # h decays
#          BR         NDA      ID1       ID2
     5.81463824E-01    2           5        -5   # BR(h -> b       bb     )
     6.43147610E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27671503E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82431383E-04    2           3        -3   # BR(h -> s       sb     )
     2.09545284E-02    2           4        -4   # BR(h -> c       cb     )
     6.86900425E-02    2          21        21   # BR(h -> g       g      )
     2.40415134E-03    2          22        22   # BR(h -> gam     gam    )
     1.68963843E-03    2          22        23   # BR(h -> Z       gam    )
     2.30399620E-01    2          24       -24   # BR(h -> W+      W-     )
     2.93733311E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.61203756E+01   # H decays
#          BR         NDA      ID1       ID2
     3.72877504E-01    2           5        -5   # BR(H -> b       bb     )
     5.90801361E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.08893052E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.47084478E-04    2           3        -3   # BR(H -> s       sb     )
     6.92958697E-08    2           4        -4   # BR(H -> c       cb     )
     6.94488885E-03    2           6        -6   # BR(H -> t       tb     )
     3.16344698E-07    2          21        21   # BR(H -> g       g      )
     1.30446397E-08    2          22        22   # BR(H -> gam     gam    )
     1.78861815E-09    2          23        22   # BR(H -> Z       gam    )
     1.62628312E-06    2          24       -24   # BR(H -> W+      W-     )
     8.12578340E-07    2          23        23   # BR(H -> Z       Z      )
     6.66130534E-06    2          25        25   # BR(H -> h       h      )
     2.20113088E-24    2          36        36   # BR(H -> A       A      )
     4.91208671E-20    2          23        36   # BR(H -> Z       A      )
     1.85654021E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.48037280E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.48037280E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.49861617E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.39877921E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66302803E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.95483941E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.94807132E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.22561997E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.70690231E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.20598509E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.42372615E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.88026126E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.48471193E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.55250839E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.55250839E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.35997837E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.61186318E+01   # A decays
#          BR         NDA      ID1       ID2
     3.72915122E-01    2           5        -5   # BR(A -> b       bb     )
     5.90822939E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.08900517E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.47133592E-04    2           3        -3   # BR(A -> s       sb     )
     6.97524695E-08    2           4        -4   # BR(A -> c       cb     )
     6.95906055E-03    2           6        -6   # BR(A -> t       tb     )
     1.42991061E-05    2          21        21   # BR(A -> g       g      )
     7.10649988E-08    2          22        22   # BR(A -> gam     gam    )
     1.57260633E-08    2          23        22   # BR(A -> Z       gam    )
     1.62287924E-06    2          23        25   # BR(A -> Z       h      )
     1.93214331E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.47992434E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.47992434E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.23697059E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.53732152E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.48214774E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.26099504E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.66248109E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.98241725E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.84637402E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.57628815E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.28833017E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.74495945E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.74495945E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.64412231E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.02169505E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.87597902E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.07760223E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.85388160E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.17700040E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.42146833E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.82891328E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.61542007E-06    2          24        25   # BR(H+ -> W+      h      )
     2.75322128E-14    2          24        36   # BR(H+ -> W+      A      )
     4.00376796E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.18731456E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.92839290E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.47391654E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.18579945E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.46933965E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.69063778E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.48026757E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.32689666E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
