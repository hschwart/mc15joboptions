#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871604E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03994498E+01   # W+
        25     1.25131632E+02   # h
        35     3.00006621E+03   # H
        36     2.99999993E+03   # A
        37     3.00090307E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04205936E+03   # ~d_L
   2000001     3.03687867E+03   # ~d_R
   1000002     3.04114166E+03   # ~u_L
   2000002     3.03781807E+03   # ~u_R
   1000003     3.04205936E+03   # ~s_L
   2000003     3.03687867E+03   # ~s_R
   1000004     3.04114166E+03   # ~c_L
   2000004     3.03781807E+03   # ~c_R
   1000005     1.04959343E+03   # ~b_1
   2000005     3.03582755E+03   # ~b_2
   1000006     1.04579459E+03   # ~t_1
   2000006     3.01886378E+03   # ~t_2
   1000011     3.00632530E+03   # ~e_L
   2000011     3.00222857E+03   # ~e_R
   1000012     3.00492188E+03   # ~nu_eL
   1000013     3.00632530E+03   # ~mu_L
   2000013     3.00222857E+03   # ~mu_R
   1000014     3.00492188E+03   # ~nu_muL
   1000015     2.98579333E+03   # ~tau_1
   2000015     3.02181880E+03   # ~tau_2
   1000016     3.00463547E+03   # ~nu_tauL
   1000021     2.35219901E+03   # ~g
   1000022     4.98438292E+01   # ~chi_10
   1000023     1.08604150E+02   # ~chi_20
   1000025    -2.99473677E+03   # ~chi_30
   1000035     2.99515403E+03   # ~chi_40
   1000024     1.08756015E+02   # ~chi_1+
   1000037     2.99586010E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886250E-01   # N_11
  1  2    -2.69515247E-03   # N_12
  1  3     1.48071936E-02   # N_13
  1  4    -9.85458086E-04   # N_14
  2  1     3.08294774E-03   # N_21
  2  2     9.99652626E-01   # N_22
  2  3    -2.60849839E-02   # N_23
  2  4     2.16691994E-03   # N_24
  3  1    -9.72460198E-03   # N_31
  3  2     1.69409273E-02   # N_32
  3  3     7.06818376E-01   # N_33
  3  4     7.07125322E-01   # N_34
  4  1    -1.11092532E-02   # N_41
  4  2     2.00091887E-02   # N_42
  4  3     7.06758873E-01   # N_43
  4  4    -7.07084233E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99317420E-01   # U_11
  1  2    -3.69417532E-02   # U_12
  2  1     3.69417532E-02   # U_21
  2  2     9.99317420E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995287E-01   # V_11
  1  2    -3.07005043E-03   # V_12
  2  1     3.07005043E-03   # V_21
  2  2     9.99995287E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447535E-01   # cos(theta_t)
  1  2    -5.57002680E-02   # sin(theta_t)
  2  1     5.57002680E-02   # -sin(theta_t)
  2  2     9.98447535E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99908532E-01   # cos(theta_b)
  1  2     1.35250743E-02   # sin(theta_b)
  2  1    -1.35250743E-02   # -sin(theta_b)
  2  2     9.99908532E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06887305E-01   # cos(theta_tau)
  1  2     7.07326189E-01   # sin(theta_tau)
  2  1    -7.07326189E-01   # -sin(theta_tau)
  2  2     7.06887305E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01736884E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68716039E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43966295E+02   # higgs               
         4     7.78683746E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68716039E+03  # The gauge couplings
     1     3.62524767E-01   # gprime(Q) DRbar
     2     6.41892651E-01   # g(Q) DRbar
     3     1.02372588E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68716039E+03  # The trilinear couplings
  1  1     2.69952941E-06   # A_u(Q) DRbar
  2  2     2.69956875E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68716039E+03  # The trilinear couplings
  1  1     6.67059701E-07   # A_d(Q) DRbar
  2  2     6.67193220E-07   # A_s(Q) DRbar
  3  3     1.56148715E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68716039E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.48485550E-07   # A_mu(Q) DRbar
  3  3     1.50302202E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68716039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50389029E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68716039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.11875527E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68716039E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04830378E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68716039E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.18826493E+04   # M^2_Hd              
        22    -9.03158195E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.43024631E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.15158311E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47648870E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47648870E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52351130E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52351130E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.27387634E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.16176913E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.18871387E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.69510922E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11417626E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.94238006E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.31686035E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.64782492E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.58919397E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.98440195E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66713520E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60179189E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14150935E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.26126252E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.26699278E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.39795391E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.47534681E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17264448E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.01617637E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.14083017E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.48442234E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.46167622E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.11007302E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.32728912E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.76827700E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.07359370E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.86397315E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.10786388E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.74001707E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.66561197E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.00078805E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.52795471E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.32962105E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.14978293E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.94736582E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16095263E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.61293457E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.53104352E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.39107249E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.78144310E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.38704980E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.11282089E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.07602779E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.66110793E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.40738346E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.73017469E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.32382714E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.73698843E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.95429840E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65000345E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.59534324E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.36202631E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.79955152E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.86373017E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54046123E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.10786388E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.74001707E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.66561197E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.00078805E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.52795471E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.32962105E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.14978293E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.94736582E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16095263E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.61293457E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.53104352E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.39107249E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.78144310E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.38704980E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.11282089E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.07602779E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.66110793E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.40738346E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.73017469E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.32382714E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.73698843E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.95429840E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65000345E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.59534324E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.36202631E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.79955152E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.86373017E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54046123E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07581410E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.54342226E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02358427E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.34600694E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.13819106E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02207305E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.98548842E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56871889E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990508E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.48700397E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.35146336E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.73683599E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07581410E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.54342226E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02358427E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.34600694E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.13819106E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02207305E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.98548842E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56871889E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990508E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.48700397E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.35146336E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.73683599E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.85134673E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43094195E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19573812E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37331993E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.79092837E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51117058E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16924690E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.43673503E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60743816E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.31911375E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.64346642E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07617301E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.72196056E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00091858E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.96365649E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     8.79321740E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02688520E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.06555388E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07617301E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.72196056E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00091858E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.96365649E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     8.79321740E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02688520E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.06555388E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07612813E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.72113992E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00066409E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.57884600E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.28678281E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02720122E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.05503112E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.82705563E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35834474E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.22648132E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35834474E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.22648132E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09618630E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.40886771E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09618630E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.40886771E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09093773E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     7.85608336E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.58937965E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.01893748E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.79842430E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.60315854E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.33906491E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.44603928E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.83295481E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     5.22804106E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.24154081E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.10497157E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.01211247E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.10497157E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.01211247E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.37898987E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     7.71961840E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     7.71961840E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     8.12127786E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.26158694E-03    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.26158694E-03    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.26023539E-03    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     7.89747817E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.37625450E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.39023984E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.38193522E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.38193522E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.08459294E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.21872419E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54050639E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54050639E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.17807455E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.17807455E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.05525530E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.05525530E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.79061889E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.06897388E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.19139480E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.48486527E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.48486527E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.43629752E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.61108913E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.51518904E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.51518904E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.20933317E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.20933317E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.35317294E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.35317294E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.80777322E-03   # h decays
#          BR         NDA      ID1       ID2
     6.67924324E-01    2           5        -5   # BR(h -> b       bb     )
     5.43343337E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92344663E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.08136397E-04    2           3        -3   # BR(h -> s       sb     )
     1.76234169E-02    2           4        -4   # BR(h -> c       cb     )
     5.74227507E-02    2          21        21   # BR(h -> g       g      )
     1.96964403E-03    2          22        22   # BR(h -> gam     gam    )
     1.31506114E-03    2          22        23   # BR(h -> Z       gam    )
     1.76637092E-01    2          24       -24   # BR(h -> W+      W-     )
     2.21308457E-02    2          23        23   # BR(h -> Z       Z      )
     4.20506591E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.41379867E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39832352E-01    2           5        -5   # BR(H -> b       bb     )
     1.75888675E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.21900323E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.63695958E-04    2           3        -3   # BR(H -> s       sb     )
     2.15427959E-07    2           4        -4   # BR(H -> c       cb     )
     2.14903160E-02    2           6        -6   # BR(H -> t       tb     )
     2.61677611E-05    2          21        21   # BR(H -> g       g      )
     2.55309591E-08    2          22        22   # BR(H -> gam     gam    )
     8.29900848E-09    2          23        22   # BR(H -> Z       gam    )
     2.89255041E-05    2          24       -24   # BR(H -> W+      W-     )
     1.44449169E-05    2          23        23   # BR(H -> Z       Z      )
     7.85259936E-05    2          25        25   # BR(H -> h       h      )
    -1.55459239E-23    2          36        36   # BR(H -> A       A      )
     9.32238356E-20    2          23        36   # BR(H -> Z       A      )
     2.41141632E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12924405E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.19755270E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.36282125E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.67913040E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.26802557E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33366828E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84369333E-01    2           5        -5   # BR(A -> b       bb     )
     1.86456707E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59265436E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.09714853E-04    2           3        -3   # BR(A -> s       sb     )
     2.28490054E-07    2           4        -4   # BR(A -> c       cb     )
     2.27808047E-02    2           6        -6   # BR(A -> t       tb     )
     6.70872900E-05    2          21        21   # BR(A -> g       g      )
     4.25891001E-08    2          22        22   # BR(A -> gam     gam    )
     6.60680819E-08    2          23        22   # BR(A -> Z       gam    )
     3.05480904E-05    2          23        25   # BR(A -> Z       h      )
     2.61294222E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21450788E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29762086E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.94188686E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04080490E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10457269E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38994126E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.45024937E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.06925420E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96603579E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02167325E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18014240E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.92036275E-05    2          24        25   # BR(H+ -> W+      h      )
     7.59638162E-14    2          24        36   # BR(H+ -> W+      A      )
     2.00422267E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.90593076E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.79201880E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
