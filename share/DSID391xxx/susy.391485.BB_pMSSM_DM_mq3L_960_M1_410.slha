#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13003325E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.32990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04192526E+01   # W+
        25     1.26075653E+02   # h
        35     4.00000804E+03   # H
        36     3.99999699E+03   # A
        37     4.00104557E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02590638E+03   # ~d_L
   2000001     4.02256406E+03   # ~d_R
   1000002     4.02525712E+03   # ~u_L
   2000002     4.02329114E+03   # ~u_R
   1000003     4.02590638E+03   # ~s_L
   2000003     4.02256406E+03   # ~s_R
   1000004     4.02525712E+03   # ~c_L
   2000004     4.02329114E+03   # ~c_R
   1000005     1.01636443E+03   # ~b_1
   2000005     4.02544289E+03   # ~b_2
   1000006     9.88936250E+02   # ~t_1
   2000006     1.80275812E+03   # ~t_2
   1000011     4.00452682E+03   # ~e_L
   2000011     4.00342481E+03   # ~e_R
   1000012     4.00341675E+03   # ~nu_eL
   1000013     4.00452682E+03   # ~mu_L
   2000013     4.00342481E+03   # ~mu_R
   1000014     4.00341675E+03   # ~nu_muL
   1000015     4.00419713E+03   # ~tau_1
   2000015     4.00802732E+03   # ~tau_2
   1000016     4.00484249E+03   # ~nu_tauL
   1000021     1.97719184E+03   # ~g
   1000022     3.96577645E+02   # ~chi_10
   1000023    -4.44564688E+02   # ~chi_20
   1000025     4.60513976E+02   # ~chi_30
   1000035     2.05212435E+03   # ~chi_40
   1000024     4.42369036E+02   # ~chi_1+
   1000037     2.05228873E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.12279563E-01   # N_11
  1  2    -1.75924975E-02   # N_12
  1  3    -4.35241838E-01   # N_13
  1  4    -3.87887816E-01   # N_14
  2  1    -3.87119524E-02   # N_21
  2  2     2.36142328E-02   # N_22
  2  3    -7.04445827E-01   # N_23
  2  4     7.08307723E-01   # N_24
  3  1     5.81981177E-01   # N_31
  3  2     2.80098398E-02   # N_32
  3  3     5.60602646E-01   # N_33
  3  4     5.88419945E-01   # N_34
  4  1    -1.09792740E-03   # N_41
  4  2     9.99173819E-01   # N_42
  4  3    -6.72999372E-03   # N_43
  4  4    -4.00647080E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.52360043E-03   # U_11
  1  2     9.99954649E-01   # U_12
  2  1    -9.99954649E-01   # U_21
  2  2     9.52360043E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.66698418E-02   # V_11
  1  2    -9.98392973E-01   # V_12
  2  1    -9.98392973E-01   # V_21
  2  2    -5.66698418E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85685353E-01   # cos(theta_t)
  1  2    -1.68595329E-01   # sin(theta_t)
  2  1     1.68595329E-01   # -sin(theta_t)
  2  2     9.85685353E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998995E-01   # cos(theta_b)
  1  2    -1.41774433E-03   # sin(theta_b)
  2  1     1.41774433E-03   # -sin(theta_b)
  2  2     9.99998995E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06123385E-01   # cos(theta_tau)
  1  2     7.08088812E-01   # sin(theta_tau)
  2  1    -7.08088812E-01   # -sin(theta_tau)
  2  2    -7.06123385E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00268239E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30033249E+03  # DRbar Higgs Parameters
         1    -4.32990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43613362E+02   # higgs               
         4     1.61069947E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30033249E+03  # The gauge couplings
     1     3.61896898E-01   # gprime(Q) DRbar
     2     6.35850388E-01   # g(Q) DRbar
     3     1.03007451E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30033249E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36776337E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30033249E+03  # The trilinear couplings
  1  1     5.04021096E-07   # A_d(Q) DRbar
  2  2     5.04067834E-07   # A_s(Q) DRbar
  3  3     9.01712890E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30033249E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11762302E-07   # A_mu(Q) DRbar
  3  3     1.12907041E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30033249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68585621E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30033249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86920384E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30033249E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03156012E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30033249E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57039398E+07   # M^2_Hd              
        22    -1.38851051E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40258126E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.68465088E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36962799E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36962799E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61950465E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61950465E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.08673565E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.08673565E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     9.67960179E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.39318218E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.13726044E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.31912047E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.15043690E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.09942473E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.74215193E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.07030171E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.70823824E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.08137795E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.68484723E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.32228704E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.49614706E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.02767091E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.00235322E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.09598105E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.26040901E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86412567E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66002589E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57588014E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.83425952E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60704918E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.60360259E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68140203E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.18265914E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12060456E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.02968426E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.77362609E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.60497094E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.02110316E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77777517E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.60472570E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26835192E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63904758E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82289382E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.50526897E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63365769E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51617501E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60022150E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.63314506E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.21183080E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.85270066E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.72287144E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45059387E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77797202E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.53869739E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.21354821E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.80881805E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82825317E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.55490337E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66649420E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51835263E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53365803E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.47526631E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.14164709E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.83184388E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.70690672E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85671464E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77777517E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.60472570E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26835192E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63904758E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82289382E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.50526897E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63365769E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51617501E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60022150E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.63314506E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.21183080E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.85270066E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.72287144E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45059387E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77797202E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.53869739E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.21354821E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.80881805E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82825317E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.55490337E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66649420E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51835263E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53365803E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.47526631E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.14164709E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.83184388E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.70690672E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85671464E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13920888E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.94840152E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.25119022E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.44708766E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78149777E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.08047874E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57803275E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04069137E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.61347743E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.49450372E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.37157083E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.69823090E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13920888E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.94840152E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.25119022E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.44708766E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78149777E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.08047874E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57803275E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04069137E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.61347743E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.49450372E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.37157083E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.69823090E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07726166E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38795597E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47965194E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.38460028E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40589393E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54183677E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81940095E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06977468E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.41869717E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.98439212E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.17063227E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43789256E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90828623E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88351017E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14026987E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.15776020E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04214147E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.59029129E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78564913E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21319661E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55500815E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14026987E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.15776020E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04214147E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.59029129E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78564913E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21319661E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55500815E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46357404E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05007878E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.45217022E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16337636E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52780688E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.55468277E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04085626E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.62910496E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33604130E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33604130E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11202863E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11202863E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10386012E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.92468616E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.51640933E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.76641861E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.32648653E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.01194741E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.13157151E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.81117849E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.66485227E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.92382745E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.62730459E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.42023367E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18205998E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53343217E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18205998E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53343217E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40157293E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51698783E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51698783E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48216042E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03132196E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03132196E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03132183E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.31022773E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.31022773E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.31022773E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.31022773E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.70076530E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.70076530E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.70076530E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.70076530E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.20763359E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.20763359E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.58461628E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.13245037E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.42137635E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.51393663E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.23639058E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.51393663E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.23639058E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.09142051E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     7.22806844E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     7.22806844E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.23901663E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.49347530E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.49347530E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.49346964E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.37645265E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.78565645E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.37645265E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.78565645E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.18323521E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.09573445E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.09573445E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.81590383E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.18742470E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.18742470E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.18742478E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.93116865E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.93116865E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.93116865E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.93116865E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.97702246E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.97702246E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.97702246E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.97702246E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.83991927E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.83991927E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.92321631E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.99366649E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.32157018E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.89282251E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.80848883E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.80848883E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.37006199E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.66122800E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.87146466E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.71321570E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.71321570E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.53283936E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.53283936E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.70915644E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.70915644E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.08702561E-03   # h decays
#          BR         NDA      ID1       ID2
     5.83237439E-01    2           5        -5   # BR(h -> b       bb     )
     6.40234186E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26640143E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80241831E-04    2           3        -3   # BR(h -> s       sb     )
     2.08574232E-02    2           4        -4   # BR(h -> c       cb     )
     6.83811871E-02    2          21        21   # BR(h -> g       g      )
     2.39299371E-03    2          22        22   # BR(h -> gam     gam    )
     1.68241339E-03    2          22        23   # BR(h -> Z       gam    )
     2.29464920E-01    2          24       -24   # BR(h -> W+      W-     )
     2.92533231E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.55240095E+01   # H decays
#          BR         NDA      ID1       ID2
     3.63542581E-01    2           5        -5   # BR(H -> b       bb     )
     5.97147266E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11136810E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49738435E-04    2           3        -3   # BR(H -> s       sb     )
     7.00469912E-08    2           4        -4   # BR(H -> c       cb     )
     7.02016693E-03    2           6        -6   # BR(H -> t       tb     )
     3.70023122E-07    2          21        21   # BR(H -> g       g      )
     5.25170955E-09    2          22        22   # BR(H -> gam     gam    )
     1.80580513E-09    2          23        22   # BR(H -> Z       gam    )
     1.76688141E-06    2          24       -24   # BR(H -> W+      W-     )
     8.82828757E-07    2          23        23   # BR(H -> Z       Z      )
     7.04698947E-06    2          25        25   # BR(H -> h       h      )
     2.44415110E-24    2          36        36   # BR(H -> A       A      )
     1.99341695E-20    2          23        36   # BR(H -> Z       A      )
     1.84763778E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55552629E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55552629E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.42358586E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.96730896E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.56845980E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.37235790E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.40285818E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.78740033E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.37995797E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.19424737E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.62604201E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.49280806E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.06611538E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     7.48947926E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.48947926E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.31400649E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.55185518E+01   # A decays
#          BR         NDA      ID1       ID2
     3.63604084E-01    2           5        -5   # BR(A -> b       bb     )
     5.97208879E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11158429E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49804752E-04    2           3        -3   # BR(A -> s       sb     )
     7.05063935E-08    2           4        -4   # BR(A -> c       cb     )
     7.03427800E-03    2           6        -6   # BR(A -> t       tb     )
     1.44536605E-05    2          21        21   # BR(A -> g       g      )
     6.44225015E-08    2          22        22   # BR(A -> gam     gam    )
     1.58941447E-08    2          23        22   # BR(A -> Z       gam    )
     1.76329699E-06    2          23        25   # BR(A -> Z       h      )
     1.87871467E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55550745E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55550745E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.11063177E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.39210407E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34234445E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.84303178E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.45217790E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.11301425E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.53016582E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.29277610E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.19495726E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     8.41130593E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     8.41130593E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.56975962E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.84867418E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.95445155E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10534819E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.74314831E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19271847E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45380545E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.72176234E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.75962687E-06    2          24        25   # BR(H+ -> W+      h      )
     2.99507072E-14    2          24        36   # BR(H+ -> W+      A      )
     5.19186137E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.61596105E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.17259747E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55272229E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.89331324E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54398041E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.81163729E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     7.71361347E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.60391531E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
