#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13384893E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04012834E+01   # W+
        25     1.26005754E+02   # h
        35     3.00020908E+03   # H
        36     2.99999957E+03   # A
        37     3.00100350E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02909536E+03   # ~d_L
   2000001     3.02377884E+03   # ~d_R
   1000002     3.02817502E+03   # ~u_L
   2000002     3.02547120E+03   # ~u_R
   1000003     3.02909536E+03   # ~s_L
   2000003     3.02377884E+03   # ~s_R
   1000004     3.02817502E+03   # ~c_L
   2000004     3.02547120E+03   # ~c_R
   1000005     6.83657184E+02   # ~b_1
   2000005     3.02316572E+03   # ~b_2
   1000006     6.77763950E+02   # ~t_1
   2000006     3.00806644E+03   # ~t_2
   1000011     3.00668784E+03   # ~e_L
   2000011     3.00145970E+03   # ~e_R
   1000012     3.00528697E+03   # ~nu_eL
   1000013     3.00668784E+03   # ~mu_L
   2000013     3.00145970E+03   # ~mu_R
   1000014     3.00528697E+03   # ~nu_muL
   1000015     2.98599716E+03   # ~tau_1
   2000015     3.02207933E+03   # ~tau_2
   1000016     3.00528814E+03   # ~nu_tauL
   1000021     2.33558418E+03   # ~g
   1000022     5.00544524E+01   # ~chi_10
   1000023     1.08390295E+02   # ~chi_20
   1000025    -3.00013784E+03   # ~chi_30
   1000035     3.00052720E+03   # ~chi_40
   1000024     1.08541759E+02   # ~chi_1+
   1000037     3.00123270E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886286E-01   # N_11
  1  2    -2.69848795E-03   # N_12
  1  3     1.48041354E-02   # N_13
  1  4    -9.85254710E-04   # N_14
  2  1     3.08675993E-03   # N_21
  2  2     9.99651625E-01   # N_22
  2  3    -2.61226347E-02   # N_23
  2  4     2.17004086E-03   # N_24
  3  1    -9.72245221E-03   # N_31
  3  2     1.69653729E-02   # N_32
  3  3     7.06817779E-01   # N_33
  3  4     7.07125362E-01   # N_34
  4  1    -1.11067918E-02   # N_41
  4  2     2.00380512E-02   # N_42
  4  3     7.06758143E-01   # N_43
  4  4    -7.07084184E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99315450E-01   # U_11
  1  2    -3.69950113E-02   # U_12
  2  1     3.69950113E-02   # U_21
  2  2     9.99315450E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995274E-01   # V_11
  1  2    -3.07447044E-03   # V_12
  2  1     3.07447044E-03   # V_21
  2  2     9.99995274E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98611015E-01   # cos(theta_t)
  1  2    -5.26881459E-02   # sin(theta_t)
  2  1     5.26881459E-02   # -sin(theta_t)
  2  2     9.98611015E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99921123E-01   # cos(theta_b)
  1  2     1.25597682E-02   # sin(theta_b)
  2  1    -1.25597682E-02   # -sin(theta_b)
  2  2     9.99921123E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06870723E-01   # cos(theta_tau)
  1  2     7.07342761E-01   # sin(theta_tau)
  2  1    -7.07342761E-01   # -sin(theta_tau)
  2  2     7.06870723E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01783294E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.33848931E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44435124E+02   # higgs               
         4     6.89085374E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.33848931E+03  # The gauge couplings
     1     3.61749706E-01   # gprime(Q) DRbar
     2     6.41586405E-01   # g(Q) DRbar
     3     1.02894136E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.33848931E+03  # The trilinear couplings
  1  1     1.68329490E-06   # A_u(Q) DRbar
  2  2     1.68331957E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.33848931E+03  # The trilinear couplings
  1  1     4.08538638E-07   # A_d(Q) DRbar
  2  2     4.08623184E-07   # A_s(Q) DRbar
  3  3     9.68447513E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.33848931E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.98505974E-08   # A_mu(Q) DRbar
  3  3     9.09279697E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.33848931E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56208720E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.33848931E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.09526557E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.33848931E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04685474E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.33848931E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.50325190E+04   # M^2_Hd              
        22    -9.11923113E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42880030E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.86352828E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47818570E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47818570E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52181430E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52181430E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.97342229E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.11457259E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.02034088E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.86820186E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18843486E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.52633916E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.84598286E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.70210509E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.15061057E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.83922004E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.73904493E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63373950E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.22675202E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.74982030E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.35737585E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.57010112E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.29416129E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22682184E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.92988249E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.63436347E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.54329556E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.43233243E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.06880424E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30881705E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.84857444E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.14496766E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.98686407E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12235218E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.67654881E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65281922E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.51170184E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.06855253E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.30405598E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.15409095E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.98635894E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19494573E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58585928E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.50907004E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.91625139E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.22541023E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41412552E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12735775E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.00982585E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64834648E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.47830283E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.34602937E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.29828377E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.78211050E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.99326913E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68228437E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.51401852E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.29543604E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.22452849E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.54722557E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54859382E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12235218E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.67654881E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65281922E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.51170184E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.06855253E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.30405598E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.15409095E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.98635894E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19494573E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58585928E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.50907004E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.91625139E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.22541023E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41412552E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12735775E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.00982585E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64834648E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.47830283E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.34602937E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.29828377E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.78211050E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.99326913E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68228437E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.51401852E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.29543604E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.22452849E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.54722557E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54859382E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07114010E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.51440884E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02455503E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.55354283E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.61724979E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02400396E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.08794232E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56161073E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990489E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.51061221E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.33605320E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.76509110E-11    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07114010E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.51440884E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02455503E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.55354283E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.61724979E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02400396E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.08794232E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56161073E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990489E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.51061221E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.33605320E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.76509110E-11    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84557954E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42352744E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19821734E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37825522E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78521356E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50349364E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17186929E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.28656574E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.06688896E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32433125E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.06261458E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07151310E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.69291592E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00188304E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.78516361E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.09430195E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02882533E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.15373279E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07151310E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.69291592E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00188304E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.78516361E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.09430195E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02882533E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.15373279E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07185408E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.69210801E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00163283E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.78582401E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.09515458E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02915228E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.05068939E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.75760144E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35847204E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.39861840E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35847204E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.39861840E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09613522E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.46623247E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09613522E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.46623247E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09078529E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.75186691E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.57068285E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.26823664E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.72332143E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.86962577E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.10785513E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.73276647E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.03664612E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.47475314E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.37177082E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     2.43904177E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.09183447E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     2.43904177E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.09183447E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.71743621E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.62283668E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.62283668E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.75943728E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.20657129E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.20657129E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.20659003E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.79530680E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.23976305E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.96803452E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.67707483E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.67707483E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.71367086E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.89331769E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.66109598E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.66109598E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.17567353E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.17567353E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.07018231E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.07018231E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.68794834E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.61637710E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.88052338E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.76090424E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.76090424E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.28484981E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.13713720E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.64029571E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.64029571E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.20399584E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.20399584E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.87342384E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.87342384E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     5.02928253E-03   # h decays
#          BR         NDA      ID1       ID2
     6.61672393E-01    2           5        -5   # BR(h -> b       bb     )
     5.23146228E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.85191755E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.92425737E-04    2           3        -3   # BR(h -> s       sb     )
     1.69418749E-02    2           4        -4   # BR(h -> c       cb     )
     5.60291027E-02    2          21        21   # BR(h -> g       g      )
     1.93894135E-03    2          22        22   # BR(h -> gam     gam    )
     1.35804197E-03    2          22        23   # BR(h -> Z       gam    )
     1.85556470E-01    2          24       -24   # BR(h -> W+      W-     )
     2.35698979E-02    2          23        23   # BR(h -> Z       Z      )
     4.10385180E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.40635825E+01   # H decays
#          BR         NDA      ID1       ID2
     7.32218296E-01    2           5        -5   # BR(H -> b       bb     )
     1.76827562E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.25220005E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.67767691E-04    2           3        -3   # BR(H -> s       sb     )
     2.16616681E-07    2           4        -4   # BR(H -> c       cb     )
     2.16089213E-02    2           6        -6   # BR(H -> t       tb     )
     3.69595097E-05    2          21        21   # BR(H -> g       g      )
     2.86210106E-08    2          22        22   # BR(H -> gam     gam    )
     8.32766491E-09    2          23        22   # BR(H -> Z       gam    )
     3.03500655E-05    2          24       -24   # BR(H -> W+      W-     )
     1.51563240E-05    2          23        23   # BR(H -> Z       Z      )
     8.08605002E-05    2          25        25   # BR(H -> h       h      )
    -1.63104091E-23    2          36        36   # BR(H -> A       A      )
     3.11636191E-17    2          23        36   # BR(H -> Z       A      )
     2.43147022E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13462112E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20750890E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.41060336E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.32891813E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.97016545E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31743411E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81698391E-01    2           5        -5   # BR(A -> b       bb     )
     1.88754310E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.67389200E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.19692535E-04    2           3        -3   # BR(A -> s       sb     )
     2.31305615E-07    2           4        -4   # BR(A -> c       cb     )
     2.30615203E-02    2           6        -6   # BR(A -> t       tb     )
     6.79139807E-05    2          21        21   # BR(A -> g       g      )
     4.30965278E-08    2          22        22   # BR(A -> gam     gam    )
     6.69033461E-08    2          23        22   # BR(A -> Z       gam    )
     3.22697692E-05    2          23        25   # BR(A -> Z       h      )
     2.65292088E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22878087E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31747170E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.04901639E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00078837E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.07790945E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.48558625E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.78842672E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.89860958E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.16475319E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.06255581E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.02521055E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.25490802E-05    2          24        25   # BR(H+ -> W+      h      )
     1.34077048E-13    2          24        36   # BR(H+ -> W+      A      )
     2.08678222E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.98938346E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.37131145E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
