evgenConfig.description = "dmS sample to dibjet"
evgenConfig.keywords = ["exotic", "BSM", "scalar"]
model  = 'dmS'
mR     = 1200
mDM    = 10000
gSM    = 1.08
gDM    = 1.00
widthR = 72.76

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet_withb.py")
