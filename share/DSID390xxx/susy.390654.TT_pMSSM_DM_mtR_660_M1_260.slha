#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12073312E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.87900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.22485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     6.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04192371E+01   # W+
        25     1.25331287E+02   # h
        35     4.00000745E+03   # H
        36     3.99999673E+03   # A
        37     4.00101679E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02327100E+03   # ~d_L
   2000001     4.02053526E+03   # ~d_R
   1000002     4.02262117E+03   # ~u_L
   2000002     4.02106497E+03   # ~u_R
   1000003     4.02327100E+03   # ~s_L
   2000003     4.02053526E+03   # ~s_R
   1000004     4.02262117E+03   # ~c_L
   2000004     4.02106497E+03   # ~c_R
   1000005     2.25149107E+03   # ~b_1
   2000005     4.02434500E+03   # ~b_2
   1000006     7.04841924E+02   # ~t_1
   2000006     2.26186161E+03   # ~t_2
   1000011     4.00385945E+03   # ~e_L
   2000011     4.00368306E+03   # ~e_R
   1000012     4.00274886E+03   # ~nu_eL
   1000013     4.00385945E+03   # ~mu_L
   2000013     4.00368306E+03   # ~mu_R
   1000014     4.00274886E+03   # ~nu_muL
   1000015     4.00479698E+03   # ~tau_1
   2000015     4.00776197E+03   # ~tau_2
   1000016     4.00442511E+03   # ~nu_tauL
   1000021     1.98928816E+03   # ~g
   1000022     2.45627230E+02   # ~chi_10
   1000023    -2.98712228E+02   # ~chi_20
   1000025     3.13818865E+02   # ~chi_30
   1000035     2.06715613E+03   # ~chi_40
   1000024     2.95800665E+02   # ~chi_1+
   1000037     2.06732354E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.29949568E-01   # N_11
  1  2    -1.46945618E-02   # N_12
  1  3    -4.29880624E-01   # N_13
  1  4    -3.55204776E-01   # N_14
  2  1    -5.95001326E-02   # N_21
  2  2     2.51476466E-02   # N_22
  2  3    -7.01873776E-01   # N_23
  2  4     7.09366289E-01   # N_24
  3  1     5.54655278E-01   # N_31
  3  2     2.64607924E-02   # N_32
  3  3     5.67945577E-01   # N_33
  3  4     6.07532032E-01   # N_34
  4  1    -9.85347848E-04   # N_41
  4  2     9.99225446E-01   # N_42
  4  3    -3.69758775E-03   # N_43
  4  4    -3.91645853E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.23446655E-03   # U_11
  1  2     9.99986300E-01   # U_12
  2  1    -9.99986300E-01   # U_21
  2  2     5.23446655E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.53987558E-02   # V_11
  1  2    -9.98464310E-01   # V_12
  2  1    -9.98464310E-01   # V_21
  2  2    -5.53987558E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.33906523E-02   # cos(theta_t)
  1  2     9.96516934E-01   # sin(theta_t)
  2  1    -9.96516934E-01   # -sin(theta_t)
  2  2    -8.33906523E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999185E-01   # cos(theta_b)
  1  2    -1.27671427E-03   # sin(theta_b)
  2  1     1.27671427E-03   # -sin(theta_b)
  2  2     9.99999185E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05650153E-01   # cos(theta_tau)
  1  2     7.08560415E-01   # sin(theta_tau)
  2  1    -7.08560415E-01   # -sin(theta_tau)
  2  2    -7.05650153E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00273661E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20733120E+03  # DRbar Higgs Parameters
         1    -2.87900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43847769E+02   # higgs               
         4     1.61732836E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20733120E+03  # The gauge couplings
     1     3.61833783E-01   # gprime(Q) DRbar
     2     6.35610572E-01   # g(Q) DRbar
     3     1.03105358E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20733120E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.14798224E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20733120E+03  # The trilinear couplings
  1  1     4.22130170E-07   # A_d(Q) DRbar
  2  2     4.22169745E-07   # A_s(Q) DRbar
  3  3     7.55301821E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20733120E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.28619220E-08   # A_mu(Q) DRbar
  3  3     9.38138447E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20733120E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68115070E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20733120E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84424651E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20733120E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03499022E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20733120E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58013089E+07   # M^2_Hd              
        22    -9.59392104E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.22485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     6.59989995E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40152415E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.23154032E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.41691922E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.77536847E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.05524657E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.09242005E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.97479653E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.17602831E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.48839180E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.22026877E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.55221727E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.80021580E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     5.00382234E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.80040689E-03    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.12867481E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     2.50537167E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.35823984E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     6.85083409E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.22504401E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.27482566E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.54569675E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.29813960E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     4.53101162E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.65223104E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.22287129E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
     3.68205675E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64551761E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60650180E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80295032E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59244718E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.47040668E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63976481E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.27886461E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13296782E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.16661463E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     8.03266874E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.98748168E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.39670737E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76216526E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.78898743E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.67904151E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.51523380E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80474074E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36827910E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59733175E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52121436E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58915976E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.85932134E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.97683265E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.71594816E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98310306E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44049592E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76235629E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.60132226E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.16106260E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.42243127E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80970676E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.79298176E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62935317E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52344936E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.52201731E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00725585E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.15939020E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.47849926E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.78425282E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85397341E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76216526E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.78898743E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.67904151E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.51523380E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80474074E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36827910E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59733175E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52121436E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58915976E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.85932134E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.97683265E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.71594816E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98310306E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44049592E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76235629E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.60132226E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.16106260E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.42243127E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80970676E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.79298176E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62935317E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52344936E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.52201731E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00725585E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.15939020E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.47849926E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.78425282E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85397341E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11507502E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07426932E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.88752288E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.97480272E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77118500E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.79971432E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55639668E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06689688E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.89838196E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.53269837E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.06628579E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.26992179E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11507502E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07426932E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.88752288E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.97480272E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77118500E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.79971432E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55639668E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06689688E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.89838196E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.53269837E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.06628579E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.26992179E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08441641E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.54902228E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.36393416E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.28976878E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38971423E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48632373E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78646892E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08256151E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.55605357E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.30779681E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.05350517E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41404714E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10375013E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83523943E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11617465E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.21586277E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.77809793E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.26700492E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77454475E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13395298E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53377148E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11617465E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.21586277E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.77809793E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.26700492E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77454475E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13395298E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53377148E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44537848E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10015530E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.60889031E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.86095133E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51201834E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75473661E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01016866E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.19883288E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33556877E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33556877E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11186982E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11186982E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10512281E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.66189570E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.28683327E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.46676418E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.45265646E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40869746E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.84828053E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.40230886E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.21779077E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.09835996E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17997132E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53075469E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17997132E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53075469E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41922931E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51101018E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51101018E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48049647E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01977527E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01977527E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01977505E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.64338710E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.64338710E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.64338710E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.64338710E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.21446487E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.21446487E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.21446487E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.21446487E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.22523243E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.22523243E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.24590575E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.00611643E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.72152960E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.67764191E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.62975874E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.67764191E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.62975874E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.28488013E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.95324805E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.95324805E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.94640838E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.96993590E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.96993590E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.96992712E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.46435644E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.38643441E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.46435644E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.38643441E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.94011568E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.92376635E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.92376635E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.77718640E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.84561310E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.84561310E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.84561315E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.29912332E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.29912332E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.29912332E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.29912332E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.76635438E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.76635438E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.76635438E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.76635438E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.68386639E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.68386639E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.66062948E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.47963324E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.47156242E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.48223756E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.40495415E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.40495415E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.02720009E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.75689108E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.11905538E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.62438852E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.62438852E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.01867385E-03   # h decays
#          BR         NDA      ID1       ID2
     5.98213474E-01    2           5        -5   # BR(h -> b       bb     )
     6.47284142E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29139042E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.86096469E-04    2           3        -3   # BR(h -> s       sb     )
     2.11113000E-02    2           4        -4   # BR(h -> c       cb     )
     6.87879490E-02    2          21        21   # BR(h -> g       g      )
     2.36802031E-03    2          22        22   # BR(h -> gam     gam    )
     1.60260315E-03    2          22        23   # BR(h -> Z       gam    )
     2.15330175E-01    2          24       -24   # BR(h -> W+      W-     )
     2.71428294E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43447978E+01   # H decays
#          BR         NDA      ID1       ID2
     3.50400322E-01    2           5        -5   # BR(H -> b       bb     )
     6.10104465E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15718162E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55157388E-04    2           3        -3   # BR(H -> s       sb     )
     7.15684610E-08    2           4        -4   # BR(H -> c       cb     )
     7.17264987E-03    2           6        -6   # BR(H -> t       tb     )
     8.72933889E-07    2          21        21   # BR(H -> g       g      )
     2.79891469E-10    2          22        22   # BR(H -> gam     gam    )
     1.84467101E-09    2          23        22   # BR(H -> Z       gam    )
     1.83394079E-06    2          24       -24   # BR(H -> W+      W-     )
     9.16335223E-07    2          23        23   # BR(H -> Z       Z      )
     6.90817687E-06    2          25        25   # BR(H -> h       h      )
    -1.18484695E-24    2          36        36   # BR(H -> A       A      )
     3.61145175E-20    2          23        36   # BR(H -> Z       A      )
     1.86791706E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62170077E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62170077E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.56470391E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.67800323E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.56732402E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.41837647E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.11524346E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.98360761E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.41040316E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.05573773E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.22741620E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.30474147E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.11891012E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.11891012E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43392563E+01   # A decays
#          BR         NDA      ID1       ID2
     3.50461854E-01    2           5        -5   # BR(A -> b       bb     )
     6.10169740E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15741071E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55226113E-04    2           3        -3   # BR(A -> s       sb     )
     7.20365514E-08    2           4        -4   # BR(A -> c       cb     )
     7.18693871E-03    2           6        -6   # BR(A -> t       tb     )
     1.47673409E-05    2          21        21   # BR(A -> g       g      )
     5.58985088E-08    2          22        22   # BR(A -> gam     gam    )
     1.62390429E-08    2          23        22   # BR(A -> Z       gam    )
     1.83029189E-06    2          23        25   # BR(A -> Z       h      )
     1.87416929E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.62186865E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.62186865E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.20711443E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.08689691E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.29708506E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.99671926E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.63043346E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.03451852E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.58709059E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.96616861E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.26173098E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.18851032E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.18851032E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42763184E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.59497412E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11033066E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16046322E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.58078036E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22394326E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51804487E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.56512003E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.83445804E-06    2          24        25   # BR(H+ -> W+      h      )
     2.67760146E-14    2          24        36   # BR(H+ -> W+      A      )
     5.73085267E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.63691853E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.09990317E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62585048E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.03406444E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60498877E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.05187858E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.41606555E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
