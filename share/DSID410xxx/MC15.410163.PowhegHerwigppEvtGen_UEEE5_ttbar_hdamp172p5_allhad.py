#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP ttbar production with Powheg hdamp equal top mass, UEEE5 tune, allhadronic'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'amoroso@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 3.
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()

#--------------------------------------------------------------
# HerwigPP (UEEE5) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')


## Add to commands
cmds = """                                                                                                                                                                                                   
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                                                       
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                                                           
"""
genSeq.Herwigpp.Commands += cmds.splitlines()


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


