evgenConfig.description = "Dijet truth jet slice JZ1B, with the A14 NNPDF23 LO tune, PhaseSpace:bias2Selection = on and muon filter"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/JetFilter_JZ1W.py")
include("MC15JobOptions/LowPtMuonFilter.py")

#genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 7.0",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = "+str(minDict[1]) ,
                            "PhaseSpace:bias2SelectionPow = 5.0"
                           ]

filtSeq.QCDTruthJetFilter.DoShape = False
filtSeq.UseEventWeight = True
filtSeq.QCDTruthJetFilter.MinPt = 40*GeV

evgenConfig.minevents = 200
