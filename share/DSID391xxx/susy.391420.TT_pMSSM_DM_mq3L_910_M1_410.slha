#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13016187E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.32990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04187477E+01   # W+
        25     1.25982324E+02   # h
        35     4.00000842E+03   # H
        36     3.99999709E+03   # A
        37     4.00103920E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02593549E+03   # ~d_L
   2000001     4.02257687E+03   # ~d_R
   1000002     4.02528522E+03   # ~u_L
   2000002     4.02335240E+03   # ~u_R
   1000003     4.02593549E+03   # ~s_L
   2000003     4.02257687E+03   # ~s_R
   1000004     4.02528522E+03   # ~c_L
   2000004     4.02335240E+03   # ~c_R
   1000005     9.70279237E+02   # ~b_1
   2000005     4.02545353E+03   # ~b_2
   1000006     9.47956071E+02   # ~t_1
   2000006     1.90124256E+03   # ~t_2
   1000011     4.00455950E+03   # ~e_L
   2000011     4.00337624E+03   # ~e_R
   1000012     4.00344834E+03   # ~nu_eL
   1000013     4.00455950E+03   # ~mu_L
   2000013     4.00337624E+03   # ~mu_R
   1000014     4.00344834E+03   # ~nu_muL
   1000015     4.00418948E+03   # ~tau_1
   2000015     4.00801329E+03   # ~tau_2
   1000016     4.00487192E+03   # ~nu_tauL
   1000021     1.97813045E+03   # ~g
   1000022     3.96656468E+02   # ~chi_10
   1000023    -4.44555376E+02   # ~chi_20
   1000025     4.60427896E+02   # ~chi_30
   1000035     2.05211531E+03   # ~chi_40
   1000024     4.42347461E+02   # ~chi_1+
   1000037     2.05227938E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.12268393E-01   # N_11
  1  2    -1.75954396E-02   # N_12
  1  3    -4.35254750E-01   # N_13
  1  4    -3.87896586E-01   # N_14
  2  1    -3.87154006E-02   # N_21
  2  2     2.36176970E-02   # N_22
  2  3    -7.04445215E-01   # N_23
  2  4     7.08308027E-01   # N_24
  3  1     5.81996538E-01   # N_31
  3  2     2.80136817E-02   # N_32
  3  3     5.60593378E-01   # N_33
  3  4     5.88413399E-01   # N_34
  4  1    -1.09818526E-03   # N_41
  4  2     9.99173578E-01   # N_42
  4  3    -6.73097212E-03   # N_43
  4  4    -4.00705561E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.52498737E-03   # U_11
  1  2     9.99954636E-01   # U_12
  2  1    -9.99954636E-01   # U_21
  2  2     9.52498737E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.66781207E-02   # V_11
  1  2    -9.98392503E-01   # V_12
  2  1    -9.98392503E-01   # V_21
  2  2    -5.66781207E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89880503E-01   # cos(theta_t)
  1  2    -1.41903452E-01   # sin(theta_t)
  2  1     1.41903452E-01   # -sin(theta_t)
  2  2     9.89880503E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999010E-01   # cos(theta_b)
  1  2    -1.40712438E-03   # sin(theta_b)
  2  1     1.40712438E-03   # -sin(theta_b)
  2  2     9.99999010E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06119214E-01   # cos(theta_tau)
  1  2     7.08092971E-01   # sin(theta_tau)
  2  1    -7.08092971E-01   # -sin(theta_tau)
  2  2    -7.06119214E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00264145E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30161874E+03  # DRbar Higgs Parameters
         1    -4.32990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43636833E+02   # higgs               
         4     1.61237089E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30161874E+03  # The gauge couplings
     1     3.61894401E-01   # gprime(Q) DRbar
     2     6.35882331E-01   # g(Q) DRbar
     3     1.03008398E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30161874E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37053633E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30161874E+03  # The trilinear couplings
  1  1     5.05157260E-07   # A_d(Q) DRbar
  2  2     5.05204090E-07   # A_s(Q) DRbar
  3  3     9.03544072E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30161874E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11787565E-07   # A_mu(Q) DRbar
  3  3     1.12930333E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30161874E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68066725E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30161874E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86575196E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30161874E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03135370E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30161874E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57041541E+07   # M^2_Hd              
        22    -1.48113511E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40274425E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.94511738E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40022633E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40022633E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59977367E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59977367E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.72261598E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.46762966E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.17553139E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.23580101E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.12103794E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.15374062E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.75231518E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08146059E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.94926995E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.12126286E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.67742704E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.27524634E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.47444466E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.26090964E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.07420296E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.15546405E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.28416922E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84861638E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66352567E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57100419E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82627221E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60102820E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59091133E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.66537310E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.15741642E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12409773E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.03391504E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.77285241E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.63025697E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.99955214E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78159150E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.60110873E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26461758E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.63572462E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81313259E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.49563686E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61415814E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51918050E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60402931E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.62436795E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.19374939E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.84840569E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.71586960E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45190289E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78179095E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.53325197E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.19872277E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.79598028E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81847878E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.55217999E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64693094E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52135544E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53742262E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.45154722E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.13674395E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.82021943E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.68764409E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85706856E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78159150E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.60110873E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26461758E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.63572462E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81313259E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.49563686E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61415814E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51918050E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60402931E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.62436795E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.19374939E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.84840569E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.71586960E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45190289E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78179095E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.53325197E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.19872277E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.79598028E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81847878E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.55217999E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64693094E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52135544E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53742262E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.45154722E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.13674395E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.82021943E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.68764409E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85706856E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13953917E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.94676274E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.25556911E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.44694957E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78155512E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.08318040E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57815277E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04063337E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.61325673E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.49477435E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.37178882E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.70132045E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13953917E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.94676274E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.25556911E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.44694957E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78155512E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.08318040E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57815277E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04063337E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.61325673E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.49477435E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.37178882E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.70132045E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07726926E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.38774357E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47855677E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.38464979E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40601637E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54085557E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81964903E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06978848E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.41852935E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.98332260E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.17056973E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43804311E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90710893E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88381465E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14059840E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.15761187E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04233719E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.58993185E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78570768E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21415656E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55512233E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14059840E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.15761187E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04233719E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.58993185E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78570768E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21415656E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55512233E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46383711E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.04997374E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.45421097E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16316726E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52792909E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.55228587E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04109764E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.53088761E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33605362E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33605362E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11203275E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11203275E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10382727E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.10230204E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.56446580E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.12674393E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.38950892E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.73585189E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.05815682E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.54225789E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.47003142E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.64882799E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.54652698E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.71519590E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18207329E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53343589E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18207329E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53343589E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40132368E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51691722E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51691722E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48199833E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03115562E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03115562E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03115549E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.39837197E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.39837197E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.39837197E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.39837197E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.99457966E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.99457966E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.99457966E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.99457966E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35254950E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35254950E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.50243059E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.15659930E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.47413448E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.50969167E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.23084256E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.50969167E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.23084256E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.08375106E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     7.21507212E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     7.21507212E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     7.22591931E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.49089806E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.49089806E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.49089240E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.36401774E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.76950915E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.36401774E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.76950915E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.04187109E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.05860633E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.05860633E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.77864579E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.11317588E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.11317588E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.11317595E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.90625165E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.90625165E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.90625165E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.90625165E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.96871689E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.96871689E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.96871689E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.96871689E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.83105222E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.83105222E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.10075208E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.71811611E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.13817982E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.82765156E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.53976234E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.53976234E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.32263329E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.56891274E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.73687349E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.73876535E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.73876535E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73895529E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73895529E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07337781E-03   # h decays
#          BR         NDA      ID1       ID2
     5.84603831E-01    2           5        -5   # BR(h -> b       bb     )
     6.41892122E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27227446E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81555748E-04    2           3        -3   # BR(h -> s       sb     )
     2.09148332E-02    2           4        -4   # BR(h -> c       cb     )
     6.85516484E-02    2          21        21   # BR(h -> g       g      )
     2.39272896E-03    2          22        22   # BR(h -> gam     gam    )
     1.67432841E-03    2          22        23   # BR(h -> Z       gam    )
     2.27947033E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90176017E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.54430819E+01   # H decays
#          BR         NDA      ID1       ID2
     3.63093294E-01    2           5        -5   # BR(H -> b       bb     )
     5.98018974E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11445025E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50102997E-04    2           3        -3   # BR(H -> s       sb     )
     7.01480941E-08    2           4        -4   # BR(H -> c       cb     )
     7.03029955E-03    2           6        -6   # BR(H -> t       tb     )
     4.59322124E-07    2          21        21   # BR(H -> g       g      )
     4.99367573E-09    2          22        22   # BR(H -> gam     gam    )
     1.80851168E-09    2          23        22   # BR(H -> Z       gam    )
     1.74835127E-06    2          24       -24   # BR(H -> W+      W-     )
     8.73570077E-07    2          23        23   # BR(H -> Z       Z      )
     6.98836753E-06    2          25        25   # BR(H -> h       h      )
     1.51438318E-24    2          36        36   # BR(H -> A       A      )
     6.30369486E-20    2          23        36   # BR(H -> Z       A      )
     1.85086600E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55779552E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55779552E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.42727420E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.97583216E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.57087269E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.37583059E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.41000389E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.79147729E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.38211392E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.20613779E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.63289127E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.13064363E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.94382681E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     7.54705981E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.54705981E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.33558493E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54383679E+01   # A decays
#          BR         NDA      ID1       ID2
     3.63149895E-01    2           5        -5   # BR(A -> b       bb     )
     5.98072673E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11463845E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50166066E-04    2           3        -3   # BR(A -> s       sb     )
     7.06083727E-08    2           4        -4   # BR(A -> c       cb     )
     7.04445225E-03    2           6        -6   # BR(A -> t       tb     )
     1.44745656E-05    2          21        21   # BR(A -> g       g      )
     6.45236400E-08    2          22        22   # BR(A -> gam     gam    )
     1.59157343E-08    2          23        22   # BR(A -> Z       gam    )
     1.74478858E-06    2          23        25   # BR(A -> Z       h      )
     1.88196284E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55775547E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55775547E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.11385407E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.40296841E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34436291E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.84714048E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     4.45797879E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.11770928E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.53254786E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.30332360E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.20222167E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     8.18872752E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     8.18872752E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.56248478E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.84278827E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.96222953E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10809829E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73938133E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19427669E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.45701121E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.71817641E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.74090770E-06    2          24        25   # BR(H+ -> W+      h      )
     2.90755090E-14    2          24        36   # BR(H+ -> W+      A      )
     5.19865385E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.62372496E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.17712416E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55474453E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.90120181E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54598533E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.82428494E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.82356456E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.57876238E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
