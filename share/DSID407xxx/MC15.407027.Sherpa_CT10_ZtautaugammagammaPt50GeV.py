include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "tau+ tau- gamma gamma production with up to two jets in ME+PS."
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.keywords = [ "electroweak", "triboson", "diphoton", "2tau", "SM" ]
evgenConfig.inputconfcheck = "Ztautaugammagamma"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 15 -15 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.02 {6,7}
  End process;
}(processes)
(selector){
  Mass 15 -15 10 E_CMS
  PT 22 50 E_CMS
  DeltaR 22 90 0.2 1000
  DeltaR 22 22 0.2 1000
  DeltaR 93 90 0.2 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""
