#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416907E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400499E+03   # H
        36     2.00000000E+03   # A
        37     2.00151466E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.95285060E+03   # ~d_L
   2000001     4.95274062E+03   # ~d_R
   1000002     4.95260468E+03   # ~u_L
   2000002     4.95266276E+03   # ~u_R
   1000003     4.95285060E+03   # ~s_L
   2000003     4.95274062E+03   # ~s_R
   1000004     4.95260468E+03   # ~c_L
   2000004     4.95266276E+03   # ~c_R
   1000005     4.95211400E+03   # ~b_1
   2000005     4.95347861E+03   # ~b_2
   1000006     5.06792604E+03   # ~t_1
   2000006     5.36020650E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99963923E+03   # ~tau_1
   2000015     5.00051965E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.97443430E+03   # ~g
   1000022     1.70131905E+03   # ~chi_10
   1000023    -1.75207621E+03   # ~chi_20
   1000025     1.78859316E+03   # ~chi_30
   1000035     3.12643723E+03   # ~chi_40
   1000024     1.74768047E+03   # ~chi_1+
   1000037     3.12643296E+03   # ~chi_2+
   1000039     2.50166667E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.28456181E-01   # N_11
  1  2    -3.73109187E-02   # N_12
  1  3     4.85132376E-01   # N_13
  1  4    -4.82292511E-01   # N_14
  2  1     2.49823325E-03   # N_21
  2  2    -3.18303779E-03   # N_22
  2  3    -7.07029194E-01   # N_23
  2  4    -7.07172784E-01   # N_24
  3  1     6.85085486E-01   # N_31
  3  2     4.23033478E-02   # N_32
  3  3    -5.13167716E-01   # N_33
  3  4     5.15293314E-01   # N_34
  4  1     1.79695838E-03   # N_41
  4  2    -9.98402820E-01   # N_42
  4  3    -3.76190307E-02   # N_43
  4  4     4.21116265E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.31471142E-02   # U_11
  1  2     9.98586693E-01   # U_12
  2  1     9.98586693E-01   # U_21
  2  2     5.31471142E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.94999072E-02   # V_11
  1  2     9.98228311E-01   # V_12
  2  1     9.98228311E-01   # V_21
  2  2     5.94999072E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99916909E-01   # cos(theta_t)
  1  2     1.28908920E-02   # sin(theta_t)
  2  1    -1.28908920E-02   # -sin(theta_t)
  2  2     9.99916909E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.78012532E-01   # cos(theta_b)
  1  2     7.35050343E-01   # sin(theta_b)
  2  1    -7.35050343E-01   # -sin(theta_b)
  2  2     6.78012532E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04680600E-01   # cos(theta_tau)
  1  2     7.09524666E-01   # sin(theta_tau)
  2  1    -7.09524666E-01   # -sin(theta_tau)
  2  2     7.04680600E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202486E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52259160E+02   # vev(Q)              
         4     3.21793749E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52737073E-01   # gprime(Q) DRbar
     2     6.26885138E-01   # g(Q) DRbar
     3     1.08126718E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02511725E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71539949E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79759249E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.90000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -3.50654159E+03   # M^2_Hd              
        22    -7.94268300E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36919197E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.64109622E-07   # gluino decays
#          BR         NDA      ID1       ID2
     3.09270447E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.75236952E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.19135927E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     7.22412562E-04    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.73605013E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.96623366E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.93586866E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.10558737E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.27146020E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.72983350E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.73605013E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.96623366E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.93586866E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.10558737E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.27146020E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.72983350E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.73090520E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.33542705E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.03019608E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     4.34466725E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.34466725E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.34466725E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.34466725E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     9.07920095E-03    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     9.07920095E-03    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.72897152E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.86920920E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.03106864E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.61481739E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.73270851E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.50205081E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.40091764E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.00140564E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.03677069E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.52425336E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.39486196E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.69809799E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.08200397E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.03203699E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.35226580E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.81911516E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.48617166E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.09594924E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -4.15927690E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.73710314E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     7.59535450E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     8.82993870E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.19315586E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.44466712E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.71108025E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.04603198E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.35403626E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.07111258E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.74081032E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.45372466E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.28825868E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.48691456E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.50012285E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.66583007E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.66481588E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.34559910E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.37062438E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.09837942E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.06313319E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.95166140E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.10923066E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.99859671E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.28071462E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.91664568E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.56358994E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.98371016E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.93426697E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.24519779E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.59821357E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.93049866E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.38497523E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.58242712E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.06320788E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.13228441E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.24640453E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.08988230E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.28562021E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.92296042E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.56924861E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.98416819E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.87374132E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.79444591E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.70551908E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.98226988E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.64789966E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89223201E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.06313319E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.95166140E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.10923066E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.99859671E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.28071462E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.91664568E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.56358994E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.98371016E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.93426697E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.24519779E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.59821357E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.93049866E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.38497523E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.58242712E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.06320788E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.13228441E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.24640453E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.08988230E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.28562021E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.92296042E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.56924861E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.98416819E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.87374132E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.79444591E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.70551908E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.98226988E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.64789966E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89223201E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.65974425E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.97578727E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78631533E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02269563E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71156509E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.19905802E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43615210E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.91050691E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.37553735E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.22252059E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.62438491E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55181599E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.65974425E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.97578727E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78631533E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02269563E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71156509E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.19905802E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43615210E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.91050691E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.37553735E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.22252059E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.62438491E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55181599E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.28994597E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.77925183E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57242378E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.48888433E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55862923E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.38935947E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.12476856E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     2.50954867E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.28772509E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.63962541E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.30498806E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.56524724E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59240114E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04737258E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.19237385E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.65993773E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14889109E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19055128E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.58069966E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72190661E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.00895529E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.43092372E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.65993773E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14889109E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19055128E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.58069966E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72190661E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.00895529E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.43092372E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.66241015E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14782419E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.18944569E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.57458857E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71937895E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.93176632E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42590140E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.34124795E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.11239639E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33600921E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33600921E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11200428E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11200428E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10396189E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.82353657E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53464052E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.08604367E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46191446E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.38500756E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53239379E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.43131341E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.65401280E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.99493394E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.99662219E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.89353473E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09843072E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.71739568E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.37795434E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.71804025E-13    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.37055196E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.33569026E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18481334E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53421193E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18481334E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53421193E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40993538E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50415358E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50415358E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47148681E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99816608E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99816608E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99816608E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.51633396E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.51633396E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.51633396E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.51633396E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.17211139E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17211139E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.17211139E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.17211139E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.34180363E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.34180363E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.88465018E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.45028911E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.12293501E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     6.81951228E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.08131158E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.77900821E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.32838401E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.57300577E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.32838401E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.57300577E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.33058802E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.71518450E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.71518450E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.91534953E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.71038018E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.71038018E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.71038018E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.14564303E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.77839108E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.14564303E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.77839108E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.37131352E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.34587737E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.34587737E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.24911959E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.26733279E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.26733279E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.26733279E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.36850756E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.36850756E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.36850756E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.36850756E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.56168538E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.56168538E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.56168538E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.56168538E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.51918084E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.51918084E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.82351660E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.68069768E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52210188E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.34711589E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46633171E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46633171E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.11143290E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     7.84472560E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41792927E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.42302957E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.08883398E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.40961385E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.58929530E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.52066480E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22102511E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02148507E-01    2           5        -5   # BR(h -> b       bb     )
     6.21995423E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20159263E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65814427E-04    2           3        -3   # BR(h -> s       sb     )
     2.00961260E-02    2           4        -4   # BR(h -> c       cb     )
     6.63673653E-02    2          21        21   # BR(h -> g       g      )
     2.30517308E-03    2          22        22   # BR(h -> gam     gam    )
     1.62564365E-03    2          22        23   # BR(h -> Z       gam    )
     2.16508456E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80632127E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78099601E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46777448E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427882E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71214308E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547301E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668508E-05    2           4        -4   # BR(H -> c       cb     )
     9.96068184E-01    2           6        -6   # BR(H -> t       tb     )
     7.97755303E-04    2          21        21   # BR(H -> g       g      )
     2.71636904E-06    2          22        22   # BR(H -> gam     gam    )
     1.16020938E-06    2          23        22   # BR(H -> Z       gam    )
     3.34767176E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66927065E-04    2          23        23   # BR(H -> Z       Z      )
     9.02234023E-04    2          25        25   # BR(H -> h       h      )
     7.29782659E-24    2          36        36   # BR(H -> A       A      )
     2.94066972E-11    2          23        36   # BR(H -> Z       A      )
     6.66028540E-12    2          24       -37   # BR(H -> W+      H-     )
     6.66028540E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381104E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47070205E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898464E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269177E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677742E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179520E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999031E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678524E-04    2          21        21   # BR(A -> g       g      )
     3.14150330E-06    2          22        22   # BR(A -> gam     gam    )
     1.35280532E-06    2          23        22   # BR(A -> Z       gam    )
     3.26234593E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472011E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35697973E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238372E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147679E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49847839E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695822E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731962E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402225E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34355354E-04    2          24        25   # BR(H+ -> W+      h      )
     2.79969189E-13    2          24        36   # BR(H+ -> W+      A      )
