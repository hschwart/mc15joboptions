#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937782E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04011154E+01   # W+
        25     1.25567991E+02   # h
        35     3.00013044E+03   # H
        36     2.99999993E+03   # A
        37     3.00109513E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03104694E+03   # ~d_L
   2000001     3.02581727E+03   # ~d_R
   1000002     3.03013551E+03   # ~u_L
   2000002     3.02818547E+03   # ~u_R
   1000003     3.03104694E+03   # ~s_L
   2000003     3.02581727E+03   # ~s_R
   1000004     3.03013551E+03   # ~c_L
   2000004     3.02818547E+03   # ~c_R
   1000005     7.41364017E+02   # ~b_1
   2000005     3.02538690E+03   # ~b_2
   1000006     7.38512826E+02   # ~t_1
   2000006     3.02192035E+03   # ~t_2
   1000011     3.00682590E+03   # ~e_L
   2000011     3.00075632E+03   # ~e_R
   1000012     3.00543729E+03   # ~nu_eL
   1000013     3.00682590E+03   # ~mu_L
   2000013     3.00075632E+03   # ~mu_R
   1000014     3.00543729E+03   # ~nu_muL
   1000015     2.98620181E+03   # ~tau_1
   2000015     3.02194389E+03   # ~tau_2
   1000016     3.00564740E+03   # ~nu_tauL
   1000021     2.33844603E+03   # ~g
   1000022     2.52363736E+02   # ~chi_10
   1000023     5.29628964E+02   # ~chi_20
   1000025    -3.00179067E+03   # ~chi_30
   1000035     3.00195771E+03   # ~chi_40
   1000024     5.29791522E+02   # ~chi_1+
   1000037     3.00281368E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891096E-01   # N_11
  1  2     7.59544237E-05   # N_12
  1  3    -1.47494522E-02   # N_13
  1  4    -4.93264292E-04   # N_14
  2  1     3.15336003E-04   # N_21
  2  2     9.99646026E-01   # N_22
  2  3     2.64211172E-02   # N_23
  2  4     3.10623301E-03   # N_24
  3  1    -1.00790312E-02   # N_31
  3  2     1.64880127E-02   # N_32
  3  3    -7.06838046E-01   # N_33
  3  4     7.07111402E-01   # N_34
  4  1     1.07753939E-02   # N_41
  4  2    -2.08797323E-02   # N_42
  4  3     7.06727921E-01   # N_43
  4  4     7.07095165E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99301920E-01   # U_11
  1  2     3.73587139E-02   # U_12
  2  1    -3.73587139E-02   # U_21
  2  2     9.99301920E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990360E-01   # V_11
  1  2    -4.39085471E-03   # V_12
  2  1    -4.39085471E-03   # V_21
  2  2    -9.99990360E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98598153E-01   # cos(theta_t)
  1  2    -5.29313596E-02   # sin(theta_t)
  2  1     5.29313596E-02   # -sin(theta_t)
  2  2     9.98598153E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99723836E-01   # cos(theta_b)
  1  2    -2.35000369E-02   # sin(theta_b)
  2  1     2.35000369E-02   # -sin(theta_b)
  2  2     9.99723836E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06936724E-01   # cos(theta_tau)
  1  2     7.07276797E-01   # sin(theta_tau)
  2  1    -7.07276797E-01   # -sin(theta_tau)
  2  2    -7.06936724E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00126280E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39377823E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44212458E+02   # higgs               
         4     1.09560683E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39377823E+03  # The gauge couplings
     1     3.61905633E-01   # gprime(Q) DRbar
     2     6.37555899E-01   # g(Q) DRbar
     3     1.02798876E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39377823E+03  # The trilinear couplings
  1  1     1.84035773E-06   # A_u(Q) DRbar
  2  2     1.84038557E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39377823E+03  # The trilinear couplings
  1  1     6.54454903E-07   # A_d(Q) DRbar
  2  2     6.54531790E-07   # A_s(Q) DRbar
  3  3     1.34066692E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39377823E+03  # The trilinear couplings
  1  1     2.92369228E-07   # A_e(Q) DRbar
  2  2     2.92383626E-07   # A_mu(Q) DRbar
  3  3     2.96438829E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39377823E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54748400E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39377823E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.89787404E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39377823E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01966137E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39377823E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.85356142E+04   # M^2_Hd              
        22    -9.09425437E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41091235E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.63332154E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48022463E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48022463E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51977537E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51977537E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.91826252E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.81798651E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.94734861E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.67085274E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18677067E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.50083158E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.92268798E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.87792661E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.28420193E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.91422753E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70861619E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62484074E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19280334E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.62706656E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.43765731E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.63167553E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.82455874E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.51787834E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.97694545E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.14260705E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.14550112E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.06937715E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.04713060E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.35662529E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.45571338E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.05818123E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.54150685E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88126682E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99175419E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59617347E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46301259E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.41483377E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19404509E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.47030173E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.14986348E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18676976E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57332953E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.49410824E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.73294404E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.47203998E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42667020E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88641409E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.97956566E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59515161E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.47383132E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.79755853E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18836796E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.90419451E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.15668224E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67354536E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47914286E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.25328679E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.36136980E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.53499336E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55208564E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88126682E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99175419E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59617347E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46301259E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.41483377E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19404509E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.47030173E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.14986348E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18676976E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57332953E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.49410824E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.73294404E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.47203998E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42667020E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88641409E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.97956566E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59515161E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.47383132E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.79755853E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18836796E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.90419451E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.15668224E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67354536E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47914286E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.25328679E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.36136980E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.53499336E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55208564E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80703199E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01459793E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99730283E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.14550988E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.28602035E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98809916E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.34023669E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54142287E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.46931188E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.80703199E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01459793E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99730283E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.14550988E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.28602035E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98809916E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.34023669E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54142287E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.46931188E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.70018631E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56771030E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14582822E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28646148E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64634229E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64944283E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11852912E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.49848248E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.93853360E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23176159E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.20861024E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80718359E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01353302E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99347716E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.25997736E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.24579184E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99298979E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.74828555E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80718359E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01353302E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99347716E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.25997736E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.24579184E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99298979E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.74828555E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80781137E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01343878E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99321971E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.03546416E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.40035865E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99333975E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.73916333E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.65755118E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.44087530E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.39874171E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.62385316E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10602994E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.92276603E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.91063392E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.76821718E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.17237317E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.64829477E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.04518956E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.95481044E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.46411922E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04359976E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.63641363E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.75209809E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.75209809E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.42312210E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.30455723E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34027242E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34027242E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.88166520E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.88166520E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.98605394E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     3.98605394E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.98605394E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     3.98605394E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.11261379E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.11261379E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.36695216E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.23037462E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.29467948E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.81332173E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.81332173E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09236884E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.89305434E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31319113E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31319113E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.95313939E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.95313939E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.20924022E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     6.20924022E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     6.20924022E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     6.20924022E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.75060229E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.75060229E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.68582386E-03   # h decays
#          BR         NDA      ID1       ID2
     5.53649663E-01    2           5        -5   # BR(h -> b       bb     )
     7.06657316E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.50156054E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.30491263E-04    2           3        -3   # BR(h -> s       sb     )
     2.30527847E-02    2           4        -4   # BR(h -> c       cb     )
     7.54916134E-02    2          21        21   # BR(h -> g       g      )
     2.60209611E-03    2          22        22   # BR(h -> gam     gam    )
     1.78315398E-03    2          22        23   # BR(h -> Z       gam    )
     2.41499766E-01    2          24       -24   # BR(h -> W+      W-     )
     3.04745436E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.86482382E+01   # H decays
#          BR         NDA      ID1       ID2
     9.03902145E-01    2           5        -5   # BR(H -> b       bb     )
     6.43445930E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.27507105E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.79378853E-04    2           3        -3   # BR(H -> s       sb     )
     7.83028628E-08    2           4        -4   # BR(H -> c       cb     )
     7.81121478E-03    2           6        -6   # BR(H -> t       tb     )
     6.61438757E-06    2          21        21   # BR(H -> g       g      )
     5.01435584E-08    2          22        22   # BR(H -> gam     gam    )
     3.20251875E-09    2          23        22   # BR(H -> Z       gam    )
     6.71465779E-07    2          24       -24   # BR(H -> W+      W-     )
     3.35318949E-07    2          23        23   # BR(H -> Z       Z      )
     5.20418610E-06    2          25        25   # BR(H -> h       h      )
    -8.95484559E-25    2          36        36   # BR(H -> A       A      )
     1.07068501E-18    2          23        36   # BR(H -> Z       A      )
     7.59753513E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.93095860E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.79670985E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.47264220E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.19909181E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.28747103E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.78062879E+01   # A decays
#          BR         NDA      ID1       ID2
     9.24040160E-01    2           5        -5   # BR(A -> b       bb     )
     6.57751420E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.32564859E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.85637939E-04    2           3        -3   # BR(A -> s       sb     )
     8.06029778E-08    2           4        -4   # BR(A -> c       cb     )
     8.03623902E-03    2           6        -6   # BR(A -> t       tb     )
     2.36659550E-05    2          21        21   # BR(A -> g       g      )
     6.32226050E-08    2          22        22   # BR(A -> gam     gam    )
     2.33131058E-08    2          23        22   # BR(A -> Z       gam    )
     6.83768556E-07    2          23        25   # BR(A -> Z       h      )
     8.66707312E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.10780507E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.33071089E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.64883279E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.13475738E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47343724E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.01636698E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12724062E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.42998678E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.25012470E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.57190850E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.26771295E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.26282967E-07    2          24        25   # BR(H+ -> W+      h      )
     5.01402962E-14    2          24        36   # BR(H+ -> W+      A      )
     4.73454954E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.40494785E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06256709E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
