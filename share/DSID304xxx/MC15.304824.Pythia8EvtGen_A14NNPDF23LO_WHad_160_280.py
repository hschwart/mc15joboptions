evgenConfig.description = "High pT Hadronic W   160 < pT < 280"
evgenConfig.process = "W + jets (W -> qqbar)"
evgenConfig.keywords = ["SM","W","jets"] 
evgenConfig.contact  = ["craig.sawyer@cern.ch", "alexander.tuna@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")	 

genSeq.Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = 160."]
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMax = 280."]
genSeq.Pythia8.Commands += ["WeakBosonAndParton:qqbar2Wg = on"]
genSeq.Pythia8.Commands += ["WeakBosonAndParton:qg2Wq = on"]
genSeq.Pythia8.Commands += ["24:onMode = off"] # switch off all W decays
genSeq.Pythia8.Commands += ["24:onIfAny = 1 2 3 4 5"] # switch on W->had

