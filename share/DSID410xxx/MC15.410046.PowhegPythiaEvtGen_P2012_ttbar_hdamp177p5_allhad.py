#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, allhadronic, mtop = 177.5 GeV'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'andreas.wildauer@cern.ch' ]

if runArgs.trfSubstepName == 'generate' :
  mTop = 177.5

  from PowhegControl import ATLASCommonParameters
  ATLASCommonParameters.mass_t  = mTop
  ATLASCommonParameters.width_t = 1.458

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.PDF     = 10800
  PowhegConfig.hdamp   = mTop

  # compensate filter efficiency
  PowhegConfig.nEvents     *= 3.
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 0
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')


