#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419607E+01   # W+
        25     1.25000000E+02   # h
        35     2.00398999E+03   # H
        36     2.00000000E+03   # A
        37     2.00152199E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989247E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989247E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99933329E+03   # ~b_1
   2000005     5.00082633E+03   # ~b_2
   1000006     4.98344797E+03   # ~t_1
   2000006     5.02094925E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.79827758E+03   # ~chi_10
   1000023    -1.85007025E+03   # ~chi_20
   1000025     1.88553225E+03   # ~chi_30
   1000035     3.00526042E+03   # ~chi_40
   1000024     1.84484592E+03   # ~chi_1+
   1000037     3.00525323E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30160867E-01   # N_11
  1  2    -4.39669044E-02   # N_12
  1  3     4.83512448E-01   # N_13
  1  4    -4.80778257E-01   # N_14
  2  1     2.36657943E-03   # N_21
  2  2    -3.19865870E-03   # N_22
  2  3    -7.07034053E-01   # N_23
  2  4    -7.07168308E-01   # N_24
  3  1     6.83266488E-01   # N_31
  3  2     5.06937675E-02   # N_32
  3  3    -5.14080891E-01   # N_33
  3  4     5.16040586E-01   # N_34
  4  1     2.53259107E-03   # N_41
  4  2    -9.97740859E-01   # N_42
  4  3    -4.51597042E-02   # N_43
  4  4     4.96725806E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.37748320E-02   # U_11
  1  2     9.97964313E-01   # U_12
  2  1     9.97964313E-01   # U_21
  2  2     6.37748320E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01548062E-02   # V_11
  1  2     9.97536116E-01   # V_12
  2  1     9.97536116E-01   # V_21
  2  2     7.01548062E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07641530E-01   # cos(theta_t)
  1  2     7.06571628E-01   # sin(theta_t)
  2  1    -7.06571628E-01   # -sin(theta_t)
  2  2     7.07641530E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81167265E-01   # cos(theta_b)
  1  2     7.32127828E-01   # sin(theta_b)
  2  1    -7.32127828E-01   # -sin(theta_b)
  2  2     6.81167265E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954615E-01   # cos(theta_tau)
  1  2     7.09252417E-01   # sin(theta_tau)
  2  1    -7.09252417E-01   # -sin(theta_tau)
  2  2     7.04954615E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90194243E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52303948E+02   # vev(Q)              
         4     3.03574549E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52718400E-01   # gprime(Q) DRbar
     2     6.26766878E-01   # g(Q) DRbar
     3     1.07402910E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02463078E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71136767E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79739668E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.81974019E+05   # M^2_Hd              
        22    -8.38698196E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36866395E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.98624327E-03   # gluino decays
#          BR         NDA      ID1       ID2
     5.84295399E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.13559513E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     5.67631343E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.54828677E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.23550171E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.74855447E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.58991272E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.70869998E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.13012219E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.54828677E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.23550171E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.74855447E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.58991272E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.70869998E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.13012219E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.61787701E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.66301925E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.83904173E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.00675576E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.73085606E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.11902853E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     3.96235365E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.96235365E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.96235365E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.96235365E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.08286484E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.08286484E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.13534499E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.51172326E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.27113138E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.17512866E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.47661818E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.36354718E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.90122160E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.45885226E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.01028684E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.30757824E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.28677130E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.05288846E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.35442519E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.19149427E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.64554393E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.33809123E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.46748759E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.62350003E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.11472129E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.92189214E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.30529914E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.56557023E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.68110395E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.63922082E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.56025676E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.71063314E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.34424286E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.72704371E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.75462436E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.91295242E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.59543202E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.17632093E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.35729487E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.06624399E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.81825540E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.25619822E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.64711757E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.01594045E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.13004196E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.25345083E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.19929642E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.56094156E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.67683533E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.02773155E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.32070779E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.34112669E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.35738246E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.43163599E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.36704587E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.10968256E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.65840257E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.39572852E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.13111610E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.25420820E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.14008853E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.36490689E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.66970133E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.96262810E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.10337315E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.82672307E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.35729487E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.06624399E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.81825540E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.25619822E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.64711757E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.01594045E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.13004196E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.25345083E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.19929642E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.56094156E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.67683533E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.02773155E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.32070779E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.34112669E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.35738246E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.43163599E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.36704587E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.10968256E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.65840257E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.39572852E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.13111610E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.25420820E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.14008853E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.36490689E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.66970133E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.96262810E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.10337315E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.82672307E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.86274364E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.96594586E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77190394E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51190883E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76414136E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.14312967E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.54662415E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85052880E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40534355E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.58027085E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59456565E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.50014124E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.86274364E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.96594586E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77190394E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51190883E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76414136E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.14312967E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.54662415E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85052880E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40534355E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.58027085E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59456565E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.50014124E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36119609E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.60922802E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.30080043E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.34503104E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.65876170E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25842902E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32857105E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.52308625E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35961747E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47806476E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.90466956E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.41602924E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69371107E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.76092121E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.39852933E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.86295669E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07030472E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04338248E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.59541987E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77933753E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.01277680E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54058366E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.86295669E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07030472E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04338248E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.59541987E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77933753E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.01277680E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54058366E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86534965E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06941088E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04251111E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.59074694E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77701641E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.84084946E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.53598528E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.49287364E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.21516786E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.06658551E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05266062E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.02219631E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.02217023E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01487054E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55083302E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53819171E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07438160E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46136788E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40076745E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52521226E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.90885980E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.64961966E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95463151E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93611039E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09258094E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02952404E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.32187524E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.99680142E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.34563924E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.80910901E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11355204E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44196840E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10688329E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44179161E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32932120E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29362172E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29351587E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.26376409E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.57781026E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.57781026E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.57781026E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.04294900E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.04294900E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.95343411E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.95343411E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.34764983E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.34764983E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.34286159E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.34286159E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35026213E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35026213E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.00293255E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.12823972E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.04200643E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.27125078E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61304614E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.36938724E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42684777E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.70174653E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.42882389E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.72048930E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.27725721E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.97424158E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.97420769E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.42687280E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.30173521E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.30173521E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.30173521E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74196248E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25571627E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72251957E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25515592E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90725948E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.15232075E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.15203056E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.06996349E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02898368E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02898368E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02898368E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26072593E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26072593E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25319356E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25319356E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.20241384E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.20241384E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.20227257E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.20227257E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16281789E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16281789E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55076375E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.64042083E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52455537E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.10976898E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46911381E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46911381E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09607597E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.58552460E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42972622E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88598784E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.02172970E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.02775770E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60508357E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.84497126E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08925156E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18123246E-01    2           5        -5   # BR(h -> b       bb     )
     6.36915391E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25444591E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77737642E-04    2           3        -3   # BR(h -> s       sb     )
     2.06113170E-02    2           4        -4   # BR(h -> c       cb     )
     6.69242659E-02    2          21        21   # BR(h -> g       g      )
     2.30104138E-03    2          22        22   # BR(h -> gam     gam    )
     1.53620897E-03    2          22        23   # BR(h -> Z       gam    )
     2.00514380E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55948192E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78084799E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46424857E-03    2           5        -5   # BR(H -> b       bb     )
     2.46438407E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71251517E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11552204E-06    2           3        -3   # BR(H -> s       sb     )
     1.00669322E-05    2           4        -4   # BR(H -> c       cb     )
     9.96075828E-01    2           6        -6   # BR(H -> t       tb     )
     7.97660389E-04    2          21        21   # BR(H -> g       g      )
     2.71584561E-06    2          22        22   # BR(H -> gam     gam    )
     1.16033449E-06    2          23        22   # BR(H -> Z       gam    )
     3.32268526E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65681151E-04    2          23        23   # BR(H -> Z       Z      )
     9.01944715E-04    2          25        25   # BR(H -> h       h      )
     7.11222870E-24    2          36        36   # BR(H -> A       A      )
     2.88615020E-11    2          23        36   # BR(H -> Z       A      )
     6.36728316E-12    2          24       -37   # BR(H -> W+      H-     )
     6.36728316E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82378833E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46715032E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899913E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62274299E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678418E-06    2           3        -3   # BR(A -> s       sb     )
     9.96185438E-06    2           4        -4   # BR(A -> c       cb     )
     9.97004954E-01    2           6        -6   # BR(A -> t       tb     )
     9.43684130E-04    2          21        21   # BR(A -> g       g      )
     3.14002092E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290435E-06    2          23        22   # BR(A -> Z       gam    )
     3.23857803E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74470464E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34909741E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49240314E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81154546E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49343364E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45699804E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732755E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404664E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.31922292E-04    2          24        25   # BR(H+ -> W+      h      )
     2.86809539E-13    2          24        36   # BR(H+ -> W+      A      )
