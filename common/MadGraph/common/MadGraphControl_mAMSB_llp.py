def get_mg_variations(mN1, mC1, xqcut=None):
    if xqcut is None:
        xqcut = 500  # default

    mglog.info('For matching, will use xqcut of '+str(xqcut))

    return xqcut

###
from MadGraphControl.MadGraphUtils import *

# in case someone needs to be able to keep the output directory for testing
keepOutput = False

import re
plist = re.search(r'.+mAMSB_(.+)\.py', runArgs.jobConfig[0]).group(1).split('_')
prod = plist[0]
m0 = int(plist[1])
m32 = int(plist[2])
tanb = 5
mass = int(plist[3])
runNumber = 100100
slha_file = 'susyhit_slha_AMSB_%s_%s_%s_%s_GeV.slha' % ( m0, m32, tanb, mass)

lifetime = float(-1)  # -1 is for stable

# get more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 2.0
if evt_multiplier > 0:
  if runArgs.maxEvents > 0:
    nevts = runArgs.maxEvents * evt_multiplier
  else:
    nevts = 5000 * evt_multiplier

if prod == 'C1N1':
    process = '''
generate p p > x1+ n1 $ susystrong @1
add process p p > x1- n1 $ susystrong @1
'''

# set number of jets done in madgraph
njets = 0

# choose pdf set
pdlabel = 'nn23lo1'
lhaid = 247000

# Set beam energy
beamEnergy = 6500.
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy * 0.5

# Set random seed
rand_seed = 1234
if hasattr(runArgs, "randomSeed"):
    # Giving a unique seed number (not exceeding the limit of ~30081^2)
    rand_seed = 1000 * int(str(runArgs.runNumber)[1:6]) + runArgs.randomSeed

if not 'MADGRAPH_DATA' in os.environ:
    os.environ['MADGRAPH_DATA'] = os.getcwd()
    mglog.warning('Setting your MADGRAPH_DATA environmental variable to the working directory')

include("/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/Pythia8/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/Pythia8/Pythia8_MadGraph.py")

# generate the new process
full_proc = """
import model mssm-full
""" + helpful_definitions() + """
# Specify process(es) to run

""" + process + """
# Output processes to MadEvent directory
output -f
"""

thedir = new_process(card_loc=full_proc)
if 1 == thedir:
    mglog.error('Error in process generation!')
mglog.info('Using process directory '+thedir)

# Grab the param card and move the new masses into place
from PyJobTransformsCore.trfutil import get_files
get_files(slha_file, keepDir=False, errorIfNotFound=True)
include('/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/common/MadGraph/SUSYMetadata.py')
(mN1, mC1) = mass_extract(slha_file, ['1000022', '1000024'])
mglog.info('chargino1 mass = '+mC1+' neutralino1 mass = '+mN1)

build_param_card(param_card_old=slha_file, param_card_new='param_card.dat')

xqcut = None
xqcut= get_mg_variations(abs(float(mC1)), abs(float(mN1)), xqcut)
mglog.info("MG5 params: %s"%(xqcut))

# Grab the run card and move it into place
extras = {'ktdurham': xqcut,
          'lhe_version': '2.0',
          'cut_decays': 'F',
          'pdlabel': pdlabel,
          'lhaid': lhaid,
          'drjj': 0.0,
          'ickkw': 0}

# AGENE-1542 : Inconsistent Weights with MG 2.6.X + SysCalc
extras['event_norm']='sum'
extras['use_syst']='F'

build_run_card(run_card_old=get_default_runcard(proc_dir=thedir),
               run_card_new='run_card.dat',
               xqcut=0,
               nevts=nevts,
               rand_seed=rand_seed,
               beamEnergy=beamEnergy,
               extras=extras)

if generate(run_card_loc='run_card.dat', param_card_loc='param_card.dat', mode=0, njobs=1, run_name='Test', proc_dir=thedir):
    mglog.error('Error generating events!')

# Move output files into the appropriate place, with the appropriate name
the_spot = arrange_output(run_name='Test', proc_dir=thedir, outputDS='madgraph_OTF._00001.events.tar.gz')
if the_spot == '':
    mglog.error('Error arranging output dataset!')

mglog.info('Removing process directory...')
shutil.rmtree(thedir, ignore_errors=True)

mglog.info('All done generating events!!')

outputDS = the_spot

if xqcut < 0 or outputDS is None or '' == outputDS:
    evgenLog.warning('Looks like something went wrong with the MadGraph generation - bailing out!')
    raise RuntimeError('Error in MadGraph generation')

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts, 'nprocs'):
        mglog.warning('Did not see option!')
    else:
        opts.nprocs = 0
    mglog.info(opts)

runArgs.qcut = xqcut
runArgs.inputGeneratorFile = outputDS
runArgs.gentype = prod

# Pythia8 setup
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
			    "1000024:mayDecay = false",
                            "1000022:mayDecay = false"
                            ]
if njets > 0:
    genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
                                "Merging:nJetMax = "+str(njets),
                                "Merging:doKTMerging = on",
                                "Merging:TMS = "+str(xqcut),
                                "Merging:ktType = 1",
                                "Merging:Dparameter = 0.4",
                                "1000024:spinType = 1",
                                "1000022:spinType = 1",]
    
if prod == 'C1N1':
        genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}{n1,1000022}",]

# Configuration for EvgenJobTransforms
# --------------------------------------------------------------
evgenLog.info('Registered generation of mAMSB')

evgenConfig.contact = ['revital.kopeliansky@cern.ch']
evgenConfig.keywords += ['SUSY', 'chargino', 'longLived']
evgenConfig.description = 'C1N1 production, C1 and N1 being long lived (mass:%s, lifetime:%s, slha:%s)' % (mC1, lifetime, slha_file)
if lifetime != 0:
    evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;preInclude=common/preInclude.AMSB.py' % (mC1, mN1, lifetime)
    #evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (mC1, mN1, lifetime)

evgenConfig.generators += ["EvtGen"]

if not hasattr(runArgs, 'inputGeneratorFile'):
    mglog.error('something went wrong with the file name.')
    runArgs.inputGeneratorFile = 'madgraph.*._events.tar.gz'
evgenConfig.inputfilecheck = runArgs.inputGeneratorFile.split('._0')[0]

bonus_file = open('pdg_extras.dat','w')
bonus_file.write( '1000024 Chargino '+mC1+' (MeV/c) boson Chargino 1\n')
bonus_file.write( '-1000024 Anti-chargino '+mC1+' (MeV/c) boson Chargino -1\n')
bonus_file.close()
 
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

import os
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)

# Clean up
del m0, m32, tanb, slha_file, mC1, mN1, lifetime, xqcut, njets
