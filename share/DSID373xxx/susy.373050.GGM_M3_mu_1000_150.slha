#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376841E+01   # W+
        25     1.26000000E+02   # h
        35     2.00414807E+03   # H
        36     2.00000000E+03   # A
        37     2.00209668E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.02256780E+03   # ~d_L
   2000001     5.02245821E+03   # ~d_R
   1000002     5.02232293E+03   # ~u_L
   2000002     5.02238116E+03   # ~u_R
   1000003     5.02256780E+03   # ~s_L
   2000003     5.02245821E+03   # ~s_R
   1000004     5.02232293E+03   # ~c_L
   2000004     5.02238116E+03   # ~c_R
   1000005     5.02243150E+03   # ~b_1
   2000005     5.02259598E+03   # ~b_2
   1000006     5.15243501E+03   # ~t_1
   2000006     5.44873323E+03   # ~t_2
   1000011     5.00008276E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984124E+03   # ~nu_eL
   1000013     5.00008276E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984124E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011982E+03   # ~tau_2
   1000016     4.99984124E+03   # ~nu_tauL
   1000021     1.14585118E+03   # ~g
   1000022     1.46507693E+02   # ~chi_10
   1000023    -1.62929835E+02   # ~chi_20
   1000025     2.94795588E+02   # ~chi_30
   1000035     3.12909416E+03   # ~chi_40
   1000024     1.60718233E+02   # ~chi_1+
   1000037     3.12909376E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08682643E-01   # N_11
  1  2    -2.47365545E-02   # N_12
  1  3     6.79344305E-01   # N_13
  1  4    -6.65277719E-01   # N_14
  2  1     2.00321475E-02   # N_21
  2  2    -4.81326355E-03   # N_22
  2  3    -7.04243124E-01   # N_23
  2  4    -7.09659895E-01   # N_24
  3  1     9.50954030E-01   # N_31
  3  2     8.56742944E-03   # N_32
  3  3    -2.05675547E-01   # N_33
  3  4     2.30890888E-01   # N_34
  4  1     4.15213629E-04   # N_41
  4  2    -9.99645704E-01   # N_42
  4  3    -1.51824195E-02   # N_43
  4  4     2.18583444E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14639175E-02   # U_11
  1  2     9.99769624E-01   # U_12
  2  1     9.99769624E-01   # U_21
  2  2     2.14639175E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09087454E-02   # V_11
  1  2     9.99522211E-01   # V_12
  2  1     9.99522211E-01   # V_21
  2  2     3.09087454E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99879793E-01   # cos(theta_t)
  1  2    -1.55048235E-02   # sin(theta_t)
  2  1     1.55048235E-02   # -sin(theta_t)
  2  2     9.99879793E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08475744E-01   # cos(theta_b)
  1  2     9.12769175E-01   # sin(theta_b)
  2  1    -9.12769175E-01   # -sin(theta_b)
  2  2     4.08475744E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76625827E-01   # cos(theta_tau)
  1  2     7.36327027E-01   # sin(theta_tau)
  2  1    -7.36327027E-01   # -sin(theta_tau)
  2  2     6.76625827E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199322E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51739910E+02   # vev(Q)              
         4     3.88486683E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146794E-01   # gprime(Q) DRbar
     2     6.29569932E-01   # g(Q) DRbar
     3     1.09169880E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02600446E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72264855E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79978986E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04080187E+06   # M^2_Hd              
        22    -5.52232695E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111688E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.60491425E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.55721790E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.88168514E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.50802216E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.41701894E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.07740015E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.29565121E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.42724938E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.91214382E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.49199889E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.24026949E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.07740015E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.29565121E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.42724938E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.91214382E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.49199889E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.24026949E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.28477334E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.44903962E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.45391756E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.66052147E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.76546447E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.10843785E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.40647919E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.40647919E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.40647919E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.40647919E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.40653690E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.40653690E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.42440574E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.45955329E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.08437451E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.42268381E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.29612843E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.34389099E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.57828915E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.98055649E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     2.65060178E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.31553619E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.20771018E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.56481421E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.36933055E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.40105542E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -4.52882418E-06    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.86289872E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -8.96076143E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.00165346E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.44009300E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.12529653E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16055224E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.57811599E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.98733732E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.45662724E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.41888978E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.47344809E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -9.74893189E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.62072546E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.95669692E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.17505545E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.65468773E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89015446E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.81825904E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.77120895E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.49380369E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.85125257E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.00299269E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.39381869E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.55823976E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.12375465E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.51094761E-08    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.58124159E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.80380563E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.42658744E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.60709856E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.13085735E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.44231786E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.16982310E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.75542089E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.93747616E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.83480496E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56437858E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.55826683E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.07787979E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.46924826E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.12851594E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.80512821E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.87971369E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.61077771E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.13130370E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.36256753E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07766134E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.53676141E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.01761307E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.32663550E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88741670E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.55823976E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.12375465E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.51094761E-08    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.58124159E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.80380563E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.42658744E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.60709856E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.13085735E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.44231786E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.16982310E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.75542089E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.93747616E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.83480496E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56437858E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.55826683E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.07787979E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.46924826E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.12851594E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.80512821E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.87971369E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.61077771E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.13130370E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.36256753E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07766134E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.53676141E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.01761307E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.32663550E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88741670E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762124E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54393697E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89093895E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04825331E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563280E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45619861E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497491E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513758E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57380300E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03030921E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03858875E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42206160E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762124E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54393697E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89093895E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04825331E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563280E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45619861E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497491E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513758E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57380300E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03030921E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03858875E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42206160E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957320E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08260076E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84779851E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56358202E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26706615E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89290044E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53594669E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825911E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319000E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61245661E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12666560E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05238925E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093022E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85230886E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98398298E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747240E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74484000E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80490392E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92079822E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774202E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33882439E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178261E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747240E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74484000E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80490392E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92079822E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774202E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33882439E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178261E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068700E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74170071E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80283964E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91860139E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477097E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48069179E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584781E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23231926E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87423881E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36074260E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36074260E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024808E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024808E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03763122E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36645341E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99036965E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71248226E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94607885E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26153793E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99434862E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31637954E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33942157E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91693232E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29780199E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34109944E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02295053E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82540100E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.02062173E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03373862E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96092464E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.33674184E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.25577524E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.51194108E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65873937E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18129005E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01320843E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27778747E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65373785E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27778747E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65373785E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83434709E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77320656E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77320656E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635536E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53276123E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53276123E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53276123E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36178864E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36178864E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36178864E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36178864E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.87263039E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.87263039E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.87263039E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.87263039E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36077589E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36077589E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63986896E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83689028E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19059105E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99715838E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99715838E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79630388E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.19357171E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.83861170E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47383169E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.37784441E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36648901E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03783233E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93134636E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02303215E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94053864E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94053864E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71048294E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76533887E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63801814E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36502434E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01170939E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77919345E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22290655E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48818527E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19243109E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33305501E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33305501E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44996786E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77874962E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80606380E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81041062E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61818603E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21657388E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01584585E-01    2           5        -5   # BR(h -> b       bb     )
     6.22646152E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20389592E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66301843E-04    2           3        -3   # BR(h -> s       sb     )
     2.01174258E-02    2           4        -4   # BR(h -> c       cb     )
     6.64356409E-02    2          21        21   # BR(h -> g       g      )
     2.32846554E-03    2          22        22   # BR(h -> gam     gam    )
     1.62712937E-03    2          22        23   # BR(h -> Z       gam    )
     2.16862608E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80928381E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01467842E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38959231E-03    2           5        -5   # BR(H -> b       bb     )
     2.32101607E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20565592E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05061378E-06    2           3        -3   # BR(H -> s       sb     )
     9.48138318E-06    2           4        -4   # BR(H -> c       cb     )
     9.38142594E-01    2           6        -6   # BR(H -> t       tb     )
     7.51347398E-04    2          21        21   # BR(H -> g       g      )
     2.62941265E-06    2          22        22   # BR(H -> gam     gam    )
     1.09160314E-06    2          23        22   # BR(H -> Z       gam    )
     3.14443175E-04    2          24       -24   # BR(H -> W+      W-     )
     1.56792690E-04    2          23        23   # BR(H -> Z       Z      )
     8.52413063E-04    2          25        25   # BR(H -> h       h      )
     8.31271039E-24    2          36        36   # BR(H -> A       A      )
     3.30046835E-11    2          23        36   # BR(H -> Z       A      )
     2.37942162E-12    2          24       -37   # BR(H -> W+      H-     )
     2.37942162E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71032045E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52086207E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36101670E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64240744E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94499167E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53675469E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87768555E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05931725E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39248052E-03    2           5        -5   # BR(A -> b       bb     )
     2.29748399E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12243586E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082590E-06    2           3        -3   # BR(A -> s       sb     )
     9.38384958E-06    2           4        -4   # BR(A -> c       cb     )
     9.39156924E-01    2           6        -6   # BR(A -> t       tb     )
     8.88929871E-04    2          21        21   # BR(A -> g       g      )
     2.69765066E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304918E-06    2          23        22   # BR(A -> Z       gam    )
     3.06424333E-04    2          23        25   # BR(A -> Z       h      )
     5.85727559E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28895111E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65402088E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84176074E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65102134E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36592392E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93385202E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97901234E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23451017E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630925E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29505077E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42068363E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13693278E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355321E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40806478E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.14040357E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33884529E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57781795E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23532243E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29301354E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
