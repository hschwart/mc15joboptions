include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += [
    "PDF:useHard = on",
    "PDF:pHardSet = LHAPDF6:NNPDF31_nlo_as_0118_luxqed",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2ee= on", # gg->ee
    "PhaseSpace:mHatMin = 1500.", # lower invariant mass
    "PhaseSpace:mHatMax = 2500.", # upper invariant mass
]

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.NLeptons = 2

evgenConfig.description = "gammagamma -> ee production with NNPDF31NLOLUXQED, 1500<M<2500GeV"
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2electron"]
evgenConfig.generators += ["Pythia8"]
