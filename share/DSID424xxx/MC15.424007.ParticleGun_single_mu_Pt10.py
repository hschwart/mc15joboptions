evgenConfig.description = "Single mu with flat phi, eta in [-3.0, 3.0], and pT = 10 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=10000, eta=[-3.0, 3.0])
