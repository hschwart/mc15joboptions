#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->gamgam"
evgenConfig.keywords = ["SM", "Higgs", "SMHiggs", "diphoton", "mH125"]
evgenConfig.contact = ["Junichi.Tanaka@cern.ch"]

#--------------------------------------------------------------
# Powheg ggH setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H = 125.
PowhegConfig.width_H = 0.00407

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 3

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
    PowhegConfig.ew = 1
else:
    PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
if PowhegConfig.mass_H <= 700.:
    masswindow = min((1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow_max)
else:
    masswindow = min((1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow_max)
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Ensure identical run cards (for testing only)
PowhegConfig.bornktmin = -1
PowhegConfig.bottomthr = -1
PowhegConfig.bottomthrpdf = -1
PowhegConfig.btildeborn = -1
PowhegConfig.btildecoll = -1
PowhegConfig.btildereal = -1
PowhegConfig.btildevirt = -1
PowhegConfig.btlscalect = -1
PowhegConfig.btlscalereal = -1
PowhegConfig.charmthr = -1
PowhegConfig.charmthrpdf = -1
PowhegConfig.check_bad_st2 = -1
PowhegConfig.hdamp = -1
PowhegConfig.itmx1 = 5
#PowhegConfig.iupperfsr = 2 # not user-configurable
#PowhegConfig.iupperisr = 1 # not user-configurable
PowhegConfig.minlo = -1
PowhegConfig.ncall1 = 50000
PowhegConfig.ncall2 = 100000
PowhegConfig.novirtual = -1
PowhegConfig.nubound = 50000
PowhegConfig.ptsqmin = -1
PowhegConfig.stage2init = -1
PowhegConfig.xupbound = 2

# Generate Powheg events
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += ['Main31:NFinal = 1']

#--------------------------------------------------------------
# Pythia8 Higgs decays
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ["25:onMode = off",      # disable Higgs decays
                            "25:onIfMatch = 22 22"] # enable H->yy
