include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")
evgenConfig.description="Sherpa ttW LO"
evgenConfig.keywords=["ttW", "SM", "inclusive"]
evgenConfig.contact=["ponyisi@cern.ch"]
evgenConfig.inputconfcheck="Sherpa_NNPDF30NNLO_ttW"

evgenConfig.process="""
(run){
  HARD_DECAYS=1;
  STABLE[6] = 0; WIDTH[6]=0.0;
  STABLE[23] = 0;
  STABLE[24] = 0; WIDTH[24]=0.0;
  ACTIVE[25] = 0;
}(run)

(processes){
  Process 93 93 -> 6 -6 24 93{2};
    Order (*,1);
  CKKW sqr(30./E_CMS);
  End process;

  Process 93 93 -> 6 -6 -24 93{2};
    Order (*,1);
  CKKW sqr(30./E_CMS);
  End process;
}(processes)

"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0"]
