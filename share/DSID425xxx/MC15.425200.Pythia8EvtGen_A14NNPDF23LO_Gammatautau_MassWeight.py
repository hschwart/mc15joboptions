include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "WeakZ0:gmZmode = 1",               # photon only
                            "23:onMode = off",                  # turn off all decays modes
                            "23:onIfAny = 15",                  # turn on the tautau decay mode
                            "15:offIfAny = 12 14",              # turn off leptonic tau decays 
                            "-15:offIfAny = 12 14",             # turn off leptonic tau decays 
                            "PhaseSpace:mHatMin = 60.",         # lower invariant mass
                            "PhaseSpace:mHatMax = 7000."]       # upper invariant mass


evgenConfig.description = "Pythia 8 gamma*->tau(had)tau(had) production with NNPDF23LO tune"
evgenConfig.contact = ["Will Davey <will.davey@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2tau"]

# mHatReweight hook
# -----------------
# Modifies *slope* (by mHat^(slope)) of falling gamma mass 
# distribution up to mass *mHatConstMin*, after which a 
# constant cross section is used. The SM gamma distribution 
# is parameterised as (mHat/cme)^(p1) * (1 - mHat/cme)^(p2)
genSeq.Pythia8.UserHook = "mHatReweight"
genSeq.Pythia8.UserParams += ["mHatReweight:Slope = 2.0",           # Multiplier to falling gamma: mHat^(slope) 
                              "mHatReweight:mHatConstMin = 1500.",  # mHat to start constant
                              "mHatReweight:p1 = -4.18717",
                              "mHatReweight:p2 = 11.5811",
                            ]

