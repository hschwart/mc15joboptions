#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17312041E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04047622E+01   # W+
        25     1.24663943E+02   # h
        35     3.00006637E+03   # H
        36     3.00000000E+03   # A
        37     3.00088485E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04344660E+03   # ~d_L
   2000001     3.03832018E+03   # ~d_R
   1000002     3.04254519E+03   # ~u_L
   2000002     3.03915745E+03   # ~u_R
   1000003     3.04344660E+03   # ~s_L
   2000003     3.03832018E+03   # ~s_R
   1000004     3.04254519E+03   # ~c_L
   2000004     3.03915745E+03   # ~c_R
   1000005     1.10590343E+03   # ~b_1
   2000005     3.03714495E+03   # ~b_2
   1000006     1.10230042E+03   # ~t_1
   2000006     3.01938808E+03   # ~t_2
   1000011     3.00623566E+03   # ~e_L
   2000011     3.00232717E+03   # ~e_R
   1000012     3.00485294E+03   # ~nu_eL
   1000013     3.00623566E+03   # ~mu_L
   2000013     3.00232717E+03   # ~mu_R
   1000014     3.00485294E+03   # ~nu_muL
   1000015     2.98546222E+03   # ~tau_1
   2000015     3.02200268E+03   # ~tau_2
   1000016     3.00451651E+03   # ~nu_tauL
   1000021     2.35418851E+03   # ~g
   1000022     4.01448235E+02   # ~chi_10
   1000023     8.35698604E+02   # ~chi_20
   1000025    -2.99389463E+03   # ~chi_30
   1000035     2.99495551E+03   # ~chi_40
   1000024     8.35861797E+02   # ~chi_1+
   1000037     2.99541693E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99881889E-01   # N_11
  1  2    -6.79513710E-04   # N_12
  1  3     1.51057938E-02   # N_13
  1  4    -2.74979207E-03   # N_14
  2  1     1.12912650E-03   # N_21
  2  2     9.99563698E-01   # N_22
  2  3    -2.81738396E-02   # N_23
  2  4     8.79618713E-03   # N_24
  3  1    -8.72530834E-03   # N_31
  3  2     1.37114070E-02   # N_32
  3  3     7.06886604E-01   # N_33
  3  4     7.07140152E-01   # N_34
  4  1    -1.26016732E-02   # N_41
  4  2     2.61524120E-02   # N_42
  4  3     7.06604117E-01   # N_43
  4  4    -7.07013346E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99205452E-01   # U_11
  1  2    -3.98555538E-02   # U_12
  2  1     3.98555538E-02   # U_21
  2  2     9.99205452E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99922562E-01   # V_11
  1  2    -1.24446387E-02   # V_12
  2  1     1.24446387E-02   # V_21
  2  2     9.99922562E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98413306E-01   # cos(theta_t)
  1  2    -5.63104822E-02   # sin(theta_t)
  2  1     5.63104822E-02   # -sin(theta_t)
  2  2     9.98413306E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99901894E-01   # cos(theta_b)
  1  2     1.40072258E-02   # sin(theta_b)
  2  1    -1.40072258E-02   # -sin(theta_b)
  2  2     9.99901894E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06971456E-01   # cos(theta_tau)
  1  2     7.07242080E-01   # sin(theta_tau)
  2  1    -7.07242080E-01   # -sin(theta_tau)
  2  2     7.06971456E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01758403E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73120405E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43900549E+02   # higgs               
         4     7.75561790E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73120405E+03  # The gauge couplings
     1     3.62654756E-01   # gprime(Q) DRbar
     2     6.36923912E-01   # g(Q) DRbar
     3     1.02311678E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73120405E+03  # The trilinear couplings
  1  1     2.86215206E-06   # A_u(Q) DRbar
  2  2     2.86219026E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73120405E+03  # The trilinear couplings
  1  1     7.98317663E-07   # A_d(Q) DRbar
  2  2     7.98447954E-07   # A_s(Q) DRbar
  3  3     1.70368642E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73120405E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.58755809E-07   # A_mu(Q) DRbar
  3  3     1.60608992E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73120405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49668210E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73120405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16849996E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73120405E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07945053E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73120405E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.65516701E+04   # M^2_Hd              
        22    -9.02094495E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40795662E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.83490451E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47616263E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47616263E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52383737E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52383737E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.35953777E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.82342680E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.51948032E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.99817700E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.08941361E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.89147867E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.47282757E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.99560714E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.06358862E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.04037475E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67262656E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.58678105E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.09653775E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.20068680E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.67916624E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.84745447E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.58462891E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17149805E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.89524253E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.02099126E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.88276931E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.77615113E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.00064335E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31149385E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.85995328E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16107323E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.96311931E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56117486E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.16262827E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52953147E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.97545264E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.10748842E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05975574E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.96223476E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.34908450E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13323457E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57457306E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.78223408E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.24105003E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.47228064E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42542479E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.56648480E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.24850274E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52759439E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.00472033E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.65571878E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.05411728E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.33734859E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.35579482E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.63836382E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47049451E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.05972924E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.40010926E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.76732997E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55294994E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56117486E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.16262827E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52953147E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.97545264E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.10748842E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05975574E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.96223476E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.34908450E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13323457E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57457306E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.78223408E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.24105003E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.47228064E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42542479E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.56648480E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.24850274E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52759439E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.00472033E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.65571878E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.05411728E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.33734859E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.35579482E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.63836382E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47049451E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.05972924E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.40010926E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.76732997E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55294994E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47397494E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08921699E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97435783E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.79208817E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.05584419E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93642452E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.72639723E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51507038E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998868E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12514875E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.48412704E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.96128506E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47397494E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08921699E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97435783E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.79208817E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.05584419E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93642452E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.72639723E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51507038E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998868E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12514875E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.48412704E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.96128506E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51893635E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75465233E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08478230E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16056537E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46730685E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84287853E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05532952E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.78602411E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.96210324E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10121394E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.03192068E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47400311E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09388078E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96486959E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.45524256E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.67688562E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94124936E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.25097275E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47400311E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09388078E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96486959E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.45524256E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.67688562E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94124936E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.25097275E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47383286E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09380301E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96457160E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.06661778E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.56524980E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94159810E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.70837028E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.73112676E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.70077210E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.82383331E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.95178756E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.83523475E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.64564042E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.41506781E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.44187780E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.15567675E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.47089441E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.87398353E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.41260165E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.71690198E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.68468603E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.97295095E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.41320110E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.41320110E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.39421299E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.67571867E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.52304117E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.52304117E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26997955E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26997955E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.92034318E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.92034318E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.63665036E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.16411926E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.66381916E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.49698681E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.49698681E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.77884244E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.52287059E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.48108358E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.48108358E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30116994E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30116994E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.54446597E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.54446597E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.72092189E-03   # h decays
#          BR         NDA      ID1       ID2
     6.73231829E-01    2           5        -5   # BR(h -> b       bb     )
     5.51313367E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.95167840E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.14428613E-04    2           3        -3   # BR(h -> s       sb     )
     1.78936298E-02    2           4        -4   # BR(h -> c       cb     )
     5.78369488E-02    2          21        21   # BR(h -> g       g      )
     1.97151907E-03    2          22        22   # BR(h -> gam     gam    )
     1.28447018E-03    2          22        23   # BR(h -> Z       gam    )
     1.70786345E-01    2          24       -24   # BR(h -> W+      W-     )
     2.12543250E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40783352E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41967252E-01    2           5        -5   # BR(H -> b       bb     )
     1.76633907E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24535287E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.66931696E-04    2           3        -3   # BR(H -> s       sb     )
     2.16359306E-07    2           4        -4   # BR(H -> c       cb     )
     2.15832238E-02    2           6        -6   # BR(H -> t       tb     )
     2.39497588E-05    2          21        21   # BR(H -> g       g      )
     2.48574757E-08    2          22        22   # BR(H -> gam     gam    )
     8.33896149E-09    2          23        22   # BR(H -> Z       gam    )
     2.96316391E-05    2          24       -24   # BR(H -> W+      W-     )
     1.47975575E-05    2          23        23   # BR(H -> Z       Z      )
     7.83636974E-05    2          25        25   # BR(H -> h       h      )
     2.37488489E-23    2          36        36   # BR(H -> A       A      )
     1.99164374E-19    2          23        36   # BR(H -> Z       A      )
     1.58923659E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.03689915E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.92815524E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.99402823E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.51867644E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.24823922E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33221106E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84170419E-01    2           5        -5   # BR(A -> b       bb     )
     1.86660664E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59986580E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10600567E-04    2           3        -3   # BR(A -> s       sb     )
     2.28739990E-07    2           4        -4   # BR(A -> c       cb     )
     2.28057236E-02    2           6        -6   # BR(A -> t       tb     )
     6.71606719E-05    2          21        21   # BR(A -> g       g      )
     6.44516903E-08    2          22        22   # BR(A -> gam     gam    )
     6.62009173E-08    2          23        22   # BR(A -> Z       gam    )
     3.11971212E-05    2          23        25   # BR(A -> Z       h      )
     2.59295215E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22415535E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29335700E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.85164296E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04602109E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10931830E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37800891E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40805949E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.09962613E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94124556E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01657310E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.20832826E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.97926334E-05    2          24        25   # BR(H+ -> W+      h      )
     6.82337552E-14    2          24        36   # BR(H+ -> W+      A      )
     1.80382477E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.14239599E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.64994535E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
