##############################################################
# Job options fragment for pp->J/psi(mu4mu4)X  
##############################################################

evgenConfig.description = "Inclusive pp->psi(2S)(mu2mu2) production"
evgenConfig.keywords = ["charmonium","2muon","inclusive"]
evgenConfig.minevents = 5000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py") 

genSeq.Pythia8B.Commands += ['9940103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9940103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9940103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9940103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['9941103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9941103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9941103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9941103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['9942103:m0 = 4.0']
genSeq.Pythia8B.Commands += ['9942103:mMin = 4.0']
genSeq.Pythia8B.Commands += ['9942103:mMax = 4.0']
genSeq.Pythia8B.Commands += ['9942103:0:products = 100443 21']

genSeq.Pythia8B.Commands += ['100443:onMode = off']
genSeq.Pythia8B.Commands += ['100443:1:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [100443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [2.0]
genSeq.Pythia8B.TriggerStateEtaCut = 3.1
genSeq.Pythia8B.MinimumCountPerCut = [2]
