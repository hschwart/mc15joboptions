#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12014422E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.52959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.05485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     7.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188577E+01   # W+
        25     1.25637195E+02   # h
        35     4.00000340E+03   # H
        36     3.99999631E+03   # A
        37     4.00103972E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02309525E+03   # ~d_L
   2000001     4.02038561E+03   # ~d_R
   1000002     4.02244505E+03   # ~u_L
   2000002     4.02097713E+03   # ~u_R
   1000003     4.02309525E+03   # ~s_L
   2000003     4.02038561E+03   # ~s_R
   1000004     4.02244505E+03   # ~c_L
   2000004     4.02097713E+03   # ~c_R
   1000005     2.08353936E+03   # ~b_1
   2000005     4.02408038E+03   # ~b_2
   1000006     7.34790677E+02   # ~t_1
   2000006     2.09608907E+03   # ~t_2
   1000011     4.00385962E+03   # ~e_L
   2000011     4.00365428E+03   # ~e_R
   1000012     4.00274817E+03   # ~nu_eL
   1000013     4.00385962E+03   # ~mu_L
   2000013     4.00365428E+03   # ~mu_R
   1000014     4.00274817E+03   # ~nu_muL
   1000015     4.00518000E+03   # ~tau_1
   2000015     4.00741662E+03   # ~tau_2
   1000016     4.00444593E+03   # ~nu_tauL
   1000021     1.98623084E+03   # ~g
   1000022     1.44840940E+02   # ~chi_10
   1000023    -1.96163465E+02   # ~chi_20
   1000025     2.11028966E+02   # ~chi_30
   1000035     2.06535977E+03   # ~chi_40
   1000024     1.92192929E+02   # ~chi_1+
   1000037     2.06553252E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15334223E-01   # N_11
  1  2    -1.34548613E-02   # N_12
  1  3    -4.64089509E-01   # N_13
  1  4    -3.45933519E-01   # N_14
  2  1    -9.38006431E-02   # N_21
  2  2     2.63826791E-02   # N_22
  2  3    -6.95905941E-01   # N_23
  2  4     7.11491612E-01   # N_24
  3  1     5.71341131E-01   # N_31
  3  2     2.51510847E-02   # N_32
  3  3     5.48026516E-01   # N_33
  3  4     6.10412707E-01   # N_34
  4  1    -9.25626984E-04   # N_41
  4  2     9.99244887E-01   # N_42
  4  3    -1.66911852E-03   # N_43
  4  4    -3.88073881E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36517066E-03   # U_11
  1  2     9.99997203E-01   # U_12
  2  1    -9.99997203E-01   # U_21
  2  2     2.36517066E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48946925E-02   # V_11
  1  2    -9.98492150E-01   # V_12
  2  1    -9.98492150E-01   # V_21
  2  2    -5.48946925E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.00664973E-01   # cos(theta_t)
  1  2     9.94920380E-01   # sin(theta_t)
  2  1    -9.94920380E-01   # -sin(theta_t)
  2  2    -1.00664973E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999705E-01   # cos(theta_b)
  1  2    -7.68114518E-04   # sin(theta_b)
  2  1     7.68114518E-04   # -sin(theta_b)
  2  2     9.99999705E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04745433E-01   # cos(theta_tau)
  1  2     7.09460270E-01   # sin(theta_tau)
  2  1    -7.09460270E-01   # -sin(theta_tau)
  2  2    -7.04745433E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00305042E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.20144218E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43766298E+02   # higgs               
         4     1.61158791E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.20144218E+03  # The gauge couplings
     1     3.61895354E-01   # gprime(Q) DRbar
     2     6.36145225E-01   # g(Q) DRbar
     3     1.03123311E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.20144218E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.13791231E-06   # A_c(Q) DRbar
  3  3     2.52959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.20144218E+03  # The trilinear couplings
  1  1     4.17113911E-07   # A_d(Q) DRbar
  2  2     4.17153288E-07   # A_s(Q) DRbar
  3  3     7.46991492E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.20144218E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     9.08014275E-08   # A_mu(Q) DRbar
  3  3     9.17357976E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.20144218E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.69340409E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.20144218E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82375354E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.20144218E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03924186E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.20144218E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58492604E+07   # M^2_Hd              
        22    -2.53672276E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.05485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     7.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40389956E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.15267407E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.93495197E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.83755895E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.16325995E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.10052570E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.85245845E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.37518278E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.60715565E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.35816091E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.63580550E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.43777503E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.33382729E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95619829E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.84300841E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.18011603E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.05620415E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.42075160E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.21990168E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.58373679E-05    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     6.96954805E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.36406733E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     1.92370110E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64225540E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68833388E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77151375E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53512188E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.66088552E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62310418E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.19887759E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13697421E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.68328798E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.40977314E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.44468912E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.89807492E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76065919E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.76044941E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.51472309E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57034462E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82269948E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.31542483E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.63311138E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51573040E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58746570E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.74656433E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94904800E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83484932E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63929269E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43690932E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76084730E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49579125E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72049066E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.09900739E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82746893E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.86797376E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66493643E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51797219E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51991589E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.78120921E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29205432E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.79026372E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.88901304E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85299315E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76065919E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.76044941E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.51472309E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57034462E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82269948E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.31542483E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.63311138E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51573040E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58746570E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.74656433E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94904800E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83484932E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63929269E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43690932E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76084730E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49579125E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72049066E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.09900739E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82746893E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.86797376E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66493643E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51797219E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51991589E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.78120921E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29205432E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.79026372E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.88901304E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85299315E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12567597E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04319700E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.73528685E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.28802723E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77022760E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.74247144E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55397997E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07883284E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65421692E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78794093E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25789904E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.63110589E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12567597E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04319700E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.73528685E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.28802723E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77022760E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.74247144E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55397997E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07883284E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65421692E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78794093E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25789904E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.63110589E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09823198E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52190538E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16713197E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34969247E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38706858E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43713242E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78090713E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10166793E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.45557075E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76596105E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10935722E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41032155E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20622818E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82753157E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12676802E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17084526E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.26129985E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60836535E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77321594E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09144266E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53157484E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12676802E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17084526E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.26129985E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60836535E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77321594E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09144266E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53157484E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45946225E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05869704E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.94892506E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16696822E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50912363E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.81193001E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00480025E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.50945043E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33586261E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33586261E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196466E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196466E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10434545E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.68914116E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87722203E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.45056890E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.14338095E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40295023E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.85421002E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.39021055E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.35397053E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.73814714E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18077515E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53161672E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18077515E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53161672E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41148003E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51189111E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51189111E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47894695E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02167033E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02167033E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02167004E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.01187703E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.01187703E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.01187703E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.01187703E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.33729463E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.33729463E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.33729463E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.33729463E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.06587290E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.06587290E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.28349601E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.51743918E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.92086055E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.52646753E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.73748323E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.52646753E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.73748323E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.28056390E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.21268322E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.21268322E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.19808608E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.47372336E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.47372336E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.47371646E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.02478640E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.22100056E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.02478640E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.22100056E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.74846782E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.19736701E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.19736701E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.10294623E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.39334514E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.39334514E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.39334518E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.45740332E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.45740332E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.45740332E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.45740332E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.48578195E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.48578195E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.48578195E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.48578195E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.42205117E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.42205117E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.68777818E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.63923786E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.34590671E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.41545358E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.39005710E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.39005710E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.57164976E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.09196081E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.03648908E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.91447534E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.91447534E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
#
#         PDG            Width
DECAY        25     4.06819232E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94447210E-01    2           5        -5   # BR(h -> b       bb     )
     6.41050098E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26930858E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81182882E-04    2           3        -3   # BR(h -> s       sb     )
     2.08952868E-02    2           4        -4   # BR(h -> c       cb     )
     6.81252627E-02    2          21        21   # BR(h -> g       g      )
     2.36528713E-03    2          22        22   # BR(h -> gam     gam    )
     1.62647540E-03    2          22        23   # BR(h -> Z       gam    )
     2.19880209E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78471456E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.42311489E+01   # H decays
#          BR         NDA      ID1       ID2
     3.43389602E-01    2           5        -5   # BR(H -> b       bb     )
     6.11382212E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16169942E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55691797E-04    2           3        -3   # BR(H -> s       sb     )
     7.17273694E-08    2           4        -4   # BR(H -> c       cb     )
     7.18857572E-03    2           6        -6   # BR(H -> t       tb     )
     9.80592186E-07    2          21        21   # BR(H -> g       g      )
     1.56788030E-11    2          22        22   # BR(H -> gam     gam    )
     1.84607878E-09    2          23        22   # BR(H -> Z       gam    )
     2.00881812E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00371329E-06    2          23        23   # BR(H -> Z       Z      )
     7.38470809E-06    2          25        25   # BR(H -> h       h      )
    -1.56910526E-25    2          36        36   # BR(H -> A       A      )
     1.26040131E-20    2          23        36   # BR(H -> Z       A      )
     1.86411373E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65292992E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65292992E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.89890143E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57154968E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.61487143E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09985476E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.82367962E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.64933756E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63370335E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.76283813E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.16534843E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.69638314E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.04722805E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.04722805E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.42278409E+01   # A decays
#          BR         NDA      ID1       ID2
     3.43436544E-01    2           5        -5   # BR(A -> b       bb     )
     6.11423318E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16184306E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55750472E-04    2           3        -3   # BR(A -> s       sb     )
     7.21845497E-08    2           4        -4   # BR(A -> c       cb     )
     7.20170419E-03    2           6        -6   # BR(A -> t       tb     )
     1.47976822E-05    2          21        21   # BR(A -> g       g      )
     4.71870693E-08    2          22        22   # BR(A -> gam     gam    )
     1.62713327E-08    2          23        22   # BR(A -> Z       gam    )
     2.00471562E-06    2          23        25   # BR(A -> Z       h      )
     1.86548151E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65306729E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65306729E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.51030261E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94762099E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30651937E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67809969E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57941790E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.50749804E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83955162E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.28583111E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.78033737E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.21585839E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.21585839E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.40983128E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.46993741E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.13047129E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16758446E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.50075692E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22797669E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52634293E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.48726180E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.01179220E-06    2          24        25   # BR(H+ -> W+      h      )
     3.00826239E-14    2          24        36   # BR(H+ -> W+      A      )
     5.60037897E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.47531760E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.35403902E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65916918E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.18264522E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61014965E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.98252304E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.59650917E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
