#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ 'CompHep', 'Pythia8' ]
evgenConfig.description = 'E6 Vector Like Lepton Pair Production'
evgenConfig.keywords = ["exotic"]
evgenConfig.contact = ['ali.osman.acar@cern.ch']
evgenConfig.inputfilecheck = 'WjjnuZlle150GeV'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

genSeq.Pythia8.Commands += [ "24:onMode = off","24:onIfAny = 1 2 3 4 5",         # decay of W
                             "23:onMode = off","23:onIfAny = 11 13" ]    # decay of Z
