model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 60.
mHD = 60.
widthZp = 2.387214e-01
widthhd = 9.415733e-02
filteff = 2.243259e-02

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]
evgenConfig.minevents = 500

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
