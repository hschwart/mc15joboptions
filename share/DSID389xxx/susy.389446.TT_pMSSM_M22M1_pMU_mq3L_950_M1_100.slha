#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871689E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03961143E+01   # W+
        25     1.25014206E+02   # h
        35     3.00007078E+03   # H
        36     2.99999994E+03   # A
        37     3.00089791E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04202642E+03   # ~d_L
   2000001     3.03687914E+03   # ~d_R
   1000002     3.04111471E+03   # ~u_L
   2000002     3.03781717E+03   # ~u_R
   1000003     3.04202642E+03   # ~s_L
   2000003     3.03687914E+03   # ~s_R
   1000004     3.04111471E+03   # ~c_L
   2000004     3.03781717E+03   # ~c_R
   1000005     1.04982109E+03   # ~b_1
   2000005     3.03581890E+03   # ~b_2
   1000006     1.04604728E+03   # ~t_1
   2000006     3.01883033E+03   # ~t_2
   1000011     3.00629012E+03   # ~e_L
   2000011     3.00222949E+03   # ~e_R
   1000012     3.00489444E+03   # ~nu_eL
   1000013     3.00629012E+03   # ~mu_L
   2000013     3.00222949E+03   # ~mu_R
   1000014     3.00489444E+03   # ~nu_muL
   1000015     2.98573284E+03   # ~tau_1
   2000015     3.02183786E+03   # ~tau_2
   1000016     3.00460599E+03   # ~nu_tauL
   1000021     2.35219868E+03   # ~g
   1000022     1.00102791E+02   # ~chi_10
   1000023     2.15503821E+02   # ~chi_20
   1000025    -2.99465097E+03   # ~chi_30
   1000035     2.99515188E+03   # ~chi_40
   1000024     2.15663770E+02   # ~chi_1+
   1000037     2.99581426E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888519E-01   # N_11
  1  2    -1.53506420E-03   # N_12
  1  3     1.48013109E-02   # N_13
  1  4    -1.23047281E-03   # N_14
  2  1     1.92559710E-03   # N_21
  2  2     9.99651452E-01   # N_22
  2  3    -2.61541601E-02   # N_23
  2  4     3.03738992E-03   # N_24
  3  1    -9.56787400E-03   # N_31
  3  2     1.63628777E-02   # N_32
  3  3     7.06831148E-01   # N_33
  3  4     7.07128306E-01   # N_34
  4  1    -1.13003165E-02   # N_41
  4  2     2.06609187E-02   # N_42
  4  3     7.06743666E-01   # N_43
  4  4    -7.07077662E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99314689E-01   # U_11
  1  2    -3.70155797E-02   # U_12
  2  1     3.70155797E-02   # U_21
  2  2     9.99314689E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990755E-01   # V_11
  1  2    -4.30000548E-03   # V_12
  2  1     4.30000548E-03   # V_21
  2  2     9.99990755E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447683E-01   # cos(theta_t)
  1  2    -5.56976150E-02   # sin(theta_t)
  2  1     5.56976150E-02   # -sin(theta_t)
  2  2     9.98447683E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99907842E-01   # cos(theta_b)
  1  2     1.35759901E-02   # sin(theta_b)
  2  1    -1.35759901E-02   # -sin(theta_b)
  2  2     9.99907842E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06918097E-01   # cos(theta_tau)
  1  2     7.07295415E-01   # sin(theta_tau)
  2  1    -7.07295415E-01   # -sin(theta_tau)
  2  2     7.06918097E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01741268E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68716890E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43938235E+02   # higgs               
         4     7.77729233E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68716890E+03  # The gauge couplings
     1     3.62563303E-01   # gprime(Q) DRbar
     2     6.40046917E-01   # g(Q) DRbar
     3     1.02372315E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68716890E+03  # The trilinear couplings
  1  1     2.70011512E-06   # A_u(Q) DRbar
  2  2     2.70015429E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68716890E+03  # The trilinear couplings
  1  1     6.76199128E-07   # A_d(Q) DRbar
  2  2     6.76331914E-07   # A_s(Q) DRbar
  3  3     1.56691089E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68716890E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.49106706E-07   # A_mu(Q) DRbar
  3  3     1.50924240E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68716890E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50444031E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68716890E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12698022E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68716890E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05349938E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68716890E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.18628762E+04   # M^2_Hd              
        22    -9.03163592E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42218067E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.15013171E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47651717E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47651717E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52348283E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52348283E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.18714612E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.23615035E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.17709676E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.69928821E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11116774E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.95295181E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.49245039E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.01137227E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.62566183E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.99222055E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67178559E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59837182E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.13086047E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.17485274E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.32671554E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.40563738E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.46169106E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17417848E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.01069820E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.08979741E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.51207176E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.47931477E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.57761523E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.32399169E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.78857943E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.08422538E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.87148203E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.06369327E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.84283257E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65460588E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.72376706E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.90703780E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.30900548E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.18712076E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.97795929E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16025230E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.61161084E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.93543631E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.35226492E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.84405338E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.38838290E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.06865028E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.03361853E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65156008E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.01879730E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.27975235E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30325214E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.76080169E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98484531E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64981483E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.59103455E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.69083430E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.69366982E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.03440789E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54089477E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.06369327E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.84283257E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65460588E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.72376706E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.90703780E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.30900548E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.18712076E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.97795929E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16025230E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.61161084E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.93543631E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.35226492E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.84405338E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.38838290E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.06865028E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.03361853E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65156008E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.01879730E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.27975235E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30325214E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.76080169E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98484531E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64981483E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.59103455E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.69083430E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.69366982E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.03440789E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54089477E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02593935E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.68754724E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01617832E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.17630577E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.38476904E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01506650E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.03598401E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56642748E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99996316E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.67879215E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.33311613E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.83898880E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02593935E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.68754724E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01617832E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.17630577E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.38476904E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01506650E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.03598401E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56642748E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99996316E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.67879215E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.33311613E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.83898880E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82507926E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46037496E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18410364E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35552139E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76502627E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54206665E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15696663E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.46586635E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.63628354E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30048906E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.67450929E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02627382E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.78776012E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00137012E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.68479853E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.33495791E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01985370E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.09538580E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02627382E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.78776012E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00137012E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.68479853E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.33495791E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01985370E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.09538580E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02622359E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.78693850E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00111349E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.31490945E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.79217295E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02017148E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.10240039E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.68851962E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85219520E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.63229375E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.02236641E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.82445106E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.56497196E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.34404577E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.38904651E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.84594970E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.77703692E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     7.88894202E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.40992479E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.61693484E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.33877013E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.33877013E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04400198E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.96272241E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54477060E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54477060E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.19671131E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.19671131E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.05983761E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.05983761E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.78667546E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02404638E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.93583824E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.43731382E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.43731382E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.47784141E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.89572934E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.51682631E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.51682631E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.22766139E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.22766139E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.40422076E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.40422076E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.79019545E-03   # h decays
#          BR         NDA      ID1       ID2
     6.69485035E-01    2           5        -5   # BR(h -> b       bb     )
     5.44833671E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92872681E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.09331539E-04    2           3        -3   # BR(h -> s       sb     )
     1.76747257E-02    2           4        -4   # BR(h -> c       cb     )
     5.74742032E-02    2          21        21   # BR(h -> g       g      )
     1.96738069E-03    2          22        22   # BR(h -> gam     gam    )
     1.30595928E-03    2          22        23   # BR(h -> Z       gam    )
     1.75118591E-01    2          24       -24   # BR(h -> W+      W-     )
     2.18885338E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.41291414E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39779274E-01    2           5        -5   # BR(H -> b       bb     )
     1.75999048E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.22290576E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.64175034E-04    2           3        -3   # BR(H -> s       sb     )
     2.15566874E-07    2           4        -4   # BR(H -> c       cb     )
     2.15041744E-02    2           6        -6   # BR(H -> t       tb     )
     2.61701949E-05    2          21        21   # BR(H -> g       g      )
     1.64144579E-08    2          22        22   # BR(H -> gam     gam    )
     8.29690923E-09    2          23        22   # BR(H -> Z       gam    )
     2.90617518E-05    2          24       -24   # BR(H -> W+      W-     )
     1.45129516E-05    2          23        23   # BR(H -> Z       Z      )
     7.85763476E-05    2          25        25   # BR(H -> h       h      )
     1.78607512E-23    2          36        36   # BR(H -> A       A      )
     2.95507082E-19    2          23        36   # BR(H -> Z       A      )
     2.35839758E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11699893E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.17424075E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.25971151E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.68068689E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.29792739E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33293536E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84252368E-01    2           5        -5   # BR(A -> b       bb     )
     1.86559232E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59627941E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10160084E-04    2           3        -3   # BR(A -> s       sb     )
     2.28615692E-07    2           4        -4   # BR(A -> c       cb     )
     2.27933309E-02    2           6        -6   # BR(A -> t       tb     )
     6.71241784E-05    2          21        21   # BR(A -> g       g      )
     3.33134519E-08    2          22        22   # BR(A -> gam     gam    )
     6.60664187E-08    2          23        22   # BR(A -> Z       gam    )
     3.06897948E-05    2          23        25   # BR(A -> Z       h      )
     2.61275806E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.20929836E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30085733E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92594226E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04080541E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10461522E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38993597E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.45023067E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.06952642E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96602590E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02167122E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18040723E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.93636208E-05    2          24        25   # BR(H+ -> W+      h      )
     7.38130484E-14    2          24        36   # BR(H+ -> W+      A      )
     1.99295092E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.38455112E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.79053188E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
