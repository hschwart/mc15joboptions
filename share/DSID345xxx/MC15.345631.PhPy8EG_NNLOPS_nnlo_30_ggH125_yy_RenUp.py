#--------------------------------------------------------------
# Powheg HJ setup starting from ATLAS defaults
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_Hj_Common.py')

# AZNLO tune
include("MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_RenUp_EvtGen_Common.py")

# use "Main31", i.e. "Power-shower + jet veto"
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

# Decay to diphoton
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 H+jet production with NNLOPS and the A14 tune'
evgenConfig.keywords    = [ 'Higgs', '1jet' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'kathrin.becker@cern.ch', 'cyril.becot@cern.ch']
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.minevents   = 2000
