#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.17330118E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03999823E+01   # W+
        25     1.23563387E+02   # h
        35     3.00005372E+03   # H
        36     3.00000002E+03   # A
        37     3.00086607E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04347150E+03   # ~d_L
   2000001     3.03837088E+03   # ~d_R
   1000002     3.04256172E+03   # ~u_L
   2000002     3.03922752E+03   # ~u_R
   1000003     3.04347150E+03   # ~s_L
   2000003     3.03837088E+03   # ~s_R
   1000004     3.04256172E+03   # ~c_L
   2000004     3.03922752E+03   # ~c_R
   1000005     1.10200732E+03   # ~b_1
   2000005     3.03721689E+03   # ~b_2
   1000006     1.10067207E+03   # ~t_1
   2000006     3.02262422E+03   # ~t_2
   1000011     3.00621714E+03   # ~e_L
   2000011     3.00231027E+03   # ~e_R
   1000012     3.00482470E+03   # ~nu_eL
   1000013     3.00621714E+03   # ~mu_L
   2000013     3.00231027E+03   # ~mu_R
   1000014     3.00482470E+03   # ~nu_muL
   1000015     2.98556341E+03   # ~tau_1
   2000015     3.02189948E+03   # ~tau_2
   1000016     3.00449876E+03   # ~nu_tauL
   1000021     2.35428569E+03   # ~g
   1000022     2.50756852E+02   # ~chi_10
   1000023     5.28868823E+02   # ~chi_20
   1000025    -2.99417858E+03   # ~chi_30
   1000035     2.99494186E+03   # ~chi_40
   1000024     5.29032192E+02   # ~chi_1+
   1000037     2.99551736E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886483E-01   # N_11
  1  2    -8.46665035E-04   # N_12
  1  3     1.49126669E-02   # N_13
  1  4    -1.97902136E-03   # N_14
  2  1     1.25811999E-03   # N_21
  2  2     9.99622395E-01   # N_22
  2  3    -2.68382809E-02   # N_23
  2  4     5.76117829E-03   # N_24
  3  1    -9.13016991E-03   # N_31
  3  2     1.49141451E-02   # N_32
  3  3     7.06861945E-01   # N_33
  3  4     7.07135347E-01   # N_34
  4  1    -1.19196532E-02   # N_41
  4  2     2.30633648E-02   # N_42
  4  3     7.06684873E-01   # N_43
  4  4    -7.07051974E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99278902E-01   # U_11
  1  2    -3.79693987E-02   # U_12
  2  1     3.79693987E-02   # U_21
  2  2     9.99278902E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966773E-01   # V_11
  1  2    -8.15186060E-03   # V_12
  2  1     8.15186060E-03   # V_21
  2  2     9.99966773E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98708036E-01   # cos(theta_t)
  1  2    -5.08159309E-02   # sin(theta_t)
  2  1     5.08159309E-02   # -sin(theta_t)
  2  2     9.98708036E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99902365E-01   # cos(theta_b)
  1  2     1.39735632E-02   # sin(theta_b)
  2  1    -1.39735632E-02   # -sin(theta_b)
  2  2     9.99902365E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06954526E-01   # cos(theta_tau)
  1  2     7.07259004E-01   # sin(theta_tau)
  2  1    -7.07259004E-01   # -sin(theta_tau)
  2  2     7.06954526E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01748081E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.73301184E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43964873E+02   # higgs               
         4     7.93772045E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.73301184E+03  # The gauge couplings
     1     3.62659471E-01   # gprime(Q) DRbar
     2     6.37961027E-01   # g(Q) DRbar
     3     1.02308897E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.73301184E+03  # The trilinear couplings
  1  1     2.83466357E-06   # A_u(Q) DRbar
  2  2     2.83470518E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.73301184E+03  # The trilinear couplings
  1  1     7.56659057E-07   # A_d(Q) DRbar
  2  2     7.56794416E-07   # A_s(Q) DRbar
  3  3     1.66542443E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.73301184E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.60754652E-07   # A_mu(Q) DRbar
  3  3     1.62698475E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.73301184E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.48131926E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.73301184E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16270898E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.73301184E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06691023E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.73301184E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.37067879E+04   # M^2_Hd              
        22    -9.01039600E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41275588E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.84678510E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48137758E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48137758E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51862242E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51862242E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.97990894E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.74451458E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.10464301E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.72090553E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.69424016E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.76925074E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.86765162E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.77136835E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.52045619E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.43397992E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.52064113E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.47056349E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.88473091E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.82326355E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.90044322E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.44571096E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.36424471E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.17444015E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.97330947E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.17417698E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.83788714E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.76968529E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.05730497E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30536173E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.84621426E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16013108E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.96104802E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.86801089E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99822871E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61004813E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.37448966E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.14777882E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.22065834E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.44352506E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.10930985E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14660518E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60202592E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.42260303E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.34212093E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.21289399E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39797130E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.87312703E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.10378723E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60786782E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.57027797E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.25084817E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21493908E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.49614792E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.11614793E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64146191E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.55929224E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.89443192E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.68201508E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.06695696E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54406999E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.86801089E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99822871E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61004813E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.37448966E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.14777882E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.22065834E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.44352506E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.10930985E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14660518E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60202592E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.42260303E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.34212093E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.21289399E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39797130E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.87312703E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.10378723E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60786782E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.57027797E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.25084817E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21493908E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.49614792E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.11614793E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64146191E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.55929224E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.89443192E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.68201508E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.06695696E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54406999E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.81263245E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01399481E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99964425E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.92839818E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.74715192E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98636042E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.64732619E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54894312E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998487E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.50744989E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.47439717E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.46366135E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.81263245E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01399481E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99964425E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.92839818E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.74715192E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98636042E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.64732619E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54894312E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998487E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.50744989E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.47439717E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.46366135E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70813578E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57064353E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14630983E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28304664E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.65096831E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65539882E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11804878E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60343940E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.77608356E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22603202E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.82426176E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.81284276E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01951249E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98934455E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.45212418E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.22515570E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99114276E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.62229111E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.81284276E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01951249E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98934455E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.45212418E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.22515570E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99114276E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.62229111E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81272135E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01943126E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98907401E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.06460497E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.14596094E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99147095E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.36082975E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.88894753E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.68981066E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.74074872E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.95329296E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.83654480E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.70616921E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.40677859E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.50806410E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     8.10528143E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.49051972E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.03721741E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49627826E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.70677948E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.56545760E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.41203307E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.47922129E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.47922129E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.52587126E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.30989764E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51344053E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.51344053E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.26636579E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.26636579E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.98576477E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.98576477E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.62782320E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.28175681E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.29031548E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.56046885E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.56046885E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.64598996E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.82822298E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.47989277E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.47989277E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.29421678E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.29421678E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.46371268E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.46371268E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.58464990E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88616534E-01    2           5        -5   # BR(h -> b       bb     )
     5.62653173E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.99186524E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.23694368E-04    2           3        -3   # BR(h -> s       sb     )
     1.82945162E-02    2           4        -4   # BR(h -> c       cb     )
     5.81466778E-02    2          21        21   # BR(h -> g       g      )
     1.95588232E-03    2          22        22   # BR(h -> gam     gam    )
     1.19569396E-03    2          22        23   # BR(h -> Z       gam    )
     1.55876250E-01    2          24       -24   # BR(h -> W+      W-     )
     1.90262477E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40326690E+01   # H decays
#          BR         NDA      ID1       ID2
     7.51313398E-01    2           5        -5   # BR(H -> b       bb     )
     1.77207994E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.26565121E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.69424768E-04    2           3        -3   # BR(H -> s       sb     )
     2.17053681E-07    2           4        -4   # BR(H -> c       cb     )
     2.16524902E-02    2           6        -6   # BR(H -> t       tb     )
     1.99258954E-05    2          21        21   # BR(H -> g       g      )
     5.35019508E-09    2          22        22   # BR(H -> gam     gam    )
     8.36096675E-09    2          23        22   # BR(H -> Z       gam    )
     2.94461443E-05    2          24       -24   # BR(H -> W+      W-     )
     1.47049167E-05    2          23        23   # BR(H -> Z       Z      )
     7.90701620E-05    2          25        25   # BR(H -> h       h      )
     1.49696606E-23    2          36        36   # BR(H -> A       A      )
    -3.36021828E-20    2          23        36   # BR(H -> Z       A      )
     2.09148929E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09280668E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.04295439E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.83355383E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.43543996E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.27074586E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.34201657E+01   # A decays
#          BR         NDA      ID1       ID2
     7.85691846E-01    2           5        -5   # BR(A -> b       bb     )
     1.85296820E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.55164360E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.04677879E-04    2           3        -3   # BR(A -> s       sb     )
     2.27068691E-07    2           4        -4   # BR(A -> c       cb     )
     2.26390926E-02    2           6        -6   # BR(A -> t       tb     )
     6.66699590E-05    2          21        21   # BR(A -> g       g      )
     3.75726092E-08    2          22        22   # BR(A -> gam     gam    )
     6.56631172E-08    2          23        22   # BR(A -> Z       gam    )
     3.06786638E-05    2          23        25   # BR(A -> Z       h      )
     2.60736662E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.20722602E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30012071E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.86510896E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04591327E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.12077225E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.37823917E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.40887364E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.17293132E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.94172803E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.01667236E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.27983875E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.94226737E-05    2          24        25   # BR(H+ -> W+      h      )
     6.12949002E-14    2          24        36   # BR(H+ -> W+      A      )
     1.92202944E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.98846798E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.91958302E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
