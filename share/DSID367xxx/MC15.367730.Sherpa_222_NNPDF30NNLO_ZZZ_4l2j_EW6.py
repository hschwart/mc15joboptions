include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "ZZZ (-> 4 charged leptons + 2 jets) +Oj@NLO+1,2j@LO."
evgenConfig.keywords = ["SM", "triboson", "multilepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "rongkun.wang@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "222_NNPDF30NNLO_ZZZ"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  %tags for process setup
  NJET:=2; LJET:=3; QCUT:=30;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;

  %decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_STATUS[23,11,-11]=2
  HDH_STATUS[23,13,-13]=2
  HDH_STATUS[23,15,-15]=2
  HDH_STATUS[23,1,-1]=2
  HDH_STATUS[23,2,-2]=2
  HDH_STATUS[23,3,-3]=2
  HDH_STATUS[23,4,-4]=2
  HDH_STATUS[23,5,-5]=2
}(run);

(processes){
  Process 901 901 -> 23 23 23 901{NJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[23]=0" ]

#evgenLog.info('4 lepton 2 jet filter is applied')

include ( 'MC15JobOptions/DecaysFinalStateFilter.py' )
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [ 23 ]
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 4
filtSeq.DecaysFinalStateFilter.NQuarks = 2
