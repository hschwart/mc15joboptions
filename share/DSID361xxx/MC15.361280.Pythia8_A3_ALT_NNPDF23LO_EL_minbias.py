# Pythia8 minimum bias EL with A3

evgenConfig.description = "elastic events, with the A3 MSTW2008LO tune. Default PomFlux4"
evgenConfig.keywords = ["QCD", "minBias", "diffraction"]
evgenConfig.generators = ["Pythia8"]

include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:elastic = on"]
