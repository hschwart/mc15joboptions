evgenConfig.description = 'MadGraph5_aMC@NLO+Herwig++ bbH production'
evgenConfig.keywords    = [ 'Higgs', 'BSMHiggs', 'MSSM', '2muon' ]
evgenConfig.contact     = [ 'christopher.john.mcnicol@cern.ch', 'sinead.farrington@cern.ch' ]

include("MC15JobOptions/aMcAtNloPythia8EvtGenControl_bbHmumu.py")
evgenConfig.generators  = [ "aMcAtNlo", "Pythia8", "EvtGen"] 
