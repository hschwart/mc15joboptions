#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10963977E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04016512E+01   # W+
        25     1.25153599E+02   # h
        35     3.00023389E+03   # H
        36     2.99999970E+03   # A
        37     3.00098231E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01743155E+03   # ~d_L
   2000001     3.01223330E+03   # ~d_R
   1000002     3.01651112E+03   # ~u_L
   2000002     3.01454460E+03   # ~u_R
   1000003     3.01743155E+03   # ~s_L
   2000003     3.01223330E+03   # ~s_R
   1000004     3.01651112E+03   # ~c_L
   2000004     3.01454460E+03   # ~c_R
   1000005     4.40731545E+02   # ~b_1
   2000005     3.01183709E+03   # ~b_2
   1000006     4.36467645E+02   # ~t_1
   2000006     2.99305992E+03   # ~t_2
   1000011     3.00674489E+03   # ~e_L
   2000011     3.00080506E+03   # ~e_R
   1000012     3.00535145E+03   # ~nu_eL
   1000013     3.00674489E+03   # ~mu_L
   2000013     3.00080506E+03   # ~mu_R
   1000014     3.00535145E+03   # ~nu_muL
   1000015     2.98590023E+03   # ~tau_1
   2000015     3.02225434E+03   # ~tau_2
   1000016     3.00557701E+03   # ~nu_tauL
   1000021     2.32108329E+03   # ~g
   1000022     2.02134840E+02   # ~chi_10
   1000023     4.22890692E+02   # ~chi_20
   1000025    -3.00434400E+03   # ~chi_30
   1000035     3.00497588E+03   # ~chi_40
   1000024     4.23047419E+02   # ~chi_1+
   1000037     3.00559293E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887593E-01   # N_11
  1  2    -9.63121426E-04   # N_12
  1  3     1.48623901E-02   # N_13
  1  4    -1.72715829E-03   # N_14
  2  1     1.36694699E-03   # N_21
  2  2     9.99632915E-01   # N_22
  2  3    -2.66215893E-02   # N_23
  2  4     4.84321991E-03   # N_24
  3  1    -9.27036919E-03   # N_31
  3  2     1.54110341E-02   # N_32
  3  3     7.06851393E-01   # N_33
  3  4     7.07133416E-01   # N_34
  4  1    -1.17044022E-02   # N_41
  4  2     2.22622273E-02   # N_42
  4  3     7.06704682E-01   # N_43
  4  4    -7.07061449E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99290420E-01   # U_11
  1  2    -3.76650436E-02   # U_12
  2  1     3.76650436E-02   # U_21
  2  2     9.99290420E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976514E-01   # V_11
  1  2    -6.85358643E-03   # V_12
  2  1     6.85358643E-03   # V_21
  2  2     9.99976514E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98905260E-01   # cos(theta_t)
  1  2    -4.67790716E-02   # sin(theta_t)
  2  1     4.67790716E-02   # -sin(theta_t)
  2  2     9.98905260E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99922231E-01   # cos(theta_b)
  1  2     1.24712450E-02   # sin(theta_b)
  2  1    -1.24712450E-02   # -sin(theta_b)
  2  2     9.99922231E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06914063E-01   # cos(theta_tau)
  1  2     7.07299447E-01   # sin(theta_tau)
  2  1    -7.07299447E-01   # -sin(theta_tau)
  2  2     7.06914063E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01973835E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.09639771E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44938789E+02   # higgs               
         4     6.11535382E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.09639771E+03  # The gauge couplings
     1     3.61127804E-01   # gprime(Q) DRbar
     2     6.37847374E-01   # g(Q) DRbar
     3     1.03333906E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.09639771E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.07329708E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.09639771E+03  # The trilinear couplings
  1  1     2.73261931E-07   # A_d(Q) DRbar
  2  2     2.73315311E-07   # A_s(Q) DRbar
  3  3     6.22617635E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.09639771E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     5.81615640E-08   # A_mu(Q) DRbar
  3  3     5.88404886E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.09639771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58768396E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.09639771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12427875E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.09639771E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05910709E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.09639771E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -9.30244039E+04   # M^2_Hd              
        22    -9.16792609E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41217915E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.55722238E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48249005E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48249005E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51750995E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51750995E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.21398480E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.69541441E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.30458559E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.06494508E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.07236039E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.03749785E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.10052684E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.15772898E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66604714E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.52541688E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.03443293E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.50120425E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.77171321E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.22828679E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.27217770E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83126600E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.27313710E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.63859891E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.55688906E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.85042416E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27507014E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95323298E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.28604467E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.17861742E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96790055E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.82477646E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60496479E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.45732534E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77152071E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.21038747E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.00130393E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.12639989E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21180211E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55661621E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.82585632E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.56751237E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.06477408E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44338095E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.97307948E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.94239828E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60265892E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.46529752E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.24476593E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.20468400E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.53781321E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.13323262E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70136280E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42465490E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.03207666E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.30466338E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.76687254E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55753370E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96790055E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.82477646E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60496479E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.45732534E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77152071E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.21038747E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.00130393E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.12639989E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21180211E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55661621E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.82585632E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.56751237E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.06477408E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44338095E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.97307948E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.94239828E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60265892E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.46529752E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.24476593E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.20468400E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.53781321E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.13323262E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70136280E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42465490E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.03207666E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.30466338E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.76687254E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55753370E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89019028E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.90003248E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00786516E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.23231123E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.05818028E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00213158E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.20884694E-10    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54266925E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998188E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.81186764E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.89019028E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.90003248E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00786516E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.23231123E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.05818028E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00213158E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.20884694E-10    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54266925E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998188E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.81186764E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74474984E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51097535E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16637161E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.32265304E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.68635308E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59422049E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13873311E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.32283064E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.37521378E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26683801E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     7.14107809E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89047110E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96221233E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99684658E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.99572518E-11    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.62961410E-11    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00693218E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.89047110E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96221233E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99684658E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.99572518E-11    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.62961410E-11    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00693218E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89111907E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.96131452E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99659059E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.97822334E-11    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.17351787E-11    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00727796E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     5.08106462E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.11529116E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68817055E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.92595167E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     7.34107857E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.61828947E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.61828947E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     9.24677455E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.59493806E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.04185137E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.43845841E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.81877748E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.17795947E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.73943398E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52605660E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.14108842E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.29279074E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.43787888E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.40981032E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.40981032E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.30411044E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.15228003E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.69848762E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.69848762E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24854570E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24854570E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.06249798E-11    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.06249798E-11    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.06249798E-11    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.06249798E-11    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     8.74275436E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     8.74275436E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.05357343E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.18918106E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.14986022E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.47425960E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.47425960E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.34205371E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.68379477E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.67542047E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.67542047E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27411105E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27411105E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.07798253E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     9.07798253E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.07798253E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     9.07798253E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.11241812E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.11241812E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.97067744E-03   # h decays
#          BR         NDA      ID1       ID2
     6.77605378E-01    2           5        -5   # BR(h -> b       bb     )
     5.26124974E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.86249248E-04    2         -13        13   # BR(h -> mu+     mu-    )
     3.95184175E-04    2           3        -3   # BR(h -> s       sb     )
     1.70482117E-02    2           4        -4   # BR(h -> c       cb     )
     5.63150028E-02    2          21        21   # BR(h -> g       g      )
     1.89683492E-03    2          22        22   # BR(h -> gam     gam    )
     1.27455827E-03    2          22        23   # BR(h -> Z       gam    )
     1.71201788E-01    2          24       -24   # BR(h -> W+      W-     )
     2.14642954E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.38589550E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39620666E-01    2           5        -5   # BR(H -> b       bb     )
     1.79439562E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.34455411E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.79107859E-04    2           3        -3   # BR(H -> s       sb     )
     2.19983437E-07    2           4        -4   # BR(H -> c       cb     )
     2.19447811E-02    2           6        -6   # BR(H -> t       tb     )
     3.50381009E-05    2          21        21   # BR(H -> g       g      )
     7.27923972E-08    2          22        22   # BR(H -> gam     gam    )
     8.39723473E-09    2          23        22   # BR(H -> Z       gam    )
     3.63666928E-05    2          24       -24   # BR(H -> W+      W-     )
     1.81609299E-05    2          23        23   # BR(H -> Z       Z      )
     8.99888658E-05    2          25        25   # BR(H -> h       h      )
     1.46875465E-23    2          36        36   # BR(H -> A       A      )
     5.47476030E-17    2          23        36   # BR(H -> Z       A      )
     2.25376556E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11794616E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.12357782E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.13838924E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.31921506E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.44448414E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31275683E+01   # A decays
#          BR         NDA      ID1       ID2
     7.80883091E-01    2           5        -5   # BR(A -> b       bb     )
     1.89426839E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.69767098E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.22613087E-04    2           3        -3   # BR(A -> s       sb     )
     2.32129752E-07    2           4        -4   # BR(A -> c       cb     )
     2.31436880E-02    2           6        -6   # BR(A -> t       tb     )
     6.81559532E-05    2          21        21   # BR(A -> g       g      )
     3.35261736E-08    2          22        22   # BR(A -> gam     gam    )
     6.71459742E-08    2          23        22   # BR(A -> Z       gam    )
     3.82419090E-05    2          23        25   # BR(A -> Z       h      )
     2.68071938E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.23032260E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.33636768E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.07151923E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.72087791E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.07507972E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.55895432E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     9.04783833E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.88049920E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.31720820E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.09392071E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.01634965E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.17269268E-05    2          24        25   # BR(H+ -> W+      h      )
     1.23988999E-13    2          24        36   # BR(H+ -> W+      A      )
     2.10274662E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.89487912E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.71812882E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
