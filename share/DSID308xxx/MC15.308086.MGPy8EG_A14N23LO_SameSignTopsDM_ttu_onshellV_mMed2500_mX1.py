include("MC15JobOptions/MadGraphControl_ttu_onshellV.py")

evgenConfig.contact  = ["sergey.senkin@cern.ch"]
evgenConfig.description = "Same-sign tt production (on-shell V) with mDM="+str(mDM)+"GeV and mMed="+str(mMed)+"GeV"
evgenConfig.keywords = ["exotic"]
