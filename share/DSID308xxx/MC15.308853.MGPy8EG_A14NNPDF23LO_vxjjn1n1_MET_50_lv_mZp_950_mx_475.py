model="LightVector"
mDM1 = 475.
mDM2 = 1900.
mZp = 950.
mHD = 125.
widthZp = 4.530812e+00
widthN2 = 6.241118e+01
filteff = 9.940358e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
