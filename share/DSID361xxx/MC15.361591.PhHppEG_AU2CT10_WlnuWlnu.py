#i#
#wheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = 'WWlvlv'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 2.0     # Dependent on filter efficiency
PowhegConfig.generate()

#if runArgs.trfSubstepName == 'generate' :
#	PowhegConfig.generateRunCard()
#	PowhegConfig.generateEvents()

#PowhegConfig.generate()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP WW_lvlv production with AZNLO CTEQ6L1 tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch' ]
evgenConfig.minevents   = 5000
evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]



#import os
#if 'ATHENA_PROC_NUMBER' in os.environ:
#    # Try to modify the opts underfoot
#    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
#    else: opts.nprocs = 0



#if runArgs.trfSubstepName == 'generate' :
#	include("MC15JobOptions/nonStandard/Herwig_AUET2_CT10_Common.py")
#	include("MC15JobOptions/nonStandard/Herwig_Powheg.py")
#	include("MC15JobOptions/nonStandard/Herwig_Photos.py")
#	include("MC15JobOptions/nonStandard/Herwig_Tauola.py")

#include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_Common.py")
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')

cmds = """                                                                                                                                                                                                   
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                                                       
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                                                           
"""
genSeq.Herwigpp.Commands += cmds.splitlines()

