evgenConfig.description = "dmS sample to dibjet"
evgenConfig.keywords = ["exotic", "BSM", "scalar"]
model  = 'dmS'
mR     = 600
mDM    = 10000
gSM    = 1.00
gDM    = 1.00
widthR = 19.35

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet_withb.py")
