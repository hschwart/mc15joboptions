genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:oneChannel = 1 0.5770   100 5 -5',
                             '25:addChannel = 1 0.0291   100 4 -4',
                             '25:addChannel = 1 0.000246 100 3 -3',
                             '25:addChannel = 1 0.00000  100 6 -6',
                             '25:addChannel = 1 0.000219 100 13 -13',
                             '25:addChannel = 1 0.0632   100 15 -15',
                             '25:addChannel = 1 0.0857   100 21 21',
                             '25:addChannel = 1 0.00228  100 22 22',
                             '25:addChannel = 1 0.00154  100 22 23',
                             '25:addChannel = 1 0.0264   100 23 23',
                             '25:addChannel = 1 0.2150   100 24 -24'
]
