#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13089923E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04154095E+01   # W+
        25     1.25685748E+02   # h
        35     4.00000116E+03   # H
        36     3.99999657E+03   # A
        37     4.00106654E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02613326E+03   # ~d_L
   2000001     4.02269789E+03   # ~d_R
   1000002     4.02547784E+03   # ~u_L
   2000002     4.02364682E+03   # ~u_R
   1000003     4.02613326E+03   # ~s_L
   2000003     4.02269789E+03   # ~s_R
   1000004     4.02547784E+03   # ~c_L
   2000004     4.02364682E+03   # ~c_R
   1000005     8.77445346E+02   # ~b_1
   2000005     4.02544003E+03   # ~b_2
   1000006     8.63543811E+02   # ~t_1
   2000006     2.14834335E+03   # ~t_2
   1000011     4.00471419E+03   # ~e_L
   2000011     4.00331226E+03   # ~e_R
   1000012     4.00359596E+03   # ~nu_eL
   1000013     4.00471419E+03   # ~mu_L
   2000013     4.00331226E+03   # ~mu_R
   1000014     4.00359596E+03   # ~nu_muL
   1000015     4.00559279E+03   # ~tau_1
   2000015     4.00672121E+03   # ~tau_2
   1000016     4.00502544E+03   # ~nu_tauL
   1000021     1.98061187E+03   # ~g
   1000022     9.32476762E+01   # ~chi_10
   1000023    -1.37087545E+02   # ~chi_20
   1000025     1.52803763E+02   # ~chi_30
   1000035     2.05233184E+03   # ~chi_40
   1000024     1.31978272E+02   # ~chi_1+
   1000037     2.05249719E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61987952E-01   # N_11
  1  2     1.37900136E-02   # N_12
  1  3     5.33785757E-01   # N_13
  1  4     3.66410921E-01   # N_14
  2  1    -1.35819940E-01   # N_21
  2  2     2.72272586E-02   # N_22
  2  3    -6.85341102E-01   # N_23
  2  4     7.14926006E-01   # N_24
  3  1     6.33187567E-01   # N_31
  3  2     2.38563644E-02   # N_32
  3  3     4.95358528E-01   # N_33
  3  4     5.94242634E-01   # N_34
  4  1    -9.00400326E-04   # N_41
  4  2     9.99249411E-01   # N_42
  4  3    -5.18796366E-04   # N_43
  4  4    -3.87238214E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.37986906E-04   # U_11
  1  2     9.99999728E-01   # U_12
  2  1    -9.99999728E-01   # U_21
  2  2     7.37986906E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47770702E-02   # V_11
  1  2    -9.98498609E-01   # V_12
  2  1    -9.98498609E-01   # V_21
  2  2    -5.47770702E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94990368E-01   # cos(theta_t)
  1  2    -9.99708337E-02   # sin(theta_t)
  2  1     9.99708337E-02   # -sin(theta_t)
  2  2     9.94990368E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999921E-01   # cos(theta_b)
  1  2    -3.97492130E-04   # sin(theta_b)
  2  1     3.97492130E-04   # -sin(theta_b)
  2  2     9.99999921E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03363023E-01   # cos(theta_tau)
  1  2     7.10830822E-01   # sin(theta_tau)
  2  1    -7.10830822E-01   # -sin(theta_tau)
  2  2    -7.03363023E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00324622E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30899232E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43554103E+02   # higgs               
         4     1.60802121E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30899232E+03  # The gauge couplings
     1     3.62141713E-01   # gprime(Q) DRbar
     2     6.37340241E-01   # g(Q) DRbar
     3     1.03002578E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30899232E+03  # The trilinear couplings
  1  1     1.38822712E-06   # A_u(Q) DRbar
  2  2     1.38824048E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30899232E+03  # The trilinear couplings
  1  1     5.08617904E-07   # A_d(Q) DRbar
  2  2     5.08665385E-07   # A_s(Q) DRbar
  3  3     9.10523759E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30899232E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08467542E-07   # A_mu(Q) DRbar
  3  3     1.09579591E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30899232E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67184438E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30899232E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.79448448E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30899232E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04228660E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30899232E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58737986E+07   # M^2_Hd              
        22    -2.69316706E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40933039E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.49049710E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43803571E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43803571E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56196429E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56196429E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.26210733E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.16526266E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.46848018E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.35579571E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.01046145E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13707623E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.09266415E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.34199383E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.01846848E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.58443071E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.99741231E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.65921174E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.59272590E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.92735224E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.29797736E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.54118336E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.65541214E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.60159918E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.92018053E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66510838E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.80136247E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.70331974E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41668828E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.87461349E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.56363644E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.57228939E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15083290E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.65990062E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.85303808E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.14046356E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.18609528E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78892440E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.49051624E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.97300722E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80567857E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81631748E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.24480933E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62036779E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51812274E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61197136E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23385370E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02633433E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.22951162E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48907774E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44339988E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78912598E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18703265E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45032780E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.06174639E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82094883E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.52034835E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65200348E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52032142E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54385594E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.43929707E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67839451E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.81828957E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.49355702E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85474567E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78892440E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.49051624E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.97300722E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80567857E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81631748E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.24480933E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62036779E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51812274E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61197136E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23385370E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02633433E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.22951162E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48907774E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44339988E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78912598E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18703265E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45032780E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.06174639E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82094883E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.52034835E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65200348E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52032142E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54385594E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.43929707E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67839451E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.81828957E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.49355702E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85474567E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16245611E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.98079405E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27345892E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50876871E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77496946E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.56126113E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56333411E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08514619E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81065423E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84377596E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00496376E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41387602E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16245611E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.98079405E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27345892E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50876871E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77496946E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.56126113E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56333411E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08514619E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81065423E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84377596E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00496376E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41387602E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11949913E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27741897E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00138818E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60063723E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39181530E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.39649022E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79034067E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12722910E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.12129770E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33040860E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35547154E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42031578E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22377969E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84749615E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16351145E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01952086E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.56052453E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75365884E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77784873E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06201180E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54103916E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16351145E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01952086E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.56052453E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75365884E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77784873E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06201180E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54103916E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49759089E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22469232E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.03120399E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20595547E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51469300E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75818087E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01611209E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.40763955E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33718004E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33718004E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11240106E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11240106E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10083780E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.52249412E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.60599877E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.46827169E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.43537145E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.77673696E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.34727930E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.36365136E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.33425632E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.43420018E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.75985134E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18463234E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53631697E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18463234E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53631697E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37507247E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52089103E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52089103E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47762348E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03943263E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03943263E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03943236E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.90878044E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.90878044E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.90878044E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.90878044E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.69595877E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.69595877E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.69595877E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.69595877E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.43671212E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.43671212E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.41472124E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03005938E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.42773599E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.12546407E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.62908952E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.12546407E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.62908952E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.19770822E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.50502744E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.50502744E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.49025058E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.04441465E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.04441465E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.04441045E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.10640539E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.91995300E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.10640539E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.91995300E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.07112413E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.81558778E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.81558778E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.68682471E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.62852559E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.62852559E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.62852568E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.51392309E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.51392309E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.51392309E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.51392309E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.83792722E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.83792722E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.83792722E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.83792722E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.73757982E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.73757982E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.52136335E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.68427709E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.90070183E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.56463229E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.25236459E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.25236459E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.42639685E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.57561572E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.89541623E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.76393077E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76393077E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.77346110E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.77346110E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07715118E-03   # h decays
#          BR         NDA      ID1       ID2
     5.93525608E-01    2           5        -5   # BR(h -> b       bb     )
     6.39939328E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26537438E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80312077E-04    2           3        -3   # BR(h -> s       sb     )
     2.08558547E-02    2           4        -4   # BR(h -> c       cb     )
     6.83118558E-02    2          21        21   # BR(h -> g       g      )
     2.36102971E-03    2          22        22   # BR(h -> gam     gam    )
     1.62965359E-03    2          22        23   # BR(h -> Z       gam    )
     2.20662406E-01    2          24       -24   # BR(h -> W+      W-     )
     2.79528094E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43345659E+01   # H decays
#          BR         NDA      ID1       ID2
     3.36993297E-01    2           5        -5   # BR(H -> b       bb     )
     6.10218085E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15758335E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55204955E-04    2           3        -3   # BR(H -> s       sb     )
     7.15964122E-08    2           4        -4   # BR(H -> c       cb     )
     7.17545103E-03    2           6        -6   # BR(H -> t       tb     )
     1.13919768E-06    2          21        21   # BR(H -> g       g      )
     3.27885843E-10    2          22        22   # BR(H -> gam     gam    )
     1.83955818E-09    2          23        22   # BR(H -> Z       gam    )
     2.11535523E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05694473E-06    2          23        23   # BR(H -> Z       Z      )
     7.77493651E-06    2          25        25   # BR(H -> h       h      )
     7.44206392E-25    2          36        36   # BR(H -> A       A      )
     3.38827020E-21    2          23        36   # BR(H -> Z       A      )
     1.86064327E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67615078E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67615078E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35992835E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60314371E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68707331E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48453259E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.96993713E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.95834543E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.05022408E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.44797305E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.40300154E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.99863015E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.24036224E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.24036224E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.32176002E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43340716E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37022347E-01    2           5        -5   # BR(A -> b       bb     )
     6.10227940E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15761650E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55250460E-04    2           3        -3   # BR(A -> s       sb     )
     7.20434228E-08    2           4        -4   # BR(A -> c       cb     )
     7.18762426E-03    2           6        -6   # BR(A -> t       tb     )
     1.47687502E-05    2          21        21   # BR(A -> g       g      )
     4.06882517E-08    2          22        22   # BR(A -> gam     gam    )
     1.62297729E-08    2          23        22   # BR(A -> Z       gam    )
     2.11092428E-06    2          23        25   # BR(A -> Z       h      )
     1.86378476E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67618740E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67618740E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93489307E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22338190E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33440339E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97309108E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57775169E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.64005308E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.31128714E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.33195555E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.83352899E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.33461602E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.33461602E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42037269E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.36684670E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11858991E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16338349E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43477891E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22559573E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52144454E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42273894E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.11844030E-06    2          24        25   # BR(H+ -> W+      h      )
     3.40449876E-14    2          24        36   # BR(H+ -> W+      A      )
     4.85230032E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.29029883E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07824190E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.68242031E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.63190566E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58349777E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.26544030E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.15691893E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.94104223E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
