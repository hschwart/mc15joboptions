#--------------------------------------------------------------
# PowHeg+Pythia bbH with H->mumu. 
# Showered in Pythia from centrally produced evgen files
#--------------------------------------------------------------


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')

genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

evgenConfig.description    = "Powheg+Pythia8 for bbH, H->mumu"
evgenConfig.keywords       = [ "SM", "bbHiggs", "SMHiggs", "mH125" , "2muon" ]
evgenConfig.contact        = ["lserkin@cern.ch", "paul.daniel.thompson@cern.ch"]
evgenConfig.inputfilecheck = 'powheg_V2.345335.bbH125_4FS_incl_NNPDF30_13TeV'
