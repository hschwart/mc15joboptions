evgenConfig.description = "MadGraph+MadSpin+Pythia8 production of HppHp to WWWZ"
evgenConfig.keywords = ["Higgs"]
evgenConfig.minevents = 10000
evgenConfig.inputfilecheck  = "TXT"
evgenConfig.contact = ["Hanlin Xu<hanlin.xu@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py" )
evgenConfig.generators += ["MadGraph", "Pythia8"]

if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass


MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 10.0
MultiLeptonFilter.NLeptons = 2




















































