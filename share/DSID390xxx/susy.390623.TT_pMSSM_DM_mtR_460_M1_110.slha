#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90361325E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.05959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.25900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.80485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04196778E+01   # W+
        25     1.24565417E+02   # h
        35     3.99999917E+03   # H
        36     3.99999477E+03   # A
        37     4.00102613E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01280409E+03   # ~d_L
   2000001     4.01226719E+03   # ~d_R
   1000002     4.01214774E+03   # ~u_L
   2000002     4.01288138E+03   # ~u_R
   1000003     4.01280409E+03   # ~s_L
   2000003     4.01226719E+03   # ~s_R
   1000004     4.01214774E+03   # ~c_L
   2000004     4.01288138E+03   # ~c_R
   1000005     1.82854518E+03   # ~b_1
   2000005     4.01843098E+03   # ~b_2
   1000006     4.43347026E+02   # ~t_1
   2000006     1.84239266E+03   # ~t_2
   1000011     4.00168001E+03   # ~e_L
   2000011     4.00361939E+03   # ~e_R
   1000012     4.00056406E+03   # ~nu_eL
   1000013     4.00168001E+03   # ~mu_L
   2000013     4.00361939E+03   # ~mu_R
   1000014     4.00056406E+03   # ~nu_muL
   1000015     4.00419455E+03   # ~tau_1
   2000015     4.00889353E+03   # ~tau_2
   1000016     4.00316513E+03   # ~nu_tauL
   1000021     1.96370732E+03   # ~g
   1000022     9.17274610E+01   # ~chi_10
   1000023    -1.36067606E+02   # ~chi_20
   1000025     1.54180989E+02   # ~chi_30
   1000035     2.06419357E+03   # ~chi_40
   1000024     1.30675800E+02   # ~chi_1+
   1000037     2.06435763E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.56398083E-01   # N_11
  1  2     1.39931472E-02   # N_12
  1  3     5.38528534E-01   # N_13
  1  4     3.71016375E-01   # N_14
  2  1    -1.36506442E-01   # N_21
  2  2     2.72999470E-02   # N_22
  2  3    -6.85071783E-01   # N_23
  2  4     7.15050597E-01   # N_24
  3  1     6.39708618E-01   # N_31
  3  2     2.37817475E-02   # N_32
  3  3     4.90574581E-01   # N_33
  3  4     5.91222372E-01   # N_34
  4  1    -9.03061037E-04   # N_41
  4  2     9.99246383E-01   # N_42
  4  3    -5.00383563E-04   # N_43
  4  4    -3.88020732E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.11967600E-04   # U_11
  1  2     9.99999747E-01   # U_12
  2  1    -9.99999747E-01   # U_21
  2  2     7.11967600E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48878775E-02   # V_11
  1  2    -9.98492524E-01   # V_12
  2  1    -9.98492524E-01   # V_21
  2  2    -5.48878775E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.00896848E-01   # cos(theta_t)
  1  2     9.94896892E-01   # sin(theta_t)
  2  1    -9.94896892E-01   # -sin(theta_t)
  2  2    -1.00896848E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999883E-01   # cos(theta_b)
  1  2    -4.83735451E-04   # sin(theta_b)
  2  1     4.83735451E-04   # -sin(theta_b)
  2  2     9.99999883E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03055721E-01   # cos(theta_tau)
  1  2     7.11134764E-01   # sin(theta_tau)
  2  1    -7.11134764E-01   # -sin(theta_tau)
  2  2    -7.03055721E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00303856E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.03613245E+02  # DRbar Higgs Parameters
         1    -1.25900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44520705E+02   # higgs               
         4     1.61479376E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.03613245E+02  # The gauge couplings
     1     3.61039768E-01   # gprime(Q) DRbar
     2     6.36133233E-01   # g(Q) DRbar
     3     1.03766110E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.03613245E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.73221807E-07   # A_c(Q) DRbar
  3  3     2.05959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.03613245E+02  # The trilinear couplings
  1  1     2.10807070E-07   # A_d(Q) DRbar
  2  2     2.10827551E-07   # A_s(Q) DRbar
  3  3     3.75522939E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.03613245E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.62768997E-08   # A_mu(Q) DRbar
  3  3     4.67492132E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.03613245E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74470442E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.03613245E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83048856E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.03613245E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03807377E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.03613245E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58495978E+07   # M^2_Hd              
        22    -9.35879246E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.80485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40381077E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.72764926E+01   # gluino decays
#          BR         NDA      ID1       ID2
     9.76117760E-03    2     1000005        -5   # BR(~g -> ~b_1  bb)
     9.76117760E-03    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90238822E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     4.90238822E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.01350096E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.49624164E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.85140638E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.91636452E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.48260493E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.86205939E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.16969940E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.31567757E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.47650212E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.29809419E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.80348830E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.15755265E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.25493501E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.18910728E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.89987140E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.61348879E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     6.07144913E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.25830412E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.62634113E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.83236017E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.72315805E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.43007852E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.59785616E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60707382E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.01462039E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14014202E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.39922858E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.66321180E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.30749068E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.78410178E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74160570E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48646142E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.93822301E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.86686585E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.84742775E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36846141E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.68244382E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.50810917E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57128584E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23914828E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05383511E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.31301974E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.51494468E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43424460E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74178446E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.20089298E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.51433293E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.41931625E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.85218002E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.03328166E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.71457266E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51038125E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.50409745E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.45828169E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.75184418E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.03990962E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.56577012E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85226618E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74160570E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48646142E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.93822301E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.86686585E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.84742775E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36846141E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.68244382E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.50810917E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57128584E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23914828E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05383511E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.31301974E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.51494468E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43424460E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74178446E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.20089298E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.51433293E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.41931625E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.85218002E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.03328166E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.71457266E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51038125E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.50409745E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.45828169E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.75184418E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.03990962E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.56577012E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85226618E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12241035E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.88839058E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.29573139E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.69742012E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77167853E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.21882133E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55677787E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07260174E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.72600771E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.86255708E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.08773218E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.40393540E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12241035E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.88839058E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.29573139E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.69742012E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77167853E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.21882133E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55677787E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07260174E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.72600771E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.86255708E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.08773218E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.40393540E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09175497E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.25873158E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01600420E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.63525989E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38508463E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.42455685E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77686779E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10435104E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.09262591E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.36506682E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.38994248E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41668766E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.23992344E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84024492E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12351518E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01202771E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.64652494E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.91722251E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77449148E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09977059E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53429561E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12351518E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01202771E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.64652494E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.91722251E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77449148E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09977059E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53429561E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45809902E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.14705875E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.10353420E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.34820507E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51004307E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.82600308E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00679490E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.52290100E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33713960E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33713960E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11238759E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11238759E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10094562E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.95934186E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.99074807E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.82559878E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.57884493E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.13233883E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.23609587E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.10861528E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.52379234E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07212478E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.71615799E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.13935247E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18596703E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53777095E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18596703E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53777095E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37810677E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52267723E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52267723E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47997978E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04238843E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04238843E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04238794E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.53930054E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.53930054E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.53930054E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.53930054E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.17976978E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.17976978E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.17976978E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.17976978E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.01985194E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.01985194E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.54184442E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.13446690E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.49480534E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.76029173E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.86243647E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.76029173E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.86243647E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.59081147E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.10341910E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.10341910E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.09398250E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.23194980E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.23194980E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.23194414E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.28867403E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.45164424E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.28867403E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.45164424E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.81572436E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.16576358E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.16576358E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.04921001E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.32799186E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.32799186E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.32799207E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.19404429E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.19404429E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.19404429E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.19404429E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.06462945E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.06462945E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.06462945E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.06462945E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.97900664E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.97900664E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.95792150E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.85335605E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.11856015E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.28756591E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.08192693E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.08192693E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.44344010E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.01281917E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.11267781E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.97348947E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.97348947E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.29333771E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     2.29333771E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.90848700E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.90848700E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.93258225E-03   # h decays
#          BR         NDA      ID1       ID2
     6.12097256E-01    2           5        -5   # BR(h -> b       bb     )
     6.57481918E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32752515E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.94351174E-04    2           3        -3   # BR(h -> s       sb     )
     2.14673029E-02    2           4        -4   # BR(h -> c       cb     )
     6.84934671E-02    2          21        21   # BR(h -> g       g      )
     2.36117024E-03    2          22        22   # BR(h -> gam     gam    )
     1.52910926E-03    2          22        23   # BR(h -> Z       gam    )
     2.02376337E-01    2          24       -24   # BR(h -> W+      W-     )
     2.52000624E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.42994615E+01   # H decays
#          BR         NDA      ID1       ID2
     3.40563052E-01    2           5        -5   # BR(H -> b       bb     )
     6.10612412E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15897759E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55369888E-04    2           3        -3   # BR(H -> s       sb     )
     7.16367259E-08    2           4        -4   # BR(H -> c       cb     )
     7.17949126E-03    2           6        -6   # BR(H -> t       tb     )
     1.05506462E-06    2          21        21   # BR(H -> g       g      )
     3.67445263E-10    2          22        22   # BR(H -> gam     gam    )
     1.84421109E-09    2          23        22   # BR(H -> Z       gam    )
     1.99969019E-06    2          24       -24   # BR(H -> W+      W-     )
     9.99152537E-07    2          23        23   # BR(H -> Z       Z      )
     7.26878786E-06    2          25        25   # BR(H -> h       h      )
    -5.94962191E-25    2          36        36   # BR(H -> A       A      )
    -1.85063993E-20    2          23        36   # BR(H -> Z       A      )
     1.86973145E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66359941E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66359941E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.37696312E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.62524512E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.69331923E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.44895170E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.04771648E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.01181777E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.08220189E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.38671237E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.28304758E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.50662849E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.51552989E-06    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.20713170E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.20713170E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     5.96678456E-08    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.42984588E+01   # A decays
#          BR         NDA      ID1       ID2
     3.40595436E-01    2           5        -5   # BR(A -> b       bb     )
     6.10627896E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15903064E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55417771E-04    2           3        -3   # BR(A -> s       sb     )
     7.20906457E-08    2           4        -4   # BR(A -> c       cb     )
     7.19233559E-03    2           6        -6   # BR(A -> t       tb     )
     1.47784393E-05    2          21        21   # BR(A -> g       g      )
     4.06619762E-08    2          22        22   # BR(A -> gam     gam    )
     1.62524914E-08    2          23        22   # BR(A -> Z       gam    )
     1.99562617E-06    2          23        25   # BR(A -> Z       h      )
     1.87293241E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66365069E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66365069E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.94811941E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.25092311E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33801455E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.93030485E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.33805279E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.68542697E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.34964378E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26245926E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.73218669E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.59304978E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.59304978E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41275424E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.41688419E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12713994E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16640657E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.46680288E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22730992E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52497117E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.45408888E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.00417746E-06    2          24        25   # BR(H+ -> W+      h      )
     2.83704489E-14    2          24        36   # BR(H+ -> W+      A      )
     4.77892389E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.38532234E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.16040307E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67106776E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.73830783E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57154082E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.04932635E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.19373400E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.76726546E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
