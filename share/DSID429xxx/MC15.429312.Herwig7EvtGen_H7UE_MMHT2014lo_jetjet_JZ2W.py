# based on MC15.426042....
## Job options file for Herwig 7, QCD jet slice production
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_EvtGen_Common.py")

evgenConfig.description = "QCD dijet production JZ2W with MMHT2014lo68cl PDF and H7UE tune."
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "Haifeng.Li@cern.ch", "orel.gueta@cern.ch" ]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 15*GeV
"""

genSeq.Herwig7.Commands += cmds.splitlines()

include("MC15JobOptions/JetFilter_JZ2W.py")
evgenConfig.minevents = 500
