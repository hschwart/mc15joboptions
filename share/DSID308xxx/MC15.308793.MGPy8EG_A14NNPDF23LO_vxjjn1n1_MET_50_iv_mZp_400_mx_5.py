model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 430.
mZp = 400.
mHD = 125.
widthZp = 1.814048e+00
widthN2 = 4.385193e-02
filteff = 7.153076E-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
