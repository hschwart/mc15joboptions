#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13087951E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.34900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04174355E+01   # W+
        25     1.25663485E+02   # h
        35     4.00001188E+03   # H
        36     3.99999735E+03   # A
        37     4.00102478E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02610768E+03   # ~d_L
   2000001     4.02267766E+03   # ~d_R
   1000002     4.02545456E+03   # ~u_L
   2000002     4.02361986E+03   # ~u_R
   1000003     4.02610768E+03   # ~s_L
   2000003     4.02267766E+03   # ~s_R
   1000004     4.02545456E+03   # ~c_L
   2000004     4.02361986E+03   # ~c_R
   1000005     8.80897054E+02   # ~b_1
   2000005     4.02552183E+03   # ~b_2
   1000006     8.66876690E+02   # ~t_1
   2000006     2.14860710E+03   # ~t_2
   1000011     4.00468747E+03   # ~e_L
   2000011     4.00320979E+03   # ~e_R
   1000012     4.00357329E+03   # ~nu_eL
   1000013     4.00468747E+03   # ~mu_L
   2000013     4.00320979E+03   # ~mu_R
   1000014     4.00357329E+03   # ~nu_muL
   1000015     4.00414451E+03   # ~tau_1
   2000015     4.00797777E+03   # ~tau_2
   1000016     4.00498199E+03   # ~nu_tauL
   1000021     1.98060029E+03   # ~g
   1000022     3.97387424E+02   # ~chi_10
   1000023    -4.46471009E+02   # ~chi_20
   1000025     4.61583077E+02   # ~chi_30
   1000035     2.05220285E+03   # ~chi_40
   1000024     4.44226458E+02   # ~chi_1+
   1000037     2.05236748E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.20529555E-01   # N_11
  1  2    -1.72227298E-02   # N_12
  1  3    -4.27123651E-01   # N_13
  1  4    -3.79473337E-01   # N_14
  2  1    -3.86372118E-02   # N_21
  2  2     2.36070213E-02   # N_22
  2  3    -7.04457775E-01   # N_23
  2  4     7.08300161E-01   # N_24
  3  1     5.70295719E-01   # N_31
  3  2     2.83048721E-02   # N_32
  3  3     5.66796905E-01   # N_33
  3  4     5.93887949E-01   # N_34
  4  1    -1.09918908E-03   # N_41
  4  2     9.99172117E-01   # N_42
  4  3    -6.77480811E-03   # N_43
  4  4    -4.00995439E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.58700232E-03   # U_11
  1  2     9.99954044E-01   # U_12
  2  1    -9.99954044E-01   # U_21
  2  2     9.58700232E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.67191482E-02   # V_11
  1  2    -9.98390173E-01   # V_12
  2  1    -9.98390173E-01   # V_21
  2  2    -5.67191482E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94935137E-01   # cos(theta_t)
  1  2    -1.00519019E-01   # sin(theta_t)
  2  1     1.00519019E-01   # -sin(theta_t)
  2  2     9.94935137E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999029E-01   # cos(theta_b)
  1  2    -1.39355626E-03   # sin(theta_b)
  2  1     1.39355626E-03   # -sin(theta_b)
  2  2     9.99999029E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06116130E-01   # cos(theta_tau)
  1  2     7.08096046E-01   # sin(theta_tau)
  2  1    -7.08096046E-01   # -sin(theta_tau)
  2  2    -7.06116130E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00254081E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30879510E+03  # DRbar Higgs Parameters
         1    -4.34900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43692918E+02   # higgs               
         4     1.61684298E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30879510E+03  # The gauge couplings
     1     3.61898485E-01   # gprime(Q) DRbar
     2     6.35952500E-01   # g(Q) DRbar
     3     1.03002559E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30879510E+03  # The trilinear couplings
  1  1     1.38605146E-06   # A_u(Q) DRbar
  2  2     1.38606502E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30879510E+03  # The trilinear couplings
  1  1     5.11280734E-07   # A_d(Q) DRbar
  2  2     5.11328173E-07   # A_s(Q) DRbar
  3  3     9.13865990E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30879510E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.12562685E-07   # A_mu(Q) DRbar
  3  3     1.13707841E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30879510E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66563400E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30879510E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85695453E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30879510E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03077013E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30879510E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57031328E+07   # M^2_Hd              
        22    -1.76601789E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40311119E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.47031557E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43748582E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43748582E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56251418E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56251418E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.79583401E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.56052378E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.21351717E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.11088062E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11507844E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.08858480E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.44665945E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.25767620E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.63088511E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.49057105E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.18614050E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.74759495E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.93120078E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.00328007E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.18920734E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.22244106E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.33473724E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.42630759E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.80165141E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66870226E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55432227E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81000088E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.59704228E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.58530978E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63264025E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.14639106E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13098998E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.05743574E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.79491046E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.69494562E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.48416717E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78760888E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.64132285E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.25844028E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.58504804E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79814221E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.48531843E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.58419806E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52379110E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61006158E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.68462539E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.12903879E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.76808472E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.70914993E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45391571E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78781397E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.55996826E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.15759610E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.42894307E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80346978E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.56723231E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61689175E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52596274E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54330153E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.60765984E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.11964167E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.61026598E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.66855352E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85760868E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78760888E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.64132285E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.25844028E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.58504804E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79814221E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.48531843E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.58419806E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52379110E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61006158E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.68462539E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.12903879E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.76808472E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.70914993E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45391571E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78781397E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.55996826E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.15759610E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.42894307E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80346978E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.56723231E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61689175E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52596274E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54330153E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.60765984E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.11964167E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.61026598E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.66855352E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85760868E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14019532E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01726847E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31307034E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.21872611E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78162320E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.20032343E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57830255E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04059014E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.74797072E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.48842189E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.23713835E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.71278537E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14019532E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01726847E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31307034E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.21872611E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78162320E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.20032343E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57830255E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04059014E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.74797072E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.48842189E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.23713835E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.71278537E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07710938E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43193130E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47583711E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34034673E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40620545E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53897901E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.82003490E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06957243E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47255982E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97835775E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.11651422E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43840387E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90141941E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88454438E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14125042E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17833375E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03954331E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.38057930E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78578034E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21828175E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55524973E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14125042E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17833375E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03954331E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.38057930E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78578034E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21828175E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55524973E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46422139E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06886806E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.42974584E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.97364881E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52821659E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54465209E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04165552E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.37250353E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33591649E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33591649E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11198720E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11198720E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10419263E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.43131514E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.64033619E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.49669504E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.27320979E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.84567051E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.09147896E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.23319248E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.18613590E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.34478323E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.89217272E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18133368E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53245238E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18133368E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53245238E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40663522E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51452139E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51452139E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48083314E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02633384E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02633384E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02633370E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.38264808E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.38264808E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.38264808E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.38264808E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.94216666E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.94216666E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.94216666E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.94216666E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.65984048E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.65984048E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.06020584E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.19313574E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.68122737E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.21246291E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.13783675E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.21246291E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.13783675E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.94999933E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.25816587E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.25816587E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.27120566E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.90839933E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.90839933E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.90839272E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.18571490E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.53817691E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.18571490E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.53817691E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.41659202E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.52787482E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.52787482E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.25954300E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.05220566E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.05220566E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.05220573E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.06100953E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.06100953E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.06100953E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.06100953E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.68697369E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.68697369E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.68697369E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.68697369E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.55215238E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.55215238E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.42980996E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.91945184E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.83699318E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.74565790E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.08925805E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.08925805E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.18534039E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.40719409E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.57405872E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.77956188E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.77956188E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.78945284E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.78945284E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02832596E-03   # h decays
#          BR         NDA      ID1       ID2
     5.89366301E-01    2           5        -5   # BR(h -> b       bb     )
     6.47398252E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29177981E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85929176E-04    2           3        -3   # BR(h -> s       sb     )
     2.11056425E-02    2           4        -4   # BR(h -> c       cb     )
     6.90870299E-02    2          21        21   # BR(h -> g       g      )
     2.39126860E-03    2          22        22   # BR(h -> gam     gam    )
     1.64629466E-03    2          22        23   # BR(h -> Z       gam    )
     2.22734461E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82140703E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52212987E+01   # H decays
#          BR         NDA      ID1       ID2
     3.62032197E-01    2           5        -5   # BR(H -> b       bb     )
     6.00421355E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12294448E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51107691E-04    2           3        -3   # BR(H -> s       sb     )
     7.04270490E-08    2           4        -4   # BR(H -> c       cb     )
     7.05825671E-03    2           6        -6   # BR(H -> t       tb     )
     8.47830426E-07    2          21        21   # BR(H -> g       g      )
     1.90093180E-09    2          22        22   # BR(H -> gam     gam    )
     1.81591971E-09    2          23        22   # BR(H -> Z       gam    )
     1.70381725E-06    2          24       -24   # BR(H -> W+      W-     )
     8.51318428E-07    2          23        23   # BR(H -> Z       Z      )
     6.84552286E-06    2          25        25   # BR(H -> h       h      )
    -3.74721728E-25    2          36        36   # BR(H -> A       A      )
     1.80358436E-20    2          23        36   # BR(H -> Z       A      )
     1.85988630E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.56316384E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.56316384E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.38809920E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.98102447E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.53786318E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.43821704E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.30442650E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.69515140E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.33504169E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.24039120E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.74889312E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.64631978E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.29858284E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.29858284E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.37659983E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52155534E+01   # A decays
#          BR         NDA      ID1       ID2
     3.62095310E-01    2           5        -5   # BR(A -> b       bb     )
     6.00486149E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12317191E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51175588E-04    2           3        -3   # BR(A -> s       sb     )
     7.08933067E-08    2           4        -4   # BR(A -> c       cb     )
     7.07287954E-03    2           6        -6   # BR(A -> t       tb     )
     1.45329752E-05    2          21        21   # BR(A -> g       g      )
     6.49150647E-08    2          22        22   # BR(A -> gam     gam    )
     1.59763096E-08    2          23        22   # BR(A -> Z       gam    )
     1.70039907E-06    2          23        25   # BR(A -> Z       h      )
     1.89163055E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.56314587E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.56314587E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.08124230E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.41259269E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.31699299E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.91881917E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.21471321E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.01415416E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.47787059E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.32315407E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.33992506E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.60247908E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.60247908E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54192396E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.82923131E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.98432814E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11591182E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73070488E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19870374E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46611907E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.70996543E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.69604855E-06    2          24        25   # BR(H+ -> W+      h      )
     2.71851488E-14    2          24        36   # BR(H+ -> W+      A      )
     5.33404704E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.70424806E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.07186584E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55961176E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.69976622E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.55086346E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00745192E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.42357804E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.48627295E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
