model="InelasticVectorEFT"
mDM1 = 80.
mDM2 = 320.
mZp = 160.
mHD = 125.
widthZp = 6.366192e-01
widthN2 = 1.504453e-02
filteff = 8.968610e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
