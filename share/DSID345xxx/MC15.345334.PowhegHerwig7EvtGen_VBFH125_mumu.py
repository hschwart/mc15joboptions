#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
#include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
#PowhegConfig.mass_H  = 125.
#PowhegConfig.width_H = 0.00407

#PowhegConfig.complexpolescheme = 1 # use CPS

# Increase number of events requested to compensate for potential Pythia losses
#PowhegConfig.nEvents *= 2

#PowhegConfig.PDF = range(260000,260101) + range(90400,90433) + [11068] + [25200] + [13165]
#PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0]
#PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0]

#PowhegConfig.generate()

#--------------------------------------------------------------
# remove additional weights so as not to cause issues for Herwig
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_701_StripWeights.py")

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------
# implements NNPDF30_nlo_as_0118 260000 as PDF
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py')
from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()

#--------------------------------------------------------------
# Higgs to mumu decay
#--------------------------------------------------------------
genSeq.Herwig7.Commands += [
  'do /Herwig/Particles/h0:SelectDecayModes h0->mu-,mu+;',
  'do /Herwig/Particles/h0:PrintDecayModes' # print out decays modes and branching ratios to the terminal/log.generate
]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+HERWIG7+EVTGEN, VBF H->mumu"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2muon", "mH125" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]
evgenConfig.generators = [ 'Powheg','Herwig7','EvtGen' ]
evgenConfig.inputfilecheck = "TXT"

