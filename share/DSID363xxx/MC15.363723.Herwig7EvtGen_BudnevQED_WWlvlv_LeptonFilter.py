evgenConfig.description = "gammagamma->WW with Budnev parameterization, leptonic decays, central lepton filter pt>15 GeV, "
evgenConfig.keywords = ["2lepton", "diboson", "SM", "WW", "electroweak", "exclusive"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]


include("MC15JobOptions/Herwig7_701_QED_EvtGen_Common.py")
from Herwig7_i import config as hw

cmds = """\

# Cuts
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*MeV
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0

#Set on-mass-shell to compare e.g. with FPMC
#set /Herwig/MatrixElements/MEgg2WW:MassOption OnMassShell

do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;


# Selected the hard process
cd /Herwig/MatrixElements

insert SimpleQCD:MatrixElements 0 MEgg2WW

"""

genSeq.Herwig7.Commands += cmds.splitlines()


include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 15000.
filtSeq.LeptonFilter.Etacut = 2.7

evgenConfig.minevents = 5000

# To avoid warning from displaced vertices, bugfix needed in herwig++
testSeq.TestHepMC.MaxTransVtxDisp = 1000000
testSeq.TestHepMC.MaxVtxDisp      = 1000000000

