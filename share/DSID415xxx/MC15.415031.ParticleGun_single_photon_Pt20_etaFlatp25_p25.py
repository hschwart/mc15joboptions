evgenConfig.description = "Single photon with flat eta-phi and fixed pT = 20 GeV"
evgenConfig.keywords = ["singleParticle", "photon"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 22
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=20000.0, eta=[-2.5,2.5])
