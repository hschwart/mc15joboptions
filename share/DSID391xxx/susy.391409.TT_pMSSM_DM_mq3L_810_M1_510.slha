#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13088116E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.23990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178916E+01   # W+
        25     1.25661827E+02   # h
        35     4.00001373E+03   # H
        36     3.99999762E+03   # A
        37     4.00101351E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02610343E+03   # ~d_L
   2000001     4.02267047E+03   # ~d_R
   1000002     4.02545056E+03   # ~u_L
   2000002     4.02360518E+03   # ~u_R
   1000003     4.02610343E+03   # ~s_L
   2000003     4.02267047E+03   # ~s_R
   1000004     4.02545056E+03   # ~c_L
   2000004     4.02360518E+03   # ~c_R
   1000005     8.82818228E+02   # ~b_1
   2000005     4.02553737E+03   # ~b_2
   1000006     8.68789466E+02   # ~t_1
   2000006     2.14883802E+03   # ~t_2
   1000011     4.00467777E+03   # ~e_L
   2000011     4.00315734E+03   # ~e_R
   1000012     4.00356414E+03   # ~nu_eL
   1000013     4.00467777E+03   # ~mu_L
   2000013     4.00315734E+03   # ~mu_R
   1000014     4.00356414E+03   # ~nu_muL
   1000015     4.00371301E+03   # ~tau_1
   2000015     4.00832550E+03   # ~tau_2
   1000016     4.00496565E+03   # ~nu_tauL
   1000021     1.98060006E+03   # ~g
   1000022     4.94614434E+02   # ~chi_10
   1000023    -5.35889988E+02   # ~chi_20
   1000025     5.54941061E+02   # ~chi_30
   1000035     2.05218272E+03   # ~chi_40
   1000024     5.33788096E+02   # ~chi_1+
   1000037     2.05234720E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.69560171E-01   # N_11
  1  2    -2.09112539E-02   # N_12
  1  3    -4.69369117E-01   # N_13
  1  4    -4.32472536E-01   # N_14
  2  1    -3.15870844E-02   # N_21
  2  2     2.27644304E-02   # N_22
  2  3    -7.05143426E-01   # N_23
  2  4     7.07994905E-01   # N_24
  3  1     6.37791482E-01   # N_31
  3  2     2.82292573E-02   # N_32
  3  3     5.31402332E-01   # N_33
  3  4     5.56809390E-01   # N_34
  4  1    -1.19389627E-03   # N_41
  4  2     9.99123420E-01   # N_42
  4  3    -8.77169059E-03   # N_43
  4  4    -4.09148244E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.24112144E-02   # U_11
  1  2     9.99922978E-01   # U_12
  2  1    -9.99922978E-01   # U_21
  2  2     1.24112144E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.78704568E-02   # V_11
  1  2    -9.98324101E-01   # V_12
  2  1    -9.98324101E-01   # V_21
  2  2    -5.78704568E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94918871E-01   # cos(theta_t)
  1  2    -1.00679889E-01   # sin(theta_t)
  2  1     1.00679889E-01   # -sin(theta_t)
  2  2     9.94918871E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998571E-01   # cos(theta_b)
  1  2    -1.69056143E-03   # sin(theta_b)
  2  1     1.69056143E-03   # -sin(theta_b)
  2  2     9.99998571E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06296212E-01   # cos(theta_tau)
  1  2     7.07916422E-01   # sin(theta_tau)
  2  1    -7.07916422E-01   # -sin(theta_tau)
  2  2    -7.06296212E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00235614E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30881157E+03  # DRbar Higgs Parameters
         1    -5.23990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43724594E+02   # higgs               
         4     1.62008691E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30881157E+03  # The gauge couplings
     1     3.61860481E-01   # gprime(Q) DRbar
     2     6.35746520E-01   # g(Q) DRbar
     3     1.03002319E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30881157E+03  # The trilinear couplings
  1  1     1.38609106E-06   # A_u(Q) DRbar
  2  2     1.38610432E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30881157E+03  # The trilinear couplings
  1  1     5.12813643E-07   # A_d(Q) DRbar
  2  2     5.12860925E-07   # A_s(Q) DRbar
  3  3     9.15436615E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30881157E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13747345E-07   # A_mu(Q) DRbar
  3  3     1.14901563E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30881157E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66403147E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30881157E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87539827E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30881157E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02788820E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30881157E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56190146E+07   # M^2_Hd              
        22    -2.62268025E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09989996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40218251E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.45878773E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43732335E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43732335E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56267665E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56267665E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.56581027E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.18891019E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.07736602E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.53446907E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.19925472E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.06414902E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.05546418E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23472644E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.73244898E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.44703658E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.67435737E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.79117004E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.08486364E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.03978658E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.94349956E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.96886523E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.69956155E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.16374680E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.71678264E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66938574E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.57769706E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81515801E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55717316E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.60509983E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63696028E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.17717492E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12702583E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.02600335E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.16164521E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.98687605E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.29706283E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78733675E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.32584042E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.37066840E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.87230524E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79517897E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.64670636E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57829157E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52478772E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60897282E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.20859588E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.37753621E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.18680710E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.37791709E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45992151E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78754316E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.36067800E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.97041371E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.23679836E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80084532E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.59738526E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61163134E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52695198E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54298858E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.36242641E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.40152086E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.69936111E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14064603E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85924186E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78733675E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.32584042E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.37066840E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.87230524E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79517897E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.64670636E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57829157E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52478772E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60897282E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.20859588E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.37753621E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.18680710E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.37791709E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45992151E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78754316E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.36067800E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.97041371E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.23679836E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80084532E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.59738526E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61163134E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52695198E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54298858E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.36242641E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.40152086E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.69936111E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14064603E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85924186E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13245628E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.67180441E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.13524438E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.56878724E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78613858E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.52770219E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58816103E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01587306E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.94161997E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.95621937E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.04841580E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.01465436E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13245628E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.67180441E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.13524438E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.56878724E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78613858E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.52770219E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58816103E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01587306E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.94161997E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.95621937E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.04841580E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.01465436E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05569108E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.13325192E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51522297E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60992231E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41267570E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.59208785E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83341899E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04518540E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.14391296E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86059654E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.43058146E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45088379E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.78605397E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.90995674E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13350460E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04913518E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.22427207E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.53850086E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79091126E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.31931997E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56468600E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13350460E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04913518E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.22427207E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.53850086E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79091126E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.31931997E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56468600E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45202069E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.52686082E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.46822697E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.02936419E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53554643E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.45786883E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05557596E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.63645269E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33706292E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33706292E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11236834E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11236834E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10113748E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.40499529E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.64991568E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.50346441E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.23325847E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.42427379E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.04856328E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.62096978E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.13913378E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.84362020E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.16092102E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18658298E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53932170E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18658298E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53932170E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36664598E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53063056E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53063056E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48695458E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.05848172E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.05848172E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.05848161E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.31212625E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.31212625E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.31212625E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.31212625E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.10404285E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.10404285E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.10404285E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.10404285E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.68561548E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.68561548E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.48489604E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04800176E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.90913059E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.57721764E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.56982847E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.57721764E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.56982847E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.38919286E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.93695642E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.93695642E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.00285567E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.12296418E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.12296418E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.12294800E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.99182119E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.58399062E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.99182119E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.58399062E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.42616189E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.92698983E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.92698983E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.64120539E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.18482014E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.18482014E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.18482015E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.14978690E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.14978690E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.14978690E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.14978690E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.83257418E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.83257418E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.83257418E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.83257418E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.70153699E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.70153699E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.40338366E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.91189808E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.10016128E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.34351828E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.04970855E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.04970855E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.67146132E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.10564824E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.34352312E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78357280E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78357280E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79368124E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79368124E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01532962E-03   # h decays
#          BR         NDA      ID1       ID2
     5.88110293E-01    2           5        -5   # BR(h -> b       bb     )
     6.49437173E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29899764E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87461300E-04    2           3        -3   # BR(h -> s       sb     )
     2.11737337E-02    2           4        -4   # BR(h -> c       cb     )
     6.93042059E-02    2          21        21   # BR(h -> g       g      )
     2.39911451E-03    2          22        22   # BR(h -> gam     gam    )
     1.65140864E-03    2          22        23   # BR(h -> Z       gam    )
     2.23400562E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82996041E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.55002860E+01   # H decays
#          BR         NDA      ID1       ID2
     3.68458580E-01    2           5        -5   # BR(H -> b       bb     )
     5.97403560E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11227429E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.49845577E-04    2           3        -3   # BR(H -> s       sb     )
     7.00678873E-08    2           4        -4   # BR(H -> c       cb     )
     7.02226127E-03    2           6        -6   # BR(H -> t       tb     )
     8.55842633E-07    2          21        21   # BR(H -> g       g      )
     3.82964761E-09    2          22        22   # BR(H -> gam     gam    )
     1.80830521E-09    2          23        22   # BR(H -> Z       gam    )
     1.60310797E-06    2          24       -24   # BR(H -> W+      W-     )
     8.00998698E-07    2          23        23   # BR(H -> Z       Z      )
     6.59194877E-06    2          25        25   # BR(H -> h       h      )
     3.67830204E-25    2          36        36   # BR(H -> A       A      )
     1.59656573E-20    2          23        36   # BR(H -> Z       A      )
     1.86854248E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.51753349E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.51753349E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.50135975E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.84804358E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.65535962E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.14963953E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.13380717E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.04670392E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.57462994E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.27520393E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.90595085E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.01826511E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.21535344E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.21535344E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.40837430E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.54970210E+01   # A decays
#          BR         NDA      ID1       ID2
     3.68505594E-01    2           5        -5   # BR(A -> b       bb     )
     5.97440668E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11240383E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.49901700E-04    2           3        -3   # BR(A -> s       sb     )
     7.05337570E-08    2           4        -4   # BR(A -> c       cb     )
     7.03700800E-03    2           6        -6   # BR(A -> t       tb     )
     1.44592671E-05    2          21        21   # BR(A -> g       g      )
     6.95831410E-08    2          22        22   # BR(A -> gam     gam    )
     1.58965439E-08    2          23        22   # BR(A -> Z       gam    )
     1.59981777E-06    2          23        25   # BR(A -> Z       h      )
     1.92757384E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.51724074E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.51724074E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.21550945E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.06950577E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.45398693E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.51691993E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.77583125E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.63683234E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.71952259E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.86634858E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.71647615E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.26607555E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.26607555E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.58001910E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.94866793E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.94345601E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.10146044E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.80714427E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19051718E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44927669E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.78408597E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.59289549E-06    2          24        25   # BR(H+ -> W+      h      )
     2.55168867E-14    2          24        36   # BR(H+ -> W+      A      )
     4.50506788E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.49219867E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.63671433E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.51134903E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.66321440E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.50568679E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.59869082E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.21252141E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.47329672E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
