model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 110.
mZp = 80.
mHD = 125.
widthZp = 3.183053e-01
widthN2 = 6.902365e-04
filteff = 3.080715e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
