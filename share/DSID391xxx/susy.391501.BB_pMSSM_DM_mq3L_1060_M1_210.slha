#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13999799E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.59990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.05990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.86485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04163740E+01   # W+
        25     1.25660408E+02   # h
        35     4.00000676E+03   # H
        36     3.99999694E+03   # A
        37     4.00107716E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02853752E+03   # ~d_L
   2000001     4.02463863E+03   # ~d_R
   1000002     4.02788560E+03   # ~u_L
   2000002     4.02533929E+03   # ~u_R
   1000003     4.02853752E+03   # ~s_L
   2000003     4.02463863E+03   # ~s_R
   1000004     4.02788560E+03   # ~c_L
   2000004     4.02533929E+03   # ~c_R
   1000005     1.11981486E+03   # ~b_1
   2000005     4.02681233E+03   # ~b_2
   1000006     1.09701968E+03   # ~t_1
   2000006     1.88121374E+03   # ~t_2
   1000011     4.00509598E+03   # ~e_L
   2000011     4.00354536E+03   # ~e_R
   1000012     4.00398174E+03   # ~nu_eL
   1000013     4.00509598E+03   # ~mu_L
   2000013     4.00354536E+03   # ~mu_R
   1000014     4.00398174E+03   # ~nu_muL
   1000015     4.00496797E+03   # ~tau_1
   2000015     4.00729405E+03   # ~tau_2
   1000016     4.00518798E+03   # ~nu_tauL
   1000021     1.98339786E+03   # ~g
   1000022     2.01137387E+02   # ~chi_10
   1000023    -2.70773997E+02   # ~chi_20
   1000025     2.79494426E+02   # ~chi_30
   1000035     2.05157115E+03   # ~chi_40
   1000024     2.67664748E+02   # ~chi_1+
   1000037     2.05173533E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.97146480E-01   # N_11
  1  2    -1.05137525E-02   # N_12
  1  3    -3.55048068E-01   # N_13
  1  4    -2.62599550E-01   # N_14
  2  1    -6.92333014E-02   # N_21
  2  2     2.54571587E-02   # N_22
  2  3    -7.00613789E-01   # N_23
  2  4     7.09717551E-01   # N_24
  3  1     4.36272887E-01   # N_31
  3  2     2.78469518E-02   # N_32
  3  3     6.18927589E-01   # N_33
  3  4     6.52548201E-01   # N_34
  4  1    -9.54743441E-04   # N_41
  4  2     9.99232676E-01   # N_42
  4  3    -3.13490329E-03   # N_43
  4  4    -3.90297258E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.43857858E-03   # U_11
  1  2     9.99990149E-01   # U_12
  2  1    -9.99990149E-01   # U_21
  2  2     4.43857858E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52087162E-02   # V_11
  1  2    -9.98474836E-01   # V_12
  2  1    -9.98474836E-01   # V_21
  2  2    -5.52087162E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.86861679E-01   # cos(theta_t)
  1  2    -1.61567406E-01   # sin(theta_t)
  2  1     1.61567406E-01   # -sin(theta_t)
  2  2     9.86861679E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999641E-01   # cos(theta_b)
  1  2    -8.47348730E-04   # sin(theta_b)
  2  1     8.47348730E-04   # -sin(theta_b)
  2  2     9.99999641E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05463228E-01   # cos(theta_tau)
  1  2     7.08746523E-01   # sin(theta_tau)
  2  1    -7.08746523E-01   # -sin(theta_tau)
  2  2    -7.05463228E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00294565E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39997994E+03  # DRbar Higgs Parameters
         1    -2.59990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43442536E+02   # higgs               
         4     1.60567342E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39997994E+03  # The gauge couplings
     1     3.62242802E-01   # gprime(Q) DRbar
     2     6.36502505E-01   # g(Q) DRbar
     3     1.02835880E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39997994E+03  # The trilinear couplings
  1  1     1.59841223E-06   # A_u(Q) DRbar
  2  2     1.59842746E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39997994E+03  # The trilinear couplings
  1  1     5.87240579E-07   # A_d(Q) DRbar
  2  2     5.87295115E-07   # A_s(Q) DRbar
  3  3     1.05040832E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39997994E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.27264350E-07   # A_mu(Q) DRbar
  3  3     1.28578207E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39997994E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65707012E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39997994E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81924673E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39997994E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03780771E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39997994E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58274465E+07   # M^2_Hd              
        22    -1.31356232E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.05990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.86485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40558568E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.03212337E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37635609E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37635609E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62364391E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62364391E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.52749989E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     5.61447635E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.31558637E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.04956848E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07339752E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.08419143E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.07622378E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.22784687E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.02888572E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.38130041E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.51708222E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.14994981E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.18731260E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.56578921E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.18607581E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.79029179E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.74998854E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.92736439E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65602035E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54902029E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79830363E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.65465211E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.91167880E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64018485E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.68124046E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13263759E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.95382628E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.06289135E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.51031421E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.27465997E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77825159E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.23176062E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32419302E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.06621397E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.84211962E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.31655034E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.67198479E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51016084E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60062796E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.50375105E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.67192780E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.06039230E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.82457691E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44091345E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77844990E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.85840167E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29866511E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.62832809E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84697271E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.43658283E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.70404951E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51235240E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53290686E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.17546923E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.97366426E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.76759666E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.37032189E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85407967E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77825159E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.23176062E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32419302E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.06621397E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.84211962E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.31655034E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.67198479E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51016084E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60062796E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.50375105E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.67192780E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.06039230E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.82457691E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44091345E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77844990E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.85840167E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29866511E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.62832809E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84697271E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.43658283E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.70404951E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51235240E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53290686E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.17546923E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.97366426E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.76759666E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.37032189E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85407967E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15573551E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27254384E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.85463261E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.86206028E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77540965E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.99729581E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56465529E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07783672E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.05609533E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.77794108E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.89612027E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.98632427E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15573551E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27254384E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.85463261E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.86206028E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77540965E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.99729581E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56465529E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07783672E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.05609533E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.77794108E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.89612027E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.98632427E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11084890E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.91208473E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28053979E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.17433080E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39708343E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44209221E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80113556E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10996972E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.03110748E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40070874E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.54953444E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42055962E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10534177E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84819340E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15678664E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38096532E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13061062E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.45976918E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77865256E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08817726E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54221732E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15678664E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38096532E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13061062E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.45976918E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77865256E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08817726E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54221732E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48680364E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25064097E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92954560E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.22764238E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51749726E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68479121E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02132296E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.90962329E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33449231E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33449231E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11150603E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11150603E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10800333E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.52997193E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.34725273E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.31615092E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.15438162E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.88198652E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.02038523E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.67820611E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.59642375E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.79349344E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.24961686E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.26145054E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17466897E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52379491E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17466897E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52379491E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45642949E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49455105E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49455105E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47324768E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98709140E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98709140E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98709120E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.46480377E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.46480377E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.46480377E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.46480377E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.15493609E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.15493609E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.15493609E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.15493609E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.20992215E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.20992215E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.34847415E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.85585268E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.73772020E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.15035436E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.49011216E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.15035436E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.49011216E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.44825517E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.39999711E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.39999711E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.38862800E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.84023001E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.84023001E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.84022600E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.05095942E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.36338599E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.05095942E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.36338599E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.12712549E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.12712549E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.39689758E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.25107981E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.25107981E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.25107986E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.62503810E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.62503810E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.62503810E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.62503810E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.41674927E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.41674927E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.41674927E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.41674927E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.85378918E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.85378918E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.52856484E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.96599477E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.17834243E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.20668656E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.65741891E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.65741891E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.44241109E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.67943269E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.54760010E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.62558283E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.62558283E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.62603016E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.62603016E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05581031E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92271633E-01    2           5        -5   # BR(h -> b       bb     )
     6.43099341E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27656187E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82703777E-04    2           3        -3   # BR(h -> s       sb     )
     2.09621976E-02    2           4        -4   # BR(h -> c       cb     )
     6.85393069E-02    2          21        21   # BR(h -> g       g      )
     2.37422522E-03    2          22        22   # BR(h -> gam     gam    )
     1.63463472E-03    2          22        23   # BR(h -> Z       gam    )
     2.21185462E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80122463E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46838157E+01   # H decays
#          BR         NDA      ID1       ID2
     3.47654658E-01    2           5        -5   # BR(H -> b       bb     )
     6.06321828E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14380713E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53575422E-04    2           3        -3   # BR(H -> s       sb     )
     7.11306933E-08    2           4        -4   # BR(H -> c       cb     )
     7.12877642E-03    2           6        -6   # BR(H -> t       tb     )
     8.46462179E-07    2          21        21   # BR(H -> g       g      )
     2.36151379E-10    2          22        22   # BR(H -> gam     gam    )
     1.83039979E-09    2          23        22   # BR(H -> Z       gam    )
     1.93472405E-06    2          24       -24   # BR(H -> W+      W-     )
     9.66691778E-07    2          23        23   # BR(H -> Z       Z      )
     7.44074998E-06    2          25        25   # BR(H -> h       h      )
    -2.70334729E-24    2          36        36   # BR(H -> A       A      )
    -2.19452834E-20    2          23        36   # BR(H -> Z       A      )
     1.85212190E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63755460E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63755460E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.98668472E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03989627E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07702680E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82167435E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.79994758E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.07078098E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.64309016E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.01100784E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.34318710E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.35576396E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.84284844E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.29595792E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.29595792E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.23273935E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.46793765E+01   # A decays
#          BR         NDA      ID1       ID2
     3.47708598E-01    2           5        -5   # BR(A -> b       bb     )
     6.06374355E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14399116E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53638552E-04    2           3        -3   # BR(A -> s       sb     )
     7.15884683E-08    2           4        -4   # BR(A -> c       cb     )
     7.14223438E-03    2           6        -6   # BR(A -> t       tb     )
     1.46754838E-05    2          21        21   # BR(A -> g       g      )
     5.34045862E-08    2          22        22   # BR(A -> gam     gam    )
     1.61299892E-08    2          23        22   # BR(A -> Z       gam    )
     1.93080547E-06    2          23        25   # BR(A -> Z       h      )
     1.85600209E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63770925E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63770925E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.73087338E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29774839E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.89416195E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.49355457E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.44890559E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07677863E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.06161355E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.10811653E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.23595196E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.43962151E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.43962151E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46721900E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.56125212E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.06617838E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14485206E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.55919831E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21509693E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49984514E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.54354748E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.93330071E-06    2          24        25   # BR(H+ -> W+      h      )
     3.54010624E-14    2          24        36   # BR(H+ -> W+      A      )
     6.80053856E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.91302617E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.01310897E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.64008898E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.15392003E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61326785E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.26007332E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.75804527E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.73160508E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
