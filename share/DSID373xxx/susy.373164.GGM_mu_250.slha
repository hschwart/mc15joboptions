#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375305E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415921E+03   # H
        36     2.00000000E+03   # A
        37     2.00206449E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.38578836E+03   # ~d_L
   2000001     4.38566533E+03   # ~d_R
   1000002     4.38551342E+03   # ~u_L
   2000002     4.38557870E+03   # ~u_R
   1000003     4.38578836E+03   # ~s_L
   2000003     4.38566533E+03   # ~s_R
   1000004     4.38551342E+03   # ~c_L
   2000004     4.38557870E+03   # ~c_R
   1000005     4.38559744E+03   # ~b_1
   2000005     4.38585789E+03   # ~b_2
   1000006     4.53280149E+03   # ~t_1
   2000006     4.86901422E+03   # ~t_2
   1000011     5.00008258E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984143E+03   # ~nu_eL
   1000013     5.00008258E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984143E+03   # ~nu_muL
   1000015     5.00001287E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984143E+03   # ~nu_tauL
   1000021     4.51228652E+03   # ~g
   1000022     2.38911724E+02   # ~chi_10
   1000023    -2.70386057E+02   # ~chi_20
   1000025     3.33133171E+02   # ~chi_30
   1000035     3.12901996E+03   # ~chi_40
   1000024     2.68142897E+02   # ~chi_1+
   1000037     3.12901952E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56244233E-01   # N_11
  1  2    -2.22784551E-02   # N_12
  1  3     5.94578875E-01   # N_13
  1  4    -5.80148244E-01   # N_14
  2  1     1.53226627E-02   # N_21
  2  2    -4.61391650E-03   # N_22
  2  3    -7.05672749E-01   # N_23
  2  4    -7.08357183E-01   # N_24
  3  1     8.30877477E-01   # N_31
  3  2     1.55207479E-02   # N_32
  3  3    -3.85028331E-01   # N_33
  3  4     4.01441041E-01   # N_34
  4  1     4.33044514E-04   # N_41
  4  2    -9.99620672E-01   # N_42
  4  3    -1.59723701E-02   # N_43
  4  4     2.24322819E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25806988E-02   # U_11
  1  2     9.99745024E-01   # U_12
  2  1     9.99745024E-01   # U_21
  2  2     2.25806988E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17198461E-02   # V_11
  1  2     9.99496799E-01   # V_12
  2  1     9.99496799E-01   # V_21
  2  2     3.17198461E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98296973E-01   # cos(theta_t)
  1  2    -5.83365554E-02   # sin(theta_t)
  2  1     5.83365554E-02   # -sin(theta_t)
  2  2     9.98296973E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13638892E-01   # cos(theta_b)
  1  2     8.58006462E-01   # sin(theta_b)
  2  1    -8.58006462E-01   # -sin(theta_b)
  2  2     5.13638892E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410084E-01   # cos(theta_tau)
  1  2     7.24371270E-01   # sin(theta_tau)
  2  1    -7.24371270E-01   # -sin(theta_tau)
  2  2     6.89410084E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90178028E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51788310E+02   # vev(Q)              
         4     3.80730441E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53062181E-01   # gprime(Q) DRbar
     2     6.28968401E-01   # g(Q) DRbar
     3     1.06610577E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02597454E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71860960E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79960071E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99471603E+06   # M^2_Hd              
        22    -4.69443883E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37848551E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73427948E+00   # gluino decays
#          BR         NDA      ID1       ID2
     4.98357075E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.98357075E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99313126E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99313126E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.00494827E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.00494827E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.99986851E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.99986851E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.98357075E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.98357075E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99313126E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99313126E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.00494827E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.00494827E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.99986851E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.99986851E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.24172288E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.24172288E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.73322668E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     4.73322668E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.24025702E-03    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     7.44336485E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.66876204E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.22235709E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.61357016E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     6.41816599E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     6.08596155E-03    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.28219422E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
    -1.29330760E-02    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
    -3.60228957E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     1.50936196E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.80048123E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23654431E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.07864543E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.34696731E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.42781277E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.36869203E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.68943139E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.77278920E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.79703237E-03    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.91689683E-03    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.32350560E-03    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005    -7.47716505E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
    -1.33165844E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -3.30117667E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
    -3.42401087E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.82333066E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.33034256E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     7.66192485E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     1.51215055E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     9.85985183E-05    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.31491149E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -4.20444996E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.59321881E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.66077457E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.19647148E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     1.28730697E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     8.59076624E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.91175114E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.73418149E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17179226E-01    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.57984839E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.34304433E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     9.22510977E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     3.10512966E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.35265779E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.89251721E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.65408602E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
#
#         PDG            Width
DECAY   1000001     1.28613519E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.05180556E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.18101427E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.50383881E-02    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17669219E-01    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.30865450E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35393872E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     2.30631565E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.10512918E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.35265758E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.89251769E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.65446171E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
#
#         PDG            Width
DECAY   1000004     1.28730697E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.59076624E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.91175114E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.73418149E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17179226E-01    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.57984839E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.34304433E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     9.22510977E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.10512966E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.35265779E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.89251721E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.65408602E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
#
#         PDG            Width
DECAY   1000003     1.28613519E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.05180556E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.18101427E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50383881E-02    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17669219E-01    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.30865450E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35393872E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     2.30631565E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.10512918E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.35265758E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.89251769E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.65446171E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
#
#         PDG            Width
DECAY   1000011     2.80276376E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87546965E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10955423E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61598417E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59514029E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11789602E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409972E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125942E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10332648E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35183332E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89432099E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99356316E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80276376E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87546965E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10955423E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61598417E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59514029E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11789602E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409972E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125942E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10332648E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35183332E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89432099E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99356316E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63123343E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88478720E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47000296E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15188665E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31199134E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95802324E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591722E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36735927E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64275621E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64532345E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92699934E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00446736E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599849E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67494455E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89411620E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80262444E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.81992481E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21883248E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41451232E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734706E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40455719E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088374E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80262444E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.81992481E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21883248E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41451232E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734706E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40455719E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088374E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80582637E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81100092E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21744158E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41289811E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438304E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54377529E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496356E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.51872423E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64998962E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33997246E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33997246E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332510E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332510E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09303989E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41056005E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06987256E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33272586E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02056352E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85099669E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07667960E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52719973E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.55049535E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25339356E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51985865E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29863096E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00958045E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08136114E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.02444026E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00599694E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92496220E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.90408601E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77237184E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.18359627E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15727537E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14242824E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69737906E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20452276E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55910387E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20452276E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55910387E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25904012E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55812025E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55812025E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48797924E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10401327E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10401327E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10401327E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18590159E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18590159E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18590159E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18590159E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.95300597E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.95300597E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.95300597E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.95300597E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.19243674E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.19243674E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.44240031E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99750240E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.75492249E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.30121440E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.25564686E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41059732E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39906792E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01535705E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20362889E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01737150E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01737150E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33846063E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26990708E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02386519E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.69061536E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20925827E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02339145E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83467673E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74472611E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22050847E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54448331E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54448331E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42035993E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67462876E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78030817E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06196329E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44037875E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21766434E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01672531E-01    2           5        -5   # BR(h -> b       bb     )
     6.22445592E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20318603E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66152199E-04    2           3        -3   # BR(h -> s       sb     )
     2.01127982E-02    2           4        -4   # BR(h -> c       cb     )
     6.64417827E-02    2          21        21   # BR(h -> g       g      )
     2.31820866E-03    2          22        22   # BR(h -> gam     gam    )
     1.62669710E-03    2          22        23   # BR(h -> Z       gam    )
     2.16811374E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80855774E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01111986E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38904498E-03    2           5        -5   # BR(H -> b       bb     )
     2.32315440E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21321571E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05158145E-06    2           3        -3   # BR(H -> s       sb     )
     9.48923691E-06    2           4        -4   # BR(H -> c       cb     )
     9.38919977E-01    2           6        -6   # BR(H -> t       tb     )
     7.51767000E-04    2          21        21   # BR(H -> g       g      )
     2.63736168E-06    2          22        22   # BR(H -> gam     gam    )
     1.09254014E-06    2          23        22   # BR(H -> Z       gam    )
     3.08655188E-04    2          24       -24   # BR(H -> W+      W-     )
     1.53906590E-04    2          23        23   # BR(H -> Z       Z      )
     8.53154183E-04    2          25        25   # BR(H -> h       h      )
     8.39718669E-24    2          36        36   # BR(H -> A       A      )
     3.34797578E-11    2          23        36   # BR(H -> Z       A      )
     2.64385914E-12    2          24       -37   # BR(H -> W+      H-     )
     2.64385914E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23400022E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15150562E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93599008E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.02048517E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96688988E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.33092573E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54888419E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05618228E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39174459E-03    2           5        -5   # BR(A -> b       bb     )
     2.29925968E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12871358E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07165353E-06    2           3        -3   # BR(A -> s       sb     )
     9.39110224E-06    2           4        -4   # BR(A -> c       cb     )
     9.39882787E-01    2           6        -6   # BR(A -> t       tb     )
     8.89616914E-04    2          21        21   # BR(A -> g       g      )
     2.66249653E-06    2          22        22   # BR(A -> gam     gam    )
     1.27398433E-06    2          23        22   # BR(A -> Z       gam    )
     3.00744546E-04    2          23        25   # BR(A -> Z       h      )
     6.12161482E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74217989E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47869549E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11500908E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99174323E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.14466860E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45959639E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97567121E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23242124E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34824332E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30188840E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41933895E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14117836E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02439917E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41581322E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.08225163E-04    2          24        25   # BR(H+ -> W+      h      )
     1.24023447E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79001105E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30721852E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98314238E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
