evgenConfig.description = "Single pi0 with flat eta-phi and pt range between 0.1 and 5 GeV"
evgenConfig.keywords = ["singleParticle", "pi0"]
       
include("MC15JobOptions/ParticleGun_Common.py")
       
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[100.0, 5000.0], eta=[2.3, 4.1])
 

