model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 200.
mHD = 200.
widthZp = 7.957744e-01
widthhd = 7.947794e-02
filteff = 8.865248e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
