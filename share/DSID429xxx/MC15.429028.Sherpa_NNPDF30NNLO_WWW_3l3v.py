include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa+OpenLoops WWW+0j@NLO+1,2j@LO with fully leptonic decays."
evgenConfig.keywords = ["SM", "triboson", "3lepton", "neutrino", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "WWW"

evgenConfig.process="""
(run){
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  LJET:=3; NJET:=2; QCUT:=30;
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;
  NLO_CSS_DISALLOW_FLAVOUR 5;

  HARD_DECAYS On;
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=0
  HDH_STATUS[24,4,-3]=0
  HDH_STATUS[-24,-2,1]=0
  HDH_STATUS[-24,-4,3]=0
}(run);

(processes){
  Process 901 901 -> 24 24 -24 901{NJET};
  Order (*,3); 
  CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;

  Process 901 901 -> 24 -24 -24 901{NJET};
  Order (*,3); 
  CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
