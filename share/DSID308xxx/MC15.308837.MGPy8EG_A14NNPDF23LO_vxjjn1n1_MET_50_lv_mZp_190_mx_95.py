model="LightVector"
mDM1 = 95.
mDM2 = 380.
mZp = 190.
mHD = 125.
widthZp = 7.559856e-01
widthN2 = 1.248224e+01
filteff = 9.090909e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
