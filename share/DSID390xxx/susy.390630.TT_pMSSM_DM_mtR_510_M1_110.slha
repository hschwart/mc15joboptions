#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10051138E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04192351E+01   # W+
        25     1.24953022E+02   # h
        35     3.99999964E+03   # H
        36     3.99999536E+03   # A
        37     4.00103062E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01668386E+03   # ~d_L
   2000001     4.01534178E+03   # ~d_R
   1000002     4.01602976E+03   # ~u_L
   2000002     4.01590050E+03   # ~u_R
   1000003     4.01668386E+03   # ~s_L
   2000003     4.01534178E+03   # ~s_R
   1000004     4.01602976E+03   # ~c_L
   2000004     4.01590050E+03   # ~c_R
   1000005     2.03029738E+03   # ~b_1
   2000005     4.02063879E+03   # ~b_2
   1000006     5.28040372E+02   # ~t_1
   2000006     2.04171120E+03   # ~t_2
   1000011     4.00247191E+03   # ~e_L
   2000011     4.00368428E+03   # ~e_R
   1000012     4.00135736E+03   # ~nu_eL
   1000013     4.00247191E+03   # ~mu_L
   2000013     4.00368428E+03   # ~mu_R
   1000014     4.00135736E+03   # ~nu_muL
   1000015     4.00463451E+03   # ~tau_1
   2000015     4.00830782E+03   # ~tau_2
   1000016     4.00362358E+03   # ~nu_tauL
   1000021     1.97452749E+03   # ~g
   1000022     9.12741082E+01   # ~chi_10
   1000023    -1.35213814E+02   # ~chi_20
   1000025     1.53487035E+02   # ~chi_30
   1000035     2.06656350E+03   # ~chi_40
   1000024     1.29758494E+02   # ~chi_1+
   1000037     2.06673988E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50894588E-01   # N_11
  1  2     1.41535821E-02   # N_12
  1  3     5.43034160E-01   # N_13
  1  4     3.75594056E-01   # N_14
  2  1    -1.37052551E-01   # N_21
  2  2     2.72894701E-02   # N_22
  2  3    -6.84861939E-01   # N_23
  2  4     7.15147542E-01   # N_24
  3  1     6.46044196E-01   # N_31
  3  2     2.36350241E-02   # N_32
  3  3     4.85878373E-01   # N_33
  3  4     5.88209562E-01   # N_34
  4  1    -9.02008906E-04   # N_41
  4  2     9.99247890E-01   # N_42
  4  3    -4.80467557E-04   # N_43
  4  4    -3.87635004E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.83782056E-04   # U_11
  1  2     9.99999766E-01   # U_12
  2  1    -9.99999766E-01   # U_21
  2  2     6.83782056E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48332411E-02   # V_11
  1  2    -9.98495526E-01   # V_12
  2  1    -9.98495526E-01   # V_21
  2  2    -5.48332411E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.96233221E-02   # cos(theta_t)
  1  2     9.95975733E-01   # sin(theta_t)
  2  1    -9.95975733E-01   # -sin(theta_t)
  2  2    -8.96233221E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999871E-01   # cos(theta_b)
  1  2    -5.07936988E-04   # sin(theta_b)
  2  1     5.07936988E-04   # -sin(theta_b)
  2  2     9.99999871E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03207942E-01   # cos(theta_tau)
  1  2     7.10984241E-01   # sin(theta_tau)
  2  1    -7.10984241E-01   # -sin(theta_tau)
  2  2    -7.03207942E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00309702E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00511382E+03  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44231423E+02   # higgs               
         4     1.61374283E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00511382E+03  # The gauge couplings
     1     3.61395370E-01   # gprime(Q) DRbar
     2     6.36274893E-01   # g(Q) DRbar
     3     1.03520437E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00511382E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.47731107E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00511382E+03  # The trilinear couplings
  1  1     2.74497273E-07   # A_d(Q) DRbar
  2  2     2.74523742E-07   # A_s(Q) DRbar
  3  3     4.90135198E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00511382E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     5.98251271E-08   # A_mu(Q) DRbar
  3  3     6.04368032E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00511382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72338734E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00511382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82196536E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00511382E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03941122E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00511382E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58561652E+07   # M^2_Hd              
        22    -2.63447226E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40445543E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.56355017E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.35555212E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.43641876E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.04779501E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.96670890E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.14185421E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.08005589E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.68355113E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.46359662E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.57288415E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.44708185E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.80074219E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.84971374E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.11251113E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.30903484E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.33668368E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.38084690E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.19689406E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.58592381E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.58142985E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63703052E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.82953600E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71270335E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41764860E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.68672165E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58694106E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.20730002E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14478948E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.23252584E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.46175807E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.63537401E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.52563703E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75370301E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45351136E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59451992E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88631610E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82012963E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.32899363E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62786363E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51647181E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58257368E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.17836359E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05770475E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.34889188E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49685570E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43669716E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75388706E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.16754328E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.51142825E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.62363469E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82483485E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.28742435E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.65971573E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51873362E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51523763E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.29783964E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.76137757E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.13231272E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.51732378E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85293703E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75370301E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45351136E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59451992E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88631610E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82012963E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.32899363E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62786363E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51647181E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58257368E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.17836359E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05770475E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.34889188E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49685570E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43669716E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75388706E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.16754328E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.51142825E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.62363469E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82483485E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.28742435E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.65971573E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51873362E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51523763E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.29783964E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.76137757E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.13231272E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.51732378E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85293703E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12160752E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77016303E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31664086E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85379802E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77036205E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.81827651E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55407061E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07672384E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64303555E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87754078E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.16920599E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.38646816E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12160752E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77016303E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31664086E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85379802E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77036205E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.81827651E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55407061E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07672384E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64303555E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87754078E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.16920599E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.38646816E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09409943E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23301584E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01734180E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66603216E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38335110E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.42508441E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77335829E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10507860E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.06627002E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.38014389E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42353509E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41351412E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.24818721E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83384767E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12271191E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00092906E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.69055211E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.06396920E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77317134E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09648564E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53163230E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12271191E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00092906E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.69055211E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.06396920E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77317134E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09648564E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53163230E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45720291E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.04600363E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.14290469E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48038896E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50834445E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.84207784E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00337946E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.37509553E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33723421E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33723421E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241916E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241916E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10069326E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.70907721E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.83162770E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.19039487E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.45064156E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.17611080E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42304912E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.73408456E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37954696E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.59595497E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.26755599E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18578572E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53772075E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18578572E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53772075E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37538379E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52359397E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52359397E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48029365E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04459917E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04459917E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04459875E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.84990440E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.84990440E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.84990440E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.84990440E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.28330467E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.28330467E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.28330467E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.28330467E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.72848523E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.72848523E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.52447349E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20575010E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.51997089E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.40061359E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.39722977E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.40061359E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.39722977E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.14981639E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.97642357E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.97642357E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.89073998E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.01936870E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.01936870E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.01936401E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.58651627E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.83905957E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.58651627E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.83905957E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.08168958E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.25520013E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.25520013E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.13586124E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.50695342E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.50695342E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.50695361E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.60872530E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.60872530E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.60872530E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.60872530E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.20285303E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.20285303E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.20285303E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.20285303E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.11500306E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.11500306E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.71023358E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.36099683E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.28284360E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.28619776E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38842883E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38842883E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.87744580E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.16285432E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.21117425E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89323571E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89323571E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.26079112E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.26079112E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.98435278E-03   # h decays
#          BR         NDA      ID1       ID2
     6.05866973E-01    2           5        -5   # BR(h -> b       bb     )
     6.50978356E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30448477E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89161216E-04    2           3        -3   # BR(h -> s       sb     )
     2.12414063E-02    2           4        -4   # BR(h -> c       cb     )
     6.86062512E-02    2          21        21   # BR(h -> g       g      )
     2.35719136E-03    2          22        22   # BR(h -> gam     gam    )
     1.56280311E-03    2          22        23   # BR(h -> Z       gam    )
     2.08433180E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61147493E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.41277824E+01   # H decays
#          BR         NDA      ID1       ID2
     3.39361489E-01    2           5        -5   # BR(H -> b       bb     )
     6.12549149E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16582542E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56179865E-04    2           3        -3   # BR(H -> s       sb     )
     7.18656241E-08    2           4        -4   # BR(H -> c       cb     )
     7.20243164E-03    2           6        -6   # BR(H -> t       tb     )
     1.05677809E-06    2          21        21   # BR(H -> g       g      )
     2.75997296E-10    2          22        22   # BR(H -> gam     gam    )
     1.84944084E-09    2          23        22   # BR(H -> Z       gam    )
     2.03874606E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01866692E-06    2          23        23   # BR(H -> Z       Z      )
     7.34845783E-06    2          25        25   # BR(H -> h       h      )
    -1.62323581E-24    2          36        36   # BR(H -> A       A      )
     5.40257420E-20    2          23        36   # BR(H -> Z       A      )
     1.87198375E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66619184E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66619184E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40007057E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.64691932E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70462677E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.41976797E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.46917875E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.10600709E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.11947653E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.39167279E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.21080148E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.22177606E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.31136444E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.31136444E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.41266453E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39394742E-01    2           5        -5   # BR(A -> b       bb     )
     6.12566295E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16588435E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56228572E-04    2           3        -3   # BR(A -> s       sb     )
     7.23194915E-08    2           4        -4   # BR(A -> c       cb     )
     7.21516706E-03    2           6        -6   # BR(A -> t       tb     )
     1.48253494E-05    2          21        21   # BR(A -> g       g      )
     4.06643839E-08    2          22        22   # BR(A -> gam     gam    )
     1.63028249E-08    2          23        22   # BR(A -> Z       gam    )
     2.03457098E-06    2          23        25   # BR(A -> Z       h      )
     1.87526878E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66624707E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66624707E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.96662275E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.27788865E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34540592E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.89542918E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.38017045E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.76953516E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.39438575E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.27222500E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.66383534E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.43374405E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.43374405E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.39515477E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.39666633E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.14713410E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17347602E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.45386346E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23131472E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53321033E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.44169346E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.04347404E-06    2          24        25   # BR(H+ -> W+      h      )
     2.90056020E-14    2          24        36   # BR(H+ -> W+      A      )
     4.71872341E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.50047955E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.24934898E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67384018E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.91814595E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57315384E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.90874031E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.21314969E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
