#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419653E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399683E+03   # H
        36     2.00000000E+03   # A
        37     2.00151866E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.92095260E+03   # ~d_L
   2000001     4.92084218E+03   # ~d_R
   1000002     4.92070569E+03   # ~u_L
   2000002     4.92076398E+03   # ~u_R
   1000003     4.92095260E+03   # ~s_L
   2000003     4.92084218E+03   # ~s_R
   1000004     4.92070569E+03   # ~c_L
   2000004     4.92076398E+03   # ~c_R
   1000005     4.92013086E+03   # ~b_1
   2000005     4.92166528E+03   # ~b_2
   1000006     5.03304860E+03   # ~t_1
   2000006     5.32422476E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.23870738E+03   # ~g
   1000022     1.90930203E+03   # ~chi_10
   1000023    -1.96180044E+03   # ~chi_20
   1000025     1.99666335E+03   # ~chi_30
   1000035     3.12547659E+03   # ~chi_40
   1000024     1.95661520E+03   # ~chi_1+
   1000037     3.12546955E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.35551386E-01   # N_11
  1  2    -4.32635377E-02   # N_12
  1  3     4.79358351E-01   # N_13
  1  4    -4.76768283E-01   # N_14
  2  1     2.23208858E-03   # N_21
  2  2    -3.05250826E-03   # N_22
  2  3    -7.07041518E-01   # N_23
  2  4    -7.07161928E-01   # N_24
  3  1     6.77460674E-01   # N_31
  3  2     5.06539791E-02   # N_32
  3  3    -5.17967445E-01   # N_33
  3  4     5.19798938E-01   # N_34
  4  1     2.49225780E-03   # N_41
  4  2    -9.97774084E-01   # N_42
  4  3    -4.49175829E-02   # N_43
  4  4     4.92247549E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.34344740E-02   # U_11
  1  2     9.97986006E-01   # U_12
  2  1     9.97986006E-01   # U_21
  2  2     6.34344740E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.95234315E-02   # V_11
  1  2     9.97580319E-01   # V_12
  2  1     9.97580319E-01   # V_21
  2  2     6.95234315E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99877282E-01   # cos(theta_t)
  1  2     1.56659165E-02   # sin(theta_t)
  2  1    -1.56659165E-02   # -sin(theta_t)
  2  2     9.99877282E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81189971E-01   # cos(theta_b)
  1  2     7.32106702E-01   # sin(theta_b)
  2  1    -7.32106702E-01   # -sin(theta_b)
  2  2     6.81189971E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954863E-01   # cos(theta_tau)
  1  2     7.09252170E-01   # sin(theta_tau)
  2  1    -7.09252170E-01   # -sin(theta_tau)
  2  2     7.04954863E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90199847E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305846E+02   # vev(Q)              
         4     3.09661617E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717973E-01   # gprime(Q) DRbar
     2     6.26764121E-01   # g(Q) DRbar
     3     1.07892770E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02490472E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71370537E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738288E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.85847827E+05   # M^2_Hd              
        22    -8.54456920E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36865171E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.65935249E-05   # gluino decays
#          BR         NDA      ID1       ID2
     6.48758548E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     8.81792257E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.24460322E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.84640789E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.01260866E-08    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.79081354E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.68211797E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.73845485E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.61930801E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.84640789E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.01260866E-08    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.79081354E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.68211797E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.73845485E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.61930801E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.84746371E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     8.88032413E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     9.20286335E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.28565939E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.28565939E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.28565939E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.28565939E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.53608305E-03    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.53608305E-03    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.47674769E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.82928667E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.04438336E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.78680140E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.95137781E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.67246407E-06    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.81061311E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.91715029E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.87742874E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.17874371E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.11425226E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.70698164E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.20121004E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.17076569E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.17629430E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.84243922E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.37648395E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.04362788E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -5.42592650E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.38720635E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.20884557E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.37941127E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.97585311E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.66333697E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.90449387E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.97371002E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.48724399E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.10716958E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.02245193E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.37469991E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.05463904E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.69682241E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.62031915E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.63302500E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.79018263E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.35424797E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.63896180E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.04857708E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.85880054E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.46063000E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.89797010E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.20056013E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.55222403E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.83919158E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.10839166E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.89862811E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.72835452E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.35931438E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.13013674E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.93589178E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.34329752E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.57047591E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.85888879E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.36573999E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.50810586E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.06921371E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.55937626E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.69391147E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.11515049E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.89911829E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.67273102E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.09458832E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.50257998E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.00080797E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.47011878E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88904514E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.85880054E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.46063000E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.89797010E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.20056013E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.55222403E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.83919158E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.10839166E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.89862811E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.72835452E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.35931438E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.13013674E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.93589178E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.34329752E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.57047591E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.85888879E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.36573999E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.50810586E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.06921371E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.55937626E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.69391147E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.11515049E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.89911829E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.67273102E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.09458832E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.50257998E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.00080797E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.47011878E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88904514E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.62939901E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.44943559E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.71642614E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.79297812E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.73835272E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.28888668E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.49449988E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.77963624E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.49025762E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.96002745E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.50966071E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.20673448E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.62939901E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.44943559E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.71642614E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.79297812E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.73835272E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.28888668E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.49449988E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.77963624E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.49025762E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.96002745E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.50966071E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.20673448E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20951780E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.72497956E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45346241E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.36599728E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.61293772E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.36080707E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.23639626E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.62764260E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.20677777E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59164052E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.02318603E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.44024808E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.64789483E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.82196769E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30637141E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.62965447E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13311911E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     9.87260865E-06    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.73661160E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.75305340E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.15084049E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.48855920E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.62965447E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13311911E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     9.87260865E-06    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.73661160E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.75305340E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.15084049E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.48855920E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.63195374E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.13212922E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.86398395E-06    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.73160011E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.75064834E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01703020E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.48379349E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.63970147E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00445706E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.00082639E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.00082639E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.00027649E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.00027649E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.93337184E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.65785577E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53812141E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05673307E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46114641E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41817459E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52573107E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.34310290E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.42859321E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00716154E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88570515E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.07133309E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.19013173E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.85748191E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.06807511E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.91682879E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.22993010E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09496250E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.41789705E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.09496250E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.41789705E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.30978719E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.23864368E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23864368E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.20982582E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.46801347E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.46801347E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.46801347E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.28770370E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.28770370E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.28770370E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.28770370E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.09590138E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.09590138E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.09590138E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.09590138E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.19534924E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.19534924E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.95193657E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.29828408E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.27925211E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.08478761E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.34944015E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.89932984E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.54448649E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.83673913E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.54448649E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.83673913E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.97540785E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.21646760E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.21646760E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.51349059E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.95847292E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.95847292E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.95847292E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.66387185E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.15459564E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.66387185E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.15459564E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.81135973E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     4.92135444E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.92135444E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     4.84048033E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     9.82857235E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     9.82857235E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     9.82857235E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.20880731E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.20880731E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.20880731E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.20880731E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.02935181E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.02935181E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.02935181E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.02935181E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.99016168E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.99016168E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.65778184E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.40556289E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52472983E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.91173044E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46907880E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46907880E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07850951E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.16276470E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44802956E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.22795687E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.11386140E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.17885017E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.36393740E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.78649671E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22222252E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02269363E-01    2           5        -5   # BR(h -> b       bb     )
     6.21814128E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20095092E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65678724E-04    2           3        -3   # BR(h -> s       sb     )
     2.00904978E-02    2           4        -4   # BR(h -> c       cb     )
     6.63497506E-02    2          21        21   # BR(h -> g       g      )
     2.30428565E-03    2          22        22   # BR(h -> gam     gam    )
     1.62519803E-03    2          22        23   # BR(h -> Z       gam    )
     2.16438464E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80552544E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78093766E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46587284E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431553E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71227287E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11549033E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668915E-05    2           4        -4   # BR(H -> c       cb     )
     9.96071989E-01    2           6        -6   # BR(H -> t       tb     )
     7.97746859E-04    2          21        21   # BR(H -> g       g      )
     2.71747145E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030684E-06    2          23        22   # BR(H -> Z       gam    )
     3.33965482E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66527318E-04    2          23        23   # BR(H -> Z       Z      )
     9.01535586E-04    2          25        25   # BR(H -> h       h      )
     7.24452388E-24    2          36        36   # BR(H -> A       A      )
     2.91090105E-11    2          23        36   # BR(H -> Z       A      )
     6.49938477E-12    2          24       -37   # BR(H -> W+      H-     )
     6.49938477E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380072E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46879756E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899122E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62271503E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678049E-06    2           3        -3   # BR(A -> s       sb     )
     9.96182208E-06    2           4        -4   # BR(A -> c       cb     )
     9.97001721E-01    2           6        -6   # BR(A -> t       tb     )
     9.43681070E-04    2          21        21   # BR(A -> g       g      )
     3.13389036E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290151E-06    2          23        22   # BR(A -> Z       gam    )
     3.25453211E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471389E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35277864E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239284E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150904E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49578966E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45697672E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732330E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403028E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33556186E-04    2          24        25   # BR(H+ -> W+      h      )
     2.83685446E-13    2          24        36   # BR(H+ -> W+      A      )
