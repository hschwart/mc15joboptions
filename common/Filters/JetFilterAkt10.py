## Truth jet filter setup for anti-kT R=1.0 truth jets

include("MC15JobOptions/AntiKt10TruthJets.py")

include("MC15JobOptions/JetFilter_Fragment.py")
filtSeq.QCDTruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"
