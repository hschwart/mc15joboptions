##############################################################
# Job options fragment for cc->mu10mu10X 
# no EvtGen, because method of multiple hadronisation in Pythia8B is incompatible with EvtGen
##############################################################
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description = "Inclusive cc->mu10mu10X production"
evgenConfig.keywords = ["charm","2muon","inclusive"]
evgenConfig.minevents = 100

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 18.']    
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = False
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.NHadronizationLoops = 40

include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [10.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]
