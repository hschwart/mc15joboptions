evgenConfig.description = "Single Pi- with flat eta-phi and log E in [5, 2000] GeV"
evgenConfig.keywords = ["singleParticle", "pi-"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -211
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(5000, 2000000.), eta=[-2.8, 2.8])
