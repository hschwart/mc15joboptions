from MadGraphControl.MadGraphUtils import *

# General settings
minevents=10000
nevents=11000
mode=0

is_Zll = "Zll" in runArgs.jobConfig[0]
is_Zvv = "Zvv" in runArgs.jobConfig[0]
is_Wlv = "Wlv" in runArgs.jobConfig[0]
is_CV = "CV" in runArgs.jobConfig[0]
is_C3 = "C3" in runArgs.jobConfig[0]
is_C2V = "C2V" in runArgs.jobConfig[0]

CVorder=" CV=1"
if is_CV:
  CVorder=" CV=2"
C3order=" C3=0"
if is_C3:
  C3order=" C3=1"
C2Vorder=" C2V=0"
if is_C2V:
  C2Vorder=" C2V=1"
QEDorder=" QED=1"
if is_Wlv:
  QEDorder=" QED=2"
orderstring = CVorder+C3order+C2Vorder+QEDorder

if is_Zll:
    mgproc="""generate p p > h h z"""+orderstring+""", z > l+ l- """
    name="ZllHH"+orderstring
    keyword=['SM']
elif is_Zvv:
    mgproc="""generate p p > h h z"""+orderstring+""", z > vl vl~ """
    name="ZvvHH"+orderstring
    keyword=['SM']
elif is_Wlv:
    mgproc="""generate p p > h h l+ vl"""+orderstring+"""\n"""
    mgproc +="""add process p p > h h l- vl~"""+orderstring
    name="WlvHH"+orderstring
    keyword=['SM']
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions, which makes no sense."%runArgs.runNumber)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HHVBF_UFO
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+mgproc+"""
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '2.0',
           'pdlabel'      :"'nn23lo1'",
           'mmll'         : 60}

higgsMass={'25':'1.250000e+02'} #MH
parameters = {}
parameters['NEW'] = {'CV':  '1.000000',  # CV
                     'C2V': '1.000000',  # C2V
                     'C3':  '1.000000' }  # C3

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', 
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,
               extras=extras)
build_param_card(param_card_old='%s/Cards/param_card.dat' % process_dir,param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)

print_cards()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir)
arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz')

#### Shower 
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=keyword 
evgenConfig.minevents = minevents
evgenConfig.contact = ['Matt Klein <matthew.henry.klein@cern.ch>']
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 -5' ]
