model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 1000.
mHD = 1000.
widthZp = 4.770293e+00
widthhd = 3.978675e-01
filteff = 9.960159e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
