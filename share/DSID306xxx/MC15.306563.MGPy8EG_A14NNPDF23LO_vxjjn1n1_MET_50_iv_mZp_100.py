model="InelasticVectorEFT"
mDM1 = 50.
mDM2 = 200.
mZp = 100.
mHD = 125.
widthZp = 3.978850e-01
widthN2 = 3.693093e-03
filteff = 7.173601e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")
