from MadGraphControl.MadGraphUtils import *

# General settings
nevents=50000
mode=0
nJobs=1
gridpack_dir=None
gridpack_mode=False
cluster_type=None
cluster_queue=None
doCluster=False

# MG Particle cuts
mllcut=40

# Merging settings
maxjetflavor=5
ickkw=0
nJetMax=4
ktdurham=30
dparameter=0.4

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = njobs
    nJobs=int(njobs)
    mode=2
    print opts

### DSID lists (extensions can include filters, systematics samples, etc.)
Zee_5fl_Np01=[311433]
Zee_5fl_Np2=[311434]
Zee_5fl_Np3=[311435]
Zee_5fl_Np4=[311436]

Zmumu_5fl_Np01=[311437]
Zmumu_5fl_Np2=[311438]
Zmumu_5fl_Np3=[311439]
Zmumu_5fl_Np4=[311440]

Ztautau_5fl_Np01=[311441]
Ztautau_5fl_Np2=[311442]
Ztautau_5fl_Np3=[311443]
Ztautau_5fl_Np4=[311444]

Znunu_5fl_Np01=[311429]
Znunu_5fl_Np2=[311430]
Znunu_5fl_Np3=[311431]
Znunu_5fl_Np4=[311432]

Znunu_5fl=Znunu_5fl_Np01+Znunu_5fl_Np2+Znunu_5fl_Np3+Znunu_5fl_Np4

#allowable min events
#[1, 2, 5, 10, 20, 25, 50, 100, 200, 500, 1000]
minEvents=10
if runArgs.runNumber in (Zee_5fl_Np01+Zmumu_5fl_Np01+Ztautau_5fl_Np01+Znunu_5fl_Np01):
    minEvents=50
    if runArgs.runNumber in Znunu_5fl_Np01:
        minEvents=25
    nevents=200000
elif runArgs.runNumber in (Zee_5fl_Np2+Zmumu_5fl_Np2+Ztautau_5fl_Np2+Znunu_5fl_Np2):
    minEvents=1000
    nevents=70000
    if runArgs.runNumber in Znunu_5fl_Np2:
        minEvents=1000        
        nevents=120000
elif runArgs.runNumber in (Zee_5fl_Np3+Zmumu_5fl_Np3+Ztautau_5fl_Np3+Znunu_5fl_Np3):
    minEvents=1000
    nevents=50000
    if runArgs.runNumber in Znunu_5fl_Np3:
        minEvents=1000        
        nevents=65000
elif runArgs.runNumber in (Zee_5fl_Np4+Zmumu_5fl_Np4+Ztautau_5fl_Np4+Znunu_5fl_Np4):
    minEvents=100
    if runArgs.runNumber in Znunu_5fl_Np4:
        minEvents=50
    nevents=10000

# Setting min events
evgenConfig.minevents=minEvents

### Electrons
if runArgs.runNumber in Zee_5fl_Np01:
    mgproc="""
generate p p > e+ e- @0
add process p p > e+ e- j @1
"""
    name='Zee_Np01'
    process="pp>e+e-"
elif runArgs.runNumber in Zee_5fl_Np2:
    mgproc="generate p p > e+ e- j j @2"
    name='Zee_Np2'
    process="pp>e+e-"
elif runArgs.runNumber in Zee_5fl_Np3:
    mgproc="generate p p > e+ e- j j j @3"
    name='Zee_Np3'
    process="pp>e+e-"
elif runArgs.runNumber in Zee_5fl_Np4:
    mgproc="generate p p > e+ e- j j j j @4"
    name='Zee_Np4'
    process="pp>e+e-"
    
    gridpack_mode=True
    gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='lsf'
        cluster_queue='8nh'
        nJobs=20

### Muons    
elif runArgs.runNumber in Zmumu_5fl_Np01:
    mgproc="""
generate p p > mu+ mu- @0
add process p p > mu+ mu- j @1
"""
    name='Zmumu_Np01'
    process="pp>mu+mu-"
elif runArgs.runNumber in Zmumu_5fl_Np2:
    mgproc="generate p p > mu+ mu- j j @2"
    name='Zmumu_Np2'
    process="pp>mu+mu-"
elif runArgs.runNumber in Zmumu_5fl_Np3:
    mgproc="generate p p > mu+ mu- j j j @3"
    name='Zmumu_Np3'
    process="pp>mu+mu-"
elif runArgs.runNumber in Zmumu_5fl_Np4:
    mgproc="generate p p > mu+ mu- j j j j @4"
    name='Zmumu_Np4'
    process="pp>mu+mu-"
    
    gridpack_mode=True
    gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='pbs'
        cluster_queue='medium'
        nJobs=20
    
### Taus
elif runArgs.runNumber in Ztautau_5fl_Np01:
    mgproc="""
generate p p > ta+ ta- @0
add process p p > ta+ ta- j @1
"""
    name='Ztautau_Np01'
    process="pp>ta+ta-"
elif runArgs.runNumber in Ztautau_5fl_Np2:
    mgproc="generate p p > ta+ ta- j j @2"
    name='Ztautau_Np2'
    process="pp>ta+ta-"
elif runArgs.runNumber in Ztautau_5fl_Np3:
    mgproc="generate p p > ta+ ta- j j j @3"
    name='Ztautau_Np3'
    process="pp>ta+ta-"
elif runArgs.runNumber in Ztautau_5fl_Np4:
    mgproc="generate p p > ta+ ta- j j j j @4"
    name='Ztautau_Np4'
    process="pp>ta+ta-"
    
    gridpack_mode=True
    gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='pbs'
        cluster_queue='medium'
        nJobs=20


### Neutrinos
elif runArgs.runNumber in Znunu_5fl_Np01:
    mgproc="""
generate p p > vl vl~ @0
add process p p > vl vl~ j @1
"""
    name='Znunu_Np01'
    process="pp>NEUTRINOS"
elif runArgs.runNumber in Znunu_5fl_Np2:
    mgproc="generate p p > vl vl~ j j @2"
    name='Znunu_Np2'
    process="pp>NEUTRINOS"
elif runArgs.runNumber in Znunu_5fl_Np3:
    mgproc="generate p p > vl vl~ j j j @3"
    name='Znunu_Np3'
    process="pp>NEUTRINOS"
elif runArgs.runNumber in Znunu_5fl_Np4:
    mgproc="generate p p > vl vl~ j j j j @4"
    name='Znunu_Np4'
    process="pp>NEUTRINOS"
    
    gridpack_mode=True
    gridpack_dir='madevent/'

    if doCluster:
        mode=1
        cluster_type='lsf'
        cluster_queue='8nh'
        nJobs=20
    
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")




#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version'  : '2.0',
           'cut_decays'   : 'F', 
           'pdlabel'      : "'nn23lo1'",
#           'pdlabel'      : "'lhapdf'",
#           'lhaid'        : 247000,
           'maxjetflavor' : maxjetflavor,
           'asrwgtflavor' : maxjetflavor,
           'ickkw'        : 0,
           'ptj'          : 20,
           'ptb'          : 20,
           'mmll'         : mllcut,      
           'mmjj'         : 0,
           'drjj'         : 0,
           'drll'         : 0,
           'drjl'         : 0.4,
           'ptl'          : 0,
           'etal'         : 10,
           'etab'         : 6,
           'etaj'         : 6,
           'ktdurham'     : ktdurham,    
           'dparameter'   : dparameter  }
if runArgs.runNumber in (Zee_5fl_Np01+Zmumu_5fl_Np01+Ztautau_5fl_Np01+Znunu_5fl_Np01):
    extras['ptllmin']=75.0
    extras['ptllmax']=13000.0
elif not (runArgs.runNumber in Znunu_5fl):
    extras['ptllmin']=100.0
    extras['ptllmax']=13000.0
elif (runArgs.runNumber in Znunu_5fl):
    extras['ptllmin']=75.0
    extras['ptllmax']=13000.0

# Generate more lhe events to avoid pythia8 failures
build_run_card(run_card_old=get_default_runcard(),run_card_new='run_card.dat', 
               nevts=nevents*3,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)

print_cards()
process_dir = new_process(grid_pack=gridpack_dir)
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,
         grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,cluster_type=cluster_type,cluster_queue=cluster_queue,
         nevents=nevents,random_seed=runArgs.randomSeed)
arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz')



#### Shower 
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=['Z','electron','jets','drellYan']
evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")    
include("MC15JobOptions/Pythia8_MadGraph.py")


PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process                                                      
PYTHIA8_nQuarksMerge=maxjetflavor
include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")                   

include("MC15JobOptions/VBFMjjIntervalFilter.py")
        
filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0 
filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 35.*GeV
filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 35.*GeV
filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
filtSeq.VBFMjjIntervalFilter.LowMjj = 800.*GeV
filtSeq.VBFMjjIntervalFilter.HighMjj = 14000.*GeV

filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = False
filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.ApplyWeighting = False;   
filtSeq.VBFMjjIntervalFilter.ApplyDphi = True; 
    
# MET filter - only MET filter for Znunu
if runArgs.runNumber in Znunu_5fl:
    from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
    filtSeq += MissingEtFilter("MissingEtFilter")
    filtSeq.MissingEtFilter.METCut = 100*GeV




