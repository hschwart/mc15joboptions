#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937464E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03989303E+01   # W+
        25     1.25822324E+02   # h
        35     3.00011564E+03   # H
        36     2.99999995E+03   # A
        37     3.00110735E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03125236E+03   # ~d_L
   2000001     3.02582830E+03   # ~d_R
   1000002     3.03032790E+03   # ~u_L
   2000002     3.02817237E+03   # ~u_R
   1000003     3.03125236E+03   # ~s_L
   2000003     3.02582830E+03   # ~s_R
   1000004     3.03032790E+03   # ~c_L
   2000004     3.02817237E+03   # ~c_R
   1000005     7.37281974E+02   # ~b_1
   2000005     3.02530211E+03   # ~b_2
   1000006     7.34575562E+02   # ~t_1
   2000006     3.02169784E+03   # ~t_2
   1000011     3.00701927E+03   # ~e_L
   2000011     3.00080318E+03   # ~e_R
   1000012     3.00561394E+03   # ~nu_eL
   1000013     3.00701927E+03   # ~mu_L
   2000013     3.00080318E+03   # ~mu_R
   1000014     3.00561394E+03   # ~nu_muL
   1000015     2.98613849E+03   # ~tau_1
   2000015     3.02223171E+03   # ~tau_2
   1000016     3.00581876E+03   # ~nu_tauL
   1000021     2.33844810E+03   # ~g
   1000022     5.06397725E+01   # ~chi_10
   1000023     1.10450859E+02   # ~chi_20
   1000025    -3.00169139E+03   # ~chi_30
   1000035     3.00186398E+03   # ~chi_40
   1000024     1.10602221E+02   # ~chi_1+
   1000037     3.00272305E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890692E-01   # N_11
  1  2     1.91384327E-03   # N_12
  1  3    -1.46527401E-02   # N_13
  1  4     4.89056870E-04   # N_14
  2  1    -1.53099948E-03   # N_21
  2  2     9.99658589E-01   # N_22
  2  3     2.60801014E-02   # N_23
  2  4    -4.35079767E-04   # N_24
  3  1     1.00463921E-02   # N_31
  3  2    -1.81163645E-02   # N_32
  3  3     7.06792230E-01   # N_33
  3  4     7.07117820E-01   # N_34
  4  1    -1.07392184E-02   # N_41
  4  2     1.87307097E-02   # N_42
  4  3    -7.06788419E-01   # N_43
  4  4     7.07095440E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99321231E-01   # U_11
  1  2     3.68385181E-02   # U_12
  2  1    -3.68385181E-02   # U_21
  2  2     9.99321231E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999810E-01   # V_11
  1  2     6.15811113E-04   # V_12
  2  1     6.15811113E-04   # V_21
  2  2    -9.99999810E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98597670E-01   # cos(theta_t)
  1  2    -5.29404710E-02   # sin(theta_t)
  2  1     5.29404710E-02   # -sin(theta_t)
  2  2     9.98597670E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99708199E-01   # cos(theta_b)
  1  2    -2.41560935E-02   # sin(theta_b)
  2  1     2.41560935E-02   # -sin(theta_b)
  2  2     9.99708199E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06874584E-01   # cos(theta_tau)
  1  2     7.07338902E-01   # sin(theta_tau)
  2  1    -7.07338902E-01   # -sin(theta_tau)
  2  2    -7.06874584E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00166147E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39374638E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44248634E+02   # higgs               
         4     1.08311050E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39374638E+03  # The gauge couplings
     1     3.61863700E-01   # gprime(Q) DRbar
     2     6.41483296E-01   # g(Q) DRbar
     3     1.02800000E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39374638E+03  # The trilinear couplings
  1  1     1.83471872E-06   # A_u(Q) DRbar
  2  2     1.83474775E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39374638E+03  # The trilinear couplings
  1  1     6.43620112E-07   # A_d(Q) DRbar
  2  2     6.43699778E-07   # A_s(Q) DRbar
  3  3     1.35534801E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39374638E+03  # The trilinear couplings
  1  1     3.10921560E-07   # A_e(Q) DRbar
  2  2     3.10937260E-07   # A_mu(Q) DRbar
  3  3     3.15443286E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39374638E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54774016E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39374638E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.00627035E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39374638E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03783704E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39374638E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.60260998E+04   # M^2_Hd              
        22    -9.09621052E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42844579E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.65196659E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48035234E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48035234E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51964766E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51964766E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.72520980E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.18543138E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.05994989E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.82150697E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19506650E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.52390742E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.34861281E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.72932724E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.15992305E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.89273145E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69813837E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63217985E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.21686565E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.48445392E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.28661735E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.53124700E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.34009126E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.64348402E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.94404427E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -6.61380344E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.21906863E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.13868850E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.24686363E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.21438235E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.70530467E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.10530533E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.61494355E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11783231E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97499013E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65166563E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80904373E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.89615492E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.30747153E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.85158774E-11    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.98111257E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19444208E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58860338E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.71851794E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.72034386E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.45388659E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41139278E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12287611E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.73432449E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65291954E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.92184605E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.10780661E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.30168638E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.84922176E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.98804838E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67570254E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52838025E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.05997613E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.36356337E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.53650323E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54716089E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11783231E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97499013E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65166563E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80904373E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.89615492E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.30747153E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.85158774E-11    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.98111257E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19444208E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58860338E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.71851794E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.72034386E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.45388659E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41139278E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12287611E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.73432449E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65291954E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.92184605E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.10780661E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.30168638E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.84922176E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.98804838E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67570254E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52838025E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.05997613E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.36356337E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.53650323E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54716089E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07033099E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.68049483E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00855078E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.87466649E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.69983469E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02339966E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.69193199E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56223501E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997661E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.33945061E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.07033099E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.68049483E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00855078E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.87466649E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.69983469E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02339966E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.69193199E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56223501E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997661E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.33945061E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.84513399E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43701754E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18645269E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37652977E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78562640E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.51740857E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15935661E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.57037803E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.93405615E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32296751E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.22656941E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07067628E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.54471719E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01729108E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.16320315E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.15491941E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02823718E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.47425321E-13    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07067628E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.54471719E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01729108E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.16320315E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.15491941E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02823718E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.47425321E-13    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07129281E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.54392288E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01704086E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.28747991E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.28415701E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02856471E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.11847108E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.42299082E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39922462E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.74511822E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39922462E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.74511822E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06872465E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.81712779E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06872465E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.81712779E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06410099E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.62000097E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.44803117E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.50990924E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10968155E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.16411413E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.93715819E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.00891825E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.31034426E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.34186298E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.79394052E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.07533790E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     3.82187777E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.07533790E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     3.82187777E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     8.40348131E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.37954284E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.37954284E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.37163842E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     5.94153997E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     5.94153997E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     5.94583260E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     9.66718942E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.03871782E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.17455681E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.99095895E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.99095895E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.70363376E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.64352272E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.30206093E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.30206093E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.12478118E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.12478118E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.85849182E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.85849182E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.85849182E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.85849182E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.02249337E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.02249337E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.60749426E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.14767385E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.98968117E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.02826710E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.02826710E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14449228E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.76549746E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.23942552E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.23942552E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.17020128E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.17020128E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.68746370E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.68746370E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     4.68746370E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.68746370E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.72836784E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.72836784E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.74880522E-03   # h decays
#          BR         NDA      ID1       ID2
     5.52795852E-01    2           5        -5   # BR(h -> b       bb     )
     6.96306749E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.46490770E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.22511686E-04    2           3        -3   # BR(h -> s       sb     )
     2.27024288E-02    2           4        -4   # BR(h -> c       cb     )
     7.46492563E-02    2          21        21   # BR(h -> g       g      )
     2.57705361E-03    2          22        22   # BR(h -> gam     gam    )
     1.79272739E-03    2          22        23   # BR(h -> Z       gam    )
     2.44141246E-01    2          24       -24   # BR(h -> W+      W-     )
     3.09165460E-02    2          23        23   # BR(h -> Z       Z      )
     2.52128044E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     3.89846293E+01   # H decays
#          BR         NDA      ID1       ID2
     9.04480710E-01    2           5        -5   # BR(H -> b       bb     )
     6.37890353E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.25542785E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.76966848E-04    2           3        -3   # BR(H -> s       sb     )
     7.76392355E-08    2           4        -4   # BR(H -> c       cb     )
     7.74501284E-03    2           6        -6   # BR(H -> t       tb     )
     6.50435262E-06    2          21        21   # BR(H -> g       g      )
     5.16190548E-08    2          22        22   # BR(H -> gam     gam    )
     3.16939392E-09    2          23        22   # BR(H -> Z       gam    )
     7.67127042E-07    2          24       -24   # BR(H -> W+      W-     )
     3.83090514E-07    2          23        23   # BR(H -> Z       Z      )
     5.16016197E-06    2          25        25   # BR(H -> h       h      )
    -1.49401169E-24    2          36        36   # BR(H -> A       A      )
     6.09788288E-19    2          23        36   # BR(H -> Z       A      )
     8.75252717E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.95518847E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.39110265E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.63857310E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.18465271E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.48535004E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.81359738E+01   # A decays
#          BR         NDA      ID1       ID2
     9.24620665E-01    2           5        -5   # BR(A -> b       bb     )
     6.52065156E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.30554335E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.83168598E-04    2           3        -3   # BR(A -> s       sb     )
     7.99061646E-08    2           4        -4   # BR(A -> c       cb     )
     7.96676569E-03    2           6        -6   # BR(A -> t       tb     )
     2.34613625E-05    2          21        21   # BR(A -> g       g      )
     4.81448241E-08    2          22        22   # BR(A -> gam     gam    )
     2.31028628E-08    2          23        22   # BR(A -> Z       gam    )
     7.81164490E-07    2          23        25   # BR(A -> Z       h      )
     9.02603487E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.07481107E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.52823192E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.71762279E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.14164736E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47355402E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.00638269E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12371042E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.43073416E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.24804943E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.56763901E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.26832210E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.20538300E-07    2          24        25   # BR(H+ -> W+      h      )
     5.29071210E-14    2          24        36   # BR(H+ -> W+      A      )
     5.01084501E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.17383356E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06375571E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
