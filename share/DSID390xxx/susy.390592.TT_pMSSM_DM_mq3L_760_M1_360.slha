#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068271E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.90900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04165616E+01   # W+
        25     1.25483365E+02   # h
        35     4.00001097E+03   # H
        36     3.99999732E+03   # A
        37     4.00102301E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02603845E+03   # ~d_L
   2000001     4.02259933E+03   # ~d_R
   1000002     4.02538363E+03   # ~u_L
   2000002     4.02366756E+03   # ~u_R
   1000003     4.02603845E+03   # ~s_L
   2000003     4.02259933E+03   # ~s_R
   1000004     4.02538363E+03   # ~c_L
   2000004     4.02366756E+03   # ~c_R
   1000005     8.36124769E+02   # ~b_1
   2000005     4.02545346E+03   # ~b_2
   1000006     8.25021952E+02   # ~t_1
   2000006     2.27853777E+03   # ~t_2
   1000011     4.00474306E+03   # ~e_L
   2000011     4.00310827E+03   # ~e_R
   1000012     4.00362697E+03   # ~nu_eL
   1000013     4.00474306E+03   # ~mu_L
   2000013     4.00310827E+03   # ~mu_R
   1000014     4.00362697E+03   # ~nu_muL
   1000015     4.00432588E+03   # ~tau_1
   2000015     4.00778045E+03   # ~tau_2
   1000016     4.00504517E+03   # ~nu_tauL
   1000021     1.98157016E+03   # ~g
   1000022     3.48691950E+02   # ~chi_10
   1000023    -4.02229306E+02   # ~chi_20
   1000025     4.15459001E+02   # ~chi_30
   1000035     2.05236639E+03   # ~chi_40
   1000024     3.99853048E+02   # ~chi_1+
   1000037     2.05253097E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.43806573E-01   # N_11
  1  2    -1.54422467E-02   # N_12
  1  3    -4.05767404E-01   # N_13
  1  4    -3.50862962E-01   # N_14
  2  1    -4.34624453E-02   # N_21
  2  2     2.40529203E-02   # N_22
  2  3    -7.03943893E-01   # N_23
  2  4     7.08516386E-01   # N_24
  3  1     5.34883503E-01   # N_31
  3  2     2.82928057E-02   # N_32
  3  3     5.82908064E-01   # N_33
  3  4     6.10997008E-01   # N_34
  4  1    -1.05854350E-03   # N_41
  4  2     9.99190928E-01   # N_42
  4  3    -5.83087621E-03   # N_43
  4  4    -3.97790043E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.25192775E-03   # U_11
  1  2     9.99965952E-01   # U_12
  2  1    -9.99965952E-01   # U_21
  2  2     8.25192775E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62665712E-02   # V_11
  1  2    -9.98415782E-01   # V_12
  2  1    -9.98415782E-01   # V_21
  2  2    -5.62665712E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96261570E-01   # cos(theta_t)
  1  2    -8.63879861E-02   # sin(theta_t)
  2  1     8.63879861E-02   # -sin(theta_t)
  2  2     9.96261570E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999230E-01   # cos(theta_b)
  1  2    -1.24096713E-03   # sin(theta_b)
  2  1     1.24096713E-03   # -sin(theta_b)
  2  2     9.99999230E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05989030E-01   # cos(theta_tau)
  1  2     7.08222768E-01   # sin(theta_tau)
  2  1    -7.08222768E-01   # -sin(theta_tau)
  2  2    -7.05989030E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00259748E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30682707E+03  # DRbar Higgs Parameters
         1    -3.90900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43712961E+02   # higgs               
         4     1.61785631E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30682707E+03  # The gauge couplings
     1     3.61908670E-01   # gprime(Q) DRbar
     2     6.36101929E-01   # g(Q) DRbar
     3     1.03009391E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30682707E+03  # The trilinear couplings
  1  1     1.38112460E-06   # A_u(Q) DRbar
  2  2     1.38113788E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30682707E+03  # The trilinear couplings
  1  1     5.08924310E-07   # A_d(Q) DRbar
  2  2     5.08971670E-07   # A_s(Q) DRbar
  3  3     9.09942554E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30682707E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11356804E-07   # A_mu(Q) DRbar
  3  3     1.12488158E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30682707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65989279E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30682707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84400909E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30682707E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03197578E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30682707E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57388173E+07   # M^2_Hd              
        22    -1.57265033E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40380886E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.72614655E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44955421E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44955421E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55044579E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55044579E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.92930515E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.35976890E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.26272793E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.27422354E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10327963E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.34464388E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.54563204E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.15723392E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.26337369E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.72444846E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.29524107E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.10153105E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.24790582E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.38708412E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.48341584E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.00640868E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.26616518E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.05138070E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.32139804E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.61194724E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.80152740E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66947314E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54138988E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79901849E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60878761E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.18363151E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61493073E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.34645140E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13591715E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.65426838E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.22669443E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.74691170E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     7.00053268E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78923345E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.79769802E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.13922054E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.43849861E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.79551815E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.42008218E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.57893868E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52455835E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61210913E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.90891930E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03205384E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.56107955E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.43542101E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45196772E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78944018E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64787408E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.92657333E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.57331429E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80069507E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16584355E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.61135290E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52673391E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54491869E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01944718E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.69159219E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.07128623E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.95635786E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85707317E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78923345E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.79769802E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.13922054E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.43849861E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.79551815E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.42008218E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.57893868E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52455835E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61210913E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.90891930E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03205384E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.56107955E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.43542101E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45196772E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78944018E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64787408E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.92657333E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.57331429E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80069507E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16584355E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.61135290E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52673391E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54491869E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01944718E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.69159219E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.07128623E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.95635786E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85707317E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14376956E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.09018391E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.28872369E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.55083775E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77977874E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.84409367E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57426688E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05087406E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.13324047E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88291564E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.84792418E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.19356259E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14376956E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.09018391E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.28872369E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.55083775E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77977874E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.84409367E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57426688E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05087406E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.13324047E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88291564E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.84792418E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.19356259E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08606140E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.57185656E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44409400E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.21422693E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40359188E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51293066E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81462216E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08010551E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.63370935E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.04950343E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.95865771E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43323706E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95423336E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87402220E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14482445E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23915730E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19480365E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.82043390E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78367170E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.18005318E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55137904E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14482445E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23915730E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19480365E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.82043390E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78367170E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.18005318E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55137904E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46972031E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12353574E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08332450E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.46397809E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52518367E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.58148532E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03590101E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.31559004E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33547698E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33547698E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11184067E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11184067E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10536469E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.59860988E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.66791449E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.53865368E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.09701979E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.52308833E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.92270243E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.38123241E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.01027535E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.27935036E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.29773617E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17925350E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52969953E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17925350E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52969953E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42247597E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50788597E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50788597E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47805401E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01308421E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01308421E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01308405E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.31318731E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.31318731E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.31318731E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.31318731E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.71063115E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.71063115E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.71063115E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.71063115E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.97178569E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.97178569E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.45644869E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.35282551E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.72531563E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.59202883E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.50895734E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.59202883E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.50895734E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     8.14527950E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.91793119E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.91793119E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.91666950E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.91680508E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.91680508E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.91679567E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.97104127E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.74572892E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.97104127E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.74572892E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.71505476E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.77637483E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.77637483E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.59979398E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.55091448E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.55091448E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.55091451E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.62770853E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.62770853E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.62770853E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.62770853E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.54255385E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.54255385E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.54255385E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.54255385E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.44761579E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.44761579E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.59713872E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.10161405E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.57533137E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.91247675E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.91826670E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.91826670E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.39316933E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49191861E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.55973070E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.79499506E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79499506E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.80873148E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.80873148E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.01014346E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92710976E-01    2           5        -5   # BR(h -> b       bb     )
     6.49413943E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29892323E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.87579735E-04    2           3        -3   # BR(h -> s       sb     )
     2.11768715E-02    2           4        -4   # BR(h -> c       cb     )
     6.92666953E-02    2          21        21   # BR(h -> g       g      )
     2.38612925E-03    2          22        22   # BR(h -> gam     gam    )
     1.62763715E-03    2          22        23   # BR(h -> Z       gam    )
     2.19454485E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77183396E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49898551E+01   # H decays
#          BR         NDA      ID1       ID2
     3.58212586E-01    2           5        -5   # BR(H -> b       bb     )
     6.02948262E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13187901E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52164498E-04    2           3        -3   # BR(H -> s       sb     )
     7.07250522E-08    2           4        -4   # BR(H -> c       cb     )
     7.08812282E-03    2           6        -6   # BR(H -> t       tb     )
     8.71892262E-07    2          21        21   # BR(H -> g       g      )
     1.06761585E-09    2          22        22   # BR(H -> gam     gam    )
     1.82277161E-09    2          23        22   # BR(H -> Z       gam    )
     1.74004806E-06    2          24       -24   # BR(H -> W+      W-     )
     8.69421235E-07    2          23        23   # BR(H -> Z       Z      )
     6.91487294E-06    2          25        25   # BR(H -> h       h      )
     4.21358415E-24    2          36        36   # BR(H -> A       A      )
     6.82170650E-20    2          23        36   # BR(H -> Z       A      )
     1.86111947E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.58626183E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.58626183E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.29392929E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.78501950E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.44051058E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.57587325E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     9.13911535E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.50967524E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.22093358E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.21871436E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.18669676E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.15419918E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.10932579E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.10932579E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.38164206E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49820971E+01   # A decays
#          BR         NDA      ID1       ID2
     3.58288615E-01    2           5        -5   # BR(A -> b       bb     )
     6.03035835E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13218697E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52242089E-04    2           3        -3   # BR(A -> s       sb     )
     7.11943223E-08    2           4        -4   # BR(A -> c       cb     )
     7.10291124E-03    2           6        -6   # BR(A -> t       tb     )
     1.45946829E-05    2          21        21   # BR(A -> g       g      )
     6.24904499E-08    2          22        22   # BR(A -> gam     gam    )
     1.60417046E-08    2          23        22   # BR(A -> Z       gam    )
     1.73663735E-06    2          23        25   # BR(A -> Z       h      )
     1.88288906E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.58637835E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.58637835E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.98997142E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.37662741E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.22053244E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.11288910E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.51302311E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.72321320E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.35573064E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.55033845E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.63540823E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.26643466E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.26643466E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51434895E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.76116883E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01425066E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12649169E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.68714492E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20469750E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47845017E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.66780372E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.73349338E-06    2          24        25   # BR(H+ -> W+      h      )
     2.70905766E-14    2          24        36   # BR(H+ -> W+      A      )
     5.75409043E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.60330810E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.78346421E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.58389943E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.22720984E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57276240E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.08129426E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.15361184E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.03383944E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )
