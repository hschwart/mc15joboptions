from MadGraphControl.MadGraphUtils import *

evgenConfig.description = "MadGraph5+Pythia8 for 4top quarks from a pair of heavy resonances in 2UED-RPP with Mkk=2000 GeV"
evgenConfig.keywords+=['quark', 'exotic']
evgenConfig.inputfilecheck = '2UEDRPP2000GeV4top'
evgenConfig.process = "4 top quark production in 2UED-RPP with Mkk=2000 GeV"
evgenConfig.contact = ["Francesco Guescini : Francesco.Guescini@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
