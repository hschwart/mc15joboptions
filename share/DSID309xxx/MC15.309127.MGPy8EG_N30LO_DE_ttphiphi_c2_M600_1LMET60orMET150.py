
# Inverse of M scale
invMscale=0.00166667

# Wilson coefficients
c1=0.000000e+00
c2=1.000000e+00

evt_multiplier=2
filter_string="1LMET60orMET150"

include("MC15JobOptions/MadGraphControl_DarkEnergy_ttphiphi.py")
