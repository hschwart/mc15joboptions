#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05405926E+01   # W+
        25     1.25000000E+02   # h
        35     2.00410325E+03   # H
        36     2.00000000E+03   # A
        37     2.00157850E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013294E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989241E+03   # ~u_L
   2000002     4.99994929E+03   # ~u_R
   1000003     5.00013294E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989241E+03   # ~c_L
   2000004     4.99994929E+03   # ~c_R
   1000005     4.99965309E+03   # ~b_1
   2000005     5.00050662E+03   # ~b_2
   1000006     4.99157831E+03   # ~t_1
   2000006     5.01286946E+03   # ~t_2
   1000011     5.00008223E+03   # ~e_L
   2000011     5.00007607E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008223E+03   # ~mu_L
   2000013     5.00007607E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99979938E+03   # ~tau_1
   2000015     5.00035953E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.00239664E+03   # ~chi_10
   1000023    -1.05009580E+03   # ~chi_20
   1000025     1.08955952E+03   # ~chi_30
   1000035     3.00313965E+03   # ~chi_40
   1000024     1.04698058E+03   # ~chi_1+
   1000037     3.00313820E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.14711637E-01   # N_11
  1  2    -2.71047556E-02   # N_12
  1  3     4.96479699E-01   # N_13
  1  4    -4.91894822E-01   # N_14
  2  1     4.16759279E-03   # N_21
  2  2    -3.83357464E-03   # N_22
  2  3    -7.06934140E-01   # N_23
  2  4    -7.07256711E-01   # N_24
  3  1     6.99406245E-01   # N_31
  3  2     2.90079147E-02   # N_32
  3  3    -5.03099248E-01   # N_33
  3  4     5.06833891E-01   # N_34
  4  1     9.00972598E-04   # N_41
  4  2    -9.99204272E-01   # N_42
  4  3    -2.53609166E-02   # N_43
  4  4     3.07706897E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -3.58459434E-02   # U_11
  1  2     9.99357328E-01   # U_12
  2  1     9.99357328E-01   # U_21
  2  2     3.58459434E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.34984648E-02   # V_11
  1  2     9.99053494E-01   # V_12
  2  1     9.99053494E-01   # V_21
  2  2     4.34984648E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08050147E-01   # cos(theta_t)
  1  2     7.06162155E-01   # sin(theta_t)
  2  1    -7.06162155E-01   # -sin(theta_t)
  2  2     7.08050147E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.61042446E-01   # cos(theta_b)
  1  2     7.50348509E-01   # sin(theta_b)
  2  1    -7.50348509E-01   # -sin(theta_b)
  2  2     6.61042446E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03207366E-01   # cos(theta_tau)
  1  2     7.10984810E-01   # sin(theta_tau)
  2  1    -7.10984810E-01   # -sin(theta_tau)
  2  2     7.03207366E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90206568E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52099526E+02   # vev(Q)              
         4     3.50289101E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52814069E-01   # gprime(Q) DRbar
     2     6.27373183E-01   # g(Q) DRbar
     3     1.08213375E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02578403E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71844407E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79827325E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.81555261E+06   # M^2_Hd              
        22    -6.45705556E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37137150E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.98481777E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.92742132E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.49366747E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.58722423E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.21592932E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.30590296E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.17323849E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.89916144E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.46198800E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.20503236E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.21592932E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.30590296E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.17323849E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.89916144E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.46198800E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.20503236E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     6.29420180E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.91963751E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.27378523E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     9.63161789E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.29633714E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.70653664E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.60052825E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.60052825E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.60052825E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.60052825E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.08016603E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.08016603E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.19533265E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.04927822E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.05180509E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.07919332E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07527897E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.09585519E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.13587286E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.31837738E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.09428565E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.73176040E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05231381E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.41267689E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.41978638E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.00924339E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82386624E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.29963380E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.34831711E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.83547359E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.31762198E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.68820563E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.30535271E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.07485387E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.62629734E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.45591257E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.54758352E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.31338763E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.07331137E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.94419631E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.83612055E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.60255201E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.69534479E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.79065231E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22425584E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.94042832E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.23370924E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.97652864E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.47819857E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.93975176E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.95665693E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.92486175E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.08232193E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.40133070E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.09892112E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.26493082E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.69897128E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.53336558E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22430380E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.03112636E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65356160E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.03713429E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.48101849E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.99644951E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.96182387E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.92535438E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00949433E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.22100900E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.09814809E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.86764739E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.40157942E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87911129E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22425584E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.94042832E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.23370924E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.97652864E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.47819857E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.93975176E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.95665693E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.92486175E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.08232193E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.40133070E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.09892112E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.26493082E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.69897128E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.53336558E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22430380E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.03112636E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65356160E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.03713429E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.48101849E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.99644951E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.96182387E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.92535438E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00949433E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.22100900E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.09814809E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.86764739E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.40157942E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87911129E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.97058715E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.52933811E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.33658077E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.06644232E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68600731E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.54829339E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.37912026E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26448691E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.14624320E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73559389E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.85357961E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.62771338E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.97058715E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.52933811E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.33658077E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.06644232E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68600731E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.54829339E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.37912026E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26448691E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.14624320E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73559389E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.85357961E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.62771338E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62157045E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.78731010E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47907275E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.66953720E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.50165447E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.83561482E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.00729090E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.37210632E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62268019E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62888493E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.88919205E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.73585756E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54130938E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     3.89567215E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08666938E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.97059440E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.11742160E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.29760978E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.93523600E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69103264E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.27979242E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.37499448E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.97059440E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.11742160E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.29760978E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.93523600E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69103264E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.27979242E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.37499448E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97353282E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11631737E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.29533930E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.92739446E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.68837338E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.26475822E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.36969269E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.10629260E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.40736101E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.32036908E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.30388506E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10679104E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10676016E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09812105E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55735957E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53026173E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13650899E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46355638E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.33022411E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53937001E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.87783944E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     5.55086890E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00992209E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87916828E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10909630E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.25614010E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.35687215E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.59820407E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.19163929E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.31917226E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18010421E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52797111E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17205111E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52775296E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38954777E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48923130E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48910544E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45370205E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.96793484E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.96793484E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.96793484E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     8.62203157E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     8.62203157E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.12700372E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.12700372E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.87401072E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.87401072E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.85759045E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.85759045E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.53794785E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.53794785E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.29043527E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.61520740E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.31815202E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.00377958E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.59937154E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.28664591E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18415783E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42248172E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.39108715E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42635696E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.14956349E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.55186690E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.55183731E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.68097649E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.90901456E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.90901456E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.90901456E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.50140181E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.23876165E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.47812131E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.23810367E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.82613269E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.39596616E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.39561351E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     7.29612526E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.47695061E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.47695061E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.47695061E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31135479E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31135479E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30420679E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30420679E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37117580E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37117580E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37104180E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37104180E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33358962E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33358962E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55739722E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.07447487E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.50711268E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.24635506E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46339575E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46339575E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16090146E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.12027946E-07    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.29063340E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.35899584E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89035028E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.98665194E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.14713391E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.45803054E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.16562208E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08500674E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17682643E-01    2           5        -5   # BR(h -> b       bb     )
     6.37600686E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25687160E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78251339E-04    2           3        -3   # BR(h -> s       sb     )
     2.06323940E-02    2           4        -4   # BR(h -> c       cb     )
     6.69940618E-02    2          21        21   # BR(h -> g       g      )
     2.30498939E-03    2          22        22   # BR(h -> gam     gam    )
     1.53773136E-03    2          22        23   # BR(h -> Z       gam    )
     2.00762759E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56214140E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78123905E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47111176E-03    2           5        -5   # BR(H -> b       bb     )
     2.46422778E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71196262E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11544219E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667517E-05    2           4        -4   # BR(H -> c       cb     )
     9.96061085E-01    2           6        -6   # BR(H -> t       tb     )
     7.97644209E-04    2          21        21   # BR(H -> g       g      )
     2.70400645E-06    2          22        22   # BR(H -> gam     gam    )
     1.15983060E-06    2          23        22   # BR(H -> Z       gam    )
     3.36038956E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67561221E-04    2          23        23   # BR(H -> Z       Z      )
     9.04218354E-04    2          25        25   # BR(H -> h       h      )
     8.27995066E-24    2          36        36   # BR(H -> A       A      )
     3.31919359E-11    2          23        36   # BR(H -> Z       A      )
     7.13292702E-12    2          24       -37   # BR(H -> W+      H-     )
     7.13292702E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382915E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47404508E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897309E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62265093E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677204E-06    2           3        -3   # BR(A -> s       sb     )
     9.96174802E-06    2           4        -4   # BR(A -> c       cb     )
     9.96994310E-01    2           6        -6   # BR(A -> t       tb     )
     9.43674055E-04    2          21        21   # BR(A -> g       g      )
     3.25325120E-06    2          22        22   # BR(A -> gam     gam    )
     1.35242889E-06    2          23        22   # BR(A -> Z       gam    )
     3.27507173E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74484764E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36432939E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237833E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145774E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50318222E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45692287E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731256E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99400892E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35682105E-04    2          24        25   # BR(H+ -> W+      h      )
     3.44135311E-13    2          24        36   # BR(H+ -> W+      A      )
