#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.35392685E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.35000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413697E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414079E+03   # H
        36     2.00000000E+03   # A
        37     2.00176806E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013261E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989270E+03   # ~u_L
   2000002     4.99994938E+03   # ~u_R
   1000003     5.00013261E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989270E+03   # ~c_L
   2000004     4.99994938E+03   # ~c_R
   1000005     4.99953154E+03   # ~b_1
   2000005     5.00062777E+03   # ~b_2
   1000006     4.98853166E+03   # ~t_1
   2000006     5.01589953E+03   # ~t_2
   1000011     5.00008199E+03   # ~e_L
   2000011     5.00007593E+03   # ~e_R
   1000012     4.99984208E+03   # ~nu_eL
   1000013     5.00008199E+03   # ~mu_L
   2000013     5.00007593E+03   # ~mu_R
   1000014     4.99984208E+03   # ~nu_muL
   1000015     4.99971967E+03   # ~tau_1
   2000015     5.00043885E+03   # ~tau_2
   1000016     4.99984208E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.34321949E+03   # ~chi_10
   1000023    -1.35207966E+03   # ~chi_20
   1000025     1.36126301E+03   # ~chi_30
   1000035     3.00152401E+03   # ~chi_40
   1000024     1.35123296E+03   # ~chi_1+
   1000037     3.00152373E+03   # ~chi_2+
   1000039     9.46839762E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.06336635E-01   # N_11
  1  2    -6.96553253E-03   # N_12
  1  3    -5.69308341E-01   # N_13
  1  4    -5.55153474E-01   # N_14
  2  1     1.60964125E-02   # N_21
  2  2    -1.77903356E-02   # N_22
  2  3     7.06794428E-01   # N_23
  2  4    -7.07012055E-01   # N_24
  3  1     7.95044980E-01   # N_31
  3  2     6.32666031E-03   # N_32
  3  3     4.19874038E-01   # N_33
  3  4     4.37686241E-01   # N_34
  4  1     5.20266768E-04   # N_41
  4  2    -9.99797459E-01   # N_42
  4  3    -5.95337973E-03   # N_43
  4  4     1.92179059E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -8.40964948E-03   # U_11
  1  2     9.99964638E-01   # U_12
  2  1     9.99964638E-01   # U_21
  2  2     8.40964948E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.71787029E-02   # V_11
  1  2    -9.99630591E-01   # V_12
  2  1     9.99630591E-01   # V_21
  2  2     2.71787029E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07838318E-01   # cos(theta_t)
  1  2    -7.06374487E-01   # sin(theta_t)
  2  1     7.06374487E-01   # -sin(theta_t)
  2  2     7.07838318E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.71610256E-01   # cos(theta_b)
  1  2     7.40904625E-01   # sin(theta_b)
  2  1    -7.40904625E-01   # -sin(theta_b)
  2  2    -6.71610256E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04119952E-01   # cos(theta_tau)
  1  2     7.10081047E-01   # sin(theta_tau)
  2  1    -7.10081047E-01   # -sin(theta_tau)
  2  2    -7.04119952E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90193672E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.35000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51901882E+02   # vev(Q)              
         4     4.46848670E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52764470E-01   # gprime(Q) DRbar
     2     6.27058248E-01   # g(Q) DRbar
     3     1.08619132E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02634637E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72638201E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79720032E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.35392685E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.01043391E+06   # M^2_Hd              
        22    -7.18113101E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36996193E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.17263825E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.11909192E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.43736935E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.81110870E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.89597886E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.49457790E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.25513958E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.02127267E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.29971572E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.81110870E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.89597886E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.49457790E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.25513958E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.02127267E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.29971572E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.10314394E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.92389112E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.57398090E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.05128259E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.05128259E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.05128259E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.05128259E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.24596819E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.58412269E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.20667579E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.57766207E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.27913356E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.17284154E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.55453041E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.66250339E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.35262422E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.04674863E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.66422220E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.61228240E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.08008244E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.73397239E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.15610708E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67065849E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.60809515E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.87169065E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.57868204E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.00158622E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.35936364E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.09653947E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.72324374E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.43610915E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.67875193E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.33687422E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.57581027E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.74645268E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.56927309E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.25688957E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.14375779E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.21939827E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.43926763E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.59998029E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.45264732E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.60452545E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.17508035E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.83698325E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.34928158E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02278961E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.29224468E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.46841149E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03272141E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.51404694E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.14888739E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60165083E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.43930146E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.71378977E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.88272892E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.35396123E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.17676927E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.41849946E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.35442462E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02324475E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22380640E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.78408120E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66131255E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.47867382E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.32690170E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89734582E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.43926763E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.59998029E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.45264732E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.60452545E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.17508035E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.83698325E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.34928158E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02278961E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.29224468E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.46841149E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03272141E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.51404694E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.14888739E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60165083E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.43930146E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.71378977E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.88272892E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.35396123E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.17676927E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.41849946E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.35442462E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02324475E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22380640E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.78408120E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66131255E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.47867382E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.32690170E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89734582E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.93282603E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.40925015E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.37093406E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.17612785E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72556041E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     8.10444188E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45613918E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.12564191E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.68623678E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.59250214E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.31116943E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.28987202E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.93282603E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.40925015E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.37093406E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.17612785E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72556041E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     8.10444188E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45613918E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.12564191E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.68623678E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.59250214E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.31116943E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.28987202E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.52877519E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.98741537E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.69293048E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.30244988E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.56759821E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.76692593E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.13807653E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.58654166E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.53797584E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.85485280E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.18587766E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.35826288E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.58767983E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.05731021E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.17828841E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.93263756E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.95483057E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     4.12858826E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.11142650E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72850324E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.46496719E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.45199365E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.93263756E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.95483057E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     4.12858826E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.11142650E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72850324E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.46496719E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.45199365E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.93539866E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.94828869E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.12470481E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.11038106E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72593674E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.78645442E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44686407E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.18039363E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.30886462E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.53795820E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04248088E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17932596E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17830216E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.31046339E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.79156017E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53286589E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48704328E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46256174E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.90416226E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52711282E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.59313928E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.04654074E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.12154643E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.50745577E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.37099780E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.33752737E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     6.04373506E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01402143E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.90570512E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40058867E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81355496E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18021963E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80673636E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.14188136E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.13887364E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.20438055E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.27167330E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.27167330E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.27167330E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.95746341E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.95746341E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.98582126E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.98582126E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.84033130E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.84033130E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.81613917E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.83985373E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.27673547E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.03837524E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.16642942E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.10341877E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.46930591E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.01864293E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.46741255E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.88391013E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.68848584E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.68659990E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.20326394E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.53186016E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.53186016E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.53186016E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.96848905E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.84378310E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.53338362E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.83032725E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     8.77870511E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     8.77276443E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.93302050E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.75312026E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.75312026E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.75312026E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.55616345E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.55616345E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.41246066E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.41246066E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.18715924E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.18715924E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.18428367E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.18428367E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.45613440E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.45613440E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.79176334E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.25096874E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.47061957E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.38438172E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46584844E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46584844E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.73517251E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.98129881E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.77018145E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.10488747E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     3.48758239E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     3.66225315E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     5.31428375E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     7.16828455E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07826297E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17080647E-01    2           5        -5   # BR(h -> b       bb     )
     6.38630426E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26051650E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79024071E-04    2           3        -3   # BR(h -> s       sb     )
     2.06668685E-02    2           4        -4   # BR(h -> c       cb     )
     6.71057244E-02    2          21        21   # BR(h -> g       g      )
     2.30257067E-03    2          22        22   # BR(h -> gam     gam    )
     1.54031550E-03    2          22        23   # BR(h -> Z       gam    )
     2.01071973E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56637827E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78117004E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48082074E-03    2           5        -5   # BR(H -> b       bb     )
     2.46436148E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71243533E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11550017E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667102E-05    2           4        -4   # BR(H -> c       cb     )
     9.96058020E-01    2           6        -6   # BR(H -> t       tb     )
     7.97616967E-04    2          21        21   # BR(H -> g       g      )
     2.73916735E-06    2          22        22   # BR(H -> gam     gam    )
     1.16008998E-06    2          23        22   # BR(H -> Z       gam    )
     3.32142624E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65618417E-04    2          23        23   # BR(H -> Z       Z      )
     9.03392146E-04    2          25        25   # BR(H -> h       h      )
     8.79447728E-24    2          36        36   # BR(H -> A       A      )
     3.47392987E-11    2          23        36   # BR(H -> Z       A      )
     5.22933441E-12    2          24       -37   # BR(H -> W+      H-     )
     5.22933441E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82385016E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48358823E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895968E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62260355E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676579E-06    2           3        -3   # BR(A -> s       sb     )
     9.96169328E-06    2           4        -4   # BR(A -> c       cb     )
     9.96988831E-01    2           6        -6   # BR(A -> t       tb     )
     9.43668869E-04    2          21        21   # BR(A -> g       g      )
     3.01870349E-06    2          22        22   # BR(A -> gam     gam    )
     1.35268331E-06    2          23        22   # BR(A -> Z       gam    )
     3.23683833E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74519606E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38570697E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238248E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147242E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51686395E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45686200E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08730039E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404723E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.31829569E-04    2          24        25   # BR(H+ -> W+      h      )
     6.06615617E-13    2          24        36   # BR(H+ -> W+      A      )
