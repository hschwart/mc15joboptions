evgenConfig.description = "Inclusive bb->mu3.5mu3.5X production, 4.0<m(mumu)<6.5GeV"
evgenConfig.keywords = ["inclusive","bottom","2muon","bbbar"]
evgenConfig.minevents = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/MultiMuonFilter.py")
include("MC15JobOptions/DiLeptonMassFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] 
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# List of B- and C-species
genSeq.Pythia8B.BPDGCodes = [511,521,531,541,553,5122,5132,5232,5332,100553,200553,300553,-511,-521,-531,-541,-5122,-5132,-5232,-5332,
                             411,421,431,4122,4132,4232,4332,4432,4412,4422,-411,-421,-431,-4122,-4132,-4232,-4332,-4432,-4412,-4422]

genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.QuarkPtCut = 5.0
genSeq.Pythia8B.AntiQuarkPtCut = 5.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.NHadronizationLoops = 10
genSeq.Pythia8B.NDecayLoops = 200

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 3.0
genSeq.Pythia8B.MinimumCountPerCut = [2]

filtSeq.MultiMuonFilter.Ptcut = 3500.
filtSeq.MultiMuonFilter.Etacut = 3.0
filtSeq.MultiMuonFilter.NMuons = 2
filtSeq.DiLeptonMassFilter.MinPt = 3500.
filtSeq.DiLeptonMassFilter.MaxEta = 3.0
filtSeq.DiLeptonMassFilter.MinMass = 4000.
filtSeq.DiLeptonMassFilter.MaxMass = 6500.
filtSeq.DiLeptonMassFilter.MinDilepPt = 6000.
filtSeq.DiLeptonMassFilter.AllowSameCharge = False
