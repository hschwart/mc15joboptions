# JO for Pythia 8, leading charged particle filter, slice JZ8

evgenConfig.description = "Dijet events with the A14 NNPDF23 LO tune, leading charged particle filter, slice JZ8"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


genSeq.Pythia8.Commands += ["HardQCD:all = on", "PhaseSpace:pTHatMin = 60."]


from GeneratorFilters.GeneratorFiltersConf import LeadingChargedParticleFilter
if "LeadingChargedParticleFilter" not in filtSeq:
    filtSeq += LeadingChargedParticleFilter()


filtSeq.LeadingChargedParticleFilter.PtMin = 65*GeV
filtSeq.LeadingChargedParticleFilter.PtMax = 90*GeV
filtSeq.LeadingChargedParticleFilter.EtaCut = 2.60

evgenConfig.minevents = 1000
