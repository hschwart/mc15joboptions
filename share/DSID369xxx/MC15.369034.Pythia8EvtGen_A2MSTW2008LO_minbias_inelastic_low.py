#based on 361034
evgenConfig.description = "Low-pT inelastic minimum bias events for pile-up, with the A2 MSTW2008LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

evgenConfig.saveJets = True

include("MC15JobOptions/Pythia8_A2_MSTW2008LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += \
    ["SoftQCD:inelastic = on"]

#include("MC15JobOptions/JetFilter_MinbiasLow.py")
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)

from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

evgenConfig.minevents = 1000
