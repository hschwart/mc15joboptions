#################################################################################
# job options fragment for inclusive W^{+}->nutau10->mu4mu2mu1 for run2
#################################################################################
# Not only qq->W production, qq->Wg and gq->Wq production channels are considered
# in this fragments.
# thresholds: mu1>10.5 or 3.5GeV, mu2>5.5 or 3.5GeV, mu3> 2 or 3.5GeV, and D_s>10GeV
#################################################################################

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
evgenConfig.description = "PosW->nutau->3mu production"
evgenConfig.keywords = ["W", "muon", "tau", "BSM"]
evgenConfig.minevents = 5000
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "pp>W>nutau>3munu"

genSeq.Pythia8.Commands += ['WeakSingleBoson:ffbar2W = on']
genSeq.Pythia8.Commands += ['WeakBosonAndParton:qqbar2Wg = on']
genSeq.Pythia8.Commands += ['WeakBosonAndParton:qg2Wq = on']

genSeq.Pythia8.Commands += [' 24:onMode = 3']
genSeq.Pythia8.Commands += [' 24:onIfMatch = -15 16']
genSeq.Pythia8.Commands += [' 15:onMode = 2']
genSeq.Pythia8.Commands += [' 15:addChannel = 3 1.0 0 13 -13 13']

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter

filtSeq += TripletChainFilter("LowPtTripletFilter")
filtSeq += TripletChainFilter("HighPtTripletFilter")
filtSeq.Expression = "LowPtTripletFilter or HighPtTripletFilter"

TripletChainFilter = filtSeq.LowPtTripletFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = -13
TripletChainFilter.PdgId2 = 13
TripletChainFilter.PdgId3 = -13
TripletChainFilter.NStep1 = 1
TripletChainFilter.NStep2 = 1
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 3500
TripletChainFilter.PtMin2 = 3500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = -15
TripletChainFilter.TripletPtMin = 10000
TripletChainFilter.TripletEtaMax = 100
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000

TripletChainFilter2 = filtSeq.HighPtTripletFilter
TripletChainFilter2.NTriplet = 1
TripletChainFilter2.PdgId1 = -13
TripletChainFilter2.PdgId2 = 13
TripletChainFilter2.PdgId3 = -13
TripletChainFilter2.NStep1 = 1
TripletChainFilter2.NStep2 = 1
TripletChainFilter2.NStep3 = 1
TripletChainFilter2.PtMin1 = 10500
TripletChainFilter2.PtMin2 = 5500
TripletChainFilter2.PtMin3 = 2000
TripletChainFilter2.EtaMax1 = 3.0
TripletChainFilter2.EtaMax2 = 3.0
TripletChainFilter2.EtaMax3 = 3.0
TripletChainFilter2.TripletPdgId = -15
TripletChainFilter2.TripletPtMin = 10000
TripletChainFilter2.TripletEtaMax = 100
TripletChainFilter2.TripletMassMin = 0
TripletChainFilter2.TripletMassMax = 10000000
