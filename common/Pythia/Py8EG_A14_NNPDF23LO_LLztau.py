###############################################################
# Pythia8 Long-lived Z' -> tautau
# Andreas Sogaard <andreas.sogaard@cern.ch>
#===============================================================
# Parse model parameters jobOption filename
ldn    = runArgs.jobConfig[0].rstrip('.py')
match  = re.search('.*_(?P<decay>\w+)_m(?P<mass>\d+)_t(?P<lifetime>[p\d]+)ns(_pt(?P<pt>\d+))?', ldn)
config = match.groupdict()

assert config['decay'] in ['leplep', 'lephad', 'hadhad', 'all'],  \
    "Decay mode '{0:s}' not supported.".format(config['decay'])

# EVGEN configuration
evgenConfig.description = "Long-lived Z' -> tautau (m = {} GeV/c^2, tau = {} mm/c, {} decays)" \
           .format(config['mass'], config['lifetime'], config['decay'])
evgenConfig.contact     = ["Andreas Sogaard <andreas.sogaard@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'longLived', 'tau' ]
evgenConfig.process     = "pp>LL Zprime>tautau"

# Specify generator and PDF choice
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

# Convert lifetime from [ns], as specified, to [mm/c] as used in Pythia.
config['tau0'] = float(config['lifetime'].replace('p', '.')) * 299.792458

# Z' parameters
genSeq.Pythia8.Commands += [
    "ParticleDecays:limitTau0 = off",      # Allow long-lived particles to decay
    "32:name = Zprime",                    # Set Z' name
    "NewGaugeBoson:ffbar2gmZZprime = on",  # Create Z' bosons
    "Zprime:gmZmode = 3",                  # Turn off Z',Z,g interference: only pure Z' contribution
    "32:onMode = off",                     # Turn off all Z' decays
    "32:onIfAny = 15",                     # Switch on only Z'->tautau decay
    "32:m0 = "   + str(config['mass']),    # Set Z' mass in GeV/c^2
    "32:tau0 = " + str(config['tau0'])     # Set Z' lifetime in mm/c
]

# Turn off checks for displaced vertices. Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp      = 1000*1000  # In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

# (Opt.) Modify heacy resonance decay filter
if config['decay'] != 'all':
    include("MC15JobOptions/XtoVVDecayFilterExtended.py")
    DecayFilter = filtSeq.XtoVVDecayFilterExtended
    DecayFilter.PDGGrandParent = 32  # Z'
    DecayFilter.PDGParent      = 15  # Taus
    DecayFilter.StatusParent   = 2

    lep = [11,13]
    had = [111,130,211,221,223,310,311,321,323]

    DecayFilter.PDGChild1 = had if (config['decay'] == 'hadhad') else lep
    DecayFilter.PDGChild2 = lep if (config['decay'] == 'leplep') else had
    pass

# (Opt.) Impose fiducial generator-level selection on Z'
if config['pt']:
    include("MC15JobOptions/ParticleFilter.py")
    ParticleFilter = filtSeq.ParticleFilter
    ParticleFilter.Ptcut     = int(config['pt']) * 1000.
    ParticleFilter.PDG       = 32
    ParticleFilter.StatusReq = -1
    pass
