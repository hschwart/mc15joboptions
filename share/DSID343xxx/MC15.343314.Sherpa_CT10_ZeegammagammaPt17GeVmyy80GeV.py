include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e+ e- gamma gamma production with up to two jets in ME+PS"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.keywords = [ "electroweak", "triboson", "diphoton", "2electron", "SM" ]
evgenConfig.inputconfcheck = "ZeegammagammaPt17GeVmyy80GeV"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 -> 11 -11 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.02 {6,7}
  End process;
}(processes)
(selector){
  Mass 11 -11 10 E_CMS
  PT 22 17 E_CMS
  DeltaR 22 90 0.2 1000
  DeltaR 22 22 0.2 1000
  DeltaR 93 90 0.2 1000
  IsolationCut  22  0.3  2  0.025
  Mass    22  22  80.0 E_CMS
}(selector)
"""
