#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12203404E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03985488E+01   # W+
        25     1.26164293E+02   # h
        35     3.00018042E+03   # H
        36     3.00000000E+03   # A
        37     3.00110262E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02350842E+03   # ~d_L
   2000001     3.01805674E+03   # ~d_R
   1000002     3.02259067E+03   # ~u_L
   2000002     3.02124548E+03   # ~u_R
   1000003     3.02350842E+03   # ~s_L
   2000003     3.01805674E+03   # ~s_R
   1000004     3.02259067E+03   # ~c_L
   2000004     3.02124548E+03   # ~c_R
   1000005     5.74586308E+02   # ~b_1
   2000005     3.01923516E+03   # ~b_2
   1000006     5.70791434E+02   # ~t_1
   2000006     3.01987959E+03   # ~t_2
   1000011     3.00730905E+03   # ~e_L
   2000011     2.99993933E+03   # ~e_R
   1000012     3.00591613E+03   # ~nu_eL
   1000013     3.00730905E+03   # ~mu_L
   2000013     2.99993933E+03   # ~mu_R
   1000014     3.00591613E+03   # ~nu_muL
   1000015     2.98629256E+03   # ~tau_1
   2000015     3.02235866E+03   # ~tau_2
   1000016     3.00640304E+03   # ~nu_tauL
   1000021     2.32897322E+03   # ~g
   1000022     1.51856888E+02   # ~chi_10
   1000023     3.21892620E+02   # ~chi_20
   1000025    -3.00656260E+03   # ~chi_30
   1000035     3.00657646E+03   # ~chi_40
   1000024     3.22053765E+02   # ~chi_1+
   1000037     3.00750249E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891894E-01   # N_11
  1  2     3.82216252E-04   # N_12
  1  3    -1.46988071E-02   # N_13
  1  4     3.31788264E-07   # N_14
  2  1     1.67083066E-06   # N_21
  2  2     9.99658268E-01   # N_22
  2  3     2.61080182E-02   # N_23
  2  4     1.31107999E-03   # N_24
  3  1     1.03968764E-02   # N_31
  3  2    -1.93859065E-02   # N_32
  3  3     7.06763306E-01   # N_33
  3  4     7.07107998E-01   # N_34
  4  1    -1.03974024E-02   # N_41
  4  2     1.75324866E-02   # N_42
  4  3    -7.06815355E-01   # N_43
  4  4     7.07104349E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99318606E-01   # U_11
  1  2     3.69096769E-02   # U_12
  2  1    -3.69096769E-02   # U_21
  2  2     9.99318606E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998285E-01   # V_11
  1  2    -1.85225823E-03   # V_12
  2  1    -1.85225823E-03   # V_21
  2  2    -9.99998285E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98639973E-01   # cos(theta_t)
  1  2    -5.21364012E-02   # sin(theta_t)
  2  1     5.21364012E-02   # -sin(theta_t)
  2  2     9.98639973E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99717953E-01   # cos(theta_b)
  1  2    -2.37489884E-02   # sin(theta_b)
  2  1     2.37489884E-02   # -sin(theta_b)
  2  2     9.99717953E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06909681E-01   # cos(theta_tau)
  1  2     7.07303826E-01   # sin(theta_tau)
  2  1    -7.07303826E-01   # -sin(theta_tau)
  2  2    -7.06909681E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00155912E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22034040E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44486345E+02   # higgs               
         4     1.14801514E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22034040E+03  # The gauge couplings
     1     3.61461232E-01   # gprime(Q) DRbar
     2     6.38522064E-01   # g(Q) DRbar
     3     1.03096170E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22034040E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37791699E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22034040E+03  # The trilinear couplings
  1  1     4.88946165E-07   # A_d(Q) DRbar
  2  2     4.89005266E-07   # A_s(Q) DRbar
  3  3     1.01818778E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22034040E+03  # The trilinear couplings
  1  1     2.31714875E-07   # A_e(Q) DRbar
  2  2     2.31726407E-07   # A_mu(Q) DRbar
  3  3     2.35003474E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22034040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.57767980E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22034040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.01395991E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22034040E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02742944E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22034040E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -6.13717834E+04   # M^2_Hd              
        22    -9.14519105E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41530499E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.21843810E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48057888E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48057888E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51942112E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51942112E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.03190086E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.93859463E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.34027368E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.46586686E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22515464E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.35385273E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.52544633E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.08008823E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.50987090E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.86993503E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72085311E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63867206E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.22729801E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.70474683E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.82456672E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.27800329E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.43954004E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.70144271E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.89079490E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -1.30039820E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.24785775E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.97332037E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.46380785E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.19722026E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.79566602E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.11104926E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.62296377E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02458283E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.90557288E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62418264E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.78384638E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.40819373E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.25050252E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.36284862E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.06625899E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20875849E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56996513E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.31225673E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.93670836E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.93334095E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43003484E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02966988E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.85551932E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62353383E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.13390051E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.44059271E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24479286E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.06565127E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.07311730E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68734885E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47500547E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.22911486E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.43146628E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.42374408E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55249945E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02458283E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.90557288E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62418264E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.78384638E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.40819373E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.25050252E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.36284862E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.06625899E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20875849E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56996513E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.31225673E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.93670836E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.93334095E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43003484E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02966988E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.85551932E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62353383E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.13390051E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.44059271E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24479286E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.06565127E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.07311730E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68734885E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47500547E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.22911486E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.43146628E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.42374408E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55249945E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96072539E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.82882845E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00711812E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.38274420E-11    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     9.91231032E-12    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00999903E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55122088E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.74238873E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.96072539E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.82882845E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00711812E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.38274420E-11    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     9.91231032E-12    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00999903E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55122088E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.74238873E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.78371626E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48506841E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17286267E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34206892E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72665327E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56596824E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14583616E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.88024733E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     4.65942074E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28803598E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     5.42185948E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96099573E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.79706091E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00544420E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.01484971E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.96099573E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.79706091E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00544420E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.01484971E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96199760E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.79618634E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00519350E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.01518786E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.39541334E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.91942135E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.46500217E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.60271833E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.69242225E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.69242225E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.77181821E-12    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.77181821E-12    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.55520757E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.81716007E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.84706274E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.67108029E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.98739635E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.28791850E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.25008547E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.74991453E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.88406251E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.48062873E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.62538853E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.69323989E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.69323989E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.95874192E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.17792326E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.37684741E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37684741E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.26454940E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.26454940E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.65539364E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.65539364E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.65539364E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.65539364E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.12106485E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.12106485E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     7.28351308E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     7.28351308E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     7.28351308E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     7.28351308E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.43774707E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.43774707E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.00067176E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.37033023E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.17527869E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.62282683E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.62282683E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.17590422E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.04310710E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25344838E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25344838E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.17693429E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.17693429E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.64210450E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.64210450E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.64210450E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.64210450E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.35406521E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.35406521E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     6.44551413E-12    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     6.44551413E-12    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     6.44551413E-12    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     6.44551413E-12    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     4.44635577E-13    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     4.44635577E-13    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.77991736E-03   # h decays
#          BR         NDA      ID1       ID2
     5.45327831E-01    2           5        -5   # BR(h -> b       bb     )
     6.92428585E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.45116326E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.19324224E-04    2           3        -3   # BR(h -> s       sb     )
     2.25648239E-02    2           4        -4   # BR(h -> c       cb     )
     7.44697612E-02    2          21        21   # BR(h -> g       g      )
     2.58874694E-03    2          22        22   # BR(h -> gam     gam    )
     1.83185816E-03    2          22        23   # BR(h -> Z       gam    )
     2.51234763E-01    2          24       -24   # BR(h -> W+      W-     )
     3.19749172E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.15565489E+01   # H decays
#          BR         NDA      ID1       ID2
     9.09845763E-01    2           5        -5   # BR(H -> b       bb     )
     5.98424534E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11588615E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.59830364E-04    2           3        -3   # BR(H -> s       sb     )
     7.28325585E-08    2           4        -4   # BR(H -> c       cb     )
     7.26551936E-03    2           6        -6   # BR(H -> t       tb     )
     4.12142712E-06    2          21        21   # BR(H -> g       g      )
     3.71551729E-08    2          22        22   # BR(H -> gam     gam    )
     2.97507693E-09    2          23        22   # BR(H -> Z       gam    )
     6.94614253E-07    2          24       -24   # BR(H -> W+      W-     )
     3.46878835E-07    2          23        23   # BR(H -> Z       Z      )
     4.87127899E-06    2          25        25   # BR(H -> h       h      )
     1.31197183E-26    2          36        36   # BR(H -> A       A      )
     5.01330307E-18    2          23        36   # BR(H -> Z       A      )
     7.79440720E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.71661910E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.89752311E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.41722659E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.11113034E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.31318095E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.06830498E+01   # A decays
#          BR         NDA      ID1       ID2
     9.29374596E-01    2           5        -5   # BR(A -> b       bb     )
     6.11240806E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16119840E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.65440041E-04    2           3        -3   # BR(A -> s       sb     )
     7.49034171E-08    2           4        -4   # BR(A -> c       cb     )
     7.46798418E-03    2           6        -6   # BR(A -> t       tb     )
     2.19924982E-05    2          21        21   # BR(A -> g       g      )
     5.58688462E-08    2          22        22   # BR(A -> gam     gam    )
     2.16550197E-08    2          23        22   # BR(A -> Z       gam    )
     7.06717027E-07    2          23        25   # BR(A -> Z       h      )
     8.26264477E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.83553233E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.13140508E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.51166970E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.42066812E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48105785E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.62726700E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.98966436E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.47875870E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.16927428E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.40557320E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.31060593E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.51511283E-07    2          24        25   # BR(H+ -> W+      h      )
     4.85072830E-14    2          24        36   # BR(H+ -> W+      A      )
     4.58858225E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.17281876E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.02654746E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
