#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.25516232E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05426294E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414226E+03   # H
        36     2.00000000E+03   # A
        37     2.00170651E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989276E+03   # ~u_L
   2000002     4.99994934E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989276E+03   # ~c_L
   2000004     4.99994934E+03   # ~c_R
   1000005     4.99916602E+03   # ~b_1
   2000005     5.00099317E+03   # ~b_2
   1000006     4.97934505E+03   # ~t_1
   2000006     5.02503477E+03   # ~t_2
   1000011     5.00008190E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008190E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99948011E+03   # ~tau_1
   2000015     5.00067835E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     2.24344526E+03   # ~chi_10
   1000023    -2.25156317E+03   # ~chi_20
   1000025     2.26181860E+03   # ~chi_30
   1000035     3.00146162E+03   # ~chi_40
   1000024     2.25082207E+03   # ~chi_1+
   1000037     3.00146127E+03   # ~chi_2+
   1000039     4.11107394E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.82543120E-01   # N_11
  1  2    -1.60265623E-02   # N_12
  1  3    -5.78800240E-01   # N_13
  1  4    -5.70418219E-01   # N_14
  2  1     9.67031705E-03   # N_21
  2  2    -1.47416888E-02   # N_22
  2  3     7.06947774E-01   # N_23
  2  4    -7.07045976E-01   # N_24
  3  1     8.12741523E-01   # N_31
  3  2     1.30183277E-02   # N_32
  3  3     4.06445479E-01   # N_33
  3  4     4.17233522E-01   # N_34
  4  1     1.10216983E-03   # N_41
  4  2    -9.99678126E-01   # N_42
  4  3     4.14714932E-03   # N_43
  4  4     2.50046260E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.89177576E-03   # U_11
  1  2     9.99982643E-01   # U_12
  2  1     9.99982643E-01   # U_21
  2  2    -5.89177576E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.53722108E-02   # V_11
  1  2    -9.99374208E-01   # V_12
  2  1     9.99374208E-01   # V_21
  2  2     3.53722108E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07544217E-01   # cos(theta_t)
  1  2    -7.06669075E-01   # sin(theta_t)
  2  1     7.06669075E-01   # -sin(theta_t)
  2  2     7.07544217E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.86043404E-01   # cos(theta_b)
  1  2     7.27560615E-01   # sin(theta_b)
  2  1    -7.27560615E-01   # -sin(theta_b)
  2  2    -6.86043404E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05361263E-01   # cos(theta_tau)
  1  2     7.08848001E-01   # sin(theta_tau)
  2  1    -7.08848001E-01   # -sin(theta_tau)
  2  2    -7.05361263E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90197178E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52079355E+02   # vev(Q)              
         4     5.00358572E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52673375E-01   # gprime(Q) DRbar
     2     6.26479247E-01   # g(Q) DRbar
     3     1.07627261E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02734245E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73306153E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79539913E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.25516232E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.61275913E+06   # M^2_Hd              
        22    -1.00281600E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36737963E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.83288860E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.69255617E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.53209426E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.69625122E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.14535848E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.25416386E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.10790163E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.99856769E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.28713676E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.69625122E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.14535848E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.25416386E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.10790163E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.99856769E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.28713676E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.09230676E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.65582753E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     9.23799263E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.25609967E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.96409946E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     9.53761758E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     9.53761758E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     9.53761758E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     9.53761758E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.51895381E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.51895381E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.18641140E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.32413207E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.86975742E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.80360353E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.92587203E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.83053512E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.83783229E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.24082675E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.35953586E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.67487243E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02779787E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.51829616E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.50862208E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.03642442E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.00405123E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.36519351E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.77209067E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.98065474E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.03144122E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.29426642E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.06944959E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.22789044E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.15543681E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.08636857E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.80887577E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.41694234E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.48816442E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.23812251E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.24699966E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32621242E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.51187427E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.93986138E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67079874E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.25415227E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.19661075E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.95295176E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.62308367E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.79246787E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.24437333E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.58555850E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.50179817E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.52868204E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.19726204E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.95111321E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.52392013E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.55197815E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67087535E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.12909167E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.96291682E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.38696492E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.62741754E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.97326475E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.25664611E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.58618705E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.45138993E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.95455777E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.08579267E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.63425683E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.11622224E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88410090E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67079874E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.25415227E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.19661075E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.95295176E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.62308367E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.79246787E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.24437333E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.58555850E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.50179817E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.52868204E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.19726204E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.95111321E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.52392013E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.55197815E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67087535E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.12909167E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.96291682E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.38696492E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.62741754E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.97326475E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.25664611E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.58618705E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.45138993E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.95455777E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.08579267E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.63425683E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.11622224E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88410090E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.78934196E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.34317295E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.84515876E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.80251824E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.85804885E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.08900568E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.72668861E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56982753E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.41221630E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.36856481E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.58683901E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.83475977E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.78934196E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.34317295E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.84515876E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.80251824E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.85804885E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.08900568E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.72668861E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56982753E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.41221630E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.36856481E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.58683901E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.83475977E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.17892249E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.56030311E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.96408865E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.96713351E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.81901698E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     6.81946273E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.64476284E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     9.76468580E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.18636381E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.45538701E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.52698095E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.02601683E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.83344929E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.92082618E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.67369906E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.78914254E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.28154142E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.81226169E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.74750472E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.86491097E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.11337059E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.71923845E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.78914254E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.28154142E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.81226169E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.74750472E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.86491097E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.11337059E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.71923845E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.79118161E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.27768304E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.81093776E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.74111431E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.86281804E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.84356354E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.71505566E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.48771143E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.31665298E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.52765825E-05    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.58070697E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.99786794E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19357495E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19235247E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.03579614E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.75831692E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.54087608E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49251800E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47411057E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.99661214E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.49283413E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.40916033E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.83418684E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.76025252E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51994524E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.71096016E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     3.34473973E-05    2     1000039        35   # BR(~chi_10 -> ~G        H)
     8.50760475E-04    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.78041794E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.11336892E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.04135725E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.05113485E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.95112787E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     8.29521374E-07    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41234381E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82897988E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.14825114E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.82079769E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.17807287E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.17446803E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.04670163E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.34453141E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.34453141E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.34453141E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.55763661E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.55763661E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.51921235E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.51921235E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.37608341E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.37608341E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.23420711E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.39649026E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.42664350E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.43303873E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.70181975E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     5.63543847E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.74470991E-07    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.07154530E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.22873937E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.64783363E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.21892009E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.43961565E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.19064455E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.18956470E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.92326125E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.59695546E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.59695546E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.59695546E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.98205914E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.45178219E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.39558150E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.43365358E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.78797003E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.47383285E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.47302993E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.22690828E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.94349919E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.94349919E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.94349919E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.38357276E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.38357276E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.19855460E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.19855460E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.94515860E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.94515860E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.94149376E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.94149376E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.99941387E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.99941387E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.75835715E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.87102712E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.65197346E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.00001251E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.48177125E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.48177125E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.36755249E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.38430456E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.22194371E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.26061770E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.14546640E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.49664558E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.87760069E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.35268505E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07294917E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16618886E-01    2           5        -5   # BR(h -> b       bb     )
     6.39470312E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26348939E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79653960E-04    2           3        -3   # BR(h -> s       sb     )
     2.06937345E-02    2           4        -4   # BR(h -> c       cb     )
     6.71907157E-02    2          21        21   # BR(h -> g       g      )
     2.30663494E-03    2          22        22   # BR(h -> gam     gam    )
     1.54239516E-03    2          22        23   # BR(h -> Z       gam    )
     2.01297335E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56972647E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78123503E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48904405E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430936E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71225106E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547636E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666490E-05    2           4        -4   # BR(H -> c       cb     )
     9.96051998E-01    2           6        -6   # BR(H -> t       tb     )
     7.97561557E-04    2          21        21   # BR(H -> g       g      )
     2.73232457E-06    2          22        22   # BR(H -> gam     gam    )
     1.16043450E-06    2          23        22   # BR(H -> Z       gam    )
     3.33201359E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66146391E-04    2          23        23   # BR(H -> Z       Z      )
     8.99671872E-04    2          25        25   # BR(H -> h       h      )
     8.55984200E-24    2          36        36   # BR(H -> A       A      )
     3.48013491E-11    2          23        36   # BR(H -> Z       A      )
     5.96145209E-12    2          24       -37   # BR(H -> W+      H-     )
     5.96145209E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82388555E+01   # A decays
#          BR         NDA      ID1       ID2
     1.49176073E-03    2           5        -5   # BR(A -> b       bb     )
     2.43893711E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62252374E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675527E-06    2           3        -3   # BR(A -> s       sb     )
     9.96160108E-06    2           4        -4   # BR(A -> c       cb     )
     9.96979604E-01    2           6        -6   # BR(A -> t       tb     )
     9.43660135E-04    2          21        21   # BR(A -> g       g      )
     3.05051984E-06    2          22        22   # BR(A -> gam     gam    )
     1.35309531E-06    2          23        22   # BR(A -> Z       gam    )
     3.24717560E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74514043E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.40368708E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49234287E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81133237E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52837131E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45679798E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08728765E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403672E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32866236E-04    2          24        25   # BR(H+ -> W+      h      )
     5.08149462E-13    2          24        36   # BR(H+ -> W+      A      )
