#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 t-channel single top production (2->3) production (top-quark), hadronic, ME CT10f4 NLO, H7 UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'hadronic', 'singleTop', 'tChannel']
evgenConfig.contact     = [ 'cescobar@cern.ch', 'ian.connelly@cern.ch' ]


#--------------------------------------------------------------
# Read in LHE files
#--------------------------------------------------------------

evgenConfig.inputfilecheck = "410253.singletop_tchan2to3nlo_top_had"

#--------------------------------------------------------------
# Herwig7 showering
#--------------------------------------------------------------

include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_CT10f4ME_LHEF_EvtGen_Common.py')

# Bug-fix for incorrect gamma-fermion coupling
genSeq.Herwig7.Commands += [ "set /Herwig/Shower/GammatoQQbarSudakov:Alpha /Herwig/Shower/AlphaQED" ]

#-------------------------------------------------------------
# Filters
#-------------------------------------------------------------
